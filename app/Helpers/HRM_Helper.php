<?php
use App\Models\HRM\CursoM;
use App\Models\HRM\TipoM;
use App\Models\HRM\Est_CivilM;
use App\Models\HRM\DadoM;
use App\Models\HRM\CargoM;


function HRM_tipo($rota, $mod = 0){
  if($mod == 0) $rota['tipos'] = TipoM::where('sts_hrm_tipo','>',0)->get();
  else $rota['tipos'] = TipoM::where('sts_hrm_tipo','>',0)->where('id_modulo',$mod)->get();

  return $rota;
}
function HRM_curso($rota){
  $rota['cursos'] = CursoM::where('sts_curso','>',0)->get();

  return $rota;
}
function HRM_civil($rota){
  $rota['civil'] = Est_CivilM::where('sts_estado_civil','>',0)->get();

  return $rota;
}
function HRM_cargo($rota){
  $rota['cargos'] = CargoM::where('sts_cargo','>',0)->get();

  return $rota;
}
function HRM_txt($text){ // LEITURA DE ARQUIVO
  $file = fopen('storage/COLABORADORES/COLABORADORES.txt', 'a+');
  $text = $text . date('Y-m-d H:i:s') . PHP_EOL;
  fwrite($file, $text);
  fclose($file);
}
function TI_check_contrato($d){
  if(isset($d['grau_instrucao_completo']))  $d['grau_instrucao_completo']=1;  else $d['grau_instrucao_completo']=0;
  if(isset($d['periculosidade']))           $d['periculosidade']=1;           else $d['periculosidade']=0;
  if($d['comissao'] != null)                $d['comissao'] = str_replace(',', '.', $req->comissao);
  return $d;
}
function TI_check_curso($d){
  if(isset($d['obrigatorio']))  $d['sts_curso']=2;  else $d['sts_curso']=1;

  return $d;
}
