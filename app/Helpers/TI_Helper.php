<?php use App\User;
use App\Models\TI\LogM;
use App\Models\TI\FluxoM;
use App\Models\TI\VersaoM;
use App\Models\TI\AclM;
use App\Models\TI\ModuloM;
use App\Models\TI\TipoM;
use App\Models\TI\UfM;
use App\Models\TI\CidadeM;
use App\Models\TI\ArquivoM;

function TI_Cidade($rota){ $rota['cidades'] = CidadeM::where('sts_cidade','>',0)->get();     return $rota; }
function TI_Ufs($rota){    $rota['ufs'] = UfM::where('sts_uf','>',0)->get();             return $rota; }
function TI_user($rota){   $rota['usuarios'] = User::where('sts_usuario','>',0)->get();  return $rota; }
function TI_tipo($rota,$rota2){
  $rota['tipos'] = TipoM::join('ti_modulos AS M', 'ti_tipos.id_modulo','M.id_modulo')
                           ->where('sts_ti_tipo','>',0)
                           ->where('M.rota',$rota2)->get();
  return $rota;
}
// ACL ACL ACL ACL ACL ACL ACL ACL ACL ACL ACL ACL ACL ACL ACL ACL ACL ACL ACL ACL ACL ACL
function TI_acl($rota){
  $aux = AclM::join('ti_modulos AS M', 'ti_permissoes.id_modulo','M.id_modulo')
  ->where('id_user', \Auth::id())
  ->where('M.rota',$rota['rota'])->get();
  $rota['acl'] = $aux['0'];

  return $rota;
}
function TI_check_acl($d){
  if(isset($d['p']))  $d['p']=1;  else $d['p']=0;
  if(isset($d['c']))  $d['c']=1;  else $d['c']=0;
  if(isset($d['r']))  $d['r']=1;  else $d['r']=0;
  if(isset($d['u']))  $d['u']=1;  else $d['u']=0;
  if(isset($d['d']))  $d['d']=1;  else $d['d']=0;
  if(isset($d['a']))  $d['a']=1;  else $d['a']=0;
  if(isset($d['f']))  $d['f']=1;  else $d['f']=0;
  if(isset($d['s']))  $d['s']=1;  else $d['s']=0;
  if(isset($d['up'])) $d['up']=1; else $d['up']=0;
  if(isset($d['dw'])) $d['dw']=1; else $d['dw']=0;
  if(isset($d['fx'])) $d['fx']=1; else $d['fx']=0;
  if(isset($d['substitui'])) $d['substitui']=1; else $d['substitui']=0;

  return $d;
}
// ALERT ALERT ALERT ALERT ALERT ALERT ALERT ALERT ALERT ALERT ALERT ALERT ALERT ALERT
function TI_alerta($vl,$tx){
  echo '<input id="testetoastr" type="hidden" value="'.$vl.'" texto="'.$tx.'">';
}
// FLUXOS FLUXOS FLUXOS FLUXOS FLUXOS FLUXOS FLUXOS FLUXOS FLUXOS FLUXOS FLUXOS FLUXOS FLUXOS
function TI_add_fluxo($fl,$md,$rt,$it){ // FLUXO/MODULO/ROTA/ID_ITEM
  if(FluxoM::where(['fluxo'=>$fl,'id_item'=>$it,'rota'=>$rt])->count() == 0){
    $idF = new FluxoM;
    $idF->fluxo   = $fl;
    $idF->modulo  = $md;
    $idF->rota    = $rt;
    $idF->id_item = $it;
    $idF->id_user = \Auth::id();
    $idF->save();
  }
}

function TI_del_fluxo($rt,$it){ // ROTA/ID_ITEM
  FluxoM::where(['id_item'=>$it,'rota'=>$rt])->update(['sts_fluxo'=>0]);
}

function TI_view_fluxo($rt,$it){ // ROTA/ID_ITEM
  FluxoM::where(['id_item'=>$it,'rota'=>$rt])->update(['sts_fluxo'=>2,'id_atendente'=>\Auth::id()]);
}
// LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG LOG
function TI_sgs_log($rota,$acao,$key=0,$old=0){
  $id = new LogM;
  $id->id_user  = \Auth::id();
  $id->rota     = $rota;
  $id->acao     = $acao;
  if($key > 0){
    $id->id_key = $key;
    $id->key_old = $old;
  }

  if($id->save()) return true;

  return false;
}
// MODULOS MODULOS MODULOS MODULOS MODULOS MODULOS MODULOS MODULOS MODULOS MODULOS MODULOS
function TI_modulo($rota){
  $rota['modulos'] = ModuloM::where('sts_modulo','>',0)->get();
  return $rota;
}
function TI_mod_id($rota,$id){
  $rota['modulos'] = ModuloM::where('sts_modulo','>',0)->where('modulo_id',$id)->get();
  return $rota;
}
// UPLOAD UPLOAD UPLOAD UPLOAD UPLOAD UPLOAD UPLOAD UPLOAD UPLOAD UPLOAD UPLOAD UPLOAD
function TI_up_file(Request $request, $pasta, $nome){
  $file       = $request->arquivo;
  $name       = $nome.'_'.date('YmdHis');
  $extension  = $request->arquivo->extension();
  $nameFile   = "{$name}.{$extension}";
  $upload     = $request->arquivo->storeAs('public/'.$pasta, $nameFile);
  if ( !$upload )
  return redirect()->back()
  ->with('error', 'Falha ao fazer upload')
  ->withInput();

  return $nameFile;
}

function TI_up_file2($arq, $pasta, $nome){
  $file       = $arq;
  $name       = $nome.'_'.date('YmdHis');
  $extension  = $arq->extension();
  $nameFile   = "{$name}.{$extension}";
  $upload     = $arq->storeAs('public/'.$pasta, $nameFile);

  if(!$upload)return redirect()->back()->with('error', 'Falha ao fazer upload')->withInput();

  return $nameFile;
}

function TI_up_arq($arq, $pasta, $nome, $rota, $id){ //dd($arq.$pasta.$rota.$id);
  $file       = $arq;
  $name       = $nome.'_'.date('YmdHis');
  $extension  = $arq->extension();
  $nameFile   = "{$name}.{$extension}";
  $upload     = $arq->storeAs('public/'.$pasta, $nameFile);

  if(!$upload)return redirect()->back()->with('error', 'Falha ao fazer upload')->withInput();

  $idA = new ArquivoM;
  $idA->id_item = $id;
  $idA->url     = $nameFile;
  $idA->arquivo = $nome;
  $idA->rota    = $rota;
  $idA->save();
}
// VERSAO VERSAO VERSAO VERSAO VERSAO VERSAO VERSAO VERSAO VERSAO VERSAO VERSAO VERSAO
function TI_up_version($sts){
  $idV       = new VersaoM;
  $idV->v1   = $_SESSION['versao']['v1'];
  if($sts == 1){
    $idV->v2 = $_SESSION['versao']['v2'] + 1;
    $idV->v3 = $idV->v4 = 0;
  }elseif($sts == 2){
    $idV->v2 = $_SESSION['versao']['v2'];
    $idV->v3 = $_SESSION['versao']['v3'] + 1;
    $idV->v4 = 0;
  }elseif($sts > 2){
    $idV->v2 = $_SESSION['versao']['v2'];
    $idV->v3 = $_SESSION['versao']['v3'];
    $idV->v4 = $_SESSION['versao']['v4'] + 1;
  }
  $idV->versao = $idV->v1.'.'.$idV->v2.'.'.$idV->v3;

  if($idV->save()) return $idV->id_versao;
  return false;
}
// MASK
function mask($val, $mask){
  $maskared = '';
  $k = 0;

  for($i = 0; $i<=strlen($mask)-1; $i++){
    if($mask[$i] == '#'){
      if(isset($val[$k]))
      $maskared .= $val[$k++];
    }else{
      if(isset($mask[$i]))
      $maskared .= $mask[$i];
    }
  }
  return $maskared;
}

function TI_option_modulos($mod,$idMod){
  foreach ($mod as $key){
    if ($key->sts_modulo == 1){
      echo "<option value='".$key->id_modulo."'";
      if($key->id_modulo==$idMod) echo "selected";
      echo " > ".strtoupper($key->modulo)." </option>";

      foreach ($mod as $key1){
        if ($key1->modulo_id == $key->id_modulo){
          echo "<option value='".$key1->id_modulo."'";
          if($key1->id_modulo==$idMod) echo "selected";
          echo " >└  ".$key1->modulo." </option>";

          foreach ($mod as $key2){
            if ($key2->modulo_id == $key1->id_modulo){
              echo "<option value='".$key2->id_modulo."'";
              if($key2->id_modulo==$idMod) echo "selected";
              echo " >└─  ".$key2->modulo." </option>";

              foreach ($mod as $key3){
                if ($key3->modulo_id == $key2->id_modulo){
                  echo "<option value='".$key3->id_modulo."'";
                  if($key3->id_modulo==$idMod) echo "selected";
                  echo " >└──  ".$key3->modulo." </option>";

                  foreach($mod as $key4){
                    if ($key4->modulo_id == $key3->id_modulo){
                      echo "<option value='".$key4->id_modulo."'";
                      if($key4->id_modulo==$idMod) echo "selected";
                      echo " >└──  ".$key4->modulo." </option>";
                    }
                  }

                }
              }

            }
          }

        }
      }
    }
  }

}
function TI_option_mod_mul($mod,$idMod){
  foreach ($mod as $key){
    if ($key->sts_modulo == 1){
      echo "<option value='".$key->id_modulo."'";
      if($idMod != 0){
      foreach ($idMod as $sel) {
        if($key->id_modulo==$sel->id_modulo) echo "selected";
      }
    }
      echo " > ".strtoupper($key->modulo)." </option>";

      foreach ($mod as $key1){
        if ($key1->modulo_id == $key->id_modulo){
          echo "<option value='".$key1->id_modulo."'";
          if($idMod != 0){
          foreach ($idMod as $sel1) {
            if($key1->id_modulo==$sel1->id_modulo) echo "selected";
          }
        }
          echo " >└  ".$key1->modulo." </option>";

          foreach ($mod as $key2){
            if ($key2->modulo_id == $key1->id_modulo){
              echo "<option value='".$key2->id_modulo."'";
              if($idMod != 0){
              foreach ($idMod as $sel2) {
                if($key2->id_modulo==$sel2->id_modulo) echo "selected";
              }
            }
              echo " >└─  ".$key2->modulo." </option>";

              foreach ($mod as $key3){
                if ($key3->modulo_id == $key2->id_modulo){
                  echo "<option value='".$key3->id_modulo."'";
                  if($idMod != 0){
                  foreach ($idMod as $sel3) {
                    if($key3->id_modulo==$sel3->id_modulo) echo "selected";
                  }
                }
                  echo " >└──  ".$key3->modulo." </option>";

                  foreach($mod as $key4){
                    if ($key4->modulo_id == $key3->id_modulo){
                      echo "<option value='".$key4->id_modulo."'";
                      if($idMod != 0){
                      foreach ($idMod as $sel4) {
                        if($key4->id_modulo==$sel4->id_modulo) echo "selected";
                      }
                    }
                      echo " >└──  ".$key4->modulo." </option>";
                    }
                  }

                }
              }

            }
          }

        }
      }
    }
  }

}
