<?php
use App\Models\EMP\SetorM;
use App\Models\EMP\EmpresaM;
use App\Models\EMP\Ramo_EmpM;
use App\Models\HRM\ColaboradorM;

function H_rota($rota){
  $rota['empresas'] = EmpresaM::join('emp_modulos_ramos','emp_empresas.id_ramo','=','emp_modulos_ramos.id_ramo')
                              ->where('sts_empresa','>',0)->get();
  return $rota;
}
function H_emp_gru($rota,$ramo = 8){
  $rota['empresas'] = EmpresaM::join('emp_modulos_ramos AS mr','emp_empresas.id_ramo','=','mr.id_ramo')
                              ->where('sts_empresa','>',0)->where('mr.id_ramo',$ramo)->get();
  return $rota;
}
function H_lead($rota,$ramo = 8){
  $rota['leads'] = EmpresaM::select('id_empresa','empresa','razao_social')
                            ->where('sts_empresa','=',1)->orderby('razao_social')->get();
  return $rota;
}
function H_setor($rota){
  $rota['setores'] = SetorM::where('sts_setor','>',0)->orderBy('setor')->get();
  return $rota;
}
function H_fornecedor($rota,$mod){
  $rota['fornecedores'] = Ramo_EmpM::join('emp_empresas as E','emp_ramo_emp.id_empresa','E.id_empresa')
                                       ->join('emp_ramos as R','emp_ramo_emp.id_ramo','R.id_ramo')
                                       ->join('emp_modulos_ramos as MR','R.id_ramo','MR.id_ramo')
                                       ->where('E.sts_empresa','>',0)
                                       ->where('MR.id_modulo',$mod)
                                       ->get();
  return $rota;
}
function H_colaborador($rota){
  $rota['colaboradores'] = ColaboradorM::where('sts_colaborador','>',0)->get();
  return $rota;
}
// HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM
use App\Models\HRM\CargoM;
use App\Models\ADM\PessoaM;

function H_cargo($rota){
  $rota['cargos'] = CargoM::where('sts_cargo','>',0)->get();
  return $rota;
}

function H_pessoa($rota){
  $rota['pessoas'] = PessoaM::where('sts_pessoa','>',0)->get();
  return $rota;
}

// HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM HRM
