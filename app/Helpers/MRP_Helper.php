<?php
use App\Models\MRP\ItemM;
use App\Models\MRP\PedidoM;
use App\Models\MRP\TipoM;
use App\Models\MRP\ProdutoM;
use App\Models\MRP\EstoqueM;
use App\Models\MRP\UndM;
use App\Models\HRM\ColaboradorM;

function H_mrp_tipo($rota, $id = 34){
  $rota['tipos'] = TipoM::where('sts_mrp_tipo','>',0)->where('id_modulo',$id)->get();
  return $rota;
}
function H_und($rota){
  $rota['unds'] = UndM::where('sts_und','>',0)->get();
  return $rota;
}
function H_est($rota){
  $rota['estoques'] = EstoqueM::where('sts_estoque','>',0)->get();
  return $rota;
}
function H_prod($rota){
  $rota['produtos'] = ProdutoM::where('sts_produto','>',0)->get();
  return $rota;
}
function H_colaboradorMRP($rota){
  $rota['colaborador'] = ColaboradorM::join('hrm_dados','hrm_colaboradores.id_dado','hrm_dados.id_dado')
                             ->where('sts_colaborador','>',0)->get();
  return $rota;
}

function MRP_up_sts_pedido($id){
  $n_item = ItemM::where('id_pedido',$id)->count();
  $ret['OLD'] = $idP = PedidoM::findOrFail($id);

  if($n_item == 0){
    $idP->sts_pedido = 0;
    $ret['with'] = ['danger' => 'Pedido sem iténs foi escluído.'];
  }else {
    $idP->sts_pedido++;
    if($idP->sts_pedido == 3) $idP->id_autorizador = \Auth::id();
    $ret['with'] = ['success' => 'Confirmado o pedido.'];
  }

  TI_del_fluxo('pedido',$id);

  if($idP->sts_pedido == 2) addFluxo('Autorizar','pedido','mrp_item',$ret['OLD']->id_pedido);
  if($idP->sts_pedido == 3) addFluxo('Separar','pedido','mrp_item',$ret['OLD']->id_pedido);
  if($idP->sts_pedido == 4) addFluxo('Entregar','pedido','mrp_item',$ret['OLD']->id_pedido);
  $idP->save();

  return $ret;
}

function MRP_up_total_pedido($id, $at){
  $idP = PedidoM::findOrFail($id);
  $idP->total = $idP->total + $at;
  $idP->save();
}

function MRP_up_ped_atendido($id, $at){
  $idP = PedidoM::findOrFail($id); echo "<script>console.log('MRP_up_ped_atendido: " .$idP->atendido.$at.$idP->total. "');</script>";
  $idP->atendido = $idP->atendido + $at;
  if($idP->atendido >= $idP->total) {
    TI_del_fluxo('Entregar','pedido','mrp_item',$id);
    $idP->sts_pedido = 5;
     echo "<script>console.log( 'MRP_up_ped_atendido: status: 5" .$idP->atendido.$idP->total. "' );</script>";
  }
  $idP->save();
}

function MRP_up_estoque($data){
  $id = ItemM::firstOrNew(['id_produto'=>$data['id_produto'],'id_estoque'=>$data['id_estoque']]);
  $at = $id->qnt;
  if(isset($data['local'])) $id->local = $data['local'];
  if($data['id_mrp_tipo'] == 5){
    if($id->qnt < $data['qnt']){
      $id->qnt = 0;
    }else {
      $id->qnt = $id->qnt - $data['qnt'];
      $at = $data['qnt'];
    }
  }else {
    $id->qnt = $id->qnt + $data['qnt'];
  }
  $id->save();

  return $at;
}

function MRP_item_estoque($rota, $id){
  $rota['item'] = ItemM::where('sts_mrp_item','>',0)->where('id_estoque',$id)->get();
  $rota['tipo_item'] = TipoM::where('sts_mrp_tipo','>',0)->where('id_modulo',34)->get();
  $rota['local'] = ItemM::select('local')
                            ->distinct('local')
                            ->where('sts_mrp_item','>',0)
                            ->where('id_estoque',$id)
                            ->orderBy('local')
                            ->get();

  return $rota;
}
