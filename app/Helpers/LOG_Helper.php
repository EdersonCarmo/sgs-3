<?php
use App\Models\LOG\TipoM;
use App\Models\LOG\VeiculoM;
use Illuminate\Http\Request;
use App\Models\LOG\VistoriaM;
use App\Models\HRM\ColaboradorM;

function H_log_tipo($rota){
  $rota['tipos'] = TipoM::where('sts_tipo','>',0)->where('id_modulo',$rota['acl']['id_modulo'])->get();
  return $rota;
}
function H_vei($rota){
  $rota['veiculos'] = VeiculoM::where('sts_veiculo','>',0)->get();
  return $rota;
}
function H_motorista($rota){
  // $rota['motoristas'] = ColaboradorM::join('hrm_dados','hrm_colaboradores.id_pessoa','hrm_dados.id_pessoa')
  // ->whereIn('sts_colaborador',[1,2])
  // ->whereNotNull('cnh')->get();
  $rota['motoristas'] = ColaboradorM::whereIn('sts_colaborador',[1,2])->get();

  return $rota;
}
// OCORRENCIA

function LOG_vistoria($rota){ // ->where('dt_vistoria','>',$dt_busca)
  $dt_ini = date('Y-m-d H:i:s', strtotime(date("Y-m-d H:i:s"). ' - 30 days'));
  $dt_fim = date("Y-m-d H:i:s");
  $rota['vistorias'] = VistoriaM::where('sts_vistoria','>',0)
                    ->where('dt_vistoria','>',$dt_ini)
                    ->where('dt_vistoria','<',$dt_fim)
                    ->get();

  return $rota;
}
// VISTORIAS
function LOG_check_vistoria($d){
  if(isset($d['farolR']))       $d['farolR']=1;       else $d['farolR']=0;
  if(isset($d['farolL']))       $d['farolL']=1;       else $d['farolL']=0;
  if(isset($d['piscaR']))       $d['piscaR']=1;       else $d['piscaR']=0;
  if(isset($d['piscaL']))       $d['piscaL']=1;       else $d['piscaL']=0;
  if(isset($d['lanternaR']))    $d['lanternaR']=1;    else $d['lanternaR']=0;
  if(isset($d['lanternaL']))    $d['lanternaL']=1;    else $d['lanternaL']=0;
  if(isset($d['luzFreio']))     $d['luzFreio']=1;     else $d['luzFreio']=0;
  if(isset($d['luzPlaca']))     $d['luzPlaca']=1;     else $d['luzPlaca']=0;
  if(isset($d['retrovisorR']))  $d['retrovisorR']=1;  else $d['retrovisorR']=0;
  if(isset($d['retrovisorL']))  $d['retrovisorL']=1;  else $d['retrovisorL']=0;
  if(isset($d['retrovisor']))   $d['retrovisor']=1;   else $d['retrovisor']=0;
  if(isset($d['vidroR']))       $d['vidroR']=1;       else $d['vidroR']=0;
  if(isset($d['vidroL']))       $d['vidroL']=1;       else $d['vidroL']=0;
  if(isset($d['vidroF']))       $d['vidroF']=1;       else $d['vidroF']=0;
  if(isset($d['vidroB']))       $d['vidroB']=1;       else $d['vidroB']=0;
  if(isset($d['forroR']))       $d['forroR']=1;       else $d['forroR']=0;
  if(isset($d['forroL']))       $d['forroL']=1;       else $d['forroL']=0;
  if(isset($d['ar']))           $d['ar']=1;           else $d['ar']=0;
  if(isset($d['bateria']))      $d['bateria']=1;      else $d['bateria']=0;
  if(isset($d['buzina']))       $d['buzina']=1;       else $d['buzina']=0;
  if(isset($d['capota']))       $d['capota']=1;       else $d['capota']=0;
  if(isset($d['chaveRoda']))    $d['chaveRoda']=1;    else $d['chaveRoda']=0;
  if(isset($d['chaveReserva'])) $d['chaveReserva']=1; else $d['chaveReserva']=0;
  if(isset($d['cinto']))        $d['cinto']=1;        else $d['cinto']=0;
  if(isset($d['documentos']))   $d['documentos']=1;   else $d['documentos']=0;
  if(isset($d['estepe']))       $d['estepe']=1;       else $d['estepe']=0;
  if(isset($d['estofamento']))  $d['estofamento']=1;  else $d['estofamento']=0;
  if(isset($d['extintor']))     $d['extintor']=1;     else $d['extintor']=0;
  if(isset($d['limpParabrisa']))$d['limpParabrisa']=1;else $d['limpParabrisa']=0;
  if(isset($d['manual']))       $d['manual']=1;       else $d['manual']=0;
  if(isset($d['radio']))        $d['radio']=1;        else $d['radio']=0;
  if(isset($d['santoAntonio'])) $d['santoAntonio']=1; else $d['santoAntonio']=0;
  if(isset($d['seguro']))       $d['seguro']=1;       else $d['seguro']=0;
  if(isset($d['tapetes']))      $d['tapetes']=1;      else $d['tapetes']=0;
  if(isset($d['triangulo']))    $d['triangulo']=1;    else $d['triangulo']=0;
  if(isset($d['vidroEletrico']))$d['vidroEletrico']=1;else $d['vidroEletrico']=0;

  return $d;
}

function LOG_sum_item_vistoria($d){
  $itens = $d->farolL + $d->farolR + $d->piscaL + $d->piscaR + $d->lanternaL + $d->lanternaR +
  $d->luzFreio + $d->luzPlaca + $d->retrovisor + $d->retrovisorL + $d->retrovisorR +
  $d->vidroL + $d->vidroR + $d->vidroF + $d->vidroB + $d->forroL + $d->forroR +
  $d->limpParabrisa + $d->buzina + $d->ar + $d->bateria + $d->estofamento + $d->extintor + $d->triangulo +
  $d->manual + $d->chaveRoda + $d->estepe + $d->chaveReserva + $d->tapetes + $d->vidroEletrico +
  $d->documentos + $d->cinto + $d->seguro + $d->capota + $d->santoAntonio + $d->radio;

  $niveis = $d->hidraulica + $d->fuidoFreio + $d->arrefecimento + $d->parabrisa + $d->pneu + $d->limpeza + $d->oleo;

  if($itens < 36 || $niveis < 630) TI_add_fluxo('Cotar','frota','manutencao',$d->id_vistoria); // FLUXO/MODULO/ROTA/ID_ITEM

  return $soma = ['nItens'=>36,'itens'=>$itens,'nNiveis'=>700,'niveis'=>$niveis];
}
