<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Auth\Events\Authenticated;
use App\Models\ADM\G_LinkM;
use App\Models\TI\VersaoM;
use App\Models\TI\TipoM;
use App\Models\TI\HelpM;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  public function __construct()  {
    $this->middleware('auth');
    session_start();

    $_SESSION['links']  = G_LinkM::all();
    $_SESSION['versao'] = VersaoM::orderBy('id_versao', 'desc')->first();
    $_SESSION['help']   = HelpM::where('sts_help','>',0)->orderBy('id_help', 'desc')->get();
    $_SESSION['tipos']  = TipoM::join('ti_modulos AS M', 'ti_tipos.id_modulo','M.id_modulo')
                                   ->where('sts_ti_tipo','>',0)
                                   ->where('M.rota','help')->get();
  }
}
