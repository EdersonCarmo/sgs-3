<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TI\ModuloM;
use App\Models\TI\PreferenciaM;
use App\Models\TI\AclM;
use App\Models\TI\FluxoM;

class HomeController extends Controller
{
  public function index()  {
    $_SESSION['estilo'] = PreferenciaM::where('id_user',\Auth::id())->first();
    $_SESSION['menu']   = ModuloM::select('*')
                                    ->join('ti_permissoes', 'ti_modulos.id_modulo', '=', 'ti_permissoes.id_modulo')
                                    ->where('ti_permissoes.id_user','=',\Auth::id())
                                    ->where('ti_modulos.sts_modulo','>',0)
                                    ->where('ti_permissoes.sts_permissao','>',0)
                                    ->orderBy('modulo')->get();
    $fluxos = FluxoM::where('sts_fluxo',1)->orWhere('sts_fluxo',2)->get();
    $acl = AclM::join('ti_modulos AS M', 'ti_permissoes.id_modulo','M.id_modulo')
                        ->where('id_user', \Auth::id())
                        ->where('ti_permissoes.id_modulo',44)->get();

    if ($acl->count() > 0 and $acl['0']->r == 1) {
      return view('TI.fluxo', compact('fluxos'));
    }
    return view('TI.inicio', compact('fluxos'));
  }

  public function principal()  {dd('STOP HomeC 1');
    return view('includes/principal');
  }

  public function logar()  {dd('STOP HomeC 2');
    return view('auth/entrar');
  }

  public function fluxos()  {
    return view('fluxos');
  }

  public function lanca()  {
    return view('fin/lanca');
  }

  public function formtest2(Request $data)  {
    dd($data);
  }
}
