<?php namespace App\Http\Controllers\LOG;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LOG\VistoriaM;
use App\Models\LOG\VeiculoM;

class VistoriaC extends Controller
{
  public $rota = ['rota' => 'vistoria','pasta'=>'LOG/VIS/'];

  public function carrega()  {
    $this->rota = TI_acl(H_emp_gru(H_vei(H_motorista(H_fornecedor($this->rota,11)))));
    $this->rota[$this->rota['rota']] = VistoriaM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('log.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, VistoriaM $id)  {
    $data = LOG_check_vistoria($request->all());
    $idV = VeiculoM::findOrFail($data['id_veiculo']);

    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idV->placa);

    $log = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = VistoriaM::findOrFail($id);

    return view('log.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function show($id)  {
    $this->carrega();
    $this->rota['print'] = VistoriaM::findOrFail($id);

    return view('PRINT.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = LOG_check_vistoria($request->all());
    $idV = VeiculoM::findOrFail($data['id_veiculo']);
    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idV->placa);

    $idUP = VistoriaM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($data);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = VistoriaM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
