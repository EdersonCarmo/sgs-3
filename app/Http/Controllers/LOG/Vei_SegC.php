<?php namespace App\Http\Controllers\LOG;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LOG\Vei_SegM;

class Vei_SegC extends Controller
{
  public $rota = ['rota' => 'vei_seg'];

  public function carrega()  {
    $this->rota = TI_acl(H_vei($this->rota));
  }

  public function show($id)  {
    $this->carrega();
    $this->rota['doc'] = $id;
    $this->rota[$this->rota['rota']] = Vei_SegM::where('id_documento', $id)->get();

    return view('LOG.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request)  {
    $data = $request->all();
    $id = new Vei_SegM;

    $log = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function destroy(Request $req, $id)  {
    $this->carrega();
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = Vei_SegM::where(['id_veiculo'=>$id,'id_documento'=>$req['id_documento']])->delete();

    return redirect()->back()->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
