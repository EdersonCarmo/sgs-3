<?php namespace App\Http\Controllers\LOG;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LOG\OcorrenciaM;

class OcorrenciaC extends Controller
{
  public $rota = ['rota' => 'ocorrencia'];

  public function carrega()  {
    $this->rota = TI_acl(H_vei(H_motorista($this->rota)));
    $this->rota = H_log_tipo(LOG_vistoria($this->rota));

    $this->rota[$this->rota['rota']] = OcorrenciaM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('log.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, OcorrenciaM $id)  { dd($request->all());
    $log = $id->create($request->all());
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = OcorrenciaM::findOrFail($id);

    return view('log.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $idUP = OcorrenciaM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = OcorrenciaM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
