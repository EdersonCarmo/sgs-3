<?php namespace App\Http\Controllers\LOG;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LOG\AbasteceM;
use App\Models\HRM\ColaboradorM;
use App\Models\LOG\VeiculoM;


class AbastecimentoC extends Controller
{
  public $rota = ['rota' => 'abastecimento','pasta'=>'LOG/ABA/'];

  public function carrega()  {
    $this->rota = TI_acl(H_emp_gru(H_vei(H_motorista(H_fornecedor($this->rota,11)))));
    $this->rota[$this->rota['rota']] = AbasteceM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('log.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, AbasteceM $id)  {
    $data = $request->all();
    $log = $id->create($data);

    if($request->arquivo){
      $idV = AbasteceM::findOrFail($log['id_'.$this->rota['rota']]);
      $idV->arquivo = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idV->placa);
      $id->save();
    }

    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = AbasteceM::findOrFail($id);

    return view('log.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();
    $idV = VeiculoM::findOrFail($data['id_veiculo']);
    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idV->placa);

    $idUP = AbasteceM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($data);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = AbasteceM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
