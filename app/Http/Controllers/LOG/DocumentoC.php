<?php namespace App\Http\Controllers\LOG;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LOG\DocumentoM;
use App\Models\LOG\VeiculoM;
use App\Models\LOG_TipoM;

class DocumentoC extends Controller
{
  public $rota = ['rota' => 'documento','pasta'=>'LOG/DOC/'];

  public function carrega()  {
    $this->rota = TI_acl(H_vei($this->rota));
    $this->rota = H_log_tipo($this->rota);
    $this->rota[$this->rota['rota']] = DocumentoM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('log.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, DocumentoM $id)  {
    $data = $request->all();
    if($data['id_veiculo'] != null ) {
      if($request->arquivo){
        $idV = VeiculoM::findOrFail($data['id_veiculo']);
        $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idT->documento);
      }
    }else {
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].'SEG/'.$data->vigencia, $idT->documento);
    }

    $log = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    TI_view_fluxo($this->rota['rota'],$id);
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = DocumentoM::findOrFail($id);

    return view('log.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    TI_del_fluxo($this->rota['rota'],$id);
    $data = $request->all();
    if(isset($data['id_veiculo'])) {
      if(isset($request->arquivo)){
        $idV = VeiculoM::findOrFail($data['id_veiculo']);
        $idT = LOG_TipoM::findOrFail($data['id_log_tipo']);
        $data['arquivo'] = TI_up_file2($request->arquivo, $this->rota['pasta'].$idV->placa.'/', $idT->tipo);
      }
    }else{
      //$data['arquivo'] = TI_up_file2($request->arquivo, $this->rota['pasta'].'SEG/', 'SEGURO_'.$data['vigencia']);
    }

    $idUP = DocumentoM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($data);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = DocumentoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
