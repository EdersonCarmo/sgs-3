<?php namespace App\Http\Controllers\LOG;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LOG\VeiculoM;

class VeiculoC extends Controller
{
  public $rota = ['rota' => 'veiculo','pasta'=>'LOG/VEI/'];

  public function carrega(){
    $this->rota = TI_acl(H_emp_gru($this->rota));
    $this->rota = H_log_tipo($this->rota);
    $this->rota[$this->rota['rota']] = VeiculoM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('log.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, VeiculoM $id)  {
    $data = $request->all();
    if($request->arquivo)
      $data['arquivo'] = TI_up_file2($request->arquivo,$this->rota['pasta'], $data['placa']);

    $log = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = VeiculoM::findOrFail($id);

    return view('log.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();
    if($request->arquivo)
      $data['arquivo'] = TI_up_file2($request->arquivo,$this->rota['pasta'], $data['placa']);

    $idUP = VeiculoM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($data);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => 'Alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id   = VeiculoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => 'Excluido com sucesso.']);
  }
}
