<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MRP_Kit_ItemM;
use App\Models\MRP\KitM;

class MRP_Kit_ItemC extends Controller
{
  public $rota = ['rota' => 'kit_item'];

  public function carrega($id)  {
    $this->rota = TI_acl(H_prod($this->rota));

    $this->rota[$this->rota['rota']] = MRP_Kit_ItemM::where('sts_'.$this->rota['rota'],'>',0)->get();
    $this->rota['kit']       = KitM::findOrFail($id);
  }

  public function show($id)  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega($id);

    return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, MRP_Kit_ItemM $id)  {
    $MRP = $id->create($request->all());

    TI_sgs_log($this->rota['rota'],'store',$MRP['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega($id);
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = MRP_Kit_ItemM::findOrFail($id);

    return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();

    if(isset($data['id_kit'])){
      $id = new MRP_Kit_ItemM;
      $id->create($data);
    }else {
      $idUP = MRP_Kit_ItemM::findOrFail($id);
      TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
      $idUP->update($request->all());
    }

    return redirect()->back()->with(['success' => 'Alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = MRP_Kit_ItemM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->back()->with(['success' => 'Excluído com sucesso.']);
  }
}
