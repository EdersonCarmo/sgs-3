<?php namespace App\Http\Controllers\ADM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ADM\PessoaM;
use App\Models\ADM\TipoM;

class PessoaC extends Controller
{
  public $rota = ['rota' => 'pessoa'];

  public function carrega($letra = "A")  {
    $this->rota = TI_acl($this->rota);

    $this->rota[$this->rota['rota']] = PessoaM::where('sts_'.$this->rota['rota'],'>',0)->get();
    $this->rota['pess'] = PessoaM::where('sts_'.$this->rota['rota'],'>',0)
                                      ->where('pessoa','LIKE',$letra.'%')->get();
    $this->rota['tipos'] = TipoM::where('sts_adm_tipo','>',0)->get();
  }

  public function index(Request $req)  {
    $this->carrega();
    if (isset($req['id_pessoa'])) {
      return redirect()->route($this->rota['rota'].'.edit',$req['id_pessoa']);
    }

    return view('adm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function show($letra)  {
    $this->carrega($letra);

    return view('adm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, PessoaM $id)  {
    $id->create($request->all());

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = PessoaM::findOrFail($id);

    return view('adm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $id = PessoaM::findOrFail($id);
    $id->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    $id = PessoaM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
