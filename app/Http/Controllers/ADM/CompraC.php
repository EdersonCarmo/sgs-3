<?php namespace App\Http\Controllers\ADM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ADM\CompraM;
use App\Models\ADM\OrcamentoM;
use App\Models\MRP\ItemM;

class CompraC extends Controller
{
  public $rota = ['rota'=>'compra','pasta'=>'ADM/ORC/'];

  public function carrega()  {
    $this->rota = TI_acl(H_prod(H_colaborador(H_setor($this->rota))));

    $this->rota[$this->rota['rota']] = CompraM::where('sts_'.$this->rota['rota'],'>',0)->get();
    $this->rota['orcamentos'] = OrcamentoM::where('sts_orcamento','>',0)->get();
    $this->rota['itens'] = ItemM::whereNotNull('id_compra')->get();
  }

  public function index()  {
    $this->carrega();

    return view('ADM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function show($id)  {
    $this->carrega();
    $this->rota['item'] = ItemM::where('sts_mrp_item','>',0)
                                     ->where('id_compra',$id)->get();
    $this->rota['input'] = ['id'=>$id,'rota'=>$this->rota['rota']];


    return view('ADM.adm_item', ['rota'=>$this->rota]);
  }

  // public function show($modulo)  {
  //   $this->carrega();
  //   $this->rota['modulos']   = ModuloM::where('rota',$modulo)->get();
  //
  //   return view('ADM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  // }

  public function store(Request $request, CompraM $id)  {
    $data = $request->all();
    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'], $request[$this->rota['rota']]);

    $id->create($data);

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = CompraM::findOrFail($id);
    $this->rota['orcamentos'] = OrcamentoM::where('id_compra', $id)->get();
    $this->rota['input'] = ['id'=>$id,'rota'=>$this->rota['rota']];

    return view('adm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();
    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'], $request[$this->rota['rota']]);

    $idUP = CompraM::findOrFail($id);

    if(isset($data['confirma'])){
      TI_add_fluxo('Autorizar','compra','compra',$id);// FLUXO/MODULO/ROTA/ID_ITEM
      $idUP->sts_compra = 2;
      $idUP->save();
      return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
    }
    if(isset($data['autoriza'])){
      TI_add_fluxo('Comprar','compra','compra',$id);// FLUXO/MODULO/ROTA/ID_ITEM
      $idUP->sts_compra = 3;
      $idUP->id_aprovador = \Auth::id();
      $idUP->save();
      return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
    }
    if(isset($data['finaliza'])){
      TI_del_fluxo($this->rota['rota'],$id);
      $idUP->sts_compra = 4;
      $idUP->dt_compra = date('Y-m-d');
      $idUP->save();
      return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
    }

    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG {ROTA|METODO|ID|OLD}
    $idUP->update($data);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_del_fluxo($this->rota['rota'],$id);
    $id   = CompraM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
