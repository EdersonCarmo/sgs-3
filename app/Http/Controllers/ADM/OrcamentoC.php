<?php namespace App\Http\Controllers\ADM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ADM\OrcamentoM;
use App\Models\ADM_CompraM;

class OrcamentoC extends Controller
{
  public $rota = ['rota'=>'orcamento','pasta'=>'ADM/ORC/'];

  public function carrega()  {
    $this->rota = TI_acl(H_colaborador($this->rota));

    $this->rota[$this->rota['rota']] = OrcamentoM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    $this->carrega();

    return view('ADM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function show($id)  {
    $this->carrega();
    $this->rota[$this->rota['rota']] = OrcamentoM::where('sts_'.$this->rota['rota'],'>',0)
                                                     ->where('id_compra', $id)->get();
    $this->rota['input'] = ['id'=>$id,'rota'=>$this->rota['rota']];
    $this->rota['compra'] = ADM_CompraM::findOrFail($id);

    return view('ADM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, OrcamentoM $id)  {
    $data = $request->all();
    if($request->arquivo)
      $data['arquivo'] = TI_up_file2($request->arquivo, $this->rota['pasta'], $request[$this->rota['rota']]);

    $id->create($data);

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = OrcamentoM::findOrFail($id);

    return view('adm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  { dd('stop');
    $data = $request->all();
    if($request->arquivo)
      $data['arquivo'] = TI_up_file2($request->arquivo, $this->rota['pasta'], $request[$this->rota['rota']]);

    $id = OrcamentoM::findOrFail($id);
    $id->update($data);

    return redirect()->back()->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function aprovaOrc($id)  { dd(something);
    $sts = 2;
    $idO = OrcamentoM::findOrFail($id);
    if($idO->sts_orcamento == 2) $sts = 1;
    $idO->sts_orcamento = $sts;
    $idO->save();

    return redirect()->route('compra.edit',$id)->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    $id   = OrcamentoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
