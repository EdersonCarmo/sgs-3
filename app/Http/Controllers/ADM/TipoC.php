<?php namespace App\Http\Controllers\ADM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ADM\TipoM;

class TipoC extends Controller
{
  public $rota = ['rota' => 'adm_tipo'];

  public function carrega()  {
    $this->rota[$this->rota['rota']] = TipoM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    $this->carrega();

    return view('adm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function show($id)  {
    $this->carrega();
    $this->rota[$this->rota['rota']] =
    TipoM::where('sts_adm_tipo','>',0)
             ->where('tabela',$id)->get();

    return view('adm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, TipoM $id)  {
    $id->create($request->all());

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = TipoM::findOrFail($id);

    return view('adm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $id = TipoM::findOrFail($id);
    $id->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    $id = TipoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
