<?php namespace App\Http\Controllers\ADM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ADM\G_LinkM;

class G_LinkC extends Controller
{
  public $rota = ['rota'=>'g_link'];

  public function carrega()  {
    $this->rota = TI_acl(TI_modulo($this->rota));
    $this->rota[$this->rota['rota']] = G_LinkM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    $this->carrega();

    return view('ADM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, G_LinkM $id)  {
    $log = $id->create($request->all());
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG {ROTA|METODO|ID|OLD}

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = G_LinkM::findOrFail($id);

    return view('adm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $idUP = G_LinkM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG {ROTA|METODO|ID|OLD}
    $idUP->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG {ROTA|METODO|ID|OLD}
    $id = G_LinkM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
