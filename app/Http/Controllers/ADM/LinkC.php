<?php namespace App\Http\Controllers\ADM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ADM\LinkM;
use App\Models\ADM\G_LinkM;

class LinkC extends Controller
{
  public $rota = ['rota'=>'link'];

  public function carrega()  {
    $this->rota = TI_acl($this->rota);
    $this->rota[$this->rota['rota']] = LinkM::where('sts_'.$this->rota['rota'],'>',0)->get();
    $this->rota['g_link'] = G_LinkM::where('sts_g_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    $this->carrega();

    return view('ADM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, LinkM $id)  {
    $log = $id->create($request->all());
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG {ROTA|METODO|ID|OLD}

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = LinkM::findOrFail($id);

    return view('adm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $idUP = LinkM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG {ROTA|METODO|ID|OLD}
    $idUP->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG {ROTA|METODO|ID|OLD}
    $id = LinkM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
