<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MRP\EstoqueM;
use App\Models\MRP\MovimentoM;
use App\Models\MRP\ProdutoM;

class MRP_ProdutoEntC extends Controller
{
  public $rota = ['rota' => 'produtoENT','pasta'=>'MRP/PRO/'];

  public function carrega()  {
    $this->rota = TI_acl(H_mrp_tipo(H_und(TI_mod_id($this->rota,31))));

    $this->rota['estoques']  = EstoqueM::where('sts_estoque','>',0)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('MRP.produto_ENT', ['rota'=>$this->rota]);
  }

  public function store2(Request $request, ProdutoM $id)  {
    $data = $request->all();

    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idT->documento);

    $MRP = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$MRP['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.','tipo'=>$data['id_mrp_tipo']]);
  }

  public function store(Request $request, ProdutoM $id, MovimentoM $mov)  {
    $data = $request->all();

    $MRP = $id->create($data);

    $data['id_produto'] = $MRP['id_produto'];
    $data['id_user'] = \Auth::user()->id;
      MRP_up_estoque($data);
      $log = $mov->create($data);
      TI_sgs_log('movimento','store',$log['id_movimento']); // REG_LOG


    TI_sgs_log($this->rota['rota'],'store',$MRP['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.','tipo'=>$data['id_mrp_tipo'],'local'=>$data['local']]);
  }
}
