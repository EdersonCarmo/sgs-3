<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\LOG\VeiculoM;

class LOG_VeiculoC extends Controller
{
  public $rota = ['rota' => 'veiculo'];

  public function index()  {
    $this->rota[$this->rota['rota']] = VeiculoM::where('sts_veiculo','>',0)->get();

    return view('log.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function create()  {
    //
  }

  public function store(Request $request, VeiculoM $id)  {
    $id->create($request->all());

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }
  public function show($id)  {
    //
  }

  public function edit($id)  {
    $this->rota[$this->rota['rota']] = VeiculoM::where('sts_veiculo','>',0)->get();
    $this->rota['edit'] = VeiculoM::findOrFail($id);

    return view('log.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $id = VeiculoM::findOrFail($id);
    $id->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => 'Alterado com sucesso.']);
  }

  public function destroy($id)  {
    $id   = VeiculoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => 'Excluido com sucesso.']);
  }
}
