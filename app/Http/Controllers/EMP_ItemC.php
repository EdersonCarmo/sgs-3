<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EMP_InventarioM;
use App\Models\EMP_ItemM;

class EMP_ItemC extends Controller
{
  public $rota = ['rota' => 'emp_item','pasta'=>'EMP/INV/'];

  public function carrega($id)  {
    $this->rota = H_rota(TI_acl(H_setor($this->rota)));
    $this->rota[$this->rota['rota']] = EMP_ItemM::where('sts_'.$this->rota['rota'],'>',0)
                                        ->where('id_inventario',$id)->get();
    $this->rota['chips'] = EMP_ItemM::where('sts_'.$this->rota['rota'],'>',0)
                                        ->where('id_inventario',3)->get();
  }

  // public function index()  {
  //   TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
  //   $this->carrega();
  //
  //   return view('EMP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  // }

  public function show($id,Request $req)  {
    TI_view_fluxo($this->rota['rota'],$id);
    $this->carrega($id);
    $this->rota['inventario'] = EMP_InventarioM::findOrFail($id);

        if(isset($req) and $req->query('act') == 'print'){
          $this->rota['item'] = $this->rota[$this->rota['rota']];
          // if(isset($_SESSION['search'])){
          //   $this->rota['item'] = $item->search($_SESSION['search']);
          //   return view('PRINT.estoque', ['rota'=>$this->rota]);
          // }
          return view('PRINT.inventario', ['rota'=>$this->rota]);
        }

    return view('EMP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, EMP_ItemM $id)  {
    $data = $request->all();

    $log = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega($id);
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = EMP_ItemM::findOrFail($id);
    $this->rota['inventario'] = EMP_InventarioM::findOrFail($this->rota['edit']->id_inventario);

    return view('EMP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();

    $idUP = EMP_ItemM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($request->all());

    return redirect()->route($this->rota['rota'].'.show',$idUP->id_inventario)->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = EMP_ItemM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
