<?php namespace App\Http\Controllers\MRP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MRP\PedidoM;

class PedidoC extends Controller
{
  public $rota = ['rota' => 'pedido','pasta'=>'MRP/PED/'];

  public function carrega()  {
    $this->rota = TI_acl(H_colaboradorMRP(H_mrp_tipo($this->rota,32)));

    $this->rota[$this->rota['rota']] = PedidoM::where(function($query){
      $p = $this->rota['acl'];
      $query->where('sts_'.$this->rota['rota'],'>',0);

      if($p->a == 0 and $p->b == 0) $query->where('id_colaborador',\Auth::user()->id);
    })->get();

  }

  public function status($id)  {
    $ret = MRP_up_sts_pedido($id);
    TI_sgs_log($this->rota['rota'],'status',$id,$ret['OLD']); // REG_LOG {ROTA|METODO|ID|OLD}

    return redirect()->route($this->rota['rota'].'.index')->with($ret['with']);
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG {ROTA|METODO|ID|OLD}
    $this->carrega();

    return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, PedidoM $id)  {
    $data = $request->all();
    $log = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG {ROTA|METODO|ID|OLD}

    $show = ['id'=>$log['id_'.$this->rota['rota']],'rota'=>$this->rota['rota']];
    return redirect()->route('mrp_item.show',$show)->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = PedidoM::findOrFail($id);

    return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();

    $idUP = PedidoM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG {ROTA|METODO|ID|OLD}
    $idUP->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_del_fluxo($this->rota['rota'],$id);
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG {ROTA|METODO|ID|OLD}
    $id = PedidoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
