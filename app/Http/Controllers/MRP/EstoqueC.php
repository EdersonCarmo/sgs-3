<?php namespace App\Http\Controllers\MRP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MRP\EstoqueM;
use App\Models\MRP\ItemM;

class EstoqueC extends Controller
{
  public $rota = ['rota' => 'estoque','pasta'=>'MRP/EST/'];

  public function carrega()  {
    $this->rota = TI_acl(H_colaborador(H_emp_gru(H_mrp_tipo($this->rota,43))));
    $this->rota[$this->rota['rota']] = EstoqueM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    unset($_SESSION['search']);
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function show($id,Request $req, ItemM $item)  { //dd($req->all());
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->rota = MRP_item_estoque($this->rota, $id);

    if(isset($req) and $req->query('act') == 'print'){
      if(isset($_SESSION['search'])){
        $this->rota['item'] = $item->search($_SESSION['search']);
        return view('PRINT.estoque', ['rota'=>$this->rota]);
      }
      return view('PRINT.estoque', ['rota'=>$this->rota]);
    }

    $this->carrega();

    return view('MRP.item_estoque', ['rota'=>$this->rota]);
  }

  public function mrp_filtro(Request $req, ItemM $item)  {
    $data = $req->all();
    unset($_SESSION['search']);
    $this->carrega();

    $this->rota = MRP_item_estoque($this->rota, $req['id_estoque']);

    $this->rota['item'] = $item->search($data);

    unset($data['_token']);
    $_SESSION['search'] = $data;

    return view('MRP.item_estoque', ['rota'=>$this->rota]);
  }

  public function store(Request $request, EstoqueM $id)  {
    $data = $request->all();

    $log = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = EstoqueM::findOrFail($id);

    return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();

    $idUP = EstoqueM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = EstoqueM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
