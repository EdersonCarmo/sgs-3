<?php namespace App\Http\Controllers\MRP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MRP\UndM;

class UndC extends Controller
{
  public $rota = ['rota' => 'und'];

  public function carrega()  {
    $this->rota = TI_acl($this->rota);
    $this->rota[$this->rota['rota']] = UndM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, UndM $id)  {
    $data = $request->all();

    $MRP = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$MRP['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = UndM::findOrFail($id);

    return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();

    $idUP = UndM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = UndM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
