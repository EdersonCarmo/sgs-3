<?php namespace App\Http\Controllers\MRP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MRP\ProdutoM;

class ProdutoC extends Controller
{
  public $rota = ['rota' => 'produto','pasta'=>'MRP/PRO/'];

  public function carrega()  {
    $this->rota = TI_acl(H_mrp_tipo(H_und(TI_mod_id($this->rota,31))));

    $this->rota[$this->rota['rota']] = ProdutoM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, ProdutoM $id)  {
    $data = $request->all();

    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idT->documento);

    $MRP = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$MRP['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = ProdutoM::findOrFail($id);

    return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();
    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idT->tipo);

    $idUP = ProdutoM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = ProdutoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
