<?php namespace App\Http\Controllers\MRP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\ColaboradorM;
use App\Models\MRP\MovimentoM;

class MovimentoC extends Controller
{
  public $rota = ['rota' => 'movimento','pasta'=>'MRP/MOV/'];

  public function carrega()  {
    $this->rota = TI_acl(H_est(H_prod(H_mrp_tipo($this->rota,36))));

    $this->rota[$this->rota['rota']] = MovimentoM::where('sts_'.$this->rota['rota'],'>',0)->get();
    $this->rota['colaborador'] = ColaboradorM::join('hrm_dados','hrm_colaboradores.id_dado','hrm_dados.id_dado')
                               ->where('sts_colaborador','>',0)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, MovimentoM $id)  {
    $data = $request->all();
    MRP_up_estoque($data);
    $log = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    if(isset($data['mais'])){
      $this->carrega();
      $this->rota['insert'] = ['id_estoque'=>$data['id_estoque'],'id_mrp_tipo'=>$data['id_mrp_tipo']];
      return view('MRP.'.$data['mais'], ['rota'=>$this->rota]);
    }
    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = MovimentoM::findOrFail($id);

    return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();

    $idUP = MovimentoM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = MovimentoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
