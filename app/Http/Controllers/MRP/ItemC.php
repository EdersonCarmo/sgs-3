<?php namespace App\Http\Controllers\MRP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MRP\ItemM;
use App\Models\MRP\MovimentoM;
use App\Models\MRP\PedidoM;

class ItemC extends Controller
{
  public $rota = ['rota' => 'mrp_item'];

  public function carrega($id,$aux)  {
    $this->rota = TI_acl(H_est(H_prod($this->rota)));

    $this->rota['input'] = ['id'=>$id,'rota'=>$aux];
    $this->rota[$this->rota['rota']] = ItemM::where('sts_'.$this->rota['rota'],'>',0)
                                     ->where('id_'.$aux,$id)->get();
    $this->rota['MRP_item_estoque'] = ItemM::whereNotNull('id_estoque')->get();
  }

  public function show($id,Request $request)  {
    $this->rota['pedido'] = PedidoM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega($id,$request->query('rota'));
    TI_view_fluxo($this->rota['rota'],$id);

    if($this->rota['pedido']->sts_pedido == 3){ // PRINT
      $ret = MRP_up_sts_pedido($id);
      TI_sgs_log('pedido','status',$id,$ret['OLD']); // REG_LOG {ROTA|METODO|ID|OLD}
      return view('PRINT.pedido', ['rota'=>$this->rota]);
    }elseif ($request->query('act') == 'print') {
      return view('PRINT.pedido', ['rota'=>$this->rota]);
    }

    return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request)  {
    $data = $request->all();
    $id = ItemM::firstOrNew(['id_produto'=>$data['id_produto'],'id_pedido'=>$data['id_pedido']]);
    $id->qnt = $id->qnt + $data['qnt'];

    if(isset($data['id_pedido'])) MRP_up_total_pedido($data['id_pedido'],$id->qnt);

    $id->save($data);
    TI_sgs_log($this->rota['rota'],'store',$id['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->rota['edit'] = ItemM::findOrFail($id);
    $this->carrega($this->rota['edit']['id_'.$this->rota['edit']->rota],$this->rota['edit']->rota);
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);

  if($this->rota['edit']->id_pedido != null){
    if($this->rota['edit']->pedidos->sts_pedido == 1)
    return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }
  return view('MRP.'.$this->rota['rota'], ['rota'=>$this->rota]);

    return redirect()->back()->with(['Danger' => 'Acesso negado.']);
  }

  public function baixa(Request $request){
    $data = $request->all();
    $y = 0;
    foreach ($data['id_mrp_item'] as $key) {
      if($data['id_estoque'][$y] > 0 and $data['qnt'][$y] > $data['atendido'][$y]){
        $atEst['id_mrp_tipo'] = 5; // 5 baixa estoque
        $atEst['id_produto'] = $data['id_produto'][$y];
        $atEst['id_estoque'] = $data['id_estoque'][$y];
        $atEst['id_pedido']  = $data['id_pedido'][$y];
        $atEst['qnt'] = $data['qnt'][$y] - $data['atendido'][$y];
        $idEst = MRP_up_estoque($atEst);

        $idI = ItemM::findOrFail($key);
        $idI->atendido = $idI->atendido + $idEst;
        $idI->save();

        MRP_up_ped_atendido($data['id_pedido'],$idEst);

        $idM = new MovimentoM;
        $idM->id_produto  = $atEst['id_produto'];
        $idM->id_estoque  = $atEst['id_estoque'];
        $idM->id_pedido   = $data['id_pedido'];
        $idM->id_user     = \Auth::user()->id;
        $idM->id_mrp_tipo = 5;
        $idM->qnt         = $atEst['qnt'];
        $idM->save();
      }

      $y++;
    }
    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();

    if(isset($data['add'])){
      $id = new ItemM;
      $log = $id->create($data);
      TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG
      return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
    }else {
      $idUP = ItemM::findOrFail($id);
      $idUP->update($request->all());
      TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
      return redirect()->back()->with(['success' => 'Alterado com sucesso.']);
    }
    return redirect()->back()->with(['danger' => 'Erro na requisição.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = ItemM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->back()->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
