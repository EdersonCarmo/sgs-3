<?php namespace App\Http\Controllers\EMP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\G_linkM;
use App\Models\TI\VersaoM;
use App\Models\EMP\TipoM;

class TipoC extends Controller
{
  public $rota = ['rota' => 'emp_tipo'];

  public function index()  {
    $this->rota[$this->rota['rota']] = TipoM::where('sts_tipo','>',0)->get();

    return view('emp.emp_tipo', ['rota' => $this->rota]);
  }

  public function show($id)  {
    $this->rota[$this->rota['rota']] =
    TipoM::where('sts_tipo','>',0)
             ->where('tabela',$id)->get();

    return view('emp.emp_tipo', ['rota' => $this->rota]);
  }

  public function store(Request $request, TipoM $id)  {
    $id->create($request->all());

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->rota[$this->rota['rota']] = TipoM::all();
    $this->rota['edit'] = TipoM::findOrFail($id);

    return view('emp.emp_tipo', ['rota' => $this->rota]);
  }

  public function update(Request $request, $id)  {
    $id = TipoM::findOrFail($id);
    $id->update($request->all());

    return redirect()->route('emp_tipo.index')->with(['success' => 'Alterado com sucesso.']);
  }

  public function destroy($id)  {
    $id   = TipoM::findOrFail($id);
    $id->update(['sts_tipo' => 0]);

    return redirect()->route('emp_tipo.index')->with(['success' => 'Excluido com sucesso.']);
  }
}
