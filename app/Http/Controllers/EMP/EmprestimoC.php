<?php namespace App\Http\Controllers\EMP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EMP_EmprestimoM;
use App\Models\EMP_ItemM;

class EmprestimoC extends Controller
{
  public $rota = ['rota' => 'emp_emprestimo','pasta'=>'EMP/EMPRE/'];

  public function carrega()  {
    $this->rota[$this->rota['rota']] = EMP_EmprestimoM::where('sts_'.$this->rota['rota'],'>',0)->get();
    $this->rota = H_rota($this->rota);
    $this->rota = H_colaborador($this->rota);
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();
    $this->rota['itens'] = EMP_ItemM::where('sts_emp_item',1)->get();

    return view('EMP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }
  public function show($id){
    $this->rota['print'] = EMP_EmprestimoM::findOrFail($id);

    return view('PRINT.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, EMP_EmprestimoM $id)  {
    $data = $request->all();

    $log = $id->create($data);
    $idE = EMP_ItemM::findOrFail($data['id_item']);
    $idE->update(['sts_emp_item' => 2]);

    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idT->documento);

    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->route($this->rota['rota'].'.show',$log['id_'.$this->rota['rota']]);
    // return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = EMP_EmprestimoM::findOrFail($id);
    $this->rota['itens'] = EMP_ItemM::where('sts_emp_item','>',0)->get();

    return view('EMP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();
    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idT->tipo);

    $idUP = EMP_EmprestimoM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG

    if(isset($data['fim'])){
      $idE = EMP_ItemM::findOrFail($data['id_item']);
      $idE->update(['sts_emp_item' => 1]);
      $data['sts_emp_emprestimo'] = 2;
    }else {
      $idE = EMP_ItemM::findOrFail($data['id_item']);
      $idE->update(['sts_emp_item' => 2]);
      $data['sts_emp_emprestimo'] = 1;
    }

    $idUP->update($data);
    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = EMP_EmprestimoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
