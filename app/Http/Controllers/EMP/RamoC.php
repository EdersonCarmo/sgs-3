<?php namespace App\Http\Controllers\EMP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EMP\RamoM;
use App\Models\EMP\Modulo_RamoM;

class RamoC extends Controller
{
  public $rota = ['rota' => 'ramo'];

  public function carrega()  {
    $this->rota = TI_acl(TI_modulo($this->rota));
    $this->rota[$this->rota['rota']] = RamoM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    $this->carrega();

    return view('adm.'.$this->rota['rota'], ['rota' => $this->rota]);
  }

  public function show($id)  {
    $this->rota[$this->rota['rota']] = RamoM::where('sts_tipo','>',0)
             ->where('tabela',$id)->get();

    return view('adm.'.$this->rota['rota'], ['rota' => $this->rota]);
  }

  public function store(Request $request, RamoM $id)  {
    $data = $request->all();
    $idR = $id->create($data);

    foreach ($data['id_modulo'] as $key) {
      $idM = new Modulo_RamoM;
      $idM->id_modulo = $key;
      $idM->id_ramo = $idR->id_ramo;
      $idM->save();
    }


    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = RamoM::findOrFail($id);

    return view('adm.'.$this->rota['rota'], ['rota' => $this->rota]);
  }

  public function update(Request $request, $id)  {
    $for = $request->all();
    Modulo_RamoM::where('id_ramo', $id)->delete();
    foreach ($for['id_modulo'] as $key) {
      $idM = Modulo_RamoM::firstOrCreate(['id_modulo'=>$key,'id_ramo'=>$id]);
    }
    $idM->save();

    $id = RamoM::findOrFail($id);
    $id->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => 'Alterado com sucesso.']);
  }

  public function destroy($id)  {
    $id = RamoM::findOrFail($id);
    $id->update(['sts_tipo' => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => 'Excluido com sucesso.']);
  }
}
