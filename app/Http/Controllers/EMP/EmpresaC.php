<?php namespace App\Http\Controllers\EMP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EMP\EmpresaM;
use App\Models\EMP\Ramo_EmpM;
use App\Models\EMP\TipoM;
use App\Models\EMP\RamoM;

class EmpresaC extends Controller
{
  public $rota = ['rota' => 'empresa'];

  public function carrega($letra = "0")  {
    $this->rota = TI_acl(H_lead($this->rota));

    $this->rota['tipos'] = TipoM::where('sts_tipo','=',1)->orderby('tipo')->get();
    $this->rota['ramos'] = RamoM::where('sts_ramo','=',1)->orderby('ramo')->get();
    $this->rota[$this->rota['rota']] = EmpresaM::select('id_empresa','empresa','razao_social')
                              ->where('sts_empresa','=',1)
                              ->where('razao_social','LIKE',$letra.'%')
                              ->orderby('razao_social')->get();
  }

  public function index(Request $req)  {
    $this->carrega();
    if (isset($req['campo'])) {
      if($req['campo'] == 'id_empresa')
        return redirect()->route($this->rota['rota'].'.edit',$req['search']);
      if($req['campo'] == 'cnpj'){
        $id = EmpresaM::where('cnpj',$req['search'])->get();
        if($id->count() == 0)
          return redirect()->back()->with(['danger' => 'CNPJ não encontrado.']);
        return redirect()->route($this->rota['rota'].'.edit',$id->id_empresa);
      }
    }
    return view('adm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function show($letra)  {
    $this->carrega($letra);
    // return view('adm.empresa', ['rota'=>$this->rota]);
    return view('adm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, EmpresaM $id)  {
    $data = $request->all();
    $idE = $id->create($data);

    foreach ($data['id_ramo'] as $key) {
      $idEM = new Ramo_EmpM;
      $idEM->id_empresa = $idE->id_empresa;
      $idEM->id_ramo = $key;
      $idEM->save();
    }
    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit']       = EmpresaM::findOrFail($id);
    $this->rota['ramos_emp']  = Ramo_EmpM::where('id_empresa',$id)->get();

    return view('adm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();
    Ramo_EmpM::where('id_empresa', $id)->delete();
    foreach ($data['id_ramo'] as $key) {
      if ($key > 0) {
        $idEM = Ramo_EmpM::firstOrNew(['id_empresa'=>$id,'id_ramo'=>$key]);
        $idEM->id_empresa = $id;
        $idEM->id_ramo = $key;
        $idEM->save();
      }
    }
    $id = EmpresaM::findOrFail($id);
    $id->update($data);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    $id = EmpresaM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
