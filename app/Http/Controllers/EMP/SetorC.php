<?php namespace App\Http\Controllers\EMP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EMP\SetorM;

class SetorC extends Controller
{
  public $rota = ['rota' => 'setor'];

  public function carrega()  {
    $this->rota[$this->rota['rota']] = SetorM::where('sts_'.$this->rota['rota'],'>',0)->get();
    $this->rota = TI_acl($this->rota);
    $this->rota = H_emp_gru($this->rota);
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('EMP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, SetorM $id)  {
    $data = $request->all();

    $log = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = SetorM::findOrFail($id);

    return view('EMP.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();

    $idUP = SetorM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = SetorM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
