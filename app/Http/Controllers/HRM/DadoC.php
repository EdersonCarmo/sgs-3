<?php namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\DadoM;

class DadoC extends Controller
{
  public $rota = ['rota' => 'emp_tipo'];

  public function index()  {
    $this->rota[$this->rota['rota']] = DadoM::where('sts_tipo','>',0)->get();

    return view('hrm.'.$this->rota['rota'],['rota' => $this->rota]);
  }

  public function store(Request $request, DadoM $id)  {
    $id->create($request->all());

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->rota[$this->rota['rota']] = DadoM::all();
    $this->rota['edit'] = DadoM::findOrFail($id);

    return view('hrm.'.$this->rota['rota'], ['rota' => $this->rota]);
  }

  public function update(Request $request, $id)  {
    $id = DadoM::findOrFail($id);
    $id->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => 'Alterado com sucesso.']);
  }

  public function destroy($id)  {
    $id   = DadoM::findOrFail($id);
    $id->update(['sts_tipo' => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => 'Excluido com sucesso.']);
  }
}
