<?php namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\CursoM;
use App\Models\HRM\Curso_CargoM;

class CursoC extends Controller
{
  public $rota = ['rota' => 'curso','pasta'=>'HRM/CURSO/'];

  public function carrega()  {
    $this->rota = TI_acl(HRM_tipo(HRM_cargo($this->rota)));

    $this->rota[$this->rota['rota']] = CursoM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    $this->carrega();

    return view('HRM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $req, CursoM $id)  {
    $data = $req->all();


    $log = $id->create($data)->id_curso;

    foreach ($data['id_curso_cargo'] as $key) {
      $idM = new Curso_CargoM;
      $idM->id_cargo = $key;
      $idM->id_curso = $log;
      $idM->save();
    }

    if($req->arq_curso)  TI_up_arq($req->arq_curso, $this->rota['pasta'].$log.'/', $data['nm_arq_curso'], 'curso', $log);
    TI_sgs_log($this->rota['rota'],'store',$log); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = CursoM::findOrFail($id);

    return view('HRM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $req, $id)  {
    $data = TI_check_curso($req->all());

    foreach ($data['id_curso_cargo'] as $key) {
      $idM = Curso_CargoM::firstOrCreate(['id_cargo'=>$key,'id_curso'=>$id]);
    }

    $idUP = CursoM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($data);

    if($req->arq_curso)  TI_up_arq($req->arq_curso, $this->rota['pasta'].$id.'/', $data['nm_arq_curso'], 'curso', $id);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id   = CursoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
