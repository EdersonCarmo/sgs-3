<?php namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\CargoM;
use App\Models\TI\AclM;

class CargoC extends Controller
{
  public $rota = ['rota' => 'cargo'];

  public function carrega($letra = "A")  {
    $this->rota = TI_acl($this->rota);
    $this->rota[$this->rota['rota']] = CargoM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    $this->carrega();

    return view('hrm.'.$this->rota['rota'],['rota' => $this->rota]);
  }

  public function store(Request $request, CargoM $id)  {
    $id->create($request->all());

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = CargoM::findOrFail($id);

    return view('hrm.'.$this->rota['rota'], ['rota' => $this->rota]);
  }

  public function update(Request $request, $id)  {
    $id = CargoM::findOrFail($id);
    $id->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => 'Alterado com sucesso.']);
  }

  public function destroy($id)  {
    $id = CargoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => 'Excluido com sucesso.']);
  }
}
