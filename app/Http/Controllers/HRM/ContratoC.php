<?php namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\ContratoM;
use App\Models\HRM\DadoM;
use App\Models\HRM\ColaboradorM;
use App\Models\ADM\PessoaM;
use App\Models\ADM\EnderecoM;
use App\Models\ADM\ContatoM;

class ContratoC extends Controller
{
  public $rota = ['rota' => 'contrato','pasta'=>'HRM/CONTRATO/'];

  public function carrega()  {
    $this->rota = TI_acl(HRM_civil(TI_Ufs(HRM_tipo(H_colaborador(H_rota(HRM_cargo(H_pessoa(TI_Cidade($this->rota)))))))));

    $this->rota[$this->rota['rota']] = ContratoM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    $this->carrega();

    return view('HRM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $req, ContratoM $id)  { //dd($req->all());
    $data = TI_check_contrato($req->all());
    // if(!$req->id_pessoa)
    //   $data['id_pessoa'] = PessoaM::create($data)->id_pessoa;

    $idP = PessoaM::findOrNew($data['id_pessoa']);
    $data['id_pessoa'] = $idP->create($data)->id_pessoa;
    $idP->end()->create($data);
    $idP->dados()->create($data);
    $idP->colaborador()->create($data);
    $idP->contato()->create($data);

    $pasta = 'COLABORADORES/'.$data['id_pessoa'].'/';

    // $idE = EnderecoM::create($data)->id_endereco;
    // $idD = DadoM::create($data)->id_dado;
    // ColaboradorM::create($data);

    $log = ContratoM::create($data);

    if($req->arq_pessoa)    TI_up_arq($req->arq_pessoa, $pasta.'ADM/', 'pessoa', 'pessoa', $data['id_pessoa']);
    if($req->arq_endereco)  TI_up_arq($req->arq_endereco, $pasta.'END/', 'endereco', 'endereco', $idE);
    if($req->arq_dado)      TI_up_arq($req->arq_dado, $pasta.'DOCUMENTO/', $data['nm_arq_dado'], 'dado', $idD);
    if($req->arq_contrato)  TI_up_arq($req->arq_contrato, $pasta.'CONTRATO/', $data['nm_arq_contrato'], 'contrato', $log->id_contrato);

    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG
    HRM_txt($data['id_pessoa'].'_'.$data['pessoa'].'_');

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }
  // public function store(Request $req, ContratoM $id)  { //dd($req->all());
  //   $data = TI_check_contrato($req->all());
  //   if(!$req->id_pessoa)
  //     $data['id_pessoa'] = PessoaM::create($data)->id_pessoa;
  //
  //   $pasta = 'COLABORADORES/'.$data['id_pessoa'].'/';
  //
  //   $idE = EnderecoM::create($data)->id_endereco;
  //   $idD = DadoM::create($data)->id_dado;
  //   ColaboradorM::create($data);
  //
  //   if($data['comissao'] != null) $data['comissao'] = str_replace(',', '.', $req->comissao);
  //   $log = ContratoM::create($data);
  //
  //   if($req->arq_pessoa)    TI_up_arq($req->arq_pessoa, $pasta.'ADM/', 'pessoa', 'pessoa', $data['id_pessoa']);
  //   if($req->arq_endereco)  TI_up_arq($req->arq_endereco, $pasta.'END/', 'endereco', 'endereco', $idE);
  //   if($req->arq_dado)      TI_up_arq($req->arq_dado, $pasta.'DOCUMENTO/', $data['nm_arq_dado'], 'dado', $idD);
  //   if($req->arq_contrato)  TI_up_arq($req->arq_contrato, $pasta.'CONTRATO/', $data['nm_arq_contrato'], 'contrato', $log->id_contrato);
  //
  //   TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG
  //   HRM_txt($data['id_pessoa'].'_'.$data['pessoa'].'_');
  //
  //   return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  // }
  //
  // public function store(Request $req, ContratoM $id)  {
  //   $data = TI_check_contrato($req->all());
  //   if(!$req->id_pessoa)
  //     $data['id_pessoa'] = PessoaM::create($data)->id_pessoa;
  //
  //   $pasta = 'COLABORADORES/'.$data['id_pessoa'].'/';
  //
  //   if($req->arq_pessoa)    $data['arq_pessoa']   = TI_up_file2($req->arq_pessoa, $pasta.'ADM/', $data['id_pessoa']);
  //   if($req->arq_endereco)  $data['arq_endereco'] = TI_up_file2($req->arq_endereco, $pasta.'END/', $data['id_pessoa']);
  //   if($req->arq_dado)      $data['arq_dado']     = TI_up_file2($req->arq_dado, $pasta.'DOCUMENTO/', $data['id_pessoa']);
  //   if($req->arq_contrato)  $data['arq_contrato'] = TI_up_file2($req->arq_contrato, $pasta.'CONTRATO/', $data['id_pessoa']);
  //
  //   $idP   = PessoaM::findOrFail($data['id_pessoa']);
  //   $idP->update(['arq_pessoa' => $data['arq_pessoa']]);
  //
  //   EnderecoM::create($data);
  //   DadoM::create($data);
  //   ColaboradorM::create($data);
  //
  //   $data['comissao'] = str_replace(',', '.', $req->comissao);
  //   $log = ContratoM::create($data);
  //
  //   TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG
  //   HRM_txt($data['id_pessoa'].'_'.$data['pessoa'].'_');
  //
  //   return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  // }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = ContratoM::findOrFail($id);

    return view('HRM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $req, $id)  {
    $data = TI_check_contrato($req->all());
    $pasta = 'COLABORADORES/'.$data['id_pessoa'].'/';

    $idP = PessoaM::findOrFail($data['id_pessoa']);
    $idP->update($data);
    $idP->end()->update($data);
    $idP->dados()->update($data);
    $idP->contatos()->update($data);
    $idP->colaborador()->update($data);

    $idUP = ContratoM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($data);

    // if($req->arq_pessoa)    TI_up_arq($req->arq_pessoa, $pasta.'ADM/', 'pessoa', $data['id_pessoa']);
    // if($req->arq_endereco)  TI_up_arq($req->arq_endereco, $pasta.'END/', 'endereco', $idP->end->id_endereco);
    // if($req->arq_dado)      TI_up_arq($req->arq_dado, $pasta.'DOCUMENTO/', 'dado', $idP->dados->id_dado);
    // if($req->arq_contrato)  TI_up_arq($req->arq_contrato, $pasta.'CONTRATO/', 'contrato', $idUP->id_contrato);
    if($req->arq_pessoa)    TI_up_arq($req->arq_pessoa, $pasta.'ADM/', 'pessoa', 'pessoa', $data['id_pessoa']);
    if($req->arq_endereco)  TI_up_arq($req->arq_endereco, $pasta.'END/', 'endereco', 'endereco', $idE);
    if($req->arq_dado)      TI_up_arq($req->arq_dado, $pasta.'DOCUMENTO/', $data['nm_arq_dado'], 'dado', $idD);
    if($req->arq_contrato)  TI_up_arq($req->arq_contrato, $pasta.'CONTRATO/', $data['nm_arq_contrato'], 'contrato', $log->id_contrato);

    return redirect()->route($this->rota['rota'].'.edit',$idUP['id_contrato'])->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {  dd('something');
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id   = ContratoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);
    $idC   = ColaboradorM::where('id_pessoa', $id->id_pessoa);
    $idC->update(['sts_colaborador' => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
