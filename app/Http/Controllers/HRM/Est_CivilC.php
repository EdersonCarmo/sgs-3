<?php namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\Est_CivilM;

class Est_CivilC extends Controller
{
  public $rota = ['rota' => 'emp_tipo'];

  public function index()  {
    $this->rota[$this->rota['rota']] = Est_CivilM::where('sts_tipo','>',0)->get();

    return view('hrm.'.$this->rota['rota'],['rota' => $this->rota]);
  }

  public function store(Request $request, Est_CivilM $id)  {
    $id->create($request->all());

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->rota[$this->rota['rota']] = Est_CivilM::all();
    $this->rota['edit'] = Est_CivilM::findOrFail($id);

    return view('hrm.'.$this->rota['rota'], ['rota' => $this->rota]);
  }

  public function update(Request $request, $id)  {
    $id = Est_CivilM::findOrFail($id);
    $id->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => 'Alterado com sucesso.']);
  }

  public function destroy($id)  {
    $id   = Est_CivilM::findOrFail($id);
    $id->update(['sts_tipo' => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => 'Excluido com sucesso.']);
  }
}
