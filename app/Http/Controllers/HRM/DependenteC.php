<?php namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\DependenteM;
use App\Models\ADM\PessoaM;

class DependenteC extends Controller
{
  public $rota = ['rota' => 'dependente','pasta'=>'COLABORADORES/'];

  public function carrega($id)  {
    $this->rota = TI_acl(HRM_tipo(H_pessoa($this->rota),64));

    $this->rota[$this->rota['rota']] = DependenteM::where('sts_'.$this->rota['rota'],'>',0)
                                                  ->where('id_pessoa', $id)->get();
    $this->rota['pessoa'] = PessoaM::findOrFail($id);
  }

  public function show($id)  {
    $this->carrega($id);

    return view('HRM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $req, DependenteM $id)  {
    $log = $id->create($req->all())->id_dependente;

    if($req->arq_dependente)  TI_up_arq($req->arq_dependente, $this->rota['pasta'].$req->id_pessoa.'/DEPENDENTE/', $req->nm_arq_dependente, 'dependente', $log);
    TI_sgs_log($this->rota['rota'],'store',$log); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->rota['edit'] = DependenteM::findOrFail($id);
    $this->carrega($this->rota['edit']->id_pessoa);
    $this->rota['pasta'] .= $this->rota['edit']->id_pessoa.'/DEPENDENTE/';
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);

    return view('HRM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $req, $id)  {
    $idD = DependenteM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idD); // REG_LOG
    $idD->update($req->all());

    if($req->arq_dependente)  TI_up_arq($req->arq_dependente, $this->rota['pasta'].$req->id_pessoa.'/DEPENDENTE/', $req->nm_arq_dependente, 'dependente', $id);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    $id = DependenteM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
