<?php namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\Ferias_PagtoM;
use App\Models\HRM\FeriasM;

class Ferias_PagtoC extends Controller
{
  public $rota = ['rota' => 'ferias_pagto','pasta'=>'HRM/FERIAS'];

  public function carrega()  {
    $this->rota = TI_acl(HRM_tipo($this->rota,61));

    $this->rota[$this->rota['rota']] = Ferias_PagtoM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    $this->carrega();

    return view('HRM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function show($id)  {
    $this->carrega();

    $this->rota['ferias'] = FeriasM::findOrFail($id);
    $this->rota[$this->rota['rota']] = Ferias_PagtoM::where('sts_'.$this->rota['rota'],'>',0)
                                                    ->where('id_ferias',$id)->get();

    return view('HRM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, FeriasM $id)  {
    $data = $request->all();

    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idT->documento);

    $log = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = FeriasM::findOrFail($id);

    return view('HRM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();
    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idT->tipo);

    $idUP = FeriasM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id   = FeriasM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
