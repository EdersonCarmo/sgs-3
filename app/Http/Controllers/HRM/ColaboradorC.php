<?php namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\ColaboradorM;
use App\Models\HRM\DadoM;

class ColaboradorC extends Controller
{
  public $rota = ['rota' => 'colaborador'];

  public function carrega()  {
    $this->rota = TI_acl(H_cargo(H_pessoa($this->rota)));

    $this->rota[$this->rota['rota']] = ColaboradorM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    $this->carrega();

    return view('hrm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, ColaboradorM $id, DadoM $idD)  {
    $data = $request->all();
    $dado = $idD->create($data);
    $data['id_dado'] = $dado->id_dado;
    $id->create($data);

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = ColaboradorM::findOrFail($id);

    return view('hrm.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();
    if($data['id_dado'] == 0){
      $idD = new DadoM;
      $d = $idD->create($data);
      $data['id_dado'] = $d->id_dado;
    }else{
      $idD = DadoM::findOrFail($data['id_dado']);
      $idD->update($data);
    }
    $id = ColaboradorM::findOrFail($id);
    $id->update($data);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    $id = ColaboradorM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
