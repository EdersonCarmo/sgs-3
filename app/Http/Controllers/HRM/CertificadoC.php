<?php namespace App\Http\Controllers\HRM;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HRM\CertificadoM;

class CertificadoC extends Controller
{
  public $rota = ['rota' => 'certificado','pasta'=>'HRM/CERT/'];

  public function carrega()  {
    $this->rota = TI_acl(HRM_tipo(HRM_curso(H_colaborador($this->rota))));

    $this->rota[$this->rota['rota']] = CertificadoM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    $this->carrega();

    return view('HRM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, CertificadoM $id)  {
    $data = $request->all();

    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idT->documento);

    $log = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = CertificadoM::findOrFail($id);

    return view('HRM.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();
    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idT->tipo);

    $idUP = CertificadoM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id   = CertificadoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
