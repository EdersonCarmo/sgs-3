<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MRP_EmprestimoM;

use Illuminate\Http\Request;

class MRP_EmprestimoC extends Controller
{
  public $rota = ['rota' => 'emprestimo','pasta'=>'MRP/EMP/'];

  public function carrega()  {
    $this->rota = TI_acl(H_mrp_tipo($this->rota,50));
    $this->rota[$this->rota['rota']] = MRP_EmprestimoM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('log.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $request, MRP_EmprestimoM $id)  {
    $data = $request->all();
    $idV = VeiculoM::findOrFail($data['id_veiculo']);
    $idT = LOG_TipoM::findOrFail($data['id_log_tipo']);

    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idT->documento);

    $log = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = MRP_EmprestimoM::findOrFail($id);

    return view('log.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $data = $request->all();
    $idV = VeiculoM::findOrFail($data['id_veiculo']);
    $idT = LOG_TipoM::findOrFail($data['id_log_tipo']);
    if($request->arquivo)
      $data['arquivo'] = TI_up_file($request, $this->rota['pasta'].$idV->placa.'/', $idT->tipo);

    $idUP = MRP_EmprestimoM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($request->all());

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = MRP_EmprestimoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
