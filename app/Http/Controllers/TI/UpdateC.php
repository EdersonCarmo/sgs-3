<?php namespace App\Http\Controllers\TI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TI\UpdateM;
use App\Models\TI\ModuloM;

class UpdateC extends Controller
{
  public $rota = ['rota' => 'update','pasta'=>'TI/UP/'];

  public function carrega()  {
    $this->rota = TI_acl(TI_modulo( H_colaborador($this->rota)));
    $this->rota[$this->rota['rota']] = UpdateM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('ti.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $req, UpdateM $id)  {
    $idM = ModuloM::findOrFail($req->id_modulo);
    $id = new UpdateM;

    $id->id_modulo      = $req->id_modulo;
    $id->id_programador = $req->id_programador;
    $id->id_solicitante = $req->id_solicitante;
    $id->desc           = $req->desc;
    $id->tempo          = $req->tempo;
    $id->id_versao      = TI_up_version($idM->sts_modulo);

    $log = $id->save();
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = UpdateM::findOrFail($id);

    return view('ti.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $req, $id)  {
    $idUP = UpdateM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG

    if($req->arquivo)
      $idUP->arquivo = TI_up_file($req, $this->rota['pasta'], $req[$this->rota['rota']]);

    $idUP->id_modulo      = $req->id_modulo;
    $idUP->id_programador = $req->id_programador;
    $idUP->id_solicitante = $req->id_solicitante;
    $idUP->desc           = $req->desc;
    $idUP->tempo          = $req->tempo;

    $idUP->save();

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = ModuloM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
