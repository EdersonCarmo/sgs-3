<?php namespace App\Http\Controllers\TI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TI\HelpM;

class HelpC extends Controller
{
  public $rota = ['rota' => 'help','pasta'=>'TI/HELP/'];

  public function carrega()  {
    $this->rota = TI_acl(TI_user($this->rota));
    $this->rota = TI_tipo($this->rota,$this->rota['rota']);
    $_SESSION['title'] = $this->rota['rota'];
    $this->rota[$this->rota['rota']]  = HelpM::where('sts_help','>',0)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('TI/help', ['rota'=>$this->rota]);
  }

  public function store(Request $req)  {
    $id = new HelpM;
    $id->id_solicitante = $req->id_solicitante;
    $id->id_ti_tipo = $req->id_ti_tipo;
    $id->help = $req->help;

    if($req->arquivo)
      $id->arquivo = TI_up_file2($req->arquivo, $this->rota['pasta'], $req[$this->rota['rota']]);

    $id->save();

    TI_add_fluxo('Ticket','help','help',$id->id_help); // FLUXO/MODULO/ROTA/ID_ITEM
    TI_sgs_log($this->rota['rota'],'store',$id->id_help); // REG_LOG

    return back()->withInput();
  }

  public function edit($id)  { //AclM::where('id_user',$id)->count();
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = HelpM::findOrFail($id);
    TI_view_fluxo($this->rota['rota'],$id);

    return view('TI/help', ['rota'=>$this->rota]);
  }

  public function update(Request $req, $id)  {
    $idUP = HelpM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG // REG_LOG

    if($req->arquivo)
      $idUP->arquivo = TI_up_file($req, $this->rota['pasta'], $req[$this->rota['rota']]);

    $idUP->update($req->all());
    switch ($req['sts_help']) {
      case '0': TI_del_fluxo('help',$id); break;
      case '1': TI_add_fluxo('Ticket','help','help',$id); break;
      // case '2': TI_view_fluxo('help',$id); break;
      // case '3': TI_view_fluxo('help',$id); break;
      case '4': TI_del_fluxo('help',$id); break;
      case '5': TI_del_fluxo('help',$id); break;

      default:  // code...
        break;
    }


    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = HelpM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
