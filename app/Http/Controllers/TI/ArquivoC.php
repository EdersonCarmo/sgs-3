<?php namespace App\Http\Controllers\TI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TI\ArquivoM;

class ArquivoC extends Controller
{
  public $rota = ['rota' => 'arquivo'];

  public function edit($id)  { 
    $id = ArquivoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->back()->with(['success' => 'Excluido com sucesso.']);
  }

  public function destroy($id)  { dd('ARQ');
    $id = ArquivoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->back()->with(['success' => 'Excluido com sucesso.']);
  }
}
