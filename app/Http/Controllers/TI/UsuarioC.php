<?php namespace App\Http\Controllers\TI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\TI\ModuloM;
use App\Models\TI\AclM;
use App\Models\HRM\ColaboradorM;
use App\User;

class UsuarioC extends Controller
{
  public $rota = ['rota' => 'usuario','pasta'=>'TI/USER/'];

  public function carrega()  {
    $this->rota = TI_acl($this->rota);
    $this->rota = H_colaborador($this->rota);
    $this->rota[$this->rota['rota']] = User::where('sts_'.$this->rota['rota'],1)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('auth/usuario', ['rota'=>$this->rota]);
  }

  public function create()  {
    $usuarios = User::all();

    return view('auth/register', compact('usuarios'));
  }

  public function store(Request $request)  {
    $id = new User;
    $id->name = $request['name'];
    $id->email = $request['email'];
    $id->password = Hash::make($request['password']);

    if($request->arquivo)
      $id->arquivo = TI_up_file($request, $this->rota['pasta'], $request[$this->rota['rota']]);

    $log = $id->save();
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    // $usuario = User::create([
    //   'name' => $data['name'],
    //   'email' => $data['email'],
    //   'password' => Hash::make($data['password']),
    // ]);

    return back()->withInput()->with(['success' => $this->rota['rota'].' Cadastrado com sucesso.']);
  }

  public function edit($id)  { //AclM::where('id_user',$id)->count();
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $cP = AclM::where('id_user',$id)->count();
    $cM = ModuloM::where('sts_modulo','>',0)->count();
    $this->rota['edit'] = User::findOrFail($id);
    $this->rota['edit2'] = $cP / $cM * 100;

    return view('auth/usuario', ['rota'=>$this->rota]);
  }

  public function update(Request $request, $id)  {
    $idUP = User::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG // REG_LOG
    $idUP->name = $request['name'];
    $idUP->email = $request['email'];
    $idUP->password = Hash::make($request['password']);

    if($request->arquivo)
      $idUP->arquivo = TI_up_file2($request->arquivo, $this->rota['pasta'], $request[$this->rota['rota']]);

    if($request->id_colaborador){
      $idC = ColaboradorM::findOrFail($request->id_colaborador);
      $idC->id_user = $id;
      $idC->save();
    }

    $idUP->save();

    return back()->withInput()->with(['success' => $this->rota['rota'].' Alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = User::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
