<?php namespace App\Http\Controllers\TI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TI\ModuloM;

class ModuloC extends Controller
{
  public $rota = ['rota' => 'modulo','pasta'=>'TI/MOD/'];

  public function carrega()  {
    $this->rota = TI_acl($this->rota);
    $this->rota[$this->rota['rota']] = ModuloM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('ti.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $req)  {
    $id = new ModuloM;
    $id->sts_modulo = 1;

    if($req->arquivo)
      $id->arquivo = TI_up_file($req, $this->rota['pasta'], $req[$this->rota['rota']]);

    if($req->modulo_id){
      $idM = ModuloM::findOrFail($req->modulo_id);
      $id->modulo_id = $req->modulo_id;
      $id->sts_modulo = $idM->sts_modulo + 1;
    }

    $id->id_versao = TI_up_version($id->sts_modulo);
    $id->modulo = $req->modulo;
    $id->cod = $req->cod;
    $id->desc = $req->desc;
    $id->rota = $req->rota;

    $log = $id->save();
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG


    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = ModuloM::findOrFail($id);

    return view('ti.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $req, $id)  {
    $idUP = ModuloM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->sts_modulo = 1;

    if($req->arquivo)
      $idUP->arquivo = TI_up_file($req, $this->rota['pasta'], $req[$this->rota['rota']]);

    if($req->modulo_id){
      $idM = ModuloM::findOrFail($req->modulo_id);
      $idUP->modulo_id = $req->modulo_id;
      $idUP->sts_modulo = $idM->sts_modulo + 1;
    }

    $idUP->modulo = $req->modulo;
    $idUP->cod = $req->cod;
    $idUP->desc = $req->desc;
    $idUP->rota = $req->rota;

    $idUP->save();

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id = ModuloM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
