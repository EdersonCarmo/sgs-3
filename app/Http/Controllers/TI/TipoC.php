<?php namespace App\Http\Controllers\TI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TI\TipoM;

class TipoC extends Controller
{
  public $rota = ['rota'=>'ti_tipo'];

  public function carrega()  {
    $this->rota = TI_acl(TI_mod_id($this->rota,8));
    $this->rota[$this->rota['rota']] = TipoM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('TI.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $req, TipoM $id)  {
    $data = $req->all();
    if($req->arquivo)
      $data['arquivo'] = TI_up_file($req, $this->rota['pasta'], $req[$this->rota['rota']]);

    $log = $id->create($data);
    TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = TipoM::findOrFail($id);

    return view('TI.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $req, $id)  {
    $data = $req->all();
    if($req->arquivo)
      $data['arquivo'] = TI_up_file($req, $this->rota['pasta'], $req[$this->rota['rota']]);

    $idUP = TipoM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG
    $idUP->update($data);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id   = TipoM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
