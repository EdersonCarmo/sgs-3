<?php namespace App\Http\Controllers\TI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TI\AclM;

class AclC extends Controller
{
  public $rota = ['rota'=>'permissao'];

  public function carrega()  {
    $this->rota = TI_acl(TI_user(TI_modulo($this->rota)));
    $this->rota[$this->rota['rota']] = AclM::where('sts_'.$this->rota['rota'],'>',0)->get();
  }

  public function copia_acl($idA, $idB, $pai = false)  {
    $idA = AclM::where('id_user',$idA)->get();
    foreach ($idB as $keyB) {
      foreach ($idA as $keyA) { //dd($keyA);
        if (AclM::where('id_user','=', $keyB)->where('id_modulo','=',$keyA->id_modulo)->count() != 0)
          AclM::where('id_user','=', $keyB)->where('id_modulo','=',$keyA->id_modulo)->update(['sts_permissao' => 0]);

        $idC = new AclM;
        $idC->permissao = $keyA->permissao;
        $idC->id_user   = $keyB;
        $idC->id_modulo = $keyA->id_modulo;
        $idC->p   = $keyA->p;
        $idC->fx  = $keyA->fx;
        $idC->c   = $keyA->c;
        $idC->r   = $keyA->r;
        $idC->u   = $keyA->u;
        $idC->d   = $keyA->d;
        $idC->up  = $keyA->up;
        $idC->dw  = $keyA->dw;
        $idC->f   = $keyA->f;
        $idC->a   = $keyA->a;
        $idC->s   = $keyA->s;
        if ($pai == 'true') $idC->id_pai = $keyA->id_permissao;
        $idC->save();
      }
    }
  }

  public function index()  {
    TI_sgs_log($this->rota['rota'],'index'); // REG_LOG
    $this->carrega();

    return view('ti.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function store(Request $req, AclM $id)  {
    if (isset($req->copia_acl)) {
      $this->copia_acl($req->id_A,$req->id_B,$req->copia_acl);

      return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
    }

    $for = $req->all();
    $data = TI_check_acl($req->all());

    foreach ($for['id_user'] as $key) {
      foreach ($for['id_modulo'] as $key1) {
        if (AclM::where('id_user','=', $key)->where('id_modulo','=',$key1)->count() == 0){
          $data['id_user'] = $key;
          $data['id_modulo'] = $key1;
          $log = $id->create($data);
          TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG
        }elseif (isset($data['sub'])) {
          AclM::where('id_user','=', $key)->where('id_modulo','=',$key1)->update(['sts_permissao' => 0]);
          $data['id_user'] = $key;
          $data['id_modulo'] = $key1;
          $log = $id->create($data);
          TI_sgs_log($this->rota['rota'],'store',$log['id_'.$this->rota['rota']]); // REG_LOG
        }
      }
    }

    return redirect()->back()->with(['success' => 'Cadastrado com sucesso.']);
  }

  public function edit($id)  {
    $this->carrega();
    if($this->rota['acl']->u == 0) return redirect()->back()->with(['danger' => 'Sem acesso.']);
    $this->rota['edit'] = AclM::findOrFail($id);

    return view('ti.'.$this->rota['rota'], ['rota'=>$this->rota]);
  }

  public function update(Request $req, $id)  {
    AclM::where('id_permissao','=', $id)->orWhere('id_pai','=',$id)->update(TI_check_acl($req->all()));

    $idUP = AclM::findOrFail($id);
    TI_sgs_log($this->rota['rota'],'update',$id,$idUP); // REG_LOG

    if(isset($req->retorna))
      return redirect()->route($this->rota['rota'].'.edit',$id)->with(['success' => $this->rota['rota'].' alterado com sucesso.']);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' alterado com sucesso.']);
  }

  public function destroy($id)  {
    TI_sgs_log($this->rota['rota'],'destroy',$id); // REG_LOG
    $id   = AclM::findOrFail($id);
    $id->update(['sts_'.$this->rota['rota'] => 0]);

    return redirect()->route($this->rota['rota'].'.index')->with(['success' => $this->rota['rota'].' excluído com sucesso.']);
  }
}
