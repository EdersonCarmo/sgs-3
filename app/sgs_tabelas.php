<?php
$_SESSION['search'] $_SESSION['links'] $_SESSION['versao'] $_SESSION['help'] $_SESSION['tipos']
// PERMISSÕES
fx['FLUXO'] p['PRINT'] c['CRIAR'] r['LEITURA'] u['ALTERAR'] d['EXCLUIR'] up['ENVIAR']
dw['BAIXAR'] f['FINALIZAR'] a['APROVAR'] s['SOLICITAR']
// PREFERENCIAS
t['tema'] sia['size icon acao'] stt['size text table']
// TI_Helper
TI_alerta($vl,$tx) TI_check_acl($d) TI_sgs_log($rota,$acao,$key=0,$old=0) TI_up_file(Request $request, $pasta, $nome)
TI_up_file2($arq, $pasta, $nome) TI_add_fluxo($fl,$md,$rt,$it) TI_del_fluxo($rt,$it) TI_view_fluxo($rt,$it) // ROTA/ID_ITEM
TI_option_modulos($mod,$idMod) TI_option_mod_mul($mod,$idMod) TI_acl($rota) TI_modulo($rota) TI_mod_id($rota,$id)
TI_tipo($rota,$rota2) TI_up_version($sts)
// SQL_Helper
H_rota($rota) H_emp_gru($rota,$ramo = 8) H_lead($rota,$ramo = 8) TI_user($rota) H_setor($rota) H_fornecedor($rota,$mod)
H_colaborador($rota) H_log_tipo($rota) H_vei($rota) H_motorista($rota) H_mrp_tipo($rota, $id = 34) H_und($rota) H_est($rota)
H_prod($rota) H_colaboradorMRP($rota) H_cargo($rota) H_pessoa($rota)
// MRP_Helper
MRP_up_sts_pedido($id) MRP_up_total_pedido($id, $at) MRP_up_ped_atendido($id, $at) MRP_up_estoque($data)
MRP_item_estoque($rota, $id)

// LOG_Helper
LOG_check_vistoria($d) LOG_sum_item_vistoria($d) LOG_vistoria($rota)

// SGS.JS
mascara(t, mask) Mudarestado(el) Mostra_form_table(el) Chip(el,val) MudarestadoSeach(el) MudaEstadoMenu(el)
