<?php namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use Notifiable;

  protected $fillable = [
    'id','name', 'email', 'password','sts_usuario','arquivo',
  ];

  protected $hidden = [
    'password', 'remember_token',
  ];

  public function preferencia()    {
    return $this->hasMany(PreferenciaM::class,'users_id','id');
  }
}
