<?php namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class CargoM extends Model
{
  protected $table = 'hrm_cargos';
  protected $primaryKey = 'id_cargo';

  protected $fillable = [
    'id_cargo',
    'updated_at',
    'created_at',
    'sts_cargo',
    'cargo',
    'nivel',
    'cbo',
    'campo',
    'piso',
    'reporte',
    'subordinado'
  ];
}
