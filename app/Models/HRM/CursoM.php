<?php namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use App\Models\HRM\Curso_CargoM;
use App\Models\TI\ArquivoM;

class CursoM extends Model
{
  protected $table = 'hrm_cursos';
  protected $primaryKey = 'id_curso';

  protected $fillable = [
    'id_curso',
    'id_hrm_tipo',
    'url',
    'curso',
    'desk',
    'dt_validade',
    'carga',
    'created_at',
    'updated_at',
    'sts_curso'
  ];

  public function cursocargo()    {
    return $this->hasMany(Curso_CargoM::class,'id_curso','id_curso');
  }

  public function arq()    {
    return $this->hasMany(ArquivoM::class,'id_item','id_curso')
                ->where('rota','curso')
                ->where('sts_arquivo','>',0);
  }
}
