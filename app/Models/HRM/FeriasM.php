<?php namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use App\Models\HRM\ColaboradorM;

class FeriasM extends Model
{
  protected $table = 'hrm_ferias';
  protected $primaryKey = 'id_ferias';

  protected $fillable = [
    'id_ferias',
    'id_colaborador',
    'periodo',
    'desc',
    'dt_vencimento',
    'sts_ferias',
    'updated_at',
    'created_at'
  ];

  public function colaborador()    {
    return $this->hasOne(ColaboradorM::class,'id_colaborador','id_colaborador');
  }
}
