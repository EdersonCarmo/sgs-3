<?php namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class Curso_CargoM extends Model
{
  protected $table = 'hrm_cursos_cargos';
  protected $primaryKey = 'id_curso_cargo';

  protected $fillable = [
    'id_curso_cargo',
    'id_curso',
    'id_cargo',
    'sts_curso_cargo',
    'created_at',
    'updated_at'
  ];
}
