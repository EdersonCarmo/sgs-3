<?php namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class CertificadoM extends Model
{
  protected $table = 'hrm_certificados';
  protected $primaryKey = 'id_certificado';

  protected $fillable = [
    'id_certificado',
    'id_colaborador',
    'id_hrm_tipo',
    'id_curso',
    'instituicao',
    'certificado',
    'desk',
    'dt_certificado',
    'dt_inicio',
    'dt_fim',
    'carga',
    'sts_certificado',
    'updated_at',
    'created_at'
  ];
}
