<?php namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class Ferias_PagtoM extends Model
{
  protected $table = 'hrm_ferias_pagtos';
  protected $primaryKey = 'id_ferias_pagto';

  protected $fillable = [
    'id_ferias_pagto',
    'id_ferias',
    'ferias_pagto',
    'arquivo',
    'dias',
    'dt_inicio',
    'dt_fim',
    'desc',
    'sts_ferias_pagto',
    'updated_at'
  ];
}
