<?php namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class CurriculoM extends Model
{
  protected $table = 'hrm_curriculos';
  protected $primaryKey = 'id_curriculo';

  protected $fillable = [
    'id_curriculo',
    'id_cidade',
    'curriculo',
    'desc',
    'funcao',
    'arquivo',
    'created_at',
    'updated_at',
    'sts_curriculo'
  ];
}
