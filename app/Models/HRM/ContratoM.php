<?php namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use App\Models\ADM\PessoaM;
use App\Models\HRM\DependenteM;
use App\Models\TI\ArquivoM;

class ContratoM extends Model
{
  protected $table = 'hrm_contratos';
  protected $primaryKey = 'id_contrato';
  protected $fillable = [
    'id_contrato',
    'id_empresa',
    'id_colaborador',
    'id_contratador',
    'id_demissor',
    'id_pessoa',
    'id_cargo',
    'contrato',
    'registro',
    'dt_admissao',
    'dt_demissao',
    'dt_renovacao',
    'dt_efetivacao',
    'comissao',
    'desc',
    'salario',
    'periculosidade',
    'sts_contrato',
    'updated_at',
    'created_at'
  ];

  public function pessoa()    {
    return $this->hasOne(PessoaM::class,'id_pessoa','id_pessoa');
  }

  public function filhos()    {
    return $this->hasMany(DependenteM::class,'id_pessoa','id_pessoa');
  }

  public function arq()    {
    return $this->hasMany(ArquivoM::class,'id_item','id_contrato')
                ->where('rota','contrato')
                ->where('sts_arquivo','>',0);
  }
}
