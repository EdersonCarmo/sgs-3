<?php namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class TipoM extends Model
{
  protected $table = 'hrm_tipos';
  protected $primaryKey = 'id_hrm_tipo';

  protected $fillable = [
    'id_hrm_tipo',
    'id_modulo',
    'hrm_tipo',
    'vencimento',
    'notifica',
    'sts_hrm_tipo',
    'updated_at',
    'created_at'
  ];
}
