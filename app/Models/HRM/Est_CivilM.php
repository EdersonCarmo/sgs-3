<?php namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class Est_CivilM extends Model
{
  protected $table = 'hrm_estado_civil';
  protected $primaryKey = 'id_estado_civil';

  protected $fillable = [
    'id_estado_civil',
    'estado_civil',
    'obs',
    'updated_at',
    'created_at',
    'sts_estado_civil'
  ];
}
