<?php namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use App\Models\TI\UfM;
use App\Models\TI\ArquivoM;

class DadoM extends Model
{
  protected $table = 'hrm_dados';
  protected $primaryKey = 'id_dado';
  protected $fillable = [
    'id_dado',
    'id_pessoa',
    'pai',
    'mae',
    'conjuge',
    'conjuge_trabalha',
    'filhos',
    'filhos_nome',
    'dt_casamento',
    'dt_nascimento',
    'ctps',
    'ctps_serie',
    'ctps_id_uf',
    'ctps_dt_exp',
    'naturalidade',
    'nit',
    'cnis',
    'rg',
    'rg_orgao',
    'rg_dt_exp',
    'rg_id_uf',
    'cpf',
    'eleitor',
    'eleitor_zona',
    'eleitor_secao',
    'cnh',
    'cnh_categoria',
    'cnh_dt_exp',
    'cnh_dt_vcto',
    'estado_civil',
    'id_estado_civil',
    'grau_instrucao',
    'grau_instrucao_completo',
    'cor_pele',
    'genero',
    'updated_at',
    'created_at',
    'sts_dado'
  ];

  public function rg_uf()    {
    return $this->hasOne(UfM::class,'rg_id_uf','id_uf');
  }

  public function arq()    {
    return $this->hasMany(ArquivoM::class,'id_item','id_dado')
                ->where('rota','dado')
                ->where('sts_arquivo','>',0);
  }
  // public function cpf_uf()    {
  //   return $this->hasOne(UfM::class,'id_cidade','id_cidade');
  // }
}
