<?php namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;
use App\Models\HRM\TipoM;
use App\Models\TI\ArquivoM;
use App\Models\ADM\PessoaM;

class DependenteM extends Model
{
  protected $table = 'hrm_dependentes';
  protected $primaryKey = 'id_dependente';

  protected $fillable = [
    'id_dependente',
    'id_pessoa',
    'id_hrm_tipo',
    'dependente',
    'arq_dependente',
    'ocupacao',
    'arquivo',
    'nacionalidade',
    'cpf',
    'sts_dependente',
    'updated_at',
    'created_at'
  ];

  public function pessoa()    {
    return $this->hasOne(PessoaM::class,'id_pessoa','id_pessoa');
  }

  public function arq()    {
    return $this->hasMany(ArquivoM::class,'id_item','id_dependente')
    ->where('rota','dependente')
    ->where('sts_arquivo','>',0);
  }

  public function tipo()    {
    return $this->hasOne(TipoM::class,'id_hrm_tipo','id_hrm_tipo');
  }
}
