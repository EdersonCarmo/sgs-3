<?php namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class ExameM extends Model
{
  protected $table = 'hrm_exames';
  protected $primaryKey = 'id_exame';

  protected $fillable = [
    'id_exame',
    'id_colaborador',
    'id_hrm_tipo',
    'exame',
    'arquivo',
    'instituicao',
    'desc',
    'dt_exame',
    'sts_exame',
    'updated_at',
    'created_at'
  ];
}
