<?php namespace App\Models\HRM;

use Illuminate\Database\Eloquent\Model;

class ColaboradorM extends Model
{
  protected $table = 'hrm_colaboradores';
  protected $primaryKey = 'id_colaborador';

  protected $fillable = [
    'id_colaborador',
    'id_contrato',
    'id_cargo',
    'id_pessoa',
    'id_dado',
    'id_empresa',
    'id_user',
    'updated_at',
    'created_at',
    'sts_colaborador'
  ];

  public function pessoas()    {
    return $this->hasOne('App\Models\ADM\PessoaM','id_pessoa','id_pessoa');
  }

  public function pessoa()    {
    return $this->hasOne('App\Models\ADM\PessoaM','id_pessoa','id_pessoa');
  }
}
