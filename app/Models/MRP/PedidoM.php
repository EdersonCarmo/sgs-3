<?php namespace App\Models\MRP;

use Illuminate\Database\Eloquent\Model;
use App\Models\HRM\ColaboradorM;

class PedidoM extends Model
{
  protected $table = 'mrp_pedidos';
  protected $primaryKey = 'id_pedido';

  protected $fillable = [
    'id_pedido',
    'id_mrp_tipo',
    'id_solicitante',
    'id_colaborador',
    'id_autorizador',
    'cod_obra',
    'obra',
    'dt_entrega',
    'dt_entrega',
    'atendido',
    'total',
    'obs',
    'updated_at',
    'created_at',
    'sts_pedido'
  ];

  public function colaboradores()    {
    return $this->hasOne(ColaboradorM::class,'id_colaborador','id_colaborador');
  }

  public function solicitantes()    {
    return $this->hasOne(ColaboradorM::class,'id_colaborador','id_solicitante');
  }

  public function autorizador()    {
    return $this->hasOne(ColaboradorM::class,'id_user','id_autorizador');
  }

  public function tipos()    {
    return $this->hasOne(TipoM::class,'id_mrp_tipo','id_mrp_tipo');
  }
}
