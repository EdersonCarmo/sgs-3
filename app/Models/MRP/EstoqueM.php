<?php namespace App\Models\MRP;

use Illuminate\Database\Eloquent\Model;
use App\Models\ADM\PessoaM;
use App\Models\EMP\EmpresaM;

class EstoqueM extends Model
{
  protected $table = 'mrp_estoques';
  protected $primaryKey = 'id_estoque';

  protected $fillable = [
    'id_estoque',
    'id_empresa',
    'id_almoxarife',
    'id_endereco',
    'id_mrp_tipo',
    'estoque',
    'created_at',
    'updated_at',
    'sts_estoque'
  ];

  public function pessoas()    {
    return $this->hasOne(PessoaM::class,'id_pessoa','id_almoxarife');
  }

  public function tipos()    {
    return $this->hasOne(TipoM::class,'id_mrp_tipo','id_mrp_tipo');
  }

  public function empresas()    {
    return $this->hasOne(EmpresaM::class,'id_empresa','id_empresa');
  }
}
