<?php namespace App\Models\MRP;

use Illuminate\Database\Eloquent\Model;

class UndM extends Model
{
  protected $table = 'mrp_unds';
  protected $primaryKey = 'id_und';

  protected $fillable = [
    'id_und',
    'und',
    'cod',
    'sts_und',
    'updated_at',
    'created_at'
  ];
}
