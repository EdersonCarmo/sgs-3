<?php namespace App\Models\MRP;

use Illuminate\Database\Eloquent\Model;

class ItemM extends Model
{
  protected $table = 'mrp_itens';
  protected $primaryKey = 'id_mrp_item';

  protected $fillable = [
    'id_mrp_item',
    'id_produto',
    'id_pedido',
    'id_estoque',
    'id_compra',
    'id_kit',
    'local',
    'rota',
    'sts_mrp_item',
    'updated_at',
    'created_at',
    'qnt'
  ];

  public function produtos()    {
    return $this->hasOne(ProdutoM::class,'id_produto','id_produto');
  }

  public function estoques()    {
    return $this->hasOne(EstoqueM::class,'id_estoque','id_estoque');
  }

  public function pedidos()    {
    return $this->hasOne(EstoqueM::class,'id_pedido','id_pedido');
  }
  //
  // public function tipos()    {
  //   return $this->hasOne(TipoM::class,'id_mrp_tipo','mrp_produto.id_mrp_tipo');
  // }

  public function search(Array $data)    {
    return $this->where(function ($query) use ($data) {
      if(isset($data['produto'])) $query->where('mrp_produtos.produto', 'like', '%'.$data['produto'].'%');
      if(isset($data['cod'])) $query->where('mrp_produtos.cod', $data['cod']);
      if(isset($data['marca'])) $query->where('mrp_produtos.marca', 'like', '%'.$data['marca'].'%');
      if(isset($data['modelo'])) $query->where('mrp_produtos.modelo', 'like', '%'.$data['modelo'].'%');
      if(isset($data['id_mrp_tipo'])) $query->whereIn('mrp_produtos.id_mrp_tipo', $data['id_mrp_tipo']);
      if(isset($data['local'])) $query->whereIn('local', $data['local']);
      // if(isset($data['qnt_min'])) $query->whereBetween('qnt', array($data['qnt_min'], $data['qnt_max']));
      if(isset($data['qnt_min'])) $query->where('qnt','>=', $data['qnt_min']);
      if(isset($data['qnt_max'])) $query->where('qnt','<=', $data['qnt_max']);
      if(isset($data['ent_ini'])) $query->where('created_at','>=', $data['ent_ini']);
      if(isset($data['ent_fim'])) $query->where('created_at','<=', $data['ent_fim']);
      if(isset($data['mov_ini'])) $query->where('updated_at','>=', $data['mov_ini']);
      if(isset($data['mov_fim'])) $query->where('updated_at','<=', $data['mov_fim']);
    })
    ->join('mrp_produtos','mrp_itens.id_produto','mrp_produtos.id_produto')
    ->where('id_estoque', $data['id_estoque'])
    ->where('sts_mrp_item','>',0)
    ->get();
  }
}
