<?php namespace App\Models\MRP;

use Illuminate\Database\Eloquent\Model;
use App\Models\TI\ModuloM;

class TipoM extends Model
{
  protected $table = 'mrp_tipos';
  protected $primaryKey = 'id_mrp_tipo';

  protected $fillable = [
    'id_mrp_tipo',
    'id_modulo',
    'mrp_tipo',
    'rota',
    'sts_mrp_tipo',
    'updated_at',
    'created_at'
  ];

  public function modulos()    {
    return $this->hasOne(ModuloM::class,'id_modulo','id_modulo');
  }
}
