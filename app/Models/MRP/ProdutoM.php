<?php namespace App\Models\MRP;

use Illuminate\Database\Eloquent\Model;

class ProdutoM extends Model
{
  protected $table = 'mrp_produtos';
  protected $primaryKey = 'id_produto';

  protected $fillable = [
    'id_produto',
    'id_mrp_tipo',
    'id_und',
    'produto',
    'chip',
    'imei',
    'operadora',
    'qnt_min',
    'qnt_min',
    'prazo_entrega',
    'ca',
    'marca',
    'modelo',
    'cod',
    'obs',
    'peso',
    'validade',
    'und',
    'created_at',
    'updated_at',
    'sts_produto'
  ];

  public function tipos()    {
    return $this->hasOne(TipoM::class,'id_mrp_tipo','id_mrp_tipo');
  }
}
