<?php namespace App\Models\MRP;

use Illuminate\Database\Eloquent\Model;

class MovimentoM extends Model
{
  protected $table = 'mrp_movimentos';
  protected $primaryKey = 'id_movimento';

  protected $fillable = [
    'id_movimento',
    'id_produto',
    'id_estoque',
    'id_user',
    'id_mrp_tipo',
    'id_pedido',
    'qnt',
    'dt_movimento',
    'sts_movimentacao',
    'updated_at',
    'created_at'
  ];

  public function produtos()    {
    return $this->hasOne(ProdutoM::class,'id_produto','id_produto');
  }

  public function tipos()    {
    return $this->hasOne(TipoM::class,'id_mrp_tipo','id_mrp_tipo');
  }
}
