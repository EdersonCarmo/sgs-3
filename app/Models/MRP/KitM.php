<?php namespace App\Models\MRP;

use Illuminate\Database\Eloquent\Model;

class KitM extends Model
{
  protected $table = 'mrp_kits';
  protected $primaryKey = 'id_kit';

  protected $fillable = [
    'id_kit',
    'kit',
    'updated_at',
    'created_at',
    'sts_kit'
  ];
}
