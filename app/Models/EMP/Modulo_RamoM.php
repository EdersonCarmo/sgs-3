<?php namespace App\Models\EMP;

use Illuminate\Database\Eloquent\Model;

class Modulo_RamoM extends Model
{
  protected $table = 'emp_modulos_ramos';
  protected $primaryKey = 'id_ramo';
  protected $fillable = [
    'id_modulo',
    'id_ramo'
  ];
}
