<?php namespace App\Models\EMP;

use Illuminate\Database\Eloquent\Model;

class TipoM extends Model
{
  protected $table = 'emp_tipos';
  protected $primaryKey = 'id_tipo';

  protected $fillable = [
    'id_tipo',
    'sts_tipo',
    'updated_at',
    'created_at',
    'tipo',
    'rota'
  ];
}
