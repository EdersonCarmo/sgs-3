<?php namespace App\Models\EMP;

use Illuminate\Database\Eloquent\Model;

class RamoM extends Model
{
  protected $table = 'emp_ramos';
  protected $primaryKey = 'id_ramo';

  protected $fillable = [
    'id_ramo',
    'ramo',
    'sts_ramo',
    'updated_at',
    'created_at',
    'rota',
    'desc'
  ];

  public function modulosramos()    {
    return $this->hasMany(Modulo_RamoM::class,'id_ramo','id_ramo');
  }
}
