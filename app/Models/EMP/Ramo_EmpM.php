<?php namespace App\Models\EMP;

use Illuminate\Database\Eloquent\Model;

class Ramo_EmpM extends Model
{
  protected $table = 'emp_ramo_emp';
  protected $primaryKey = 'id_ramo_emp';

  protected $fillable = [
    'id_ramo_emp',
    'id_ramo',
    'id_empresa',
    'created_at',
    'updated_at',
    'sts_ramo_emp'
  ];

  public function empresas()    {
    return $this->hasOne(EmpresaM::class,'id_empresa','id_empresa');
  }
  // public function atendente()    {
  //   return $this->hasOne('\App\User','id','id_atendente');
  // }
}
