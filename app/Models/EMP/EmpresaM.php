<?php namespace App\Models\EMP;

use Illuminate\Database\Eloquent\Model;

class EmpresaM extends Model
{
  protected $table = 'emp_empresas';
  protected $primaryKey = 'id_empresa';

  protected $fillable = [
    'id_empresa',
    'empresa',
    'created_at',
    'updated_at',
    'sts_empresa',
    'razao_social',
    'observacoes',
    'data_cadastro',
    'natureza_juridica',
    'tipo',
    'faturamento',
    'faixa_funcionario',
    'porte',
    'tempo_mercado',
    'ie',
    'site',
    'cnpj',
    'id_tipo'
  ];

  public function tipos()    {
    return $this->hasMany(TipoM::class);
  }

  public function ramos()    {
    return $this->hasMany(RamoM::class,'id_ramo','id_ramo');
  }

  public function ramos_emp()    {
    return $this->belongsToMany(Ramo_EmpM::class,'id_empresa','id_empresa');
  }
}
