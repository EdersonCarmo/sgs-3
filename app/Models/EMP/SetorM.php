<?php namespace App\Models\EMP;

use Illuminate\Database\Eloquent\Model;

class SetorM extends Model
{
  protected $table = 'emp_setores';
  protected $primaryKey = 'id_setor';

  protected $fillable = [
    'id_setor',
    'setor',
    'updated_at',
    'created_at',
    'sts_setor',
    'id_empresa'
  ];
}
