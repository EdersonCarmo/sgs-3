<?php namespace App\Models\TI;

use Illuminate\Database\Eloquent\Model;

class UpdateM extends Model
{
  protected $table = 'ti_updates';
  protected $primaryKey = 'id_update';

  protected $fillable = [
    'id_update',
    'desc',
    'created_at',
    'updated_at',
    'sts_update',
    'id_modulo',
    'id_versao',
    'id_solicitante',
    'id_programador',
    'arquivo',
    'tempo'
  ];

  public function versoes()    {
    return $this->hasOne(VersaoM::class,'id_versao','id_versao');
  }

  public function modulos()    {
    return $this->hasOne(ModuloM::class,'id_modulo','id_modulo');
  }
}
