<?php namespace App\Models\TI;

use Illuminate\Database\Eloquent\Model;

class VersaoM extends Model
{
  protected $table = 'ti_versoes';
  protected $primaryKey = 'id_versao';

  protected $fillable = [
    'id_versao',
    'versao',
    'sts_versao',
    'v1',
    'v2',
    'v3',
    'v4'
  ];
}
