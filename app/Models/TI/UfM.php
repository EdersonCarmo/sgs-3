<?php namespace App\Models\TI;

use Illuminate\Database\Eloquent\Model;

class UfM extends Model
{
  protected $table = 'ti_uf';
  protected $primaryKey = 'id_uf';

  protected $fillable = [
    'id_uf',
    'estado',
    'uf',
    'cod_uf',
    'pais',
    'updated_at',
    'created_at',
    'sts_uf'
  ];
}
