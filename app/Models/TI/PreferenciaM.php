<?php namespace App\Models\TI;

use Illuminate\Database\Eloquent\Model;

class PreferenciaM extends Model
{
  protected $table = 'ti_preferencias';
  protected $primaryKey = 'id_preferencia';

  protected $fillable = [
    'id_preferencia',
    'id_user',
    'sts_preferencia',
    'bg_color',
  ];
}
