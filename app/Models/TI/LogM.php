<?php namespace App\Models\TI;

use Illuminate\Database\Eloquent\Model;

class LogM extends Model
{
  protected $table = 'ti_logs';
  protected $primaryKey = 'id_log';

  protected $fillable = [
    'id_log',
    'id_user',
    'id_key',
    'rota',
    'acao',
    'key_old',
    'create_at',
    'updated_at',
    'sts_log'
  ];
}
