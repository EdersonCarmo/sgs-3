<?php namespace App\Models\TI;

use Illuminate\Database\Eloquent\Model;

class TipoM extends Model
{
  protected $table = 'ti_tipos';
  protected $primaryKey = 'id_ti_tipo';

  protected $fillable = [
    'id_ti_tipo',
    'ti_tipo',
    'id_modulo',
    'rota',
    'sts_ti_tipo',
    'created_at',
    'updated_at'
  ];
}
