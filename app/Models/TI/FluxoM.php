<?php namespace App\Models\TI;

use Illuminate\Database\Eloquent\Model;

class FluxoM extends Model
{
  protected $table = 'ti_fluxos';
  protected $primaryKey = 'id_fluxo';

  protected $fillable = [
    'id_fluxo',
    'id_atendente',
    'id_user',
    'fluxo',
    'rota',
    'id_item',
    'sts_fluxo',
    'created_at',
    'updated_at'
  ];

}
