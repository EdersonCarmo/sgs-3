<?php namespace App\Models\TI;

use Illuminate\Database\Eloquent\Model;

class ModuloM extends Model
{
  protected $table = 'ti_modulos';
  protected $primaryKey = 'id_modulo';

  protected $fillable = [
    'id_modulo',
    'modulo',
    'sts_modulo',
    'created_at',
    'updated_at',
    'id_versao',
    'modulo_id',
    'desc',
    'cod',
    'arquivo',
    'rota'
  ];

  public function versoes()    {
    return $this->hasOne(VersaoM::class,'id_versao');
  }

  public function permissoes()    {
    return $this->hasOne(AclM::class,'id_modulo','id_modulo');
  }

  public function modulos()    {
    return $this->hasOne(ModuloM::class,'modulo_id','id_modulo');
  }
}
