<?php namespace App\Models\TI;

use Illuminate\Database\Eloquent\Model;
use App\Models\HRM\DadoM;
use App\Models\HRM\ContratoM;
use App\Models\ADM\PessoaM;
use App\Models\ADM\EnderecoM;

class ArquivoM extends Model
{
  protected $table = 'ti_arquivos';
  protected $primaryKey = 'id_arquivo';

  protected $fillable = [
    'id_arquivo',
    'id_endereco',
    'id_dado',
    'id_contrato',
    'id_pessoa',
    'arquivo',
    'size',
    'url',
    'sts_arquivo',
    'created_at',
    'updated_at'
  ];

  public function dados()    {
    return $this->belongsTo(DadoM::class,'id_pessoa','id_pessoa');
  }

  public function end()    {
    return $this->belongsTo(EnderecoM::class,'id_pessoa','id_pessoa');
  }

  public function pessoa()    {
    return $this->belongsTo(PessoaM::class,'id_pessoa','id_pessoa');
  }

  public function contrato()    {
    return $this->belongsTo(ContratoM::class,'id_pessoa','id_pessoa');
  }
}
