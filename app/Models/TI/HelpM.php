<?php namespace App\Models\TI;

use Illuminate\Database\Eloquent\Model;

class HelpM extends Model
{
  protected $table = 'ti_helps';
  protected $primaryKey = 'id_help';

  protected $fillable = [
    'id_help',
    'id_solicitante',
    'id_atendente',
    'id_ti_tipo',
    'help',
    'prioridade',
    'arquivo',
    'solucao',
    'sts_help',
    'created_at',
    'updated_at'
  ];

  public function tipos()    {
    return $this->hasOne('App\Models\TI\TipoM','id_ti_tipo','id_ti_tipo');
  }
  public function solicitante()    {
    return $this->hasOne('\App\User','id','id_solicitante');
  }
  public function atendente()    {
    return $this->hasOne('\App\User','id','id_atendente');
  }
}
