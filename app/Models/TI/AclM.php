<?php namespace App\Models\TI;

use Illuminate\Database\Eloquent\Model;

class AclM extends Model
{
  protected $table = 'ti_permissoes';
  protected $primaryKey = 'id_permissao';

  protected $fillable = [
    'id_permissao',
    'permissao',
    'sts_permissao',
    'created_at',
    'updated_at',
    'id_user',
    'id_modulo',
    'fx',
    'p',
    'c',
    'r',
    'u',
    'd',
    'up',
    'dw',
    'f',
    'a',
    's'
  ];

  public function modulos()    {
    return $this->hasMany(ModuloM::class,'id_modulo','id_modulo');
  }

  public function modulo()    {
    return $this->hasOne(ModuloM::class,'id_modulo','id_modulo');
  }

  public function usuario()    {
    return $this->hasOne('\App\User','id','id_user');
  }
}
