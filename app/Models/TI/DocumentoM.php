<?php namespace App\Models\TI;

use Illuminate\Database\Eloquent\Model;

class DocumentoM extends Model
{
  protected $table = 'ti_documentos';
  protected $primaryKey = 'id_documento';
  protected $fillable = [
    'id_documento',
    'id_modulo',
    'id_ti_tipo',
    'documento',
    'texto',
    'created_at',
    'updated_at',
    'sts_documento'
  ];
}
