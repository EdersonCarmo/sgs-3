<?php namespace App\Models\ADM;

use Illuminate\Database\Eloquent\Model;

class OrcamentoM extends Model
{
  protected $table = 'adm_orcamentos';
  protected $primaryKey = 'id_orcamento';

  protected $fillable = [
    'id_orcamento',
    'created_at',
    'updated_at',
    'sts_orcamento',
    'fornecedor',
    'arquivo',
    'orcamento',
    'telefone',
    'email',
    'valor',
    'frete',
    'validade',
    'dt_orcamento',
    'id_compra'
  ];
}
