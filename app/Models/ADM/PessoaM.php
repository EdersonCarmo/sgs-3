<?php namespace App\Models\ADM;

use Illuminate\Database\Eloquent\Model;
use App\Models\HRM\DadoM;
use App\Models\ADM\ContatoM;
use App\Models\HRM\ColaboradorM;
use App\Models\ADM\EnderecoM;
use App\Models\TI\ArquivoM;

class PessoaM extends Model
{
  protected $table = 'adm_pessoas';
  protected $primaryKey = 'id_pessoa';
  protected $fillable = [
    'id_pessoa',
    'pessoa',
    'cpf',
    'created_at',
    'updated_at',
    'sts_pessoa',
    'id_tipo'
  ];

  public function contato()    {
    return $this->belongsTo(ContatoM::class,'id_pessoa','id_pessoa');
  }

  public function dados()    {
    return $this->belongsTo(DadoM::class,'id_pessoa','id_pessoa');
  }

  public function end()    {
    return $this->belongsTo(EnderecoM::class,'id_pessoa','id_pessoa');
  }

  public function colaborador()    {
    return $this->belongsTo(ColaboradorM::class,'id_pessoa','id_pessoa');
  }

  public function arq()    {
    return $this->belongsTo(ArquivoM::class,'id_pessoa','id_item')
                ->where('rota','pessoa')
                ->where('sts_arquivo','>',0);
  }
}
