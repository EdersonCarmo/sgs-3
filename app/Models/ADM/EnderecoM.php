<?php namespace App\Models\ADM;

use Illuminate\Database\Eloquent\Model;
use App\Models\ADM\CidadeM;
use App\Models\TI\ArquivoM;

class EnderecoM extends Model
{
  protected $table = 'ti_enderecos';
  protected $primaryKey = 'id_endereco';
  protected $fillable = [
    'id_endereco',
    'id_pessoa',
    'id_empresa',
    'id_cidade',
    'num',
    'complemento',
    'cep',
    'bairro',
    'logradouro',
    'updated_at',
    'created_at',
    'sts_endereco'
  ];

  public function cidade()    {
    return $this->hasOne(CidadeM::class,'id_cidade','id_cidade');
  }

  public function arq()    {
    return $this->belongsTo(ArquivoM::class,'id_endereco','id_item')
                ->where('rota','endereco')
                ->where('sts_arquivo','>',0);
  }
}
