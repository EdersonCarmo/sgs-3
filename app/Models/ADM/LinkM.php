<?php namespace App\Models\ADM;

use Illuminate\Database\Eloquent\Model;

class LinkM extends Model
{
  protected $table = 'adm_links';
  protected $primaryKey = 'id_link';

  protected $fillable = [
    'id_link','id_g_link','link','sts_link','url',
  ];

  public function glinks()    {
    return $this->hasMany(G_LinkM::class,'id_g_link','id_g_link');
  }
}
