<?php namespace App\Models\ADM;

use Illuminate\Database\Eloquent\Model;

class TipoM extends Model
{
  protected $table = 'adm_tipos';
  protected $primaryKey = 'id_adm_tipo';

  protected $fillable = [
    'id_adm_tipo',
    'adm_tipo',
    'updated_at',
    'created_at',
    'sts_adm_tipo',
    'tabela'
  ];
}
