<?php namespace App\Models\ADM;

use Illuminate\Database\Eloquent\Model;

class G_LinkM extends Model
{
  protected $table = 'adm_g_links';
  protected $primaryKey = 'id_g_link';

  protected $fillable = [
    'id_g_link','id_modulo', 'g_link', 'sts_g_link',
  ];

  public function links()    {
    return $this->hasMany(LinkM::class,'id_g_link','id_g_link');
  }
  public function modulos()    {
    return $this->hasOne('App\Models\TI\ModuloM','id_modulo','id_modulo');
  }
}
