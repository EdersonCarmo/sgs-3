<?php namespace App\Models\ADM;

use Illuminate\Database\Eloquent\Model;

class CompraM extends Model
{
  protected $table = 'adm_compras';
  protected $primaryKey = 'id_compra';

  protected $fillable = [
    'id_compra',
    'id_user',
    'id_colaborador',
    'id_aprovador',
    'id_setor',
    'compra',
    'dt_compra',
    'dt_entrega',
    'pagto',
    'valor',
    'sts_compra',
    'created_at',
    'updated_at'
  ];

  public function orcamentos()    {
    return $this->belongsToMany(OrcamentoM::class,'id_compra','id_compra');
  }
  public function setores()    {
    return $this->hasOne('App\Models\EMP_SetorM','id_setor','id_setor');
  }
}
