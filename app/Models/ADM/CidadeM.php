<?php namespace App\Models\ADM;

use Illuminate\Database\Eloquent\Model;
use App\Models\ADM\UfM;

class CidadeM extends Model
{
  protected $table = 'ti_cidades';
  protected $primaryKey = 'id_cidade';

  protected $fillable = [
    'id_cidade',
    'id_uf',
    'cidade',
    'cod_cidade',
    'km',
    'updated_at',
    'created_at',
    'sts_cidade'
  ];

  public function uf()    {
    return $this->hasOne(UfM::class,'id_uf','id_uf');
  }
}
