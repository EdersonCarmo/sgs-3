<?php namespace App\Models\ADM;

use Illuminate\Database\Eloquent\Model;
use App\Models\ADM\PessoaM;

class ContatoM extends Model
{
  protected $table = 'ti_contatos';
  protected $primaryKey = 'id_contato';
  protected $fillable = [
    'id_contato',
    'id_empresa',
    'id_pessoa',
    'contato',
    'fixo1',
    'fixo2',
    'cel1',
    'cel2',
    'email1',
    'email2',
    'whats',
    'sts_contato',
    'created_at',
    'updated_at'
  ];

  public function pessoa()    {
    return $this->hasOne(PessoaM::class,'id_pessoa','id_pessoa');
  }
}
