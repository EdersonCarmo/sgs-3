<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MRP_Pedido_ItemM extends Model
{
  protected $table = 'mrp_tipos';
  protected $primaryKey = ['id_produto','id_pedido'];

  protected $fillable = [
    'id_produto',
    'id_pedido',
    'id_kit',
    'updated_at',
    'created_at',
    'sts_pedido_item'
  ];
}
