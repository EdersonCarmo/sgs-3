<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MRP_EmprestimoM extends Model
{
  protected $table = 'mrp_emprestimos';
  protected $primaryKey = 'id_emprestimo';

  protected $fillable = [
    'id_emprestimo',
    'retirada',
    'entrega',
    'id_colaborador',
    'itens_id_produto',
    'itens_id_estoque',
    'updated_at',
    'created_at',
    'sts_emprestimo'
  ];
}
