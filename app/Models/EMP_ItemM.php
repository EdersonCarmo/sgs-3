<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EMP_ItemM extends Model
{
  protected $table = 'emp_itens';
  protected $primaryKey = 'id_emp_item';

  protected $fillable = [
    'id_emp_item',
    'emp_item_id',
    'id_inventario',
    'emp_item',
    'desk',
    'local',
    'chip',
    'operadora',
    'imei',
    'modelo',
    'validade',
    'peso',
    'cod',
    'marca',
    'updated_at',
    'created_at',
    'sts_emp_item',
    'id_setor'
  ];

  public function setores()    {
    return $this->hasOne(EMP_SetorM::class,'id_setor','id_setor');
  }

  public function inventarios()    {
    return $this->hasOne(EMP_SetorM::class,'id_inventario','id_inventario');
  }
}
