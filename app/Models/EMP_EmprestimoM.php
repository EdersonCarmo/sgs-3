<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EMP_EmprestimoM extends Model
{
  protected $table = 'emp_emprestimos';
  protected $primaryKey = 'id_emp_emprestimo';

  protected $fillable = [
    'id_emp_emprestimo',
    'id_item',
    'id_user',
    'id_colaborador',
    'id_autorizador',
    'emp_emprestimo',
    'inicio',
    'fim',
    'arquivo',
    'sts_emp_emprestimo',
    'updated_at',
    'created_at'
  ];

  public function colaborador()    {
    return $this->hasOne(HRM_ColaboradorM::class,'id_colaborador','id_colaborador');
  }

  public function autorizador()    {
    return $this->hasOne(HRM_ColaboradorM::class,'id_colaborador','id_autorizador');
  }

  public function itens()    {
    return $this->hasOne(EMP_ItemM::class,'id_emp_item','id_item');
  }
}
