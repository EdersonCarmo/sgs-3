<?php namespace App\Models\LOG;

use Illuminate\Database\Eloquent\Model;
use App\Models\HRM\ColaboradorM;

class MultaM extends Model
{
  protected $table = 'log_multas';
  protected $primaryKey = 'id_multa';

  protected $fillable = [
    'id_multa',
    'multa',
    'pontos',
    'dt_multa',
    'created_at',
    'updated_at',
    'sts_multa',
    'id_veiculo',
    'id_colaborador',
    'id_motorista',
    'valor',
    'dt_vencimento',
    'arquivo',
    'id_log_tipo'
  ];

  public function colaboradores()    {
    return $this->hasOne(ColaboradorM::class,'id_colaborador','id_motorista');
  }

  public function veiculos()    {
    return $this->hasOne(VeiculoM::class,'id_veiculo','id_veiculo');
  }

  public function tipos()    {
    return $this->hasOne(TipoM::class,'id_log_tipo','id_log_tipo');
  }
}
