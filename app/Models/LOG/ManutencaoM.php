<?php namespace App\Models\LOG;

use Illuminate\Database\Eloquent\Model;
use App\Models\HRM\ColaboradorM;
use App\Models\ADM\PessoaM;
use App\Models\EMP\EmpresaM;

class ManutencaoM extends Model
{
  protected $table = 'log_manutencoes';
  protected $primaryKey = 'id_manutencao';

  protected $fillable = [
    'id_manutencao',
    'id_veiculo',
    'id_colaborador',
    'id_motorista',
    'id_fornecedor',
    'id_log_tipo',
    'manutencao',
    'dt_inicio',
    'dt_fim',
    'arquivo',
    'valor',
    'odo',
    'sts_manutencao',
    'updated_at',
    'created_at'
  ];

  public function motoristas()    {
    return $this->hasOne(ColaboradorM::class,'id_colaborador','id_motorista');
  }

  public function pessoas()    {
    return $this->hasOne(PessoaM::class,'id_pessoa','id_motorista');
  }

  public function veiculos()    {
    return $this->hasOne(VeiculoM::class,'id_veiculo','id_veiculo');
  }

  public function empresas()    {
    return $this->hasOne(EmpresaM::class,'id_empresa','id_fornecedor');
  }

  public function tipos()    {
    return $this->hasOne(TipoM::class,'id_log_tipo','id_log_tipo');
  }
}
