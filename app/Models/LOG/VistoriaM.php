<?php namespace App\Models\LOG;

  use Illuminate\Database\Eloquent\Model;
  use App\Models\HRM\ColaboradorM;

class VistoriaM extends Model
{
  protected $table = 'log_vistorias';
  protected $primaryKey = 'id_vistoria';

  protected $fillable = [
    'id_vistoria',
    'created_at',
    'updated_at',
    'sts_vistoria',
    'id_veiculo',
    'id_vistoriador',
    'id_motorista',
    'dt_vistoria',
    'hr_vistoria',
    'vistoria',
    'pneu',
    'limpeza',
    'oleo',
    'obs_pneu',
    'obs_limpeza',
    'obs_oleo',
    'odo',
    'farolL',
    'farolR',
    'piscaL',
    'piscaR',
    'lanternaL',
    'lanternaR',
    'luzFreio',
    'luzPlaca',
    'buzina',
    'ar',
    'retrovisor',
    'retrovisorL',
    'retrovisorR',
    'hidraulica',
    'parabrisa',
    'fuidoFreio',
    'arrefecimento',
    'limpParabrisa',
    'vidroL',
    'vidroR',
    'vidroF',
    'vidroB',
    'bateria',
    'forroL',
    'forroR',
    'estofamento',
    'extintor',
    'triangulo',
    'manual',
    'chaveRoda',
    'estepe',
    'chaveReserva',
    'tapetes',
    'vidroEletrico',
    'documentos',
    'cinto',
    'seguro',
    'capota',
    'santoAntonio',
    'vl_bateria',
    'radio',
    'arquivo'
  ];

  public function motoristas()    {
    return $this->hasOne(ColaboradorM::class,'id_colaborador','id_motorista');
  }

  public function vistoriadores()    {
    return $this->hasOne(ColaboradorM::class,'id_colaborador','id_vistoriador');
  }

  public function veiculos()    {
    return $this->hasOne(VeiculoM::class,'id_veiculo','id_veiculo');
  }
}
