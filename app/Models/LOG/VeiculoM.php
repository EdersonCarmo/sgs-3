<?php namespace App\Models\LOG;

use Illuminate\Database\Eloquent\Model;

class VeiculoM extends Model
{
  protected $table = 'log_veiculos';
  protected $primaryKey = 'id_veiculo';

  protected $fillable = [
    'id_veiculo',
    'veiculo',
    'id_tipo',
    'updated_at',
    'created_at',
    'sts_veiculo',
    'id_empresa',
    'imobilizado',
    'modelo',
    'marca',
    'ano_fabricacao',
    'ano_modelo',
    'placa',
    'combustivel',
    'passageiros',
    'carga',
    'chassi',
    'cor',
    'arquivo'
  ];

  public function tipos()    {
    return $this->hasOne(TipoM::class,'id_log_tipo','id_tipo');
  }
}
