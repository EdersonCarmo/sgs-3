<?php namespace App\Models\LOG;

use Illuminate\Database\Eloquent\Model;
use App\Models\HRM\ColaboradorM;

class OcorrenciaM extends Model
{
  protected $table = 'log_ocorrencias';
  protected $primaryKey = 'id_ocorrencia';

  protected $fillable = [
    'id_ocorrencia',
    'ocorrencia',
    'tipo',
    'sts_ocorrencia',
    'dt_ocorrencia',
    'created_at',
    'updated_at',
    'id_veiculo',
    'id_motorista',
    'id_vistoria'
  ];

  public function colaboradores()    {
    return $this->hasOne(ColaboradorM::class,'id_colaborador','id_motorista');
  }

  public function veiculos()    {
    return $this->hasOne(VeiculoM::class,'id_veiculo','id_veiculo');
  }
}
