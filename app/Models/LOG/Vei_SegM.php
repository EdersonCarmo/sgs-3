<?php namespace App\Models\LOG;

use Illuminate\Database\Eloquent\Model;

class Vei_SegM extends Model
{
  protected $table = 'log_vei_seg';
  protected $primaryKey = 'id_veiculo';

  protected $fillable = [
    'id_veiculo',
    'id_documento',
    'created_at',
    'updated_at'
  ];

  public function veiculos()    {
    return $this->hasOne(VeiculoM::class,'id_veiculo','id_veiculo');
  }
}
