<?php namespace App\Models\LOG;

use Illuminate\Database\Eloquent\Model;

class TipoM extends Model
{
  protected $table = 'log_tipos';
  protected $primaryKey = 'id_log_tipo';

  protected $fillable = [
    'id_log_tipo',
    'id_modulo',
    'tipo',
    'created_at',
    'updated_at',
    'sts_tipo',
    'rota',
    'vencimento',
    'notifica'
  ];

  public function modulos()    {
    return $this->hasOne('App\Models\TI\ModuloM','id_modulo','id_modulo');
  }
}
