<?php namespace App\Models\LOG;

use Illuminate\Database\Eloquent\Model;
use App\Models\EMP\EmpresaM;
use App\Models\HRM\ColaboradorM;

class AbasteceM extends Model
{
  protected $table = 'log_abastecimentos';
  protected $primaryKey = 'id_abastecimento';

  protected $fillable = [
    'id_abastecimento',
    'abastecimento',
    'dt_abastecimento',
    'combustivel',
    'id_veiculo',
    'id_motorista',
    'litros',
    'valor',
    'id_colaborador',
    'dt_abastecimento',
    'odo',
    'updated_at',
    'created_at',
    'sts_abastecimento',
    'id_fornecedor',
    'arquivo'
  ];

  public function pessoas()    {
    return $this->hasOne(PessoaM::class,'id_pessoa','id_motorista');
  }

  public function motoristas()    {
    return $this->hasOne(ColaboradorM::class,'id_colaborador','id_motorista');
  }

  public function veiculos()    {
    return $this->hasOne(VeiculoM::class,'id_veiculo','id_veiculo');
  }

  public function empresas()    {
    return $this->hasOne(EmpresaM::class,'id_empresa','id_fornecedor');
  }
}
