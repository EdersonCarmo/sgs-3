<?php namespace App\Models\LOG;

use Illuminate\Database\Eloquent\Model;

class DocumentoM extends Model
{
  protected $table = 'log_documentos';
  protected $primaryKey = 'id_documento';

  protected $fillable = [
    'id_documento',
    'id_log_tipo',
    'vigencia',
    'dt_vencimento',
    'created_at',
    'updated_at',
    'sts_documento',
    'id_veiculo',
    'id_colaborador',
    'cod_documento',
    'arquivo'
  ];

  public function veiculos()    {
    return $this->belongsTo(VeiculoM::class,'id_veiculo','id_veiculo');
  }

  public function tipos()    {
    return $this->belongsTo(TipoM::class,'id_log_tipo','id_log_tipo');
  }
}
