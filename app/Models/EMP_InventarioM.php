<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EMP_InventarioM extends Model
{
  protected $table = 'emp_inventarios';
  protected $primaryKey = 'id_inventario';

  protected $fillable = [
    'id_inventario',
    'id_empresa',
    'inventario',
    'updated_at',
    'created_at',
    'sts_inventario'
  ];
}
