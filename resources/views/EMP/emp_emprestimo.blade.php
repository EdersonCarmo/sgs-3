@extends('layouts.sgs')

@section('content')
  @php
  $idC = $idI = $idA = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])){
    $idC = $rota['edit']->id_colaborador;
    $idA = $rota['edit']->id_autorizador;
    $idI = $rota['edit']->id_item;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form" enctype="multipart/form-data">
                  <input type="hidden" name="id_item" value="{{ $idI }}">
                  <input type="hidden" name="id_colaborador" value="{{ $idC }}">
                  <input type="hidden" name="id_autorizador" value="{{ $idA }}">
                  @method('PUT')
                @else
                  <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form" enctype="multipart/form-data">
                    <input type="hidden" name="id_user" value="{{ Auth::user()->id }}">
                  @endif
                  @csrf
                  <div class="row justify-content-between">
                    <div class="col-8">
                      <div class="form-group">
                        <label for="exampleInputName2">EMPRESTIMOS</label>
                      </div>
                    </div>
                    {{-- ACL FORM CAD --}}
                    <div class="col-1">
                      @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mudarestado('div-cad-{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                    </div>
                    {{-- ACL FORM CAD --}}
                  </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}">
                    <div class="row">

                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Item</span>
                          </div>
                          <div class="col-12 form-control">
                            <select class="js-example-basic-single" name="id_item" style="width: 200px" {{ isset($rota['edit']) ? "disabled":""}} required>
                              <option value=""></option>
                              @foreach ($rota['itens'] as $key)
                                <option value="{{ $key->id_emp_item }}" {{ $key->id_emp_item==$idI?"selected":"" }} title="{{ $key->emp_item }}">
                                  {{ substr($key->emp_item , 0, 20)}}
                                </option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>


                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Colaborador</span>
                          </div>
                          <div class="col-12 form-control">
                            <select class="js-example-basic-single" name="id_colaborador" style="width: 200px" {{ isset($rota['edit']) ? "disabled":""}} required>
                              <option value=""></option>
                              @foreach ($rota['colaboradores'] as $key)
                                <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador === $idC ? 'selected':''}} title="{{ $key->pessoas->pessoa }}">
                                  {{ substr($key->pessoas->pessoa , 0, 20)}}
                                </option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Autorizador</span>
                          </div>
                          <div class="col-12 form-control">
                            <select class="js-example-basic-single" name="id_autorizador" style="width: 200px" {{ isset($rota['edit']) ? "disabled":""}} required>
                              <option value=""></option>
                              @foreach ($rota['colaboradores'] as $key)
                                <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador === $idA ? 'selected':''}} title="{{ $key->pessoas->pessoa }}">
                                  {{ substr($key->pessoas->pessoa , 0, 20)}}
                                </option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>

                    </div>
                    <div class="row mt-2">

                    <div class="col-3">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-level-up-alt text-danger" title="Saída"></i></span>
                        </div>
                        <input type="date" name="inicio" class="form-control" value="{{ $rota['edit']->inicio or old('inicio') }}" required>
                      </div>
                    </div>

                    <div class="col-3">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-level-down-alt text-success" title="Entrada"></i></span>
                        </div>
                        <input type="date" name="fim" class="form-control" value="{{ $rota['edit']->fim or old('fim') }}">
                      </div>
                    </div>

                    <div class="col-6">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Descrição</span>
                        </div>
                        <input type="text" name="emp_emprestimo" class="form-control" oninput="this.value = this.value.toUpperCase()" value="{{ $rota['edit']->desk or old('desk') }}">
                      </div>
                    </div>

                    </div>
                    <div class="row mt-2">

                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Localização</span>
                          </div>
                          <input type="text" name="local" class="form-control" placeholder="00-00-A" oninput="this.value = this.value.toUpperCase()" value="{{ $rota['edit']->local or old('local') }}">
                        </div>
                      </div>
                      {{-- ACL UPLOAD --}}
                      @if ($acl->up == 1)
                        <div class="col-4">
                          <div class="input-group">
                            <input id="arquivo" type="file" name="arquivo" class="form-control" value="{{ $rota['edit']->arquivo or old('arquivo') }}">
                          </div>
                        </div>
                      @endif
                      {{-- ACL UPLOAD --}}

                      <div class="col-2">
                        <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                          {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                        </button>
                      </div>
                    </div>
                  </div>

                </form>
              </div>

              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper  dt-bootstrap4 no-footer mt-3">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Item
                      </th>
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Colaborador
                      </th>
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Autorizado
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending">
                        Ação
                      </th>
                    </tr>
                  </thead>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota[$rota['rota']] as $key)
                      @php
                        switch ($key->sts_emp_emprestimo) {
                          case '2': $cor = 'success'; $tex = 'Entrada';  $ico = 'down-alt'; break;
                          case '1': $cor = 'danger'; $tex = 'Saída';    $ico = 'up-alt'; break;
                          default:  $cor = 'dark';    $tex = 'Cancelado';   break;
                        }
                      @endphp
                      <tr>
                        <td><i class="fas fa-level-{{$ico}} text-{{$cor}}" title="{{$tex}}"></i> {{ $key[$rota['rota']] }}</div>
                        <td>{{ $key->colaborador->pessoas->pessoa}}</div>
                        <td>{{ $key->autorizador->pessoas->pessoa}}</div>
                      </td>
                      <td>
                        {{-- ACL ACOES TABLE --}}
                        <div class="row">

                            <div class="">
                              <a href="{{route($rota['rota'].'.show',$key->id_emp_emprestimo) }}" target="new" class="btn btn-block btn-ghost-primary" title="Termo de uso">
                                <i class="fas fa-print"></i>
                              </a>
                            </div>

                          @if (isset($key->arquivo))
                            @if ($acl->dw == 1)
                              <div class="">
                                <a href="{{ asset('storage/'.$rota['pasta'].$key->arquivo)}}" target="new" title="{{$key->arquivo}}" class="btn btn-block btn-ghost-success">
                                  <i class="far fa-file-pdf"></i>
                                </a>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-file-pdf"></i></div>
                            @endif
                          @else
                            <div class="btn text-dark"><i class="fas fa-ban"></i></div>
                          @endif

                          @if ($acl->u == 1)
                            <div class="">
                              <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                <i class="far fa-edit"></i>
                              </a>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                          @endif

                          @if ($acl->d == 1)
                            <div class="">
                              <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                  <i class="far fa-trash-alt"></i>
                                </button>
                              </form>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                          @endif
                        </div>
                        {{-- ACL ACOES TABLE --}}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
