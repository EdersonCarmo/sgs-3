@extends('layouts.sgs')

@section('content')
  @php
  $idU = $idT = $idC = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])) {
    $idU = $rota['edit']->id_und;
    $idT = $rota['edit']->id_setor;
    $idC = $rota['edit']->emp_item_id;
  }
  $print = ['id'=>$rota['emp_item']['0']['id_inventario'],'rota'=>$rota['rota'],'act'=>'print'];
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form">
                @method('PUT')
              @else
                <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form">
                  <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                  <input type="hidden" name="id_inventario" value="{{ $rota['inventario']->id_inventario }}">
                @endif
                @csrf
                <div class="row justify-content-between">
                  <div class="col-8">
                    <div class="form-group">
                      <label for="exampleInputName2">ITENS - {{ $rota['inventario']->inventario }}</label>
                    </div>
                  </div>

                  <div class="row col-2 justify-content-between">
                      <div class="">
                        @if ($acl->p == 1)
                        <a href="{{route('emp_item.show',$print) }}" target="new" class="btn btn-ghost-warning ml-2" title="Imprimir">
                          <i class="fas fa-print fa-2x"></i>
                        </a>
                      @endif
                      </div>
                    {{-- ACL FORM CAD --}}
                    @if ($acl->c == 1)
                      <button id="btn-plus" type="button" class="btn btn-lg btn-pill btn-outline-info ml-3" onclick="MudarestadoSeach('div-cad-{{$rota['rota']}}')" title="Filtros">
                        <i class="fa fa-search"></i>
                      </button>
                    @endif
                    {{-- ACL FORM CAD --}}
                  </div>
                </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                    <div class="row">

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Setor</span>
                          </div>
                          <select id="select1" name="id_setor" class="form-control">
                            <option value="">Selecione ...</option>
                            @foreach ($rota['setores'] as $key)
                                <option value="{{ $key->id_setor }}" {{ $key->id_setor === $idT ? 'selected':''}}>{{ $key->setor }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        @if ($rota['inventario']->inventario == "CHIP_LINHA")
                          <div class="col-3">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">Nº</span>
                              </div>
                              <input type="text" name="chip" class="form-control" value="{{ $rota['edit']->chip or old('emp_item') }}" oninput="this.value = this.value.toUpperCase()">
                            </div>
                          </div>

                          <div class="col-3">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">Operadora</span>
                              </div>
                              <input type="text" name="operadora" class="form-control" value="{{ $rota['edit']->operadora or old('operadora') }}" oninput="this.value = this.value.toUpperCase()">
                            </div>
                          </div>
                        @else
                          <div class="col-3">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">Nome</span>
                              </div>
                              <input type="text" name="emp_item" class="form-control" value="{{ $rota['edit']->emp_item or old('emp_item') }}" oninput="this.value = this.value.toUpperCase()">
                            </div>
                          </div>

                          <div class="col-3">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">Marca</span>
                              </div>
                              <input type="text" name="marca" class="form-control" value="{{ $rota['edit']->marca or old('marca') }}" oninput="this.value = this.value.toUpperCase()">
                            </div>
                          </div>
                        @endif

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">{{ $rota['inventario']->inventario == "CHIP_LINHA"?'ICCID':'Código' }}</span>
                          </div>
                          <input type="text" name="cod" class="form-control" value="{{ $rota['edit']->cod or old('cod') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>
                    </div>

                    @if ($rota['inventario']->inventario != "CHIP_LINHA")
                    <div class="row mt-2">
                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Modelo</span>
                          </div>
                          <input type="text" name="modelo" class="form-control" value="{{ $rota['edit']->modelo or old('modelo') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">CA</span>
                          </div>
                          <input type="text" name="ca" class="form-control" value="{{ $rota['edit']->ca or old('ca') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-2">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Peso</span>
                          </div>
                          <input type="text" name="peso" class="form-control" value="{{ $rota['edit']->peso or old('peso') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Validade</span>
                          </div>
                          <input type="date" name="validade" class="form-control" value="{{ $rota['edit']->validade or old('validade') }}">
                        </div>
                      </div>

                    </div>
                    @endif
                    <div class="row mt-2">
                      @if ($rota['inventario']->inventario == "CELULAR")
                        <div class="col-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Imei</span>
                            </div>
                            <input type="text" name="imei" class="form-control" value="{{ $rota['edit']->imei or old('imei') }}" oninput="this.value = this.value.toUpperCase()">
                          </div>
                        </div>

                        <div class="col-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Chip</span>
                            </div>
                            <select id="select1" name="emp_item_id" class="form-control">
                              <option value="">Selecione ...</option>
                              @foreach ($rota['chips'] as $key)
                                <option value="{{ $key->id_emp_item }}" {{ $key->id_emp_item === $idC ? 'selected':''}}>{{ mask($key->chip,'(##) #####-####') }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      @endif

                    {{-- </div>
                    <div class="row mt-2"> --}}

                      <div class="col-5">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Descrição</span>
                          </div>
                          <input type="text" name="desk" class="form-control" value="{{ $rota['edit']->desk or old('desk') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-1">
                        <button type="submit" class="btn btn-outline-success" id="submit">
                          {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                        </button>
                      </div>
                    </div>


                  </div>
                </form>
              </div>
              {{-- END FORM --}}
              {{-- START LIST --}}
              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 215px;">
                        Cód./ Item
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                          @if ($rota['inventario']->inventario == "CHIP_LINHA")
                            Operadora
                          @else
                            Modelo
                          @endif
                      </th>
                      @if ($rota['inventario']->inventario != "CHIP_LINHA")
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                          Marca
                        </th>
                      @endif
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                        Setor
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                </div>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota[$rota['rota']] as $key)
                      <tr>
                        <td>
                          @if ($rota['inventario']->inventario == "CHIP_LINHA")
                            {{ mask($key->chip,'(##) #####-####') }}
                          @else
                            {{ $key['cod'] }}_{{ $key[$rota['rota']] }}
                          @endif
                        </td>
                        <td>
                          @if ($rota['inventario']->inventario == "CHIP_LINHA")
                            {{ $key->operadora }}
                          @else
                            {{ $key->modelo }}
                          @endif
                        </td>
                        @if ($rota['inventario']->inventario != "CHIP_LINHA")
                          <td>
                            {{ $key->smarca }}
                          </td>
                        @endif
                        <td>
                          {{ $key->setores->setor }}
                          @if ($rota['inventario']->inventario != "CHIP_LINHA")
                            /{{ $key->smarca }}
                          @endif
                        </td>
                        <td>
                          {{-- ACL ACOES TABLE --}}
                          <div class="row">
                            @if ($acl->u == 1)
                              <div class="">
                                <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                  <i class="far fa-edit"></i>
                                </a>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                            @endif

                            @if ($acl->d == 1)
                              <div class="">
                                <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                    <i class="far fa-trash-alt"></i>
                                  </button>
                                </form>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                            @endif
                          </div>
                          {{-- ACL ACOES TABLE --}}
                        </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>

              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">DOCUMENTO</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>One fine body…</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                  </div>

                </div>

              </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
