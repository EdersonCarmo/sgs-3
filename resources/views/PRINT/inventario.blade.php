@extends('layouts.print')

@section('content')
  @php
    $it = 1; $cont = 0;
    // echo "<script>window.open('#', '_blank')</script>";
    echo "<script>window.print();</script>";
  @endphp
    <br>
    <div  style="font-family: Arial">
  {{-- HEADER PRINT --}}
  <div class="form-control">
  <div class="row">

    <div class="col-4">
      <img src="{{ asset('img/logo.png') }}" width="320" height="125" alt="">
    </div>
    <div class="col-8">
      <h2>SITUAÇÃO DE ESTOQUE SGS </h2>
      <h3>Nº ESTOQUE: {{$rota['item']['0']->id_estoque}}</h3>
      <div class="row">

      <div class="col-8">
        <strong></strong><br>
        <strong></strong><br>
        <strong></strong>
      </div>
    </div>
    </div>
  </div>
  </div>
  {{-- HEADER PRINT --}}
  {{-- BODY PRINT --}}
  <div class="form-control">
    <div class="row justify-content-between">
      <div class="col-12 text-white" style="background-color:#6688de">Filtros</div>
    </div>

    @php
      if($_SESSION['search']['produto']) echo 'Produto: '.$_SESSION['search']['produto'];
      if($_SESSION['search']['qnt_min']) echo 'Mínnimo: '.$_SESSION['search']['qnt_min'];
      if($_SESSION['search']['qnt_max']) echo 'Máximo: '.$_SESSION['search']['qnt_max'];
      if($_SESSION['search']['cod'])     echo 'Código: '.$_SESSION['search']['cod'];
      if($_SESSION['search']['marca'])   echo 'Marca: '.$_SESSION['search']['marca'];
      if($_SESSION['search']['modelo'])  echo 'Modelo: '.$_SESSION['search']['modelo'];
      if($_SESSION['search']['ent_ini']) echo 'Data inícial de entrada: '.$_SESSION['search']['ent_ini'];
      if($_SESSION['search']['ent_fim']) echo 'Data final de entrada: '.$_SESSION['search']['ent_fim'];
      if($_SESSION['search']['mov_ini']) echo 'Data inícial de movimentação: '.$_SESSION['search']['mov_ini'];
      if($_SESSION['search']['mov_fim']) echo 'Data inícial de movimentação: '.$_SESSION['search']['mov_fim'];
      // if(isset($_SESSION['search']['local'])){
      //   echo 'Localização(ões): ';
      //   foreach ($_SESSION['search']['local'] as $key) {
      //     echo $key.', ';
      //   }
      // }
      // if(isset($_SESSION['search']['id_mrp_tipo'])){
      //   echo 'Grupo(s): ';
      //   foreach ($_SESSION['search']['id_mrp_tipo'] as $key) {
      //     foreach ($rota['tipo_item'] as $key1) {
      //       if($key1->id_mrp_tipo == $key) echo $key1->mrp_tipo.', ';
      //     }
      //   }
      // }
    @endphp
  </div>
  <div class="form-control">
    <div class="row justify-content-between">
      <div class="col-1 text-white" style="background-color:#02918D">Itém</div>
      <div class="col-2 text-white" style="background-color:#02918D">Código</div>
      <div class="col-5 text-white" style="background-color:#02918D">Descrição</div>
      <div class="col-2 text-white" style="background-color:#02918D">Local</div>
      <div class="col-2 text-white" style="background-color:#02918D">QNT</div>

      @foreach ($rota['item'] as $key)
        @php
          $cont = $cont + $key->qnt;
        @endphp
        <div class="col-1 border">{{ $it++ }}</div>
        <div class="col-2 border">{{ $key->produtos->id_produto }}</div>
        <div class="col-5 border">{{ $key->produtos->produto }}</div>
        <div class="col-2 border">{{ $key->local }}</div>
        <div class="col-2 border">{{ str_replace('.', ',', $key->qnt) }}</div>
      @endforeach
    </div>
  </div>
  {{-- BODY PRINT --}}
  {{-- FOOTER PRINT --}}
  <div class="form-control">
    <div class="row">
      <div class="col-3">
      <div style="font-size: 10px">Impresso: {{ date('d/m/Y H:i')}}</div>
      </div>
      <div class="col-7">
      </div>
      <div class="col-2">
        Total: {{ str_replace('.', ',', $cont) }}
      </div>
    </div>
  </div>
  {{-- FOOTER PRINT --}}
</div>
  <style type="text/css">
  <!--
  html{
    overflow-y:auto;
    overflow-x:hidden;
  }
  -->
  </style>
@endsection
