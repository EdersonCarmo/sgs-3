@extends('layouts.print')

@section('content')
  @php
    // echo "<script>window.open('#', '_blank')</script>";
    echo "<script>window.print();</script>";
  @endphp
  <br>
  {{-- HEADER PRINT --}}
  <div class="form-control border-0">
  <div class="row">

    <div class="col-4">
      <img src="{{ asset('img/logo.png') }}" width="300" height="110" alt="">
    </div>
    <div class="col-8">
      <h4><strong>TERMO DE RESPONSABILIDADE DE APARELHO CELULAR CORPORATIVO</strong></h4>
      <h5>Nº PEDIDO: {{ $rota['print']->id_emp_emprestimo }}</h5>
      <div class="row">

      <div class="col-4">
        <h5>SOLICITANTE:</h5>
      </div>
      <div class="col-8">
        <strong>{{ $rota['print']->colaborador->pessoas->pessoa }}</strong><br>
      </div>
    </div>
    </div>
  </div>
  </div>
  {{-- HEADER PRINT --}}
  {{-- BODY PRINT --}}
  {{-- <div class="form-control">
    <div class="row justify-content-between">
      <div class="col-3 text-white" style="background-color:#02918D">Código</div>
      <div class="col-6 text-white" style="background-color:#02918D">Descrição</div>
      <div class="col-3 text-white" style="background-color:#02918D">QNT</div>

      @foreach ($rota[$rota['rota']] as $key)
        <div class="col-3 border">{{ $key->produtos->id_produto }}</div>
        <div class="col-6 border">{{ $key->produtos->produto }}</div>
        <div class="col-3 border">{{ str_replace('.', ',', $key->qnt) }}</div>
      @endforeach
    </div>
  </div> --}}
  {{-- BODY PRINT --}}
  {{-- FOOTER PRINT --}}

  <div class="form-control border-0" style="font-family: Arial">
    <br>
    A empresa SINERGIA PRESTADORA DE SERVIÇOS S/S LTDA EPP, situada na Rodovia SP 344, KM 2, Bairro: Jardim Recanto,
    inscrita no CNPJ sob o nº 09.316.476/0001-00 entrega neste ato, o SIMcard com linha corporativa: número:
    «ICCID»  - {{ mask($rota['print']->itens->chip,'(##) #####-####') }} ao funcionário <strong>{{ $rota['print']->colaborador->pessoas->pessoa }}</strong> portador do
    CPF sob o nº  {{ mask($rota['print']->colaborador->pessoas->cpf,'###.###.###-##') }} doravante denominado
    simplesmente "USUÁRIO" sob as seguintes condições:
    <br><br>
    1. O equipamento deverá ser utilizado <strong>ÚNICA e EXCLUSIVAMENTE</strong> a serviço da empresa tendo em vista a atividade a ser
    exercida pelo USUÁRIO;
    <br><br>
    2. Ficará o USUÁRIO responsável pelo uso e conservação do equipamento;
    <br><br>
    3. O USUÁRIO tem somente a DETENÇÃO, tendo em vista o uso exclusivo para prestação de serviços profissionais e
    NÃO a PROPRIEDADE do equipamento, sendo terminantemente proibido o empréstimo, aluguel ou cessão deste a terceiros;
    <br><br>
    4. Ao término da prestação de serviço ou do contrato individual de trabalho, o USUÁRIO compromete-se a devolver o
    equipamento em perfeito estado no mesmo dia em que for comunicado ou comunique seu desligamento, considerando o
    desgaste natural pelo uso normal do equipamento;
    <br><br>
    5. Fica proibido o uso do aparelho para envio de mensagens de texto, navegação em internet e ligações que não sejam
    em função da empresa. Ligações para outros números devem ser feitas de forma a cobrar. Caso contrário será cobrado o
    valor estipulado pela fatura no fim do mês.
    <br><br>
    6. Se o equipamento for danificado ou inutilizado por emprego inadequado, mau uso, negligência ou extravio, a empresa
    e cobrará o valor de um equipamento da mesma marca ou equivalente ao da praça. Valor este estipulado em ________.
    <br><br>
  </div>
  <div class="form-control border-0">
    <div class="text-center">
        <br>
        São João da Boa Vista, ____ de ______________ de 20_____
    </div>
  </div>
  </div>
  <div class="form-control border-0">
    <div class="row">
      <div class="col-6">
        <div class="text-center">
          <br>
          ASS: ________________________________________________
          <h6>Responsável de TI</h6>
        </div>
      </div>
      <div class="col-6">
        <div class="text-center">
          <br>
          ASS: ________________________________________________
          <h6>Declaro estar ciente e de acordo com as cláusulas acima.</h6>
        </div>
      </div>
    </div>
  </div>
  {{-- FOOTER PRINT --}}
@endsection
