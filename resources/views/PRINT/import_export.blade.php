@extends('layouts.sgs')

@section('content')
<main class="main">
  <div class="container">

    <div class="panel panel-primary">

      <div class="panel-heading">Laravel 5.5 - import export data into excel and csv using maatwebsite </div>

      <div class="panel-body">

        <div class="row">

          <div class="col-xs-12 col-sm-12 col-md-12">

            <a href="{{ route('export.file',['type'=>'xls']) }}">Download Excel xls</a> |

            <a href="{{ route('export.file',['type'=>'xlsx']) }}">Download Excel xlsx</a> |

            <a href="{{ route('export.file',['type'=>'csv']) }}">Download CSV</a>

          </div>

        </div>
        <form class="" action="{{ route('import.file')}}" method="post" enctype="multipart/form-data">


        <div class="row">

          <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

              {{-- {!! Form::label('sample_file','Select File to Import:',['class'=>'col-md-3']) !!} --}}
              <label for="sample_file">Selecione o arq</label>

              <div class="col-md-9">

                  <div class="input-group">
                    <input id="arquivo" type="file" name="sample_file" class="form-control" value="{{ $rota['edit']->arquivo or old('arquivo') }}">
                  </div>

                {{-- {!! Form::file('sample_file', array('class' => 'form-control')) !!} --}}

                {{-- {!! $errors->first('sample_file', '<p class="alert alert-danger">:message</p>') !!} --}}

              </div>

            </div>

          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" name="button">Enviar</button>
          </div>

        </div>

        </form>

      </div>

    </div>

  </div>
  </div>
@endsection
