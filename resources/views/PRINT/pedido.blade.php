@extends('layouts.print')

@section('content')
  @php
  // echo "<script>window.open('#', '_blank')</script>";
  echo "<script>window.print();</script>";
  @endphp
  <br>
  {{-- HEADER PRINT --}}
  <div  style="font-family: Arial">

    <div class="form-control">
      <div class="row">

        <div class="col-4">
          <img src="{{ asset('img/logo.png') }}" width="320" height="125" alt="">
        </div>
        <div class="col-8">
          <h2>REQUISIÇÃO DE MATÉRIAL SGS_3</h2>
          <h3>Nº PEDIDO: {{ $rota['input']['id'] }}</h3>
          <div class="row">

            <div class="col-2">
              <h6>SOLICITANTE:</h6>
              <h6>AUTORIZAÇÃO:</h6>
              <h6>ALMOXARIFE:</h6>
              <h6>FINALIDADE:</h6>
            </div>
            <div class="col-10">
              <strong>{{ $rota['pedido']->solicitantes->pessoas->pessoa }}</strong><br>
              <strong>{{ $rota['pedido']->autorizador->pessoas->pessoa }}</strong><br>
              <strong>{{ $rota['pedido']->colaboradores->pessoas->pessoa }}</strong><br>
              <strong>{{ $rota['pedido']->obra.'  '.$rota['pedido']->id_obra }}</strong>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- HEADER PRINT --}}
    {{-- BODY PRINT --}}
    <div class="form-control">
      <div class="row justify-content-between">
        <div class="col-2 text-white" style="background-color:#02918D">Código Interno</div>
        <div class="col-2 text-white" style="background-color:#02918D">Código Elektro</div>
        <div class="col-6 text-white" style="background-color:#02918D">Descrição</div>
        <div class="col-2 text-white" style="background-color:#02918D">QNT</div>

        @foreach ($rota[$rota['rota']] as $key)
          <div class="col-2 border">{{ $key->produtos->id_produto }}</div>
          <div class="col-2 border">{{ $key->produtos->cod }}</div>
          <div class="col-6 border">{{ $key->produtos->produto }}</div>
          <div class="col-2 border">{{ str_replace('.', ',', $key->qnt) }}</div>
        @endforeach
      </div>
    </div>
    {{-- BODY PRINT --}}
    {{-- FOOTER PRINT --}}
    <div class="form-control">
      <div class="row">
        <div class="col-3">
          <div class="">DATA: ___/___/_____</div>
          <div style="font-size: 10px">Impresso: {{ date('d/m/Y H:i')}}</div>
        </div>
        <div class="col-6">
          ASS: ____________________________________________
          <div style="font-size: 11px">Confirmo o recebimento dos itêns acima informados.</div>
        </div>
      </div>
    </div>
    {{-- FOOTER PRINT --}}
  </div>
@endsection
