@extends('layouts.print')

@section('content')
  @php
  echo "<script>window.print();</script>";
  $edit = $rota['print'];

  $idV = $edit->id_veiculo;
  $idM = $edit->id_motorista;
  $idF = $edit->id_vistoriador;
  $idP = $edit->pneu;
  $idL = $edit->limpeza;
  $idO = $edit->oleo;
  $idH = $edit->hidraulica;
  $idLP = $edit->parabrisa;
  $idFF = $edit->fuidoFreio;
  $idLA = $edit->arrefecimento;
  system("TZ=BRT date");
  @endphp
  <br>
  {{-- HEADER PRINT --}}
  <div  style="font-family: Arial">

    <div class="form-control">
      <div class="row">

        <div class="col-4">
          <img src="{{ asset('img/logo.png') }}" width="320" height="125" alt="">
        </div>
        <div class="col-8">
          <h2>CHECKLIST DE VEÍCULOS - SGS</h2>
          <h3>Nº VISTORIA: {{ $rota['print']->id_vistoria }}</h3>
          <div class="row">

            <div class="col-2">
              <h6>PLACA:</h6>
              <h6>MODELO:</h6>
              <h6>MOTORISTA:</h6>
              <h6>VISTORIADOR:</h6>
            </div>
            <div class="col-8">
              <strong>{{ $edit->veiculos->placa }}</strong><br>
              <strong>{{ $edit->veiculos->modelo }}</strong><br>
              <strong>{{ $edit->motoristas->pessoas->pessoa }}</strong><br>
              <strong>{{ $edit->vistoriadores->pessoas->pessoa }}</strong><br>
            </div>
          </div>
        </div>
      </div>
    </div>
    {{-- HEADER PRINT --}}
    {{-- BODY PRINT --}}
    <div class="form-control">
      <div class="row justify-content-between">
        <div class="col-4">

          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text text-p0"><div class="text-vert">Descrição</div></span>
            </div>
            <textarea name="vistoria" id="textarea-input" rows="6" class="form-control" disabled>{{ $edit->vistoria or old('vistoria') }}</textarea>
          </div>

          <div class="">


            <div class="row">
              <div class="input-group-prepend ml-3">
                Farol
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->farolR == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_1">Dir.</label>
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->farolL == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_2">Esq.</label>
              </div>
            </div>

            <div class="row">
              <div class="input-group-prepend ml-3">
                Pisca
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->piscaR == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_3">Dir.</label>
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->piscaL == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_4">Esq.</label>
              </div>
            </div>

            <div class="row">
              <div class="input-group-prepend ml-3">
                Lanterna
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->lanternaR == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_5">Dir.</label>
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->lanternaL == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_6">Esq.</label>
              </div>
            </div>

            <div class="row">
              <div class="input-group-prepend ml-3">
                Luz
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->luzFreio == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_7">Freio</label>
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->luzPlaca == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_8">Placa</label>
              </div>
            </div>

            <div class="row">
              <div class="input-group-prepend ml-3">
                Retrovisor
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->retrovisorR == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_9">Dir.</label>
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->retrovisorL == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_10">Esq.</label>
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->retrovisor == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_11">Interno</label>
              </div>
            </div>

            <div class="row">
              <div class="input-group-prepend ml-3">
                Vidros
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->vidroR == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_12">Dir.</label>
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->vidroL == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_13">Esq.</label>
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->vidroF == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_14">Diant.</label>
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->vidroB == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_15">Tras.</label>
              </div>
            </div>

            <div class="row">
              <div class="input-group-prepend ml-3">
                Forro das Portas
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->forroR == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_16">Dir.</label>
              </div>
              <div class="custom-control custom-checkbox ml-3">
                <input type="checkbox" class="custom-control-input" {{ $edit->forroL == 0?"":"checked" }} disabled>
                <label class="custom-control-label" for="CCV_35">Esq.</label>
              </div>
            </div>
          </div>

        </div>
        <div class="col-5">

          <div class="row">
            <div class="input-group">
              <div class="input-group-prepend ml-3">
                <span class="input-group-text">Data</span>
              </div>
              <input type="date" class="form-control col-5" value="{{ $edit->dt_vistoria }}" disabled>

              <div class="input-group-prepend ml-3">
                <span class="input-group-text">Hora</span>
              </div>
              <input type="time" class="form-control col-3" value="{{ $edit->hr_vistoria }}" disabled>
            </div>
          </div>
          <div class="input-group mt-3">
            <div class="input-group-prepend">
              <span class="input-group-text">Odo.:</span>
            </div>
            <input type="text" class="form-control" value="{{ $edit->odo }}" disabled>
          </div>

          <div class="input-group mt-3">
            <div class="input-group-prepend">
              <span class="input-group-text">Bateria</span>
            </div>
            <input type="date" class="form-control" value="{{ $edit->vl_bateria }}" disabled>
          </div>

          <div class="row">
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->ar == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_18">Ar</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->bateria == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_20">Bateria</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->buzina == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_17">Buzina</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->capota == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_32">Capota</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->chaveRoda == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_24">Chave de Roda</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->chaveReserva == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_26">Chave Reserva</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->cinto == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_30">Cintos</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->documentos == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_29">Documentos</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->estepe == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_25">Estepe</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->estofamento == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_40">Estofamento</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->extintor == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_21">Extintor</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->limpParabrisa == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_19">Limpador Parabrisa</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->manual == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_23">Manual</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->radio == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_34">Rádio</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->santoAntonio == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_33">Santo Antônio</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->seguro == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_31">Seguro</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->tapetes == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_27">Tapetes</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->triangulo == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_22">Triangulo</label>
            </div>
            <div class="custom-control custom-checkbox ml-3">
              <input type="checkbox" class="custom-control-input" {{ $edit->vidroEletrico == 0?"":"checked" }} disabled>
              <label class="custom-control-label" for="CCV_28">Vidro Elétrico</label>
            </div>
          </div>
        </div>
        <div class="col-3">
          <div class="row justify-content-between">
            <label for="customRange3">Nível do Pneu</label>{{$idP}}%
            <input type="range" class="col-3" value="{{ $idP }}" min="0" max="100" step="10" disabled>
          </div>
          <div class="row justify-content-between">
            <label for="customRange3">Nível Oléo Motor</label>{{$idO}}%
            <input type="range" class="col-3" value="{{ $idO }}" min="0" max="100" step="10" disabled>
          </div>
          <div class="row justify-content-between">
            <label for="customRange3">Nível de Hidráulica</label>{{$idH}}%
            <input type="range" class="col-3" value="{{ $idH }}" min="0" max="100" step="10" disabled>
          </div>
          <div class="row justify-content-between">
            <label for="customRange3">Nível limpador Parabrisa</label>{{$idLP}}%
            <input type="range" class="col-3" value="{{ $idLP }}" min="0" max="100" step="10" disabled>
          </div>
          <div class="row justify-content-between">
            <label for="customRange3">Nível Fluído Freio</label>{{$idFF}}%
            <input type="range" class="col-3" value="{{ $idFF }}" min="0" max="100" step="10" disabled>
          </div>
          <div class="row justify-content-between">
            <label for="customRange3">Nível Arrefecimento</label>{{$idLA}}%
            <input type="range" class="col-3" value="{{ $idLA }}" min="0" max="100" step="10" disabled>
          </div>
          <div class="row justify-content-between">
            <label for="customRange3">Limpeza</label>{{$idL}}%
            <input type="range" class="col-3" value="{{ $idL }}" min="0" max="100" step="10" disabled>
          </div>

        </div>

      </div>
    </div>
    {{-- BODY PRINT --}}
    {{-- FOOTER PRINT --}}
    <div class="form-control">
      <div class="row">
        <div class="col-3">
          <div class="">DATA: ___/___/_____</div>
          <div style="font-size: 10px">Impresso: {{ date('d/m/Y h:i')}}</div>
        </div>
        <div class="col-6">
          ASS: ____________________________________________
          <h6>Confirmo o as informações listasas acima.</h6>
        </div>
      </div>
    </div>
  </div>
  {{-- FOOTER PRINT --}}
@endsection
