<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@component('includes/head')
@endcomponent

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

  @component('includes/menu')
  @endcomponent

  @component('includes/menu_esq')
  @endcomponent

  @component('includes/principal')
  @endcomponent

  @component('includes/menu_dir')
  @endcomponent

  @component('includes/footer')
  @endcomponent

</body>
</html>
