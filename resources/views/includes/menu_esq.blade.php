
<div class="app-body">
  <div class="sidebar">
    <nav class="sidebar-nav">

      @if (Auth::id() == 1)
        @foreach ($_SESSION['menu'] as $key3)
          @if ($key3->sts_modulo > 1 AND $key3->rota != 'null')
            <a class="bg-secondary text-white" href="{{ route($key3->rota.'.index') }}">{{ $key3->modulo }}</a>
          @endif
        @endforeach        
      @else

        <ul class="nav">
          <li class="nav-title">MENU</li>

          @foreach ($_SESSION['menu'] as $key)
            @if ($key->permissoes->r == 1 AND $key->sts_modulo == 1)
              <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                  <i class="fas fa-ellipsis-v"></i> {{ strlen($key->modulo) > 12 ? $key->cod:$key->modulo }}
                </a>
                <ul class="nav-dropdown-items">
                  @foreach ($_SESSION['menu'] as $key1)
                    @if ($key1->modulo_id == $key->id_modulo && $key1->permissoes['r'] == 1)
                      <li class="nav-item">
                        <a class="nav-link" href="{{ $key1->rota == 'null'? '#': route($key1->rota.'.index') }}" onmouseover="MudaEstadoMenu('div-menuE-{{$key1->id_modulo}}')">{{ $key1->modulo }}</a>

                        @foreach ($_SESSION['menu'] as $key2)
                          @if ($key2->modulo_id == $key1->id_modulo && $key2->r == 1)

                            <div id="div-menuE-{{$key1->id_modulo}}" class="divClose">
                              @foreach ($_SESSION['menu'] as $key3)
                                @if ($key3->modulo_id == $key1->id_modulo && $key3->r == 1)
                                  <a class="dropdown-item bg-secondary" href="{{ route($key3->rota.'.index') }}">{{ $key3->modulo }}</a>
                                @endif
                              @endforeach
                            </div>

                          @endif
                        @endforeach

                      </li>
                    @endif
                  @endforeach
                </ul>
              </li>
            @endif
          @endforeach
        </ul>
      @endif

    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
  </div>
