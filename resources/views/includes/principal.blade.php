@extends('layouts.sgs')

@section('content')

<main class="main">
  <div class="container-fluid">
    <div class="animated fadeIn">

      <div class="row">

        <div class="col-md-12 mb-10">
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#fluxo" role="tab" aria-controls="fluxo">Fluxos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#ADM_Compras" role="tab" aria-controls="ADM_Compras">ADM_Compras</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#FIN_Lançamentos" role="tab" aria-controls="FIN_Lançamentos">FIN_Lançamentos</a>
            </li>
            <li class="nav-item">
            </li>
          </ul>

          <div class="tab-content">
            <!-- TABS -->
            <div class="tab-pane active" id="fluxo" role="tabpanel">

              @include('fluxos')

            </div>
            <!-- TABS -->
            <div class="tab-pane" id="ADM_Compras" role="tabpanel">

              @include('auth/cad_user')

            </div>
            <!-- TABS -->
            <div class="tab-pane" id="FIN_Lançamentos" role="tabpanel">

              @include('fin/lanca')

            </div>
            <!-- TABS -->
            <!--/.row-->
          </div>
        </div>

      </main>

      @endsection
