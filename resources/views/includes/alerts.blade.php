<!-- MESSAGE ALERT -->
<div>
  <!-- MESSAGE ALERT SUCCESS -->
  @if (session('success'))
    {{ TI_alerta("success",session('success')) }}
  @endif
  <!-- MESSAGE ALERT SUCCESS -->
  <!-- MESSAGE ALERT DANGER -->
  @if (session('danger'))
    {{ TI_alerta("error",session('danger')) }}
  @endif
  <!-- MESSAGE ALERT DANGER -->
  <!-- MESSAGE ALERT INFO -->
  @if (session('info'))
    {{ TI_alerta("info",session('info')) }}
  @endif
  <!-- MESSAGE ALERT INFO -->
  <!-- MESSAGE ALERT ALERTA -->
  @if (session('warning'))
    {{ TI_alerta("warning",session('warning')) }}
  @endif
  <!-- MESSAGE ALERT ALERTA -->
</div>
<!-- MESSAGE ALERT   -->
<!-- MESSAGE ALERT -->
{{-- <div>
  <!-- MESSAGE ALERT SUCCESS -->
  @if (session('success'))
    <script>setTimeout("document.getElementById('alerta-1').style.display = 'none'", 5000););</script>
    <p id="alerta-1" class="alert alert-success">{{ session('success') }}</p>
    @php
      echo "<script>toastr.info('OK sucesso.');</script>";
    @endphp
    <a class="btn btn-info" onblur="toastr.info('OK sucesso.');">Info message</a>
  @endif
  <!-- MESSAGE ALERT SUCCESS -->
  <!-- MESSAGE ALERT DANGER -->
  @if (session('danger'))
    <script>setTimeout("document.getElementById('alerta-1').style.display = 'none'", 5000);</script>
    <p id="alerta-1" class="alert alert-danger">{{ session('danger') }}</p>
  @endif
  <!-- MESSAGE ALERT DANGER -->
</div> --}}
<!-- MESSAGE ALERT   -->
