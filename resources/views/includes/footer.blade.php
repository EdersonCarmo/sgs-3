
<footer class="app-footer">
  <div>
    <a href="#">SGS v{{ $_SESSION['versao']->versao }}</a>
    <span>© {{ date("Y")}} Sistema de Gestão Sinergia.</span>
  </div>
  <div class="ml-auto">
    <span>Desenvolvido por</span>
    <a href="#">Sinergia-TI</a>
  </div>
</footer>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery.mask.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
{{-- <script src="{{ asset('js/tooltip.js') }}"></script> --}}
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendors/pace-progress/js/pace.min.js') }}"></script>
<script src="{{ asset('vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('vendors/coreui-pro/js/coreui.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/jquery.dataTables.js') }}" class="view-script"></script>
<script type="text/javascript" src="{{ asset('js/dataTables.bootstrap4.js') }}" class="view-script"></script>
<script type="text/javascript" src="{{ asset('js/datatables.js') }}" class="view-script"></script>
{{-- <script type="text/javascript" src="{{ asset('js/bootstrap-select.min.js') }}" class="view-script"></script> --}}

<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="{{ asset('bootstrap-dynamic-tabs/bootstrap-dynamic-tabs.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/abas.js') }}"></script>
{{-- ALERT --}}
<script type="text/javascript" src="{{ asset('vendors/toastr.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/toastr.js') }}"></script>

<script src="{{ asset('vendors/daterangepicker.js') }}"></script>
<script src="{{ asset('vendors/select2.min.js') }}"></script>
<script src="{{ asset('js/advanced-forms.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});

$('#ui-view').ajaxLoad();
$(document).ajaxComplete(function() {
  Pace.restart()
});

</script>
