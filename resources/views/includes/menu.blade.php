{{-- {{dd($_SESSION['teste'])}} --}}
<header class="app-header navbar" style="background-color:#02918D">
  <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="{{ route('home') }}">
    <img class="navbar-brand-full" src="{{ asset('img/logo.png') }}" width="89" height="25" alt="Sinergia">
    <img class="navbar-brand-minimized" src="{{ asset('img/favicon.png') }}" width="30" height="30" alt="Sinergia Logo">
  </a>

  <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
    <span class="navbar-toggler-icon"></span>
  </button>

  <ul class="nav navbar-nav d-md-down-none">

    @foreach ($_SESSION['menu'] as $key)
      @if ($key->permissoes->r == 1 AND $key->sts_modulo == 1)
        <div class="dropdown show">
          <li class="nav-item px-2">
            <button class="btn btn-link dropdown-toggle text-white" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" title="{{ $key->modulo }}">
              {{ strlen($key->modulo) > 12 ? $key->cod:$key->modulo }}
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 35px, 0px);">
              @foreach ($_SESSION['menu'] as $key1)
                @if ($key1->modulo_id == $key->id_modulo && $key1->permissoes['r'] == 1 && $key1->sts_modulo < 4)
                  <a class="dropdown-item" href="{{ $key1->rota == 'null'? '#': route($key1->rota.'.index') }}" onmouseover="MudaEstadoMenu('div-menu-{{$key1->id_modulo}}')">{{ $key1->modulo }}</a>

                  @foreach ($_SESSION['menu'] as $key2)
                    @if ($key2->modulo_id == $key1->id_modulo && $key2->permissoes['r'] == 1)

                      <div id="div-menu-{{$key1->id_modulo}}" class="divClose">
                        @foreach ($_SESSION['menu'] as $key3)
                          @if ($key3->modulo_id == $key1->id_modulo && $key3->permissoes['r'] == 1)
                            <div class="row">
                              <div class="col-9">
                                <a class="dropdown-item bg-secondary" href="{{ route($key3->rota.'.index') }}">{{ $key3->modulo }}</a>
                              </div>
                              <div class="col-3">
                                <a class="text-dark" target="new" href="{{ route($key3->rota.'.index') }}"><i class="fas fa-angle-double-right"></i></a>
                              </div>
                            </div>
                          @endif
                        @endforeach
                      </div>

                    @endif
                  @endforeach

                @endif
              @endforeach
            </div>
          </li>
        </div>
      @endif
    @endforeach

  </ul>
{{-- ALERT BARRA MENU --}}
  <ul class="nav navbar-nav ml-auto">
    <li class="nav-item dropdown d-md-down-none">
      <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        <i class="icon-bell"></i>
        <span class="badge badge-pill badge-danger">5</span>
      </a>
      <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">
        <div class="dropdown-header text-center">
          <strong>You have 5 notifications</strong>
        </div>
        <a class="dropdown-item" href="#">
          <i class="icon-user-follow text-success"></i> New user registered
        </a>
        <a class="dropdown-item" href="#">
          <i class="icon-user-unfollow text-danger"></i> User deleted
        </a>
        <a class="dropdown-item" href="#">
          <i class="icon-chart text-info"></i> Sales report is ready
        </a>
        <a class="dropdown-item" href="#">
          <i class="icon-basket-loaded text-primary"></i> New client
        </a>
        <a class="dropdown-item" href="#">
          <i class="icon-speedometer text-warning"></i> Server overloaded
        </a>
      </div>
    </li>
    {{-- ALERT BARRA MENU --}}
    </ul>
    <button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show">
      <img class="img-avatar" width="50" height="50" src="{{ asset('storage/TI/USER/'.Auth::user()->arquivo) }}" alt="{{ Auth::user()->email }}" title="{{ Auth::user()->name }}">
    </button>
    <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
      <img class="img-avatar" width="50" height="50" src="{{ asset('storage/TI/USER/'.Auth::user()->arquivo) }}" alt="{{ Auth::user()->email }}" title="{{ Auth::user()->name }}">
    </button>


  </header>
