@php
$sty = 'style-dark.css';
$name = Route::currentRouteName();
@endphp

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="SGS Sistema de Gestão Sinergia">
  <meta name="author" content="Ederson Carmo">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <title>SGS  {{ Request::segment(1).'.'.ucfirst(Request::segment(2))}}</title>
  <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon" />

  <link href="{{ asset('css/coreui-icons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/flag-icon.min.css')    }}" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
  <link href="https://coreui.io/demo/dark/vendors/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

  <link href="{{ asset('css/'.$sty) }}" rel="stylesheet">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('bootstrap-dynamic-tabs/bootstrap-dynamic-tabs.css') }}">
  {{-- ALERT --}}
  <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}"> 


  <link href="{{ asset('css/dataTables.bootstrap4.css') }}" rel="stylesheet">
  <link href="{{ asset('css/styleSGS.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/daterangepicker.min.css') }}">
  <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">

                    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" /> --}}
</head>
