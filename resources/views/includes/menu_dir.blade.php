
@php
$idT = 0;
if(isset($rota['edit'])){
  $idT = $rota['edit']->id_ti_tipo;
}
@endphp
<!-- MENU MENU MENU  -->
<aside class="aside-menu">
  <ul class="nav nav-tabs" role="tablist">
    {{-- <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#timeline" role="tab">
    <i class="icon-speech"></i>
  </a>
</li> --}}
<li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#links" role="tab" title="Links">
    <i class="fas fa-list"></i>
  </a>
</li>
<li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#settings" role="tab" title="Configurações">
    <i class="fas fa-cogs"></i>
  </a>
</li>
<li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#perfil" role="tab" title="Perfil">
    <i class="fas fa-user-cog"></i>
  </a>
</li>
<li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="{{ route('logout') }}" role="tab" title="Sair do SGS"
  onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
  <i class="fas fa-sign-out-alt"></i>
</a>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
</form>
</li>
</ul>

<div class="tab-content">

{{-- MENU LINKS --}}
    <div class="tab-pane active p-3" id="links" role="tabpanel">
      <h6>Links</h6>
      {{-- LINKS LIVRES --}}
      @foreach ($_SESSION['links'] as $key)
          @if ($key->id_modulo == null)
            <div class="message">
              <div class="row">
                <div class="col-5">
                  <a class="nav-link" data-toggle="tab" href="#links" role="tab" title="Editar">
                    <div class="text-truncate font-weight-bold">{{ $key->g_link }}</div>
                  </a>
                </div>
                <div class="col-7">
                  @foreach ($key->links as $key1)
                    <a href="{{ $key1->url }}"><small class="text-white">{{ $key1->link }}</small></a>
                  @endforeach
                </div>
              </div>
            </div>
            <hr>
          @endif
      {{-- LINKS PARA MODULOS --}}
        @foreach ($_SESSION['menu'] as $keyM)
          @if ($key->id_modulo == $keyM->id_modulo)
            <div class="message">
              <div class="row">
                <div class="col-5">
                  <a class="nav-link" data-toggle="tab" href="#links" role="tab" title="Editar">
                    <div class="text-truncate font-weight-bold">{{ $key->g_link }}</div>
                  </a>
                </div>
                <div class="col-7">
                  @foreach ($key->links as $key1)
                    <a href="{{ $key1->url }}"><small class="text-white">{{ $key1->link }}</small></a>
                  @endforeach
                </div>
              </div>
            </div>
            <hr>
          @endif
        @endforeach
      @endforeach
      {{-- <a class="nav-link" data-toggle="tab" href="#links" role="tab" title="Cadastrar links">
        <i class="icon-plus"></i>
      </a> --}}

    </div>
    {{-- MENU CONFIGURAÇÕES --}}
    <div class="tab-pane p-3" id="settings" role="tabpanel">
      {{-- <h6>Configurações</h6>
      <div class="aside-options">
        <div class="clearfix mt-4">
          <small>
            <b>Cor de fundo</b>
          </small>
          <label class="switch switch-label switch-pill switch-success switch-sm float-right">
            <input class="switch-input" type="checkbox" >
            <span class="switch-slider" data-checked="BR" data-unchecked="PT"></span>
          </label>
        </div>
        <div>
          <small class="text-muted">'BR' ativa o fundo banco.<br> 'PT' ativa o fundo preto.</small>
        </div>
      </div>
      <div class="aside-options">
        <div class="clearfix mt-3">
          <small>
            <b>Mostra menu lateral</b>
          </small>
          <label class="switch switch-label switch-pill switch-success switch-sm float-right">
            <input class="switch-input" type="checkbox" checked>
            <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
          </label>
        </div>
        <div>
          <small class="text-muted">Lorem ipsum dolor sit amet.</small>
        </div>
      </div>
      <div class="aside-options">
        <div class="clearfix mt-3">
          <small>
            <b>Option 3</b>
          </small>
          <label class="switch switch-label switch-pill switch-success switch-sm float-right">
            <input class="switch-input" type="checkbox">
            <span class="switch-slider" data-checked="On" data-unchecked="Off"></span>
          </label>
        </div>
      </div>
      <div class="aside-options">
        <div class="clearfix mt-3">
          <small>
            <b>Option 4</b>
          </small>
          <label class="switch switch-label switch-pill switch-success switch-sm float-right">
            <input class="switch-input" type="checkbox" checked="">
            <span class="switch-slider" data-checked="On" data-unchecked="Off"></span>
          </label>
        </div>
      </div>
      <hr> --}}
      <h6>HelpDesk</h6>
      <form action="{{ route('help.store') }}" method="post" class="form" enctype="multipart/form-data">
        <input type="hidden" name="id_solicitante" value="{{ Auth::user()->id }}">
        @csrf

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fa fa-commenting-o"></i></span>
            </div>
            <textarea id="textarea-input" name="help" rows="3" class="form-control">{{ $rota['edit']->help or '' }}</textarea>
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Tipo</span>
            </div>
            <select id="select1" name="id_ti_tipo" class="form-control">
              @foreach ($_SESSION['tipos'] as $key)
                <option value="{{ $key->id_ti_tipo }}" {{ $key->id_ti_tipo==$idT?"selected":"" }}>
                  {{ $key->ti_tipo }}
                </option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <input id="arquivo" type="file" name="arquivo" class="form-control" value="{{ $rota['edit']->arquivo or old('arquivo') }}">
          </div>
        </div>

        <button type="submit" class="btn btn-outline-success btn-block mt-3" id="submit">
          Abrir Chamado
        </button>

      </form>
      <hr>
      @foreach ($_SESSION['help'] as $key)
        @php
        switch ($key->sts_help) {
          case '0': $cor = 'dark';      $tx = 'Cancelado';  break;
          case '1': $cor = 'warning';   $tx = 'Pendente';   break;
          case '2': $cor = 'secondary'; $tx = 'Na fina';    break;
          case '3': $cor = 'info';      $tx = 'Em analise'; break;
          case '4': $cor = 'light';     $tx = 'Retronado';  break;
          case '5': $cor = 'success';   $tx = 'Finalizado'; break;
          default:  $cor = 'dark';      $tx = 'Erro';       break;
        }
        @endphp
        @if ($key->id_solicitante == Auth::user()->id)
          <div class="row">
            @if ($key->sts_help > 3)
              <a href="#" class="card-header-action btn-maximize text-success" data-toggle="collapse" data-target="#help-{{$key->id_help}}" aria-expanded="true">
                <i class="fas fa-search-plus ml-1"></i>
              </a>
            @else
              <i class="fas fa-search ml-2 mr-1"></i>
            @endif
              {{ $key->id_help }}_{{ substr($key->help, 0, 25) }}
              <i class="fas fa-tag text-{{$cor}}" title="{{$tx}}"></i><br>
          </div>
          @if ($key->sts_help > 3)
          <div class="card">
            <div class="card-body collapse" id="help-{{$key->id_help}}">
              {{$key->solucao}}
            </div>
          </div>
          @endif

        @endif
      @endforeach
    </div>
    {{-- MENU CONFIGURAÇÕES --}}
    {{-- MENU PERFIL --}}
    <div class="tab-pane p-3" id="perfil" role="tabpanel">
      <h6>Perfil: {{ Auth::user()->name }}</h6>

      <form action="{{ route('usuario.update', Auth::user()->id) }}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Nome</span>
            </div>
            <input type="text" name="name" class="form-control" value="{{ Auth::user()->name }}">
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Cel</span>
            </div>
            <input type="text" name="cel" class="form-control">
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Email</span>
            </div>
            <input type="email" name="email" class="form-control" value="{{ Auth::user()->email }}" disabled>
            <input type="hidden" name="email" class="form-control" value="{{ Auth::user()->email }}">
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Senha</span>
            </div>
            <input type="password" name="passwordOld" class="form-control" value="">
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Nova</span>
            </div>
            <input type="password" name="password" class="form-control" value="123456">
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Repete</span>
            </div>
            <input type="password" name="password2" class="form-control" value="123456">
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <input id="arquivo" type="file" name="arquivo" class="form-control" value="{{ $rota['edit']->arquivo or old('arquivo') }}">
          </div>
        </div>


        <button type="submit" class="btn btn-outline-success btn-block col-8 mt-3" id="submit">
          Salvar alterações
        </button>

      </form>

    </div>
    {{-- MENU PERFIL --}}
  </div>
</aside>
</div>
