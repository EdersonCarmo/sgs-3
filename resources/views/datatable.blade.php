@extends('layouts.sgs')

@section('content')
  <main class="main">
    <div class="container-fluid">
      <div class="animated fadeIn">

        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
          <div class="row">
            <div>
              <div class="row">
                <div class="col-12">
                  <table class="table table-striped table-sm table-hover datatable dataTable no-footer" id="DataTables_Table_0" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">

                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 215px;">
                        Username
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                        Date registered
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                        Role
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 88px;">
                        Status
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending" style="width: 191px;">
                        Actions
                      </th>
                    </tr>
                  </thead>
                  <tbody>

                    <tr role="row" class="odd">
                      <td class="sorting_1">Adam Alister</td>
                      <td>2012/01/21</td>
                      <td>Staff</td>
                      <td>
                        <span class="badge badge-success">Active</span>
                      </td>
                      <td>
                        <a class="btn btn-success" href="#">
                          <i class="fa fa-search-plus "></i>
                        </a>
                        <a class="btn btn-info" href="#">
                          <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-danger" href="#">
                          <i class="fa fa-trash-o "></i>
                        </a>
                      </td>
                    </tr>
                    <tr role="row" class="even">
                      <td class="sorting_1">Adinah Ralph</td>
                      <td>2012/06/01</td>
                      <td>Admin</td>
                      <td>
                        <span class="badge badge-dark">Inactive</span>
                      </td>
                      <td>
                        <a class="btn btn-success" href="#">
                          <i class="fa fa-search-plus "></i>
                        </a>
                        <a class="btn btn-info" href="#">
                          <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-danger" href="#">
                          <i class="fa fa-trash-o "></i>
                        </a>
                      </td>
                    </tr>
                    <tr role="row" class="odd">
                      <td class="sorting_1">Ajith Hristijan</td>
                      <td>2012/03/01</td>
                      <td>Member</td>
                      <td>
                        <span class="badge badge-warning">Pending</span>
                      </td>
                      <td>
                        <a class="btn btn-success" href="#">
                          <i class="fa fa-search-plus "></i>
                        </a>
                        <a class="btn btn-info" href="#">
                          <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-danger" href="#">
                          <i class="fa fa-trash-o "></i>
                        </a>
                      </td>
                    </tr>
                    <tr role="row" class="even">
                      <td class="sorting_1">Alphonse Ivo</td>
                      <td>2012/01/01</td>
                      <td>Member</td>
                      <td>
                        <span class="badge badge-success">Active</span>
                      </td>
                      <td>
                        <a class="btn btn-success" href="#">
                          <i class="fa fa-search-plus "></i>
                        </a>
                        <a class="btn btn-info" href="#">
                          <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-danger" href="#">
                          <i class="fa fa-trash-o "></i>
                        </a>
                      </td>
                    </tr>
                    <tr role="row" class="odd">
                      <td class="sorting_1">Anton Phunihel</td>
                      <td>2012/01/01</td>
                      <td>Member</td>
                      <td>
                        <span class="badge badge-success">Active</span>
                      </td>
                      <td>
                        <a class="btn btn-success" href="#">
                          <i class="fa fa-search-plus "></i>
                        </a>
                        <a class="btn btn-info" href="#">
                          <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-danger" href="#">
                          <i class="fa fa-trash-o "></i>
                        </a>
                      </td>
                    </tr>
                    <tr role="row" class="even">
                      <td class="sorting_1">Bao Gaspar</td>
                      <td>2012/01/01</td>
                      <td>Member</td>
                      <td>
                        <span class="badge badge-success">Active</span>
                      </td>
                      <td>
                        <a class="btn btn-success" href="#">
                          <i class="fa fa-search-plus "></i>
                        </a>
                        <a class="btn btn-info" href="#">
                          <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-danger" href="#">
                          <i class="fa fa-trash-o "></i>
                        </a>
                      </td>
                    </tr>
                    <tr role="row" class="odd">
                      <td class="sorting_1">Bernhard Shelah</td>
                      <td>2012/06/01</td>
                      <td>Admin</td>
                      <td>
                        <span class="badge badge-dark">Inactive</span>
                      </td>
                      <td>
                        <a class="btn btn-success" href="#">
                          <i class="fa fa-search-plus "></i>
                        </a>
                        <a class="btn btn-info" href="#">
                          <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-danger" href="#">
                          <i class="fa fa-trash-o "></i>
                        </a>
                      </td>
                    </tr>
                    <tr role="row" class="even">
                      <td class="sorting_1">Bünyamin Kasper</td>
                      <td>2012/08/23</td>
                      <td>Staff</td>
                      <td>
                        <span class="badge badge-danger">Banned</span>
                      </td>
                      <td>
                        <a class="btn btn-success" href="#">
                          <i class="fa fa-search-plus "></i>
                        </a>
                        <a class="btn btn-info" href="#">
                          <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-danger" href="#">
                          <i class="fa fa-trash-o "></i>
                        </a>
                      </td>
                    </tr>
                    <tr role="row" class="odd">
                      <td class="sorting_1">Carlito Roffe</td>
                      <td>2012/08/23</td>
                      <td>Staff</td>
                      <td>
                        <span class="badge badge-danger">Banned</span>
                      </td>
                      <td>
                        <a class="btn btn-success" href="#">
                          <i class="fa fa-search-plus "></i>
                        </a>
                        <a class="btn btn-info" href="#">
                          <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-danger" href="#">
                          <i class="fa fa-trash-o "></i>
                        </a>
                      </td>
                    </tr>
                    <tr role="row" class="even">
                      <td class="sorting_1">Chidubem Gottlob</td>
                      <td>2012/02/01</td>
                      <td>Staff</td>
                      <td>
                        <span class="badge badge-danger">Banned</span>
                      </td>
                      <td>
                        <a class="btn btn-success" href="#">
                          <i class="fa fa-search-plus "></i>
                        </a>
                        <a class="btn btn-info" href="#">
                          <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-danger" href="#">
                          <i class="fa fa-trash-o "></i>
                        </a>
                      </td>
                    </tr>
                    <tr role="row" class="even">
                      <td class="sorting_1">Chidubem1 Gottlob</td>
                      <td>2012/02/01</td>
                      <td>Staff</td>
                      <td>
                        <span class="badge badge-danger">Banned</span>
                      </td>
                      <td>
                        <a class="btn btn-success" href="#">
                          <i class="fa fa-search-plus "></i>
                        </a>
                        <a class="btn btn-info" href="#">
                          <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-danger" href="#">
                          <i class="fa fa-trash-o "></i>
                        </a>
                      </td>
                    </tr>
                    <tr role="row" class="even">
                      <td class="sorting_1">Chidubem2 Gottlob</td>
                      <td>2012/02/01</td>
                      <td>Staff</td>
                      <td>
                        <span class="badge badge-danger">Banned</span>
                      </td>
                      <td>
                        <a class="btn btn-success" href="#">
                          <i class="fa fa-search-plus "></i>
                        </a>
                        <a class="btn btn-info" href="#">
                          <i class="fa fa-edit "></i>
                        </a>
                        <a class="btn btn-danger" href="#">
                          <i class="fa fa-trash-o "></i>
                        </a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            </div>
            </div>
            </div>


          </main>

        @endsection
