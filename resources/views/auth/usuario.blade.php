@extends('layouts.sgs')

@section('content')
  @php
  $idCol = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])){
    $idCol = $rota['edit']->id_colaborador;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- START FORM --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id']) }}" method="post" class="form" enctype="multipart/form-data">
                  @method('PUT')
                @else
                  <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form" enctype="multipart/form-data">
                    <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                  @endif
                  @csrf
                  <div class="row">

                    <div class="col-9">
                      <div class="row justify-content-between">
                        <div class="col-8">
                          <div class="form-group">
                            <label for="exampleInputName2">CADASTRO DE USUÁRIOS </label>
                          </div>
                        </div>
                      </div>

                      <div class="row">

                        <div class="col-4">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Nome</span>
                            </div>
                              <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                               oninput="this.value = this.value.toUpperCase()" value="{{ $rota['edit']->name or old('name') }}" required autofocus>

                              @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('name') }}</strong>
                                </span>
                              @endif
                          </div>
                        </div>

                        <div class="col-4">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Senha</span>
                            </div>

                              <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                 value="{{ $rota['edit']->password or old('password') }}" name="password" required>

                              @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('password') }}</strong>
                                </span>
                              @endif
                          </div>
                        </div>

                        <div class="col-4">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">E-Mail</span>
                            </div>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $rota['edit']->email or old('email') }}" required>

                            @if ($errors->has('email'))
                              <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                              </span>
                            @endif
                          </div>
                        </div>

                      </div>

                      <div class="row mt-3">

                        <div class="col-5">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Colaborador</span>
                            </div>
                            <div class="col-12 form-control">
                              <select class="js-example-basic-single" name="id_colaborador" style="width: 220px">
                                <option value="" selected>Selecione ...</option>
                                @foreach ($rota['colaboradores'] as $key)
                                  <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador==$idCol?"selected":"" }}>
                                    {{ $key->pessoas->pessoa }}
                                  </option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>
                        {{-- ACL UPLOAD --}}
                        @if ($acl->up == 1)
                          <div class="col-5">
                            <div class="input-group">
                              <input id="arquivo" type="file" name="arquivo" class="form-control" value="{{ $rota['edit']->arquivo or old('arquivo') }}">
                            </div>
                          </div>
                        @endif
                        {{-- ACL UPLOAD --}}

                        <div class="col-2">
                          @if ($acl->c == 1)
                            <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                              {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                            </button>
                          @endif
                        </div>
                      </div>

                      <input type="hidden" name="id_tipo" value="1">
                    </div>

                    <div class="col-3">
                      <div class="card-body bg-dark">
                        @isset($rota['edit'])
                          <div class="h1 text-muted text-right mb-4">
                            <img src="{{ asset('storage/TI/USER/'.$rota['edit']->arquivo) }}" width="200" alt="">
                          </div>
                          <div class="progress progress-xs mt-3 mb-0">
                            <div class="progress-bar bg-success" role="progressbar" style="width: {{round($rota['edit2'])}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" title="{{round($rota['edit2'])}}%"></div>
                          </div>
                        @endisset
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              {{-- END FORM --}}
              {{-- START LIST --}}
              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 215px;">
                        Nome
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                        E-Mail
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota[$rota['rota']] as $key)
                      <tr>
                        <td>{{$key->name}}</td>
                        <td>{{$key->email}}</td>
                        <td>
                          {{-- ACL ACOES TABLE --}}
                          <div class="row">
                            @if (isset($key->arquivo))
                              @if ($acl->dw == 1)
                                <div class="">
                                  <a href="{{ asset('storage/'.$rota['pasta'].$key->arquivo)}}" target="new" title="{{$key->arquivo}}" class="btn btn-sm btn-ghost-success">
                                    <i class="far fa-file-image"></i>
                                  </a>
                                </div>
                              @else
                                <div class="btn btn-sm text-secondary"><i class="far fa-file-image"></i></div>
                              @endif
                            @else
                              <div class="btn btn-sm text-dark"><i class="fas fa-ban"></i></div>
                            @endif

                            @if ($acl->u == 1)
                              <div class="">
                                <a href="{{route($rota['rota'].'.edit',$key['id']) }}" class="btn btn-sm btn-ghost-warning" title="Editar">
                                  <i class="far fa-edit"></i>
                                </a>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                            @endif

                            @if ($acl->d == 1)
                              <div class="">
                                <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id']) }}">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-sm btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                    <i class="far fa-trash-alt"></i>
                                  </button>
                                </form>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                            @endif
                          </div>
                          {{-- ACL ACOES TABLE --}}
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>

            </div>
            {{-- END LIST --}}
          </div>

        </div>
      </div>
    </main>
  @endsection
