<!DOCTYPE html>
<!--
* @version v2.0.0
* Copyright (c) 2018
* Licensed under MIT
-->

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
  <meta name="author" content="Ederson Carmo">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <title>Login SGS</title>
  <!-- Icons-->
  <!-- Main styles for this application-->
  <link href="{{ asset('css/style-dark.css') }}" rel="stylesheet">
</head>
<body class="app flex-row align-items-center">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8 col-sm-12">
        <div class="card-group">
          <div class="card p-4" style="background-color:#02918D">
            <div class="card-body">
              <div class="input-group mb-3">
                <img src="{{ asset('img/logo_branco.png') }}" width="270" height="125" alt="">
              </div>

              <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                @csrf
                <div class="input-group mb-3">
                  {{-- <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="icon-user"></i>
                    </span>
                  </div> --}}
                  <div class="col-6">
                    <a class="text-white">E-Mail</a>
                  </div>
                  <div class="col-12">
                    <input id="email" type="email" class="col-12 form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                    name="email" value="{{ old('email') }}" placeholder="E-Mail" required autofocus>
                  </div>

                  @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                  @endif
                </div>

                <div class="input-group mb-4">
                  {{-- <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="fas fa-unlock-alt"></i>
                    </span>
                  </div> --}}
                  <div class="col-6">
                    <a class="text-white">Senha</a>
                  </div>
                  <div class="col-12">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                    name="password" placeholder="Senha" required>
                  </div>

                  @if ($errors->has('password'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                  @endif
                </div>

                <div class="row">
                  <div class="col-6 col-sm-6 ml-3">
                    <button type="submit" class="btn btn-white px-4" style="color:#02918D" >Entrar</button>
                  </div>

                </form>

                <div class="col-6 col-sm-12 text-right">
                  <a href="#" class="text-white">Recuperar senha?</a>
                </div>
              </div>
            </div>
          </div>

          <div class="card bg-white py-5 d-md-down-none" style="width:44%; color:#02918D">
            <div class="card-body text-center">
              <div>
                <h2>Sinergia-TI</h2>
                <p>Olá tudo bem, seja bem vindo ao novo SGS 3. Se estiver com dificuldade de acesso ou sem acesso ao sistema, clique no botão ajuda que iremos o mais rapidamente lhe retornar. Estamos sempre a disposição, obrigado até mais.</p>
                <button type="button" class="btn active mt-3" style="background-color:#02918D; color:#fff">Ajuda</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap and necessary plugins-->
</body>
</html>
