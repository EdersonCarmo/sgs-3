@extends('layouts.sgs')

@section('content')
  @php
  $idV = $idM = $idF = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])){
    $idV = $rota['edit']->id_veiculo;
    $idM = $rota['edit']->id_motorista;
    $idF = $rota['edit']->id_vistoriador;
    $idP = $rota['edit']->pneu;
    $idL = $rota['edit']->limpeza;
    $idO = $rota['edit']->oleo;
    $idH = $rota['edit']->hidraulica;
    $idLP = $rota['edit']->parabrisa;
    $idFF = $rota['edit']->fuidoFreio;
    $idLA = $rota['edit']->arrefecimento;
    $edit = $rota['edit'];
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- START FORM --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form" enctype="multipart/form-data">
                  @method('PUT')
                  <input type="hidden" name="id_veiculo" value="{{ $idV }}">
                  <input type="hidden" name="id_motorista" value="{{ $idM }}">
                  <input type="hidden" name="id_vistoriador" value="{{ $idF }}">
                @else
                  <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form" enctype="multipart/form-data">
                    <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                  @endif
                  @csrf
                  <div class="row justify-content-between">
                    <div class="col-8">
                      <div class="form-group">
                        <label for="exampleInputName2">VISTORIAS </label>
                      </div>
                    </div>
                    {{-- ACL FORM CAD --}}
                    <div class="col-1">
                      @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                    </div>
                    {{-- ACL FORM CAD --}}
                  </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                    <div class="row">
                      <div class="col-5">

                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Veículo</span>
                            </div>
                            <div class="col-12 form-control">
                              <select class="js-example-basic-single" name="id_veiculo" style="width: 280px" {{ isset($rota['edit']) ? "disabled":""}} required>
                                <option value="" selected>Selecione ...</option>
                                @foreach ($rota['veiculos'] as $key)
                                  <option value="{{ $key->id_veiculo }}" {{ $key->id_veiculo==$idV?"selected":"" }}>
                                    {{ strtoupper($key->placa).'_'.$key->veiculo }}
                                  </option>
                                @endforeach
                              </select>
                            </div>
                          </div>

                        <div class="input-group mt-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Vistoriador</span>
                          </div>
                          <div class="col-12 form-control">
                            <select class="js-example-basic-single" name="id_vistoriador" style="width: 300px" {{ isset($rota['edit']) ? "disabled":""}} required>
                              <option value="" selected>Selecione ...</option>
                              @foreach ($rota['motoristas'] as $key)
                                <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador==$idF?"selected":"" }}>
                                  {{ $key->pessoas['pessoa'] }}
                                </option>
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="input-group mt-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Motorista</span>
                          </div>
                          <div class="col-12 form-control">
                            <select class="js-example-basic-single" name="id_motorista" style="width: 280px" {{ isset($rota['edit']) ? "disabled":""}} required>
                              <option value="" selected>Selecione ...</option>
                              @foreach ($rota['motoristas'] as $key)
                                <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador==$idM?"selected":"" }}>
                                  {{ $key->pessoas['pessoa'] }}
                                </option>
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="mt-3 ml-3">
                        <div class="row">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Data</span>
                            </div>
                            <input name="dt_vistoria" type="date" class="form-control col-5" value="{{ $edit->dt_vistoria or old('dt_vistoria') }}">

                            <div class="input-group-prepend ml-3">
                              <span class="input-group-text">Hora</span>
                            </div>
                            <input name="hr_vistoria" type="time" class="form-control col-3" value="{{ $edit->hr_vistoria or old('hr_vistoria') }}">
                          </div>
                        </div>
                        </div>

                        <div class="input-group mt-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text text-p0"><div class="text-vert">Descrição</div></span>
                          </div>
                          <textarea name="vistoria" id="textarea-input" rows="6" class="form-control">{{ $edit->vistoria or old('vistoria') }}</textarea>
                        </div>

                        <div class="input-group mt-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Odo.:</span>
                          </div>
                          <input name="odo" type="text" class="form-control" value="{{ $edit->odo or old('odo') }}">
                        </div>

                        <div class="input-group mt-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Bateria</span>
                          </div>
                          <input name="vl_bateria" type="date" class="form-control" value="{{ $edit->vl_bateria or old('vl_bateria') }}">
                        </div>
                        {{-- ACL UPLOAD --}}
                        @if ($acl->up == 1)
                          <div class="mt-3">
                            <div class="input-group">
                              <input id="arquivo" type="file" name="arquivo" class="form-control" value="{{ $rota['edit']->arquivo or old('arquivo') }}">
                            </div>
                          </div>
                        @endif
                        {{-- ACL UPLOAD --}}
                      </div>

                      <div class="col-5">

                        <div class="row">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Farol</span>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="farolR" type="checkbox" class="custom-control-input" id="CCV_1" {{ isset($edit->farolR)?$edit->farolR == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_1">Dir.</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="farolL" type="checkbox" class="custom-control-input" id="CCV_2" {{ isset($edit->farolL)?$edit->farolL == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_2">Esq.</label>
                          </div>
                        </div>

                        <div class="row">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Pisca</span>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="piscaR" type="checkbox" class="custom-control-input" id="CCV_3" {{ isset($edit->piscaR)?$edit->piscaR == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_3">Dir.</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="piscaL" type="checkbox" class="custom-control-input" id="CCV_4" {{ isset($edit->piscaL)?$edit->piscaL == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_4">Esq.</label>
                          </div>
                        </div>

                        <div class="row">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Lanterna</span>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="lanternaR" type="checkbox" class="custom-control-input" id="CCV_5" {{ isset($edit->lanternaR)?$edit->lanternaR == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_5">Dir.</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="lanternaL" type="checkbox" class="custom-control-input" id="CCV_6" {{ isset($edit->lanternaL)?$edit->lanternaL == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_6">Esq.</label>
                          </div>
                        </div>

                        <div class="row">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Luz</span>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="luzFreio" type="checkbox" class="custom-control-input" id="CCV_7" {{ isset($edit->luzFreio)?$edit->luzFreio == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_7">Freio</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="luzPlaca" type="checkbox" class="custom-control-input" id="CCV_8" {{ isset($edit->luzPlaca)?$edit->luzPlaca == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_8">Placa</label>
                          </div>
                        </div>

                        <div class="row">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Retrovisor</span>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="retrovisorR" type="checkbox" class="custom-control-input" id="CCV_9" {{ isset($edit->retrovisorR)?$edit->retrovisorR == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_9">Dir.</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="retrovisorL" type="checkbox" class="custom-control-input" id="CCV_10" {{ isset($edit->retrovisorL)?$edit->retrovisorL == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_10">Esq.</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="retrovisor" type="checkbox" class="custom-control-input" id="CCV_11" {{ isset($edit->retrovisor)?$edit->retrovisor == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_11">Interno</label>
                          </div>
                        </div>

                        <div class="row">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Vidros</span>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="vidroR" type="checkbox" class="custom-control-input" id="CCV_12" {{ isset($edit->vidroR)?$edit->vidroR == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_12">Dir.</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="vidroL" type="checkbox" class="custom-control-input" id="CCV_13" {{ isset($edit->vidroL)?$edit->vidroL == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_13">Esq.</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="vidroF" type="checkbox" class="custom-control-input" id="CCV_14" {{ isset($edit->vidroF)?$edit->vidroF == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_14">Diant.</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="vidroB" type="checkbox" class="custom-control-input" id="CCV_15" {{ isset($edit->vidroB)?$edit->vidroB == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_15">Tras.</label>
                          </div>
                        </div>

                        <div class="row">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Forro das Portas</span>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="forroR" type="checkbox" class="custom-control-input" id="CCV_16" {{ isset($edit->forroR)?$edit->forroR == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_16">Dir.</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="forroL" type="checkbox" class="custom-control-input" id="CCV_35" {{ isset($edit->forroL)?$edit->forroL == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_35">Esq.</label>
                          </div>
                        </div>

                        <hr>

                        <div class="row">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Outros</span>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="ar" type="checkbox" class="custom-control-input" id="CCV_18" {{ isset($edit->ar)?$edit->ar == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_18">Ar</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="bateria" type="checkbox" class="custom-control-input" id="CCV_20" {{ isset($edit->bateria)?$edit->bateria == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_20">Bateria</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="buzina" type="checkbox" class="custom-control-input" id="CCV_17" {{ isset($edit->buzina)?$edit->buzina == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_17">Buzina</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="capota" type="checkbox" class="custom-control-input" id="CCV_32" {{ isset($edit->capota)?$edit->capota == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_32">Capota</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="chaveRoda" type="checkbox" class="custom-control-input" id="CCV_24" {{ isset($edit->chaveRoda)?$edit->chaveRoda == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_24">Chave de Roda</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="chaveReserva" type="checkbox" class="custom-control-input" id="CCV_26" {{ isset($edit->chaveReserva)?$edit->chaveReserva == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_26">Chave Reserva</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="cinto" type="checkbox" class="custom-control-input" id="CCV_30" {{ isset($edit->cinto)?$edit->cinto == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_30">Cintos</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="documentos" type="checkbox" class="custom-control-input" id="CCV_29" {{ isset($edit->documentos)?$edit->documentos == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_29">Documentos</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="estepe" type="checkbox" class="custom-control-input" id="CCV_25" {{ isset($edit->estepe)?$edit->estepe == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_25">Estepe</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="estofamento" type="checkbox" class="custom-control-input" id="CCV_40" {{ isset($edit->estofamento)?$edit->estofamento == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_40">Estofamento</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="extintor" type="checkbox" class="custom-control-input" id="CCV_21" {{ isset($edit->extintor)?$edit->extintor == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_21">Extintor</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="limpParabrisa" type="checkbox" class="custom-control-input" id="CCV_19" {{ isset($edit->limpParabrisa)?$edit->limpParabrisa == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_19">Limpador Parabrisa</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="manual" type="checkbox" class="custom-control-input" id="CCV_23" {{ isset($edit->manual)?$edit->manual == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_23">Manual</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="radio" type="checkbox" class="custom-control-input" id="CCV_34" {{ isset($edit->radio)?$edit->radio == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_34">Rádio</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="santoAntonio" type="checkbox" class="custom-control-input" id="CCV_33" {{ isset($edit->santoAntonio)?$edit->santoAntonio == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_33">Santo Antônio</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="seguro" type="checkbox" class="custom-control-input" id="CCV_31" {{ isset($edit->seguro)?$edit->seguro == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_31">Seguro</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="tapetes" type="checkbox" class="custom-control-input" id="CCV_27" {{ isset($edit->tapetes)?$edit->tapetes == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_27">Tapetes</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="triangulo" type="checkbox" class="custom-control-input" id="CCV_22" {{ isset($edit->triangulo)?$edit->triangulo == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_22">Triangulo</label>
                          </div>
                          <div class="custom-control custom-checkbox ml-3">
                            <input name="vidroEletrico" type="checkbox" class="custom-control-input" id="CCV_28" {{ isset($edit->vidroEletrico)?$edit->vidroEletrico == 0?"":"checked":"checked" }}>
                            <label class="custom-control-label" for="CCV_28">Vidro Elétrico</label>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-6">
                            <button type="submit" class="btn btn-block btn-outline-success mt-3" id="submit">
                              {{ isset($rota['edit'])?"Salvar Vistoria":"Cadastrar Vistoria"}}
                            </button>
                          </div>
                          <div class="col-6">
                            @if (!isset($rota['edit']))
                              <button type="submit" name="ocorrencia" class="btn btn-block btn-outline-success mt-3" id="submit" value="Ocorrência">
                                Vistoria + Ocorrência
                              </button>
                            @endif
                          </div>
                        </div>

                      </div>


                      <div class="col-2">

                        <label for="customRange3">Pneu</label>
                        <input name="pneu" type="range" class="custom-range" id="vol1" value="{{ $idP or 90 }}" min="0" max="100"
                        step="10" oninput="display.value=value" onchange="vol1.title=value" title="{{ $idP or 90 }}">

                        <label for="customRange3">Nível Oléo Motor</label>
                        <input name="oleo" type="range" class="custom-range" id="vol2" value="{{ $idO or 90 }}" min="0" max="100"
                        step="10" oninput="display.value=value" onchange="vol2.title=value" title="{{ $idO or 90 }}">

                        <label for="customRange3">Nível de Hidráulica</label>
                        <input name="hidraulica" type="range" class="custom-range" id="vol3" value="{{ $idH or 90 }}" min="0" max="100"
                        step="10" oninput="display.value=value" onchange="vol3.title=value" title="{{ $idH or 90 }}">

                        <label for="customRange3">Nível limpador Parabrisa</label>
                        <input name="parabrisa" type="range" class="custom-range" id="vol4" value="{{ $idLP or 90 }}" min="0" max="100"
                        step="10" oninput="display.value=value" onchange="vol4.title=value" title="{{ $idLP or 90 }}">

                        <label for="customRange3">Nível Fluído Freio</label>
                        <input name="fuidoFreio" type="range" class="custom-range" id="vol5" value="{{ $idFF or 90 }}" min="0" max="100"
                        step="10" oninput="display.value=value" onchange="vol5.title=value" title="{{ $idFF or 90 }}">

                        <label for="customRange3">Nível Líquido Arrefecimento</label>
                        <input name="arrefecimento" type="range" class="custom-range" id="vol6" value="{{ $idLA or 90 }}" min="0" max="100"
                        step="10" oninput="display.value=value" onchange="vol6.title=value" title="{{ $idLA or 90 }}">

                        <label for="customRange3">Limpeza</label>
                        <input name="limpeza" type="range" class="custom-range" id="vol7" value="{{ $idL or 90 }}" min="0" max="100"
                        step="10" oninput="display.value=value" onchange="vol7.title=value" title="{{ $idL or 90 }}">
                        {{-- <input type="text" width="10px" id="display" value="0" oninput="vol1.value=value" onchange="vol1.value=value"> --}}
                      </div>

                    </div>

                  </div>

                </form>
              </div>
              {{-- END FORM --}}
              {{-- START LIST --}}
              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        {{ ucfirst($rota['rota']) }}
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                        Placa / Veículo
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                        Itens / Níveis
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 130px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota[$rota['rota']] as $key)
                      @php
                        $s = LOG_sum_item_vistoria($key);
                      @endphp
                      <tr>
                        <td>
                          {{ date('d/m/Y', strtotime($key->dt_vistoria)) }}{{ date(' H:i', strtotime($key->hr_vistoria)) }}
                          <a href="#" class="card-header-action btn-maximize" data-toggle="collapse" data-target="#collapse-{{$key['id_'.$rota['rota']]}}" aria-expanded="true">
                            <i class="fas fa-search-plus"></i>
                          </a>
                        </div>

                        <div class="collapse close" id="collapse-{{$key['id_'.$rota['rota']]}}" >
                          <div class="text-white">
                            <p class="text-left text-list">
                              Vistoria: {{$key->vistoria}} <br>
                              Motorista: {{$key->motoristas->pessoas->pessoa}} <br>
                              Vistoriador: {{$key->vistoriadores->pessoas->pessoa}}
                            </p>
                          </div>
                        </div>
                      </td>
                      <td>{{ strtoupper($key->veiculos->placa) }}_{{ strtoupper($key->veiculos->veiculo) }}</td>
                      <td>
                        <span class="badge badge-{{ $s['itens'] == $s['nItens'] ?'success':'danger' }}">
                          {{ $s['itens'].'/'.$s['nItens'] }}
                        </span> /
                        <span class="badge badge-{{ $s['niveis']>=$s['nNiveis']*0.9?'success':'danger' }}">
                          {{ round($s['niveis']/$s['nNiveis']*100) }}%
                        </span>
                      </td>
                      <td>
                        {{-- ACL ACOES TABLE --}}
                        <div class="row">
                          {{-- ARQ PDF --}}
                          @if (isset($key->arquivo))
                            @if ($acl->dw == 1)
                              <div class="">
                                <a href="{{ asset('storage/LOG/VIS/'.$key->veiculos->placa.'/'.$key->arquivo)}}" target="new" title="{{$key->arquivo}}" class="btn btn-block btn-ghost-success">
                                  <i class="far fa-file-pdf"></i>
                                </a>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-file-pdf"></i></div>
                            @endif
                          @else
                            <div class="btn text-dark"><i class="fas fa-ban"></i></div>
                          @endif
                          {{-- PRINT --}}
                          @if ($acl->dw == 1)
                            <div class="">
                              <a href="{{route('vistoria.show',$key->id_vistoria) }}" target="new" class="btn btn-block btn-ghost-primary" title="Imprimir vistoria">
                                <i class="fas fa-print"></i>
                              </a>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-file-pdf"></i></div>
                          @endif
                          {{-- OCORRENCIA --}}
                          {{-- @if ($acl->u == 1)
                            <div class="">
                              <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-info" title="Ocorrência">
                                <i class="far fa-edit"></i>
                              </a>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                          @endif --}}
                          {{-- EDIT --}}
                          @if ($acl->u == 1)
                            <div class="">
                              <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                <i class="far fa-edit"></i>
                              </a>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                          @endif
                          {{-- DELETE --}}
                          @if ($acl->d == 1)
                            <div class="">
                              <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                  <i class="far fa-trash-alt"></i>
                                </button>
                              </form>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                          @endif
                        </div>
                        {{-- ACL ACOES TABLE --}}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>

          </div>
          {{-- END LIST --}}
        </div>

      </div>
    </div>
  </main>
@endsection
