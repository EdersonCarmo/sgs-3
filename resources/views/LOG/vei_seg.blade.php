@extends('layouts.sgs')

@section('content')
  @php
  $idV = $idT = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])) {
    $idV = $rota['edit']->id_veiculo;
    $idT = $rota['edit']->id_logTipo;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}"
                method="post" class="form" enctype="multipart/form-data">
                @method('PUT')
              @else
                <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form" enctype="multipart/form-data">
                  <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                  <input type="hidden" name="id_documento" value="{{ $rota['doc']}}">
                @endif
                @csrf
                <div class="row justify-content-between">
                  <div class="col-8">
                    <div class="form-group">
                      <label for="exampleInputName2">DOCUMENTO: {{ $rota['doc'] }} VEÍCULOS </label>
                    </div>
                  </div>
                  {{-- ACL FORM CAD --}}
                    @if ($acl->c == 1)
                  {{-- ACL FORM CAD --}}
                </div>

                  <div>

                    <div class="row">

                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Veículo</span>
                          </div>
                          <div class="col-12 form-control">
                            <select class="js-example-basic-single" name="id_veiculo" style="width: 200px" {{ isset($rota['edit']) ? "disabled":""}} required>
                              <option value="" selected>Selecione ...</option>
                              @foreach ($rota['veiculos'] as $key)
                                <option value="{{ $key->id_veiculo }}" {{ $key->id_veiculo==$idV?"selected":"" }}>
                                  {{ strtoupper($key->placa).'_'.$key->veiculo }}
                                </option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>

                      <button type="submit" class="btn btn-outline-success" id="submit">
                        Adicionar
                      </button>
                    </div>
                  @endif

                  </div>
                </form>
              </div>
              {{-- END FORM --}}
              {{-- START LIST --}}
              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                        Veículo
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                        Vigência
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota[$rota['rota']] as $key)
                      @php
                        $del = ['id_documento'=>$key->id_documento,'id_veiculo'=>$key->id_veiculo]
                      @endphp
                      <tr>
                      <td>{{ $key->veiculos->placa }}</td>
                      <td>{{ $key->vigencia }}</td>
                      <td>
                        {{-- ACL ACOES TABLE --}}
                        <div class="row">
                          @if ($acl->d == 1)
                            <div class="">
                              <form method="POST" action="{{route($rota['rota'].'.destroy',$key->id_veiculo) }}">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="id_documento" value="{{ $rota['doc']}}">
                                <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                  <i class="far fa-trash-alt"></i>
                                </button>
                              </form>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                          @endif
                        </div>
                        {{-- ACL ACOES TABLE --}}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>

              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">DOCUMENTO</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>One fine body…</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                  </div>

                </div>

              </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
