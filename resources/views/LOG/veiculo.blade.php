@extends('layouts.sgs')

@section('content')
  @php
  $idC = $idT = $idE = NULL;
  $acl = $rota['acl'];
  if(isset($rota['edit'])){
    $idC = $rota['edit']->combustivel;
    $idT = $rota['edit']->id_tipo;
    $idE = $rota['edit']->id_empresa;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">
          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form" enctype="multipart/form-data">
                  @method('PUT')
                @else
                  <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form" enctype="multipart/form-data">
                  @endif
                  @csrf
                  <div class="row justify-content-between">
                    <div class="col-8">
                      <div class="form-group">
                        <label for="exampleInputName2">CADASTRO DE VEÍCULOS</label>
                      </div>
                    </div>
                    {{-- ACL FORM CAD --}}
                    <div class="col-1">
                      @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                    </div>
                    {{-- ACL FORM CAD --}}
                  </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                    <div class="row">
                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Veículo</span>
                          </div>
                          <input type="text" name="veiculo" class="form-control" value="{{ $rota['edit']->veiculo or old('veiculo') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Modelo</span>
                          </div>
                          <input type="text" name="modelo" class="form-control" value="{{ $rota['edit']->modelo or old('modelo') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Marca</span>
                          </div>
                          <input type="text" name="marca" class="form-control" value="{{ $rota['edit']->marca or old('marca') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Fabricação</span>
                          </div>
                          <input type="number" min="1990" max="{{ date('Y') + 1 }}" name="ano_fabricacao" class="form-control" value="{{ $rota['edit']->ano_fabricacao or old('ano_fabricacao') }}">
                        </div>
                      </div>
                    </div>

                    <div class="row mt-3">

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Ano Modelo</span>
                          </div>
                          <input type="number" min="1990" max="{{ date('Y') + 1 }}" name="ano_modelo" class="form-control" value="{{ $rota['edit']->ano_modelo or old('ano_modelo') }}">
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Placa</span>
                          </div>
                          <input type="text" name="placa" class="form-control" value="{{ $rota['edit']->placa or old('placa') }}" oninput="this.value = this.value.toUpperCase()" required>
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Combustível</span>
                          </div>
                          <select id="select1" name="combustivel" class="form-control">
                            <option value="0"{{ $idC=='0'?"selected":"" }}>ETANOL</option>
                            <option value="1"{{ $idC=='1'?"selected":"" }}>GASOLINA</option>
                            <option value="2"{{ $idC=='2'?"selected":"" }}>DIESEL</option>
                            <option value="3"{{ $idC=='3'?"selected":"" }}>GNV</option>
                            <option value="4"{{ $idC=='4'?"selected":"" }}>FLEX</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">N° passageiros</span>
                          </div>
                          <input type="number" min="1" max="48" name="passageiros" class="form-control" value="{{ $rota['edit']->passageiros or old('passageiros') }}">
                        </div>
                      </div>
                    </div>

                    <div class="row mt-3">

                    <div class="col-3">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Tipo</span>
                        </div>
                        <select id="select1" name="id_tipo" class="form-control">
                          <option value="1">Selecione ...</option>
                          @foreach ($rota['tipos'] as $key)
                            <option value="{{ $key->id_logTipo }}" {{ $key->id_logTipo==$idT?"selected":"" }}>
                              {{ $key->tipo }}
                            </option>
                          @endforeach
                        </select>
                      </div>
                    </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Carga KG</span>
                          </div>
                          <input type="number" min="100" max="10000000" name="carga" class="form-control" value="{{ $rota['edit']->carga or old('carga') }}">
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Chassi</span>
                          </div>
                          <input type="text" name="chassi" class="form-control" value="{{ $rota['edit']->chassi or old('chassi') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Cor</span>
                          </div>
                          <input type="text" name="cor" class="form-control" value="{{ $rota['edit']->cor or old('cor') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      </div>
                      <div class="row mt-3">

                        <div class="col-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Empresa</span>
                            </div>
                            <div class="col-12 form-control">
                              <select class="js-example-basic-single" name="id_empresa" style="width: 200px" {{ isset($rota['edit']) ? "disabled":""}} required>
                                <option value="" selected>Selecione ...</option>
                                @foreach ($rota['empresas'] as $key)
                                  <option value="{{ $key->id_empresa }}" {{ $key->id_empresa == $idE ? 'selected':''}} >
                                    {{ $key->empresa }}
                                  </option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>

                      {{-- ACL UPLOAD --}}
                      @if ($acl->up == 1)
                        <div class="col-5">
                          <div class="input-group">
                            <input id="arquivo" type="file" name="arquivo" class="form-control" value="{{ $rota['edit']->arquivo or old('arquivo') }}">
                          </div>
                        </div>
                      @endif
                      {{-- ACL UPLOAD --}}

                      <div class="col-3">
                        <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                          {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                        </button>
                      </div>
                    </div>

                  </div>
                </form>
              </div>
              {{-- END FORM --}}
              {{-- START LIST --}}
              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" aria-sort="ascending">
                        Veículo
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" >
                        Placa
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" >
                        Marca
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" style="width: 79px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                {{-- LIST --}}
                <tbody>
                  @foreach ($rota[$rota['rota']] as $key)
                    <tr>
                      <td>
                        {{ $key[$rota['rota']] }}
                        <a href="#" class="card-header-action btn-maximize" data-toggle="collapse" data-target="#coll-{{$key->id_veiculo}},#coll-{{$key->id_veiculo}}-1,#coll-{{$key->id_veiculo}}-2" aria-expanded="true">
                          <i class="fas fa-search-plus"></i>
                        </a>
                      </div>

                      <div class="collapse close col-12" id="coll-{{$key->id_veiculo}}" >
                        <div class="text-white">
                          <h6>
                            Modelo: {{ $key->modelo }} <br>
                            Tipo: {{ $key->tipos->tipo }} <br>
                            Passageiros: {{ $key->passageiros }}
                          </h6>
                        </div>
                      </div>

                    </td>
                    <td>
                      {{ strtoupper($key->placa) }}
                      @php
                        if($key->id_tipo == 3) echo '<i class="fas fa-truck"></i>';
                        if($key->id_tipo == 4) echo '<i class="fas fa-truck"></i>';
                        if($key->id_tipo == 5) echo '<i class="fa fa-car" aria-hidden="true"></i>';
                      @endphp

                      <div class="collapse close col-12" id="coll-{{$key->id_veiculo}}-1" >
                        <div class="text-white">
                          <h6>
                            Fabricação: {{ $key->ano_fabricacao }} <br>
                            Ano Modelo: {{ $key->ano_modelo }} <br>
                            Combustível:
                            @php
                            switch ($key->combustivel) {
                              case '0': echo 'Etanol'; break;
                              case '1': echo 'Gasolina'; break;
                              case '2': echo 'Diesel'; break;
                              case '3': echo 'GNV'; break;
                              default: echo 'Erro';  break;
                            }
                            @endphp
                          </h6>
                        </div>
                      </div>

                    </td>
                    <td>
                      {{ $key->marca }}

                      <div class="collapse close col-12" id="coll-{{$key->id_veiculo}}-2" >
                        <div class="text-white">
                          <h6>
                            Carga: {{ number_format($key->carga/1000, 2, ',', ' ') }} Ton<br>
                            Chassi: {{ $key->chassi }}
                          </h6>
                        </div>
                      </div>

                    </td>
                    <td>
                      {{-- ACL ACOES TABLE --}}
                      <div class="row">
                        @if (isset($key->arquivo))
                          @if ($acl->dw == 1)
                            <div class="">
                              <a href="{{ asset('storage/'.$rota['pasta'].$key->arquivo)}}" target="new" title="{{$key->arquivo}}" class="btn btn-block btn-ghost-success">
                                <i class="far fa-file-pdf"></i>
                              </a>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-file-pdf"></i></div>
                          @endif
                        @else
                          <div class="btn text-dark"><i class="fas fa-ban"></i></div>
                        @endif

                        @if ($acl->u == 1)
                          <div class="">
                            <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                              <i class="far fa-edit"></i>
                            </a>
                          </div>
                        @else
                          <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                        @endif

                        @if ($acl->d == 1)
                          <div class="">
                            <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                <i class="far fa-trash-alt"></i>
                              </button>
                            </form>
                          </div>
                        @else
                          <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                        @endif
                      </div>
                      {{-- ACL ACOES TABLE --}}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          </div>

</div>
</div>

</div>
</div>
</main>
@endsection
