@extends('layouts.sgs')

@section('content')
  @php
    $idV = $idM = $idF = $idC = 0;
    $acl = $rota['acl'];
    if(isset($rota['edit'])){
      $idV = $rota['edit']->id_veiculo;
      $idM = $rota['edit']->id_motorista;
      $idF = $rota['edit']->id_fornecedor;
      $idC = $rota['edit']->combustivel;
    }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- START FORM --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form" enctype="multipart/form-data">
                  @method('PUT')
                  <input type="hidden" name="id_veiculo" value="{{ $idV }}">
              @else
                <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form" enctype="multipart/form-data">
                <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
              @endif
              @csrf

              <div class="row justify-content-between">
                <div class="col-8">
                  <div class="form-group">
                    <label for="exampleInputName2">{{ strtoupper($rota['rota']) }} </label>
                  </div>
                </div>
                {{-- ACL FORM CAD --}}
                <div class="col-1">
                  @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                </div>
                {{-- ACL FORM CAD --}}
              </div>

              <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                  <div class="row">

                    <div class="col-3">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Veículo</span>
                        </div>
                        <div class="col-12 form-control">
                          <select class="js-example-basic-single" name="id_veiculo" style="width: 200px" {{ isset($rota['edit']) ? "disabled":""}} required>
                            <option value="" selected>Selecione ...</option>
                            @foreach ($rota['veiculos'] as $key)
                              <option value="{{ $key->id_veiculo }}" {{ $key->id_veiculo==$idV?"selected":"" }}>
                                {{ strtoupper($key->placa).'_'.$key->veiculo }}
                              </option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-4">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Motorista</span>
                        </div>
                        <div class="col-12 form-control">
                          <select class="js-example-basic-single" name="id_motorista" style="width: 260px" {{ isset($rota['edit']) ? "disabled":""}} required>
                            <option value="" selected>Selecione ...</option>
                            @foreach ($rota['motoristas'] as $key)
                              @if ($key->pessoa->dados->cnh != NULL)
                                <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador==$idM?"selected":"" }}>
                                  {{ $key->pessoas['pessoa'] }}
                                </option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="col-5">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Fornecedor</span>
                        </div>
                        <div class="col-12 form-control">
                          <select class="js-example-basic-single" name="id_fornecedor" style="width: 300px" {{ isset($rota['edit']) ? "disabled":""}} required>
                            <option value="" selected>Selecione ...</option>
                            @foreach ($rota['fornecedores'] as $key)
                              <option value="{{ $key->id_empresa }}" {{ $key->id_empresa==$idF?"selected":"" }}>
                                {{ $key->empresa }}
                              </option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>

                  </div>

                  <div class="row mt-3">
                    <div class="col-4">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Combustível</span>
                        </div>
                        <select id="select1" name="combustivel" class="form-control">
                          <option>Selecione ...</option>
                          <option value="0"{{ 0==$idC?"selected":"" }}>ETANOL</option>
                          <option value="1"{{ 1==$idC?"selected":"" }}>GASOLINA</option>
                          <option value="2"{{ 2==$idC?"selected":"" }}>DIESEL</option>
                          <option value="3"{{ 3==$idC?"selected":"" }}>GNV</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-5">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Data</span>
                        </div>
                        <input type="date" name="dt_abastecimento" class="form-control" value="{{ $rota['edit']->dt_abastecimento or old('dt_abastecimento') }}">
                      </div>
                    </div>

                    <div class="col-3">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Litros</span>
                        </div>
                        <input type="number" step="0.01" min="1" max="300" name="litros" class="form-control" value="{{ $rota['edit']->litros or old('litros') }}">
                      </div>
                    </div>
                  </div>

                  <div class="row mt-3">

                    <div class="col-4">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Odometro</span>
                        </div>
                        <input type="number" name="odo" class="form-control" value="{{ $rota['edit']->odo or old('odo') }}">
                      </div>
                    </div>
                    {{-- ACL UPLOAD --}}
                    @if ($acl->up == 1)
                      <div class="col-5">
                        <div class="input-group">
                          <input id="arquivo" type="file" name="arquivo" class="form-control" value="{{ $rota['edit']->arquivo or old('arquivo') }}">
                        </div>
                      </div>
                    @endif
                    {{-- ACL UPLOAD --}}

                    <div class="col-3">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Valor</span>
                        </div>
                        <input type="number" step="0.01" name="valor" class="form-control" value="{{ $rota['edit']->valor or old('valor') }}">
                      </div>
                    </div>

                    <div class="col-3 mt-2">
                      <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                        {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                      </button>
                    </div>
                  </div>

                    <input type="hidden" name="id_tipo" value="1">
                  </div>
                </form>
              </div>
              {{-- END FORM --}}
              {{-- START LIST --}}
              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 265px;">
                        Placa
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 164px;">
                        Valor
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 154px;">
                        Litros
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota[$rota['rota']] as $key)
                      <tr>
                        <td>
                          {{ strtoupper($key->veiculos->placa) }}
                          <a href="#" class="card-header-action btn-maximize" data-toggle="collapse" data-target="#coll-{{$key['id_'.$rota['rota']]}}, #coll-{{$key['id_'.$rota['rota']]}}-1, #coll-{{$key['id_'.$rota['rota']]}}-2" aria-expanded="true">
                            <i class="fas fa-search-plus"></i>
                          </a>
                        </div>

                        <div class="collapse close col-12" id="coll-{{$key['id_'.$rota['rota']]}}" >
                          <div class="text-white">
                            <h6>
                              Motorista: {{$key->motoristas->pessoa->pessoa}} <br>
                              Posto: {{$key->empresas->empresa}} <br>
                            </h6>
                          </div>
                        </div>

                      </td>
                        <td>
                          R$ {{ number_format($key->valor, 2, ',', '.') }}
                          <div class="collapse close col-12" id="coll-{{$key['id_'.$rota['rota']]}}-1" >
                            <div class="text-white">
                              <h6>
                                Combustível:
                                @php
                                  switch ($key->combustivel) {
                                    case '0': echo 'Etanol'; break;
                                    case '1': echo 'Gasolina'; break;
                                    case '2': echo 'Diesel'; break;
                                    case '3': echo 'GNV'; break;
                                    default: echo 'Erro';  break;
                                  }
                                @endphp <br>
                                Odometro: {{$key->odo}} <br>
                              </h6>
                            </div>
                          </div>
                        </td>
                        <td>
                          {{ number_format($key->litros, 3, ',', '.') }}
                          <div class="collapse close col-12" id="coll-{{$key['id_'.$rota['rota']]}}-2" >
                            <div class="text-white">
                              <h6>
                                Data: {{ date('d/m/Y', strtotime($key->dt_abastecimento)) }} <br>
                                Veículo: {{$key->veiculos->veiculo}} <br>
                              </h6>
                            </div>
                          </div>
                        </td>
                        <td>
                          {{-- ACL ACOES TABLE --}}
                          <div class="row">
                            @if (isset($key->arquivo))
                              @if ($acl->dw == 1)
                                <div class="">
                                  <a href="{{ asset('storage/LOG/ABA/'.$key->veiculos->placa.'/'.$key->arquivo)}}" target="new" title="{{$key->arquivo}}" class="btn btn-block btn-ghost-success">
                                    <i class="far fa-file-pdf"></i>
                                  </a>
                                </div>
                              @else
                                <div class="btn text-secondary"><i class="far fa-file-pdf"></i></div>
                              @endif
                            @else
                              <div class="btn text-dark"><i class="fas fa-ban"></i></div>
                            @endif

                            @if ($acl->u == 1)
                              <div class="">
                                <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                  <i class="far fa-edit"></i>
                                </a>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                            @endif

                            @if ($acl->d == 1)
                              <div class="">
                                <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                    <i class="far fa-trash-alt"></i>
                                  </button>
                                </form>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                            @endif
                          </div>
                          {{-- ACL ACOES TABLE --}}
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              </div>

            </div>
            {{-- END LIST --}}
          </div>

        </div>
      </div>
    </main>
  @endsection
