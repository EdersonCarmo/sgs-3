@extends('layouts.sgs')

@section('content')
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">
          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form">
                  @method('PUT')
              @else
                <form action="{{ route($rota['rota'].'.store')}}" method="post" class="form-inline">
              @endif
                @csrf
                <div class="form-group">
                  <label for="exampleInputName2">CADASTRO DE {{ strtoupper($rota['rota']) }} </label>
                    <input name="{{$rota['rota']}}" type="text" class="form-control mr-2 ml-2" placeholder=""
                    value="{{ $rota['edit'][$rota['rota']] or old($rota['rota']) }}">
                </div>
                <div class="form-group">
                  <button id="btn-plus" type="submit" class="btn btn-outline-success" id="submit">
                    {{ isset($rota['edit'])?"Alterar":"Cadastrar"}}</button>
                </div>

                <input type="hidden" name="id_tipo" value="1">
              </form>
            </div>
            <div class="car-body">



              {{-- DATATABLE --}}
              <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer mt-3">

                <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                <thead>
                  <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 215px;">
                      {{ ucfirst($rota['rota']) }}
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                      Status
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                      Ação
                    </th>
                  </tr>
                </thead>
                {{-- LIST --}}
                <tbody>
                  @foreach ($rota[$rota['rota']] as $key)
                    <tr>
                      <td>{{ $key[$rota['rota']] }}</td>
                      <td>
                        <span class="badge badge-{{ $key->tipo=1?'success':'secondary' }}">
                          {{ $key->tipo=1?'ATIVO':'INATIVO' }}
                        </span>
                      </td>
                      <td>
                        <div class="row">
                          <div class="col-6">
                            <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-outline-warning" title="Editar">
                              <i class="far fa-edit"></i>
                            </a>
                          </div>
                          <div class="col-6">
                            <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class="btn btn-block btn-outline-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                <i class="far fa-trash-alt"></i>
                              </button>
                            </form>
                          </div>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
