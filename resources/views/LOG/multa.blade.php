@extends('layouts.sgs')

@section('content')
  @php
  $idV = $idM = $idF = $idT = 0;
  $dt_tipo = 'datetime-local';
  $acl = $rota['acl'];
  if(isset($rota['edit'])){
    $idV = $rota['edit']->id_veiculo;
    $idM = $rota['edit']->id_motorista;
    $idF = $rota['edit']->id_fornecedor;
    $idT = $rota['edit']->id_logTipo;
    $dt_tipo = 'datetime';
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form" enctype="multipart/form-data">
                  @method('PUT')
                @else
                  <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form" enctype="multipart/form-data">
                    <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                  @endif
                  @csrf
                  <div class="row justify-content-between">
                    <div class="col-8">
                      <div class="form-group">
                        <label for="exampleInputName2">MULTAS</label>
                      </div>
                    </div>
                    {{-- ACL FORM CAD --}}
                    <div class="col-1">
                      @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                    </div>
                    {{-- ACL FORM CAD --}}
                  </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                    <div class="row">

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Multa</span>
                          </div>
                          <select id="select1" name="id_logTipo" class="form-control">
                            <option>Selecione ...</option>
                            @foreach ($rota['tipos'] as $key)
                              <option value="{{ $key->id_logTipo }}" {{ $key->id_logTipo == $idT ? 'selected':''}} >
                                {{ $key->tipo }}
                              </option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-2">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Código</span>
                          </div>
                          <input type="text" name="cod_documento" class="form-control" value="{{ $rota['edit']->cod_documento or old('cod_documento') }}">
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Veículo</span>
                          </div>
                          <div class="col-12 form-control">
                            <select class="js-example-basic-single" name="id_veiculo" style="width: 150px" {{ isset($rota['edit']) ? "disabled":""}} required>
                              <option value="" selected>Selecione ...</option>
                              @foreach ($rota['veiculos'] as $key)
                                <option value="{{ $key->id_veiculo }}" {{ $key->id_veiculo==$idV?"selected":"" }}>
                                  {{ strtoupper($key->placa).'_'.$key->veiculo }}
                                </option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Motorista</span>
                          </div>
                          <div class="col-12 form-control">
                            <select class="js-example-basic-single" name="id_motorista" style="width: 220px" {{ isset($rota['edit']) ? "disabled":""}} required>
                              <option value="" selected>Selecione ...</option>
                              @foreach ($rota['motoristas'] as $key)
                                <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador==$idM?"selected":"" }}>
                                  {{ $key->pessoas['pessoa'] }}
                                </option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>

                    </div>

                    <div class="row mt-3">

                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Observação</span>
                          </div>
                          <input type="text" name="multa" class="form-control" value="{{ $rota['edit']->multa or old('multa') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Data Hora</span>
                          </div>
                          <input type="{{ $dt_tipo }}" name="dt_multa" class="form-control" value="{{ $rota['edit']->dt_multa or old('dt_multa') }}">
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Vencimento</span>
                          </div>
                          <input type="date" name="dt_vencimento" class="form-control" value="{{ $rota['edit']->dt_vencimento or old('dt_vencimento') }}">
                        </div>
                      </div>

                    </div>

                    <div class="row mt-3">

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Valor</span>
                          </div>
                          <input type="number" step="0.01" name="valor" class="form-control" value="{{ $rota['edit']->valor or old('valor') }}">
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Pontos</span>
                          </div>
                          <input type="text" name="pontos" class="form-control" value="{{ $rota['edit']->pontos or old('pontos') }}">
                        </div>
                      </div>
                      {{-- ACL UPLOAD --}}
                      @if ($acl->up == 1)
                        <div class="col-4">
                          <div class="input-group">
                            <input id="arquivo" type="file" name="arquivo" class="form-control" value="{{ $rota['edit']->arquivo or old('arquivo') }}">
                          </div>
                        </div>
                      @endif
                      {{-- ACL UPLOAD --}}
                      <div class="col-2">
                        <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                          {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                        </button>
                      </div>
                    </div>


                  </div>
                  <input type="hidden" name="id_tipo" value="1">
                </form>
              </div>
              {{-- END FORM --}}
              {{-- START LIST --}}
              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 215px;">
                        {{ ucfirst($rota['rota']) }}
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                        Valor
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota[$rota['rota']] as $key)
                      <tr>
                        <td>
                          {{ $key[$rota['rota']] }}
                          <a href="#" class="card-header-action btn-maximize" data-toggle="collapse" data-target="#coll-{{$key->id_multa}},#coll-{{$key->id_multa}}-1" aria-expanded="true">
                            <i class="fas fa-search-plus"></i>
                          </a>
                        </div>

                        <div class="collapse close col-12" id="coll-{{$key->id_multa}}" >
                          <div class="text-white">
                            <p class="text-left text-list">
                              Motorista: {{$key->colaboradores->pessoas->pessoa}} <br>
                              Data: {{ date('d/m/Y', strtotime($key->dt_multa)) }} 
                              Vencimento: {{ date('d/m/Y', strtotime($key->dt_vencimento)) }}
                            </p>
                          </div>
                        </div>
                      </td>
                      <td>
                        R$ {{ str_replace(".",",",$key->valor) }}

                        <div class="collapse close col-12" id="coll-{{$key->id_multa}}" >
                          <div class="text-white">
                            <p class="text-left text-list">
                              Veículo: {{$key->veiculos->veiculo}} <br>
                              Pontos: {{$key->pontos}}
                            </p>
                          </div>
                        </div>
                      </td>
                      <td>
                        {{-- ACL ACOES TABLE --}}
                        <div class="row">
                          @if (isset($key->arquivo))
                            @if ($acl->dw == 1)
                              <div class="">
                                <a href="{{ asset('storage/LOG/MUL/'.$key->veiculos->placa.'/'.$key->arquivo)}}" target="new" title="{{$key->arquivo}}" class="btn btn-block btn-ghost-success">
                                  <i class="far fa-file-pdf"></i>
                                </a>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-file-pdf"></i></div>
                            @endif
                          @else
                            <div class="btn text-dark"><i class="fas fa-ban"></i></div>
                          @endif

                          @if ($acl->u == 1)
                            <div class="">
                              <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                <i class="far fa-edit"></i>
                              </a>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                          @endif

                          @if ($acl->d == 1)
                            <div class="">
                              <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                  <i class="far fa-trash-alt"></i>
                                </button>
                              </form>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                          @endif
                        </div>
                        {{-- ACL ACOES TABLE --}}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
