@extends('layouts.sgs')

@section('content')
  @php
    $idV = $idM = $idF = $idVi = 0; $data = date("Y-m-d H:i:s");
    $acl = $rota['acl'];
      if(isset($rota['edit'])){
        $idV = $rota['edit']->id_veiculo;
        $idM = $rota['edit']->id_motorista;
        $idF = $rota['edit']->id_fornecedor;
        $idVi = $rota['edit']->id_vistoria;
      }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form">
                  @method('PUT')
                @else
                  <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form">
                    <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                  @endif
                  @csrf
                  <div class="row justify-content-between">
                    <div class="col-8">
                      <div class="form-group">
                        <label for="exampleInputName2">OCORRÊNCIAS</label>
                      </div>
                    </div>
                    {{-- ACL FORM CAD --}}
                    <div class="col-1">
                      @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                    </div>
                    {{-- ACL FORM CAD --}}
                  </div>

                    <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                      <div class="row">

                        <div class="col-5">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text text-p0"><div class="text-vert">DESCRIÇÃO</div></span>
                            </div>
                            <textarea id="textarea-input" name="ocorrencia" rows="6" class="form-control">{{ $rota['edit']->ocorrencia or old('ocorrencia') }}</textarea>
                          </div>
                        </div>

                        <div class="col-3">

                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">Veículo</span>
                              </div>
                              <div class="col-12 form-control">
                                <select class="js-example-basic-single" name="id_veiculo" style="width: 200px" {{ isset($rota['edit']) ? "disabled":""}} required>
                                  <option value="" selected>Selecione ...</option>
                                  @foreach ($rota['veiculos'] as $key)
                                    <option value="{{ $key->id_veiculo }}" {{ $key->id_veiculo==$idV?"selected":"" }}>
                                      {{ strtoupper($key->placa).'_'.$key->veiculo }}
                                    </option>
                                  @endforeach
                                </select>
                              </div>
                            </div>

                            <div class="input-group mt-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text">Motorista</span>
                              </div>
                              <div class="col-12 form-control">
                                <select class="js-example-basic-single" name="id_motorista" style="width: 220px" {{ isset($rota['edit']) ? "disabled":""}} required>
                                  <option value="" selected>Selecione ...</option>
                                  @foreach ($rota['motoristas'] as $key)
                                    <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador==$idM?"selected":"" }}>
                                      {{ $key->pessoas['pessoa'] }}
                                    </option>
                                  @endforeach
                                </select>
                              </div>
                            </div>

                        </div>

                        <div class="col-4">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Data</span>
                            </div>
                            <input type="date" name="dt_ocorrencia" class="form-control" value="{{ $rota['edit']->dt_ocorrencia or old('dt_ocorrencia') }}">
                          </div>

                            <div class="input-group mt-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text">Vistorias</span>
                              </div>
                              <div class="col-12 form-control">
                                <select class="js-example-basic-single" name="id_vistoria" style="width: 220px" {{ isset($rota['edit']) ? "disabled":""}} required>
                                  <option value="" selected>Selecione ...</option>
                                  @foreach ($rota['vistorias'] as $key)
                                    <option value="{{ $key->id_vistoria }}" {{ $key->id_vistoria==$idVi?"selected":"" }}>
                                      {{ $key->id_vistoria.'_'.$key->veiculos->placa.'_'.date('d/m/Y', strtotime($key->dt_vistoria)) }}
                                    </option>
                                  @endforeach
                                </select>
                              </div>
                            </div>

                          <button type="submit" class="btn btn-block btn-outline-success mt-3" id="submit">
                            {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}</button>
                          </div>
                        </div>

                      </div>

                    </form>
                  </div>
                  {{-- END FORM --}}
                  {{-- START LIST --}}
                  <div class="car-body">
                    {{-- DATATABLE --}}
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                      <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                      aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                      <thead>
                        <tr role="row">
                          <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 215px;">
                            Ocorrência
                          </th>
                          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                            Veículo
                          </th>
                          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                            Motorista
                          </th>
                          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                            Ação
                          </th>
                        </tr>
                      </thead>
                      {{-- LIST --}}
                      <tbody>
                        @foreach ($rota[$rota['rota']] as $key)
                          <tr>
                            <td>
                              {{ $key[$rota['rota']] }}
                              <a href="#" class="card-header-action btn-maximize" data-toggle="collapse" data-target="#collapse-{{$key['id_'.$rota['rota']]}}" aria-expanded="true">
                                <i class="fas fa-search-plus"></i>
                              </a>
                            </div>

                            <div class="collapse close col-12" id="collapse-{{$key['id_'.$rota['rota']]}}" >
                              <div class="text-white">
                                <h6>
                                  Ocorrência: {{$key->ocorrencia}} <br>
                                  Vistoria: {{$key->id_vistoria}} <br>
                                  Data: {{ date('d/m/Y', strtotime($key->dt_ocorrencia)) }}
                                </h6>
                              </div>
                            </div>
                          </td>
                            <td>{{$key->veiculos->veiculo}}</td>
                            <td>{{$key->colaboradores->pessoas->pessoa}}</td>
                            <td>
                              {{-- ACL ACOES TABLE --}}
                              <div class="row">
                                @if ($acl->u == 1)
                                  <div class="">
                                    <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                      <i class="far fa-edit"></i>
                                    </a>
                                  </div>
                                @else
                                  <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                                @endif

                                @if ($acl->d == 1)
                                  <div class="">
                                    <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                      @csrf
                                      @method('DELETE')
                                      <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                        <i class="far fa-trash-alt"></i>
                                      </button>
                                    </form>
                                  </div>
                                @else
                                  <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                                @endif
                              </div>
                              {{-- ACL ACOES TABLE --}}
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                  </div>

                </div>
              </div>

            </div>
          </div>
        </main>
      @endsection
