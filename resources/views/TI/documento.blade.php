@extends('layouts.sgs')

@section('content')
  @php
  $idT = $sts = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])){
    $edit = $rota['edit'];
    $idT = $rota['edit']->id_hrm_tipo;
    $sts = $rota['edit']->sts_curso;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form" enctype="multipart/form-data">
                  @method('PUT')
                @else
                  <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form" enctype="multipart/form-data">
                    <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                  @endif
                  @csrf
                  <div class="row justify-content-between">
                    <div class="col-8">
                      <div class="form-group">
                        <label for="exampleInputName2">DOCUMENTOS</label>
                      </div>
                    </div>
                    {{-- ACL FORM CAD --}}
                    <div class="col-1">
                      @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                    </div>
                    {{-- ACL FORM CAD --}}
                  </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                    <div class="row">
                      <div class="col-md-8 col-xs-12">
                        <div class="row">

                          <div class="col-md-5 col-xs-12 mt-3">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">Tipo</span>
                              </div>
                              <select id="select1" name="id_hrm_tipo" class="form-control" required>
                                <option value="">Selecione ...</option>
                                @foreach ($rota['tipos'] as $key)
                                  @if ($key->id_modulo == 56)
                                    <option value="{{ $key->id_hrm_tipo }}" {{ $key->id_hrm_tipo==$idT?"selected":"" }}>
                                      {{ $key->hrm_tipo }}
                                    </option>
                                  @endif
                                @endforeach
                              </select>
                            </div>
                          </div>

                          <div class="col-md-7 col-xs-12 mt-3">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">Nome</span>
                              </div>
                              <input type="text" name="curso" class="form-control" oninput="this.value = this.value.toUpperCase()" value="{{ $rota['edit']->curso or old('curso') }}">
                            </div>
                          </div>

                        </div>
                        <div class="row">

                          <div class="col-md-8 col-xs-12 mt-3">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text text-p0"><div class="text-vert">DESCRIÇÃO</div></span>
                              </div>
                              <textarea name="desk" rows="7" class="form-control">{{ $rota['edit']->desk or old('desk') }}</textarea>
                            </div>
                          </div>

                          <div class="col-md-4 col-xs-12">

                            <div class="row mt-3">
                              <div class="col-7">
                                <span class="input-text" title="Obrigatório">Obrigatório</span>
                              </div>
                              <div class="col-5">
                                <label class="switch switch-label switch-pill switch-outline-success ml-xs-2">
                                  <input name="obrigatorio" type="checkbox" class="switch-input" {{ $sts == 2?"checked":"" }}>
                                  <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
                                </label>
                              </div>
                            </div>

                            <div class="row mt-3">
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text">Carga</span>
                                </div>
                                <input type="time" name="carga" class="form-control" oninput="this.value = this.value.toUpperCase()" value="{{ $rota['edit']->carga or old('carga') }}">
                              </div>
                            </div>

                            <div class="row mt-3">
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text">Válido</span>
                                </div>
                                <input type="number" name="validade" class="form-control" oninput="this.value = this.value.toUpperCase()" value="{{ $rota['edit']->validade or old('validade') }}" title="Quantidade de dias">
                              </div>
                            </div>

                          </div>

                        </div>

                      </div>
                      <div class="col-md-4 col-xs-12 mt-3">

                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><div class="text-vert">CARGOS</div></span>
                          </div>
                          <select id="select1" size="10" name="id_curso_cargo[]" class="form-control" multiple>
                            @foreach ($rota['cargos'] as $key)
                              <option value="{{ $key->id_cargo }}"
                                @if (isset($rota['edit']))
                                  @foreach ($rota['edit']->cursocargo as $key1)
                                    {{ $key->id_cargo==$key1->id_cargo?"selected":"" }}
                                  @endforeach
                                @endif
                                >
                                {{ $key->cargo }}
                              </option>
                            @endforeach
                          </select>
                        </div>

                      </div>
{{-- BARRA ARQUIVO BARRA ARQUIVO BARRA ARQUIVO BARRA ARQUIVO BARRA ARQUIVO BARRA ARQUIVO BARRA ARQUIVO BARRA ARQUIVO BARRA ARQUIVO --}}
                      <div class="col-12 bg-dark pb-3">
                        <div class="row">
                          {{-- ACL UPLOAD --}}
                          @if ($acl->up == 1)
                            <div class="col-md-4 colxs-12 mt-3">
                              <div class="input-group">
                                <input type="file" name="arq_curso" class="form-control" value="">
                              </div>
                            </div>
                          @endif
                          {{-- ACL UPLOAD --}}
                          <div class="col-md-4 col-xs-12 mt-3">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">Nome arquivo</span>
                              </div>
                              <input type="text" name="nm_arq_curso" class="form-control" oninput="this.value = this.value.toUpperCase()">
                            </div>
                          </div>

                          @if (isset($rota['edit']) && $edit->arq->count() > 0)
                            <div class="col-md-4 col-xs-12 mt-3">
                              <div class="row">
                                @foreach ($edit->arq as $key1)
                                  <div class="btn-group">
                                    <button class="btn btn-ghost-primary" type="button" onclick="window.open('{{ asset('storage/'.$rota['pasta'].$edit->id_curso.'/'.$key1->url) }}', '_blank');" title="Documento">
                                      <i class="far fa-id-card"></i>
                                    </button>
                                    <button class="btn btn-sm btn-ghost-primary dropdown-toggle dropdown-toggle-split" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(49px, 35px, 0px);">
                                      @if ($acl->d == 1)
                                        <div class="">
                                          <a href="{{route('hrm_arq.edit',$key1->id_arquivo) }}" class="btn btn-block btn-ghost-danger" title="Editar" onclick="return confirm('Confirma a exclusão?')">
                                            <i class="far fa-trash-alt"></i>
                                          </a>
                                        </div>
                                      @else
                                        <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                                      @endif
                                    </div>
                                  </div>
                                @endforeach
                              </div>
                            </div>
                          @endif

                        </div>
                      </div>
                      {{-- BARRA ARQUIVO BARRA ARQUIVO BARRA ARQUIVO BARRA ARQUIVO BARRA ARQUIVO BARRA ARQUIVO BARRA ARQUIVO BARRA ARQUIVO BARRA ARQUIVO --}}

                      <div class="col-md-2 colxs-12 mt-3">
                        <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                          {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                        </button>
                      </div>


                    </div>
                  </div>

                </form>
              </div>

              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper  dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Cursos
                      </th>
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Descrição
                      </th>
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Arquivos
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 90px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                  {{-- LIST --}}
                  <tbody>
                  </div>
                    @foreach ($rota[$rota['rota']] as $key)
                      <tr>
                        <td>
                          {{ $key[$rota['rota']] }}
                        </td>
                        <td>
                          {{ $key['desk'] }}
                        </td>
                        <td>
                          @if (isset($key->arq))
                            @if ($acl->dw == 1)
                              <div class="row">
                              @foreach ($key->arq as $key1)
                              <div class="">
                                <a href="{{ asset('storage/'.$rota['pasta'].$key->id_curso.'/'.$key1->url)}}" target="new" title="{{$key1->arquivo}}" class="btn btn-block btn-ghost-success">
                                  <i class="far fa-file-pdf"></i>
                                </a>
                              </div>
                              @endforeach
                            </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-file-pdf"></i></div>
                            @endif
                          @else
                            <div class="btn text-dark"><i class="fas fa-ban"></i></div>
                          @endif
                        </td>
                        <td>
                        {{-- ACL ACOES TABLE --}}
                        <div class="row">
                          @if (isset($key->arquivo))
                            @if ($acl->dw == 1)
                              <div class="">
                                <a href="{{ asset('storage/'.$rota['pasta'].$key->arquivo)}}" target="new" title="{{$key->arquivo}}" class="btn btn-block btn-ghost-success">
                                  <i class="far fa-file-pdf"></i>
                                </a>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-file-pdf"></i></div>
                            @endif
                          @else
                            <div class="btn text-dark"><i class="fas fa-ban"></i></div>
                          @endif

                          @if ($acl->u == 1)
                            <div class="">
                              <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                <i class="far fa-edit"></i>
                              </a>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                          @endif

                          @if ($acl->d == 1)
                            <div class="">
                              <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                  <i class="far fa-trash-alt"></i>
                                </button>
                              </form>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                          @endif
                        </div>
                        {{-- ACL ACOES TABLE --}}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
