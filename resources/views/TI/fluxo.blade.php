@extends('layouts.sgs')

@section('content')

  <main class="main">
    <div class="container-fluid">
      <div class="animated fadeIn">

        <div class="row">

          <div class="container col-md-12">
            <div class="card mt-3">
              <div class="car-body mt-3 mb-1">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Fluxo
                      </th>
                      <th class="sorting" aria-label="Date registered: activate to sort column ascending">
                        Módulo
                      </th>
                      <th class="sorting" aria-label="Role: activate to sort column ascending">
                        Data
                      </th>
                    </tr>
                  </thead>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($fluxos as $key)
                      @foreach ($_SESSION['menu'] as $key1)
                        @if ($key->modulo == $key1->rota)

                          @php
                          switch ($key->rota) {
                            case 'mrp_item':
                            $show = ['id'=>$key->id_item,'rota'=>$key->modulo];
                            $rota = $key->rota.'.show';
                            $ico = 'fas fa-shopping-cart';
                            break;

                            case 'compra':
                            $show = $key->id_item;
                            $rota = $key->rota.'.edit';
                            $ico = 'fas fa-shopping-basket';
                            break;

                            case 'documento':
                            $show = $key->id_item;
                            $rota = $key->rota.'.edit';
                            $ico = 'fas fa-dollar';
                            break;

                            default:
                            $show = $key->id_item;
                            $rota = $key->rota.'.edit';
                            $ico = 'far fa-comment-alt';
                            break;
                          }
                          @endphp
                          <tr>
                            <td>
                              <div class="row">
                                <div class="col-9">
                                  <a href="{{route($rota,$show) }}" class="btn btn-ghost-primary" title="Abrir pedido">
                                  <i class="{{ $ico }} text-warning"></i>
                                </a>
                                  {{ $key->fluxo }}
                                </div>
                              </div>
                            </td>
                            <td>{{ ucfirst($key->modulo).': '.$key->id_item }}</td>
                            <td>
                              <div class="row justify-content-between">
                                <div class="col-10">
                                  {{ date('d/m/Y', strtotime($key->updated_at)) }}
                                </div>
                                <div class="col-2" title="{{$key->sts_fluxo==1?'Não visualizado':'Visualizado'}}">
                                  <i class="far fa-eye{{$key->sts_fluxo==1?'-slash text-info':' text-secondary'}}"></i>
                                </div>
                              </div>
                            </td>
                          </tr>
                        @endif
                      @endforeach
                    @endforeach
                  </tbody>
                </table>
              </div>

            </div>
          </div>
        </div>

      </div>

    </div>

  </main>

@endsection
