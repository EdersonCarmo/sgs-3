
@extends('layouts.sgs')

@section('content')
  @php
    $idM = $idP = $idS = 0;
    $acl = $rota['acl'];
    if(isset($rota['edit'])){
      $idM = $rota['edit']->id_modulo;
      $idP = $rota['edit']->id_programador;
      $idS = $rota['edit']->id_solicitante;
    }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form">
                  @method('PUT')
                @else
                  <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form">
                    <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                  @endif
                  @csrf
                  <div class="row justify-content-between">
                    <div class="col-8">
                      <div class="form-group">
                        <label for="exampleInputName2">CADASTRO DE {{ strtoupper($rota['rota']) }} </label>
                      </div>
                    </div>
                    {{-- ACL FORM CAD --}}
                    <div class="col-1">
                      @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                    </div>
                    {{-- ACL FORM CAD --}}
                  </div>

                    <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                      <div class="row">

                        <div class="col-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Módulo</span>
                            </div>
                            <select id="select1" name="id_modulo" class="form-control">
                              <option value="0">Selecione...</option>
                              @php
                                TI_option_modulos($rota['modulos'],$idM);
                              @endphp
                              {{-- @foreach ($rota['modulos'] as $key)
                                <option value="{{ $key->id_modulo }}" {{ $key->id_modulo==$idM?"selected":"" }}>
                                  {{ isset($key->modulo_id)?$key->modulo_id.'.'.$key->id_modulo:$key->id_modulo }}_{{ $key->modulo }}
                                </option>
                              @endforeach --}}
                            </select>
                          </div>
                        </div>

                        <div class="col-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Programador</span>
                            </div>
                            <select id="select1" name="id_programador" class="form-control">
                              <option value="0">Selecione...</option>
                              @foreach ($rota['colaboradores'] as $key)
                                <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador==$idP?"selected":"" }}>
                                  {{ $key->pessoas->pessoa }}
                                </option>
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="col-4">
                          <div class="input-group">
                            <input id="upload" type="file" name="arquivo" class="form-control">
                          </div>
                        </div>

                        <div class="col-2">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">STS</span>
                            </div>
                            <input type="text" name="sts_update" class="form-control" value="{{ $rota['edit']->sts_update or old('sts_update') }}" disabled>
                          </div>
                        </div>
                      </div>

                      <div class="row mt-3">

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Solicitante</span>
                          </div>
                          <select id="select1" name="id_solicitante" class="form-control">
                            <option value="0">Selecione...</option>
                            @foreach ($rota['colaboradores'] as $key)
                              <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador==$idS?"selected":"" }}>
                                {{ $key->pessoas->pessoa }}
                              </option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                        <div class="col-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Tempo</span>
                            </div>
                            <input type="number" name="tempo" class="form-control" value="{{ $tempo['edit']->tempo or old('tempo') }}">
                          </div>
                        </div>

                        <div class="col-4">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Descrição</span>
                            </div>
                            <textarea name="desc" id="textarea-input" rows="2" class="form-control" placeholder="">
                              {{ $rota['edit']->desc or '' }}
                            </textarea>
                          </div>
                        </div>

                        <div class="col-2">
                          <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                            {{ isset($rota['edit'])?"Alterar":"Cadastrar"}}
                          </button>
                        </div>
                      </div>

                      </div>
                    </form>
                  </div>

                  <div class="car-body">
                    {{-- DATATABLE --}}
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                      <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                      aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                      <thead>
                        <tr role="row">
                          <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 215px;">
                            Descrição
                          </th>
                          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                            Módulo
                          </th>
                          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                            Versão
                          </th>
                          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                            Ação
                          </th>
                        </tr>
                      </thead>
                      {{-- LIST --}}
                      <tbody>
                        @foreach ($rota[$rota['rota']] as $key)
                          <tr>
                            <td>{{ $key->desc }} </td>
                            <td>{{ $key->modulos->modulo }}</td>
                            <td>{{ $key->versoes->versao }}</td>
                            <td>
                              <div class="row">
                                <div class="">
                                  <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-sm btn-ghost-warning" title="Editar">
                                    <i class="far fa-edit"></i>
                                  </a>
                                </div>
                                <div class="">
                                  <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                      <i class="far fa-trash-alt"></i>
                                    </button>
                                  </form>
                                </div>
                              </div>
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>

                </div>
                </div>
              </div>

            </div>
          </div>
        </main>
      @endsection
