@extends('layouts.sgs')

@section('content')
  @php
  $idUser = $idMod = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])){
    $idUser = $rota['edit']->id_user;
    $idMod = $rota['edit']->id_modulo;
    $edit = $rota['edit'];
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- START FORM --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form">
                  @method('PUT')
                @else
                  <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form">
                    <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                  @endif
                  @csrf
                  <div class="row justify-content-between">
                    <div class="col-8">
                      <div class="form-group">
                        <label for="exampleInputName2">PERMISSÕES </label>
                      </div>
                    </div>
                    {{-- ACL FORM CAD --}}
                    <div class="col-2">
                      @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-info ml-3" onclick="Mostra_form_table('{{$rota['rota']}}-2')">
                          <i class='fa fa-plus'></i>
                        </button>
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                    </div>
                    {{-- ACL FORM CAD --}}
                  </div>
{{-- FORM EDIT PERMISSAO --}}
                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">

                    <div class="row">
                      <div class="col-3">

                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text text-p0"><div class="text-vert">USUÁRIOS</div></span>
                          </div>
                          <select id="select1" size="10" name="id_user[]" class="form-control" {{ isset($rota['edit'])?'disabled':''}} multiple>
                            {{-- <option>Selecione ...</option> --}}
                            @foreach ($rota['usuarios'] as $key)
                              <option value="{{ $key->id }}" {{ $key->id == $idUser ? 'selected':''}} >
                                {{ $key->name }}
                              </option>
                            @endforeach
                          </select>
                        </div>

                      </div>
                      <div class="col-3">

                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" style="padding: 0px 0px 0px 0px"><div class="text-vert">MÓDULOS</div></span>
                          </div>
                          <select id="select1" size="10" name="id_modulo[]" class="form-control" {{ isset($rota['edit'])?'disabled':''}} multiple>
                            {{-- <option>Selecione ...</option> --}}
                            @php
                            TI_option_modulos($rota['modulos'],$idMod);
                            @endphp
                          </select>
                        </div>

                      </div>

                      <div class="col-6">
                        <div class="row">

                          <div class="col-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="far fa-eye text-success" title="Visualizar"></i>
                                <label class="switch switch-label switch-pill switch-outline-success ml-2">
                                  <input name="r" type="checkbox" class="switch-input" {{ isset($edit->r)?$edit->r == 0?"":"checked":"checked" }}>
                                  <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
                                </label>
                              </span>
                            </div>

                            <div class="input-group-prepend mt-1">
                              <span class="input-group-text"><i class="fas fa-plus-circle text-primary" title="Criar"></i>
                                <label class="switch switch-label switch-pill switch-outline-success ml-2">
                                  <input name="c" type="checkbox" class="switch-input" {{ isset($edit->c)?$edit->c == 0?"":"checked":"checked" }}>
                                  <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
                                </label>
                              </span>
                            </div>

                            <div class="input-group-prepend mt-1">
                              <span class="input-group-text"><i class="fas fa-edit text-warning" title="Alterar"></i>
                                <label class="switch switch-label switch-pill switch-outline-success ml-2">
                                  <input name="u" type="checkbox" class="switch-input" {{ isset($edit->u)?$edit->u == 0?"":"checked":"checked" }}>
                                  <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
                                </label>
                              </span>
                            </div>

                            <div class="input-group-prepend mt-1">
                              <span class="input-group-text"><i class="fas fa-trash-alt text-danger" title="Deletar"></i>
                                <label class="switch switch-label switch-pill switch-outline-success ml-2">
                                  <input name="d" type="checkbox" class="switch-input" {{ isset($edit->d)?$edit->d == 0?"":"checked":"checked" }}>
                                  <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
                                </label>
                              </span>
                            </div>

                          </div>
                          <div class="col-3">

                            <div class="">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-upload" title="Enviar"></i>
                                  <label class="switch switch-label switch-pill switch-outline-success ml-2">
                                    <input name="up" type="checkbox" class="switch-input" {{ isset($edit->up)?$edit->up == 0?"":"checked":"checked" }}>
                                    <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
                                  </label>
                                </span>
                              </div>

                              <div class="input-group-prepend mt-1">
                                <span class="input-group-text"><i class="fas fa-download" title="Baixar"></i>
                                  <label class="switch switch-label switch-pill switch-outline-success ml-2">
                                    <input name="dw" type="checkbox" class="switch-input" {{ isset($edit->dw)?$edit->dw == 0?"":"checked":"checked" }}>
                                    <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
                                  </label>
                                </span>
                              </div>

                              <div class="input-group-prepend mt-1">
                                <span class="input-group-text"><i class="fas fa-check-circle" title="Aprovar"></i>
                                  <label class="switch switch-label switch-pill switch-outline-success ml-2">
                                    <input name="a" type="checkbox" class="switch-input" {{ isset($edit->a)?$edit->a == 0?"":"checked":"checked" }}>
                                    <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
                                  </label>
                                </span>
                              </div>

                              <div class="input-group-prepend mt-1">
                                <span class="input-group-text"><i class="fas fa-hands-helping" title="Finalizar"></i>
                                  <label class="switch switch-label switch-pill switch-outline-success ml-2">
                                    <input name="f" type="checkbox" class="switch-input" {{ isset($edit->f)?$edit->f == 0?"":"checked":"checked" }}>
                                    <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
                                  </label>
                                </span>
                              </div>

                            </div>
                          </div>

                        <div class="col-4">

                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-clipboard" title="Solicitar"></i>
                            <label class="switch switch-label switch-pill switch-outline-success ml-2">
                              <input name="s" type="checkbox" class="switch-input" {{ isset($edit->s)?$edit->s == 0?"":"checked":"checked" }}>
                              <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
                            </label>
                          </span>
                        </div>

                        <div class="input-group-prepend mt-1">
                          <span class="input-group-text"><i class="fas fa-print" title="Imprimir"></i>
                            <label class="switch switch-label switch-pill switch-outline-success ml-2">
                              <input name="p" type="checkbox" class="switch-input" {{ isset($edit->s)?$edit->p == 0?"":"checked":"checked" }}>
                              <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
                            </label>
                          </span>
                        </div>

                          {{-- <div class="col-6">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">Fluxo
                                  <label class="switch switch-label switch-pill switch-outline-success">
                                    <input name="fx" type="checkbox" class="switch-input" {{ isset($edit->r)?$edit->fx == 0?"":"checked":"checked" }}>
                                    <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
                                  </label>
                                </span>
                              </div>
                            </div>
                          </div> --}}

                            <div class="input-group mt-1">
                              <div class="input-group-prepend">
                                <span class="input-group-text">Subistitui
                                  <label class="switch switch-label switch-pill switch-outline-success">
                                    <input name="sub" type="checkbox" class="switch-input">
                                    <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
                                  </label>
                                </span>
                              </div>
                            </div>

                            <div class="row mt-1 ml-1">
                              <div class="">
                                <button type="submit" class="btn btn-block btn-outline-success mt-1" id="submit">
                                  {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                                </button>
                              </div>
                              <div class="ml-1">
                                <button type="submit" class="btn btn-block btn-ghost-success mt-1" id="submit" name="retorna" value="edit">
                                  <i class="fas fa-redo-alt"></i>
                                </button>
                              </div>
                            </div>



                        </div>

                        </div>
                      </div>
                    </div>
                  </div>
{{-- FORM EDIT PERMISSAO --}}
{{-- FORM GRUPOS --}}
                  <div id="div-cad-{{$rota['rota']}}-2" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">

                    <div class="row">
                      <div class="col-3">

                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text text-p0"><div class="text-vert">GRUPOS</div></span>
                          </div>
                          <select id="select1" size="10" name="id_A" class="form-control" {{ isset($rota['edit'])?'disabled':''}}>
                            {{-- <option>Selecione ...</option> --}}
                            @foreach ($rota['usuarios'] as $key)
                              <option value="{{ $key->id }}" {{ $key->id == $idUser ? 'selected':''}} >
                                {{ $key->name }}
                              </option>
                            @endforeach
                          </select>
                        </div>

                      </div>
De para
                      <div class="col-3">

                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text text-p0"><div class="text-vert">USUÁRIOS</div></span>
                          </div>
                          <select id="select1" size="10" name="id_B[]" class="form-control" {{ isset($rota['edit'])?'disabled':''}} multiple>
                            {{-- <option>Selecione ...</option> --}}
                            @foreach ($rota['usuarios'] as $key)
                              <option value="{{ $key->id }}" {{ $key->id == $idUser ? 'selected':''}} >
                                {{ $key->name }}
                              </option>
                            @endforeach
                          </select>
                        </div>

                      </div>

                      <div class="ml-3">
                        <div class="mb-3">
                          <button type="submit" class="btn btn-block btn-outline-success mt-1" id="submit" name="copia_acl" value="false" title="Não atualiza futuras alterações.">
                            Copiar permissões
                          </button>
                        </div>
                        <div class="">
                          <button type="submit" class="btn btn-block btn-outline-success mt-1" id="submit" name="copia_acl" value="true" title="Atualiza junco com grupo.">
                            Inserir no grupo
                          </button>
                        </div>
                      </div>



                    </div>
                  </div>
{{-- FORM GRUPOS --}}
              </form>
            </div>
            {{-- END FORM --}}
            {{-- START LIST --}}
            <div class="car-body">
              {{-- DATATABLE --}}
              <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                <thead>
                  <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                      Usuário
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                      Módulo
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                      Permissões
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 50px;">
                      Ação
                    </th>
                  </tr>
                </thead>
              </div>
              {{-- LIST --}}
              <tbody>
                @foreach ($rota[$rota['rota']] as $key)
                  <tr>
                    <td>{{ $key->usuario->name }}</td>
                    <td>
                      @if ($key->modulo->modulo_id === null)
                        <span class="badge badge-info">{{$key->modulo->id_modulo}}_Menu</span>
                      @else
                        <span class="badge badge-warning">{{$key->modulo->id_modulo}}_SubMenu</span>
                      @endif
                      {{ $key->modulo->modulo }}
                    </td>
                    <td>
                      <button type="button" class="btn btn-sm btn-pill btn-ghost-{{ $key->r == 1 ? 'success':'dark' }}" title="Vizualiza"><i class="far fa-eye"></i></button>
                      <button type="button" class="btn btn-sm btn-pill btn-ghost-{{ $key->c == 1 ? 'success':'dark' }}" title="Cria"><i class="fas fa-plus-circle"></i></button>
                      <button type="button" class="btn btn-sm btn-pill btn-ghost-{{ $key->u == 1 ? 'success':'dark' }}" title="Altera"><i class="fas fa-edit"></i></button>
                      <button type="button" class="btn btn-sm btn-pill btn-ghost-{{ $key->d == 1 ? 'success':'dark' }}" title="Deleta"><i class="fas fa-trash-alt"></i></button>
                      <button type="button" class="btn btn-sm btn-pill btn-ghost-{{ $key->up == 1 ? 'success':'dark' }}" title="Envia"><i class="fas fa-upload"></i></button>
                      <button type="button" class="btn btn-sm btn-pill btn-ghost-{{ $key->dw == 1 ? 'success':'dark' }}" title="Baixa"><i class="fas fa-download"></i></button>
                      <button type="button" class="btn btn-sm btn-pill btn-ghost-{{ $key->a == 1 ? 'success':'dark' }}" title="Aprova"><i class="fas fa-check-circle"></i></button>
                      <button type="button" class="btn btn-sm btn-pill btn-ghost-{{ $key->f == 1 ? 'success':'dark' }}" title="Finaliza"><i class="fas fa-hands-helping"></i></button>
                      <button type="button" class="btn btn-sm btn-pill btn-ghost-{{ $key->s == 1 ? 'success':'dark' }}" title="Solicita"><i class="fas fa-clipboard"></i></button>
                      <button type="button" class="btn btn-sm btn-pill btn-ghost-{{ $key->p == 1 ? 'success':'dark' }}" title="Imprimi"><i class="fas fa-print"></i></button>
                    </td>
                    <td>
                      {{-- ACL ACOES TABLE --}}
                      <div class="row">
                        @if ($acl->u == 1)
                          <div class="">
                            <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-sm btn-ghost-warning" title="Editar">
                              <i class="far fa-edit"></i>
                            </a>
                          </div>
                        @else
                          <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                        @endif

                        @if ($acl->d == 1)
                          <div class="">
                            <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class="btn btn-sm btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                <i class="far fa-trash-alt"></i>
                              </button>
                            </form>
                          </div>
                        @else
                          <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                        @endif
                      </div>
                      {{-- ACL ACOES TABLE --}}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          </div>

        </div>
        {{-- END LIST --}}
      </div>

    </div>
  </div>
</main>
@endsection
