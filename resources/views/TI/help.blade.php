
@extends('layouts.sgs')

@section('content')
  @php
  $idA = $idS = $idT = $idP = $sts = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])){
    $idA = $rota['edit']->id_atendente;
    $idS = $rota['edit']->id_solicitante;
    $idT = $rota['edit']->id_ti_tipo;
    $idP = $rota['edit']->prioridade;
    $sts = $rota['edit']->sts_help;
  }
@endphp
<main class="main">
  <br>
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-md-12">

        @component('includes/alerts')
        @endcomponent

        <div class="card">
          {{-- FORM CADASTRO --}}
          <div class="card-header">
            @if (isset($rota['edit']))
              <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form" enctype="multipart/form-data">
                @method('PUT')
              @else
                <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form" enctype="multipart/form-data">
                  <input type="hidden" name="id_colaborador" value="{{ Auth::id() }}">
                @endif
                @csrf
                <div class="row justify-content-between">
                  <div class="col-8">
                    <div class="form-group">
                      <label for="exampleInputName2">HELPDESK</label>
                    </div>
                  </div>
                  {{-- ACL FORM CAD --}}
                  <div class="col-1">
                    @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                  </div>
                  {{-- ACL FORM CAD --}}
                </div>

                <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                  <div class="row">
                    <div class="col-4">

                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text text-p0"><div class="text-vert">Descrição</div></span>
                        </div>
                        <textarea id="textarea-input" name="help" rows="4" class="form-control">{{ $rota['edit']->help or '' }}</textarea>
                      </div>

                      @if (isset($rota['edit']))
                        <div class="input-group mt-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text text-p0"><div class="text-vert">Solução</div></span>
                          </div>
                          <textarea id="textarea-input" name="solucao" rows="4" class="form-control">{{ $rota['edit']->solucao or '' }}</textarea>
                        </div>
                      @endif

                    </div>
                    <div class="row col-8">

                      <div class="col-6">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Tipo</span>
                          </div>
                          <select id="select1" name="id_ti_tipo" class="form-control">
                            <option value="0">Selecione...</option>
                            @foreach ($rota['tipos'] as $key)
                              <option value="{{ $key->id_ti_tipo }}" {{ $key->id_ti_tipo==$idT?"selected":"" }}>
                                {{ $key->ti_tipo }}
                              </option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-6">
                        <div class="input-group">
                          <input id="upload" type="file" name="arquivo" class="form-control">
                        </div>
                      </div>

                      @if ($acl->a == 1)
                        <div class="col-6 mt-3">
                          <div class="input-group">
                            <label for="customRange3">Prioridade</label>
                            <input name="prioridade" type="range" class="custom-range" id="vol3" value="{{ $idP or 0 }}" min="0" max="10"
                            step="1" oninput="display.value=value" onchange="vol3.title=value" title="{{ $idP or 0 }}">
                          </div>

                        </div>
                        <div class="col-6 mt-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Solicitante</span>
                            </div>
                            <select id="select1" name="id_solicitante" class="form-control">
                              <option value="0">Selecione...</option>
                              @foreach ($rota['usuarios'] as $key)
                                <option value="{{ $key->id }}" {{ $key->id==$idS?"selected":"" }}>
                                  {{ $key->name }}
                                </option>
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="col-6 mt-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Atendente</span>
                            </div>
                            <select id="select1" name="id_atendente" class="form-control" required>
                              <option value="">Selecione...</option>
                              @foreach ($rota['usuarios'] as $key)
                                <option value="{{ $key->id }}" {{ $key->id==$idA?"selected":"" }}>
                                  {{ $key->name }}
                                </option>
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="col-6 mt-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Status</span>
                            </div>
                            <select id="select1" name="sts_help" class="form-control">
                              <option value="0">Selecione...</option>
                              <option value="0" {{ $sts==0?"selected":"" }}>Cancelado</option>
                              <option value="1" {{ $sts==1?"selected":"" }}>Pendente</option>
                              <option value="2" {{ $sts==2?"selected":"" }}>Fila</option>
                              <option value="3" {{ $sts==3?"selected":"" }}>Analise</option>
                              <option value="4" {{ $sts==4?"selected":"" }}>Retorno</option>
                              <option value="5" {{ $sts==5?"selected":"" }}>Finalizado</option>
                            </select>
                          </div>
                        </div>
                      @endif

                      @if (isset($rota['edit']))

                        <div class="col-6 mt-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Data</span>
                            </div>
                            <div class="">
                              <input class="form-control" type="datetime" name="" value="{{ $rota['edit']->created_at }}" disabled>
                            </div>

                            @if (isset($rota['edit']->arquivo))
                              @if ($acl->dw == 1)
                                <div class="">
                                  <a href="{{ asset('storage/TI/HELP/'.$rota['edit']->arquivo)}}" target="new" title="{{$rota['edit']->arquivo}}" class="btn btn-block btn-ghost-success">
                                    <i class="far fa-file-pdf"></i>
                                  </a>
                                </div>
                              @else
                                <div class="btn text-secondary"><i class="far fa-file-pdf"></i></div>
                              @endif
                            @else
                              <div class="btn text-dark"><i class="fas fa-ban"></i></div>

                            @endif
                          </div>
                        </div>

                      @endif

                      @if ($acl->u == 1)
                        <div class="col-6 mt-3">
                          <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                            {{ isset($rota['edit'])?"Alterar Ticket":"Criar Ticket"}}
                          </button>
                        </div>
                      @endif

                    </div>
                  </div>

                </div>
              </form>
            </div>

            <div class="car-body">
              {{-- DATATABLE --}}
              <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper area_print container-fluid dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">

                <thead>
                  <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 215px;">
                      Tipo
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                      Tipo
                    </th>
                    {{-- <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                    Versão
                  </th> --}}
                  <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                    Ação
                  </th>
                </tr>
              </thead>
              {{-- LIST --}}
              <tbody>
                @foreach ($rota[$rota['rota']] as $key)
                  @php
                  switch ($key->sts_help) {
                    case '0': $cor = 'dark';      $tx = 'Cancelado';  break;
                    case '1': $cor = 'warning';   $tx = 'Pendente';   break;
                    case '2': $cor = 'secondary'; $tx = 'Na fina';    break;
                    case '3': $cor = 'info';      $tx = 'Em analise'; break;
                    case '4': $cor = 'light';     $tx = 'Retronado';  break;
                    case '5': $cor = 'success';   $tx = 'Finalizado'; break;
                    default:  $cor = 'dark';      $tx = 'Erro';       break;
                  }
                  @endphp
                  <tr>
                    <td><i class="fas fa-tag text-{{$cor}}" title="{{$tx}}"></i> {{ $key->help }}</td>
                    <td>{{ $key->tipos->ti_tipo }}</td>
                    <td>
                      {{-- ACL ACOES TABLE --}}
                      <div class="row">
                        @if (isset($key->arquivo))
                          @if ($acl->dw == 1)
                            <div class="">
                              <a href="{{ asset('storage/TI/HELP/'.$key->arquivo)}}" target="new" title="{{$key->arquivo}}" class="btn btn-sm btn-ghost-success">
                                <i class="far fa-file-pdf"></i>
                              </a>
                            </div>
                          @else
                            <div class="btn btn-sm text-secondary"><i class="far fa-file-pdf"></i></div>
                          @endif
                        @else
                          <div class="btn btn-sm text-dark"><i class="fas fa-ban"></i></div>
                        @endif
                        @if ($acl->u == 1)
                          <div class="">
                            <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-sm btn-ghost-warning" title="Editar">
                              <i class="far fa-edit"></i>
                            </a>
                          </div>
                        @else
                          <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                        @endif

                        @if ($acl->d == 1)
                          <div class="">
                            <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class="btn btn-sm btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                <i class="far fa-trash-alt"></i>
                              </button>
                            </form>
                          </div>
                        @else
                          <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                        @endif
                      </div>
                      {{-- ACL ACOES TABLE --}}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>

        </div>
        </div>
      </div>

    </div>
  </div>
</main>
@endsection
