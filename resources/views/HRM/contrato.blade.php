@extends('layouts.sgs')

@section('content')
  @php
  $idC = $idT = 0; //dd($rota['edit']->pessoa->end->arq['url']);
  $acl = $rota['acl'];
  if(isset($rota['edit'])){
    $idC = $rota['edit']->id_colaborador;
    $idT = $rota['edit']->id_hrm_tipo;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card animated fadeIn">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form name="form1" action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form" enctype="multipart/form-data">
                  @method('PUT')
                  <input type="hidden" name="id_pessoa" value="{{ $rota['edit']['id_pessoa'] }}">
                @else
                  <form name="form1" action="{{ route($rota['rota'].'.store') }}" method="post" class="form" enctype="multipart/form-data">
                    <input id="id_pessoa" type="hidden" name="id_pessoa" value="">
                  @endif
                  @csrf
                  <div class="row justify-content-between">
                    <div class="col-md-8 col-xs-6">
                      <div class="form-group">
                        <label for="exampleInputName2">COLABORADORES</label>
                      </div>
                    </div>
                    {{-- ACL FORM CAD --}}
                    <div class="col-md-1 col-xs-6">
                      @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                    </div>
                    {{-- ACL FORM CAD --}}
                  </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">


                  <div class="col-md-12">
                    <ul class="nav nav-tabs" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#colaborador" role="tab" aria-controls="colaborador">Colaborador</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#endereco" role="tab" aria-controls="endereco">Endereço</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#documento" role="tab" aria-controls="documento">Documentos</a>
                      </li>
                      @if (isset($rota['edit']))
                        <li class="nav-item">
                          <a class="nav-link " data-toggle="tab" href="#dependente" role="tab" aria-controls="dependente">Dependentes</a>
                        </li>
                      @endif
                      <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#teste" role="tab" aria-controls="teste">teste</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#contrato" role="tab" aria-controls="contrato">Contrato</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#print" role="tab" aria-controls="print">Print</a>
                      </li>
                    </ul>

                    @component('HRM/contrato_tabpanel',['rota' => $rota])
                    @endcomponent

                  </div>



                      <div class="col-md-2 col-xs-12 mt-2">
                        <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                          {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                        </button>
                      </div>

                </form>
              </div>
              </div>

              {{-- TABLE LIST TABLE LIST TABLE LIST TABLE LIST TABLE LIST TABLE LIST TABLE LIST TABLE LIST TABLE LIST TABLE LIST TABLE LIST TABLE LIST TABLE LIST TABLE LIST --}}

              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper  dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Colaborador
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 90px;">
                        Arquivos
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 90px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota[$rota['rota']] as $key)
                      <tr>
                        <td>
                          {{ $key->pessoa->pessoa }}
                        </div>
                      </td>
                      <td>
                        <div class="row">

                        @if ($key->pessoa->end['arq']['url'])
                          <div class="">
                            <a href="{{ asset('storage/COLABORADORES/'.$key->id_pessoa.'/END/'.$key->pessoa->end['arq']['url']) }}" target="new" title="Endereço" class="btn btn-block btn-ghost-success">
                              <i class="fas fa-map-marker-alt"></i>
                            </a>
                          </div>
                        @endif
                        @if (isset($key->pessoa->dados->arq))
                          @foreach ($key->pessoa->dados->arq as $key1)
                            <div class="">
                              <a href="{{ asset('storage/COLABORADORES/'.$key->id_pessoa.'/DOCUMENTO/'.$key1->url) }}" target="new" title="Documento" class="btn btn-block btn-ghost-success">
                                <i class="far fa-id-card"></i>
                              </a>
                            </div>
                          @endforeach
                        @endif
                        @if ($key->arq->count() > 0)
                          @foreach ($key->arq as $key1)
                            <div class="">
                              <a href="{{ asset('storage/COLABORADORES/'.$key->id_pessoa.'/DOCUMENTO/'.$key1->url) }}" target="new" title="Contrato" class="btn btn-block btn-ghost-success">
                                <i class="fas fa-file-contract"></i>
                              </a>
                            </div>
                          @endforeach
                        @endif
                      </div>
                      </td>
                      <td>
                        {{-- ACL ACOES TABLE --}}
                        <div class="row">
                          @if (isset($key->arquivo))
                            @if ($acl->dw == 1)
                              <div class="">
                                <a href="{{ asset('storage/'.$rota['pasta'].$key->arquivo)}}" target="new" title="{{$key->arquivo}}" class="btn btn-block btn-ghost-success">
                                  <i class="far fa-file-pdf"></i>
                                </a>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-file-pdf"></i></div>
                            @endif
                          @else
                            <div class="btn text-dark"><i class="fas fa-ban"></i></div>
                          @endif

                          @if ($acl->u == 1)
                            <div class="">
                              <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                <i class="far fa-edit"></i>
                              </a>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                          @endif

                          @if ($acl->d == 1)
                            <div class="">
                              <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                  <i class="far fa-trash-alt"></i>
                                </button>
                              </form>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                          @endif
                        </div>
                        {{-- ACL ACOES TABLE --}}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
