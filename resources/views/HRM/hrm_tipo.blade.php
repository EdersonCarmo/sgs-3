
@extends('layouts.sgs')

@section('content')
  @php
  $idM = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])){
    $idM = $rota['edit']->id_modulo;
  }
@endphp
<main class="main">
  <br>
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-md-12">

        @component('includes/alerts')
        @endcomponent

        <div class="card">
          {{-- FORM CADASTRO --}}
          <div class="card-header">
            @if (isset($rota['edit']))
              <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form">
                @method('PUT')
              @else
                <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form">
                  <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                @endif
                @csrf
                <div class="row justify-content-between">
                  <div class="col-8">
                    <div class="form-group">
                      <label for="exampleInputName2">CADASTRO DE TIPOS </label>
                    </div>
                  </div>
                  {{-- ACL FORM CAD --}}
                  <div class="col-1">
                    @if ($acl->c == 1)
                      <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                        <i class='fa fa-plus'></i>
                      </button>
                    @endif
                  </div>
                  {{-- ACL FORM CAD --}}
                </div>

                <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                  <div class="row">

                    <div class="col-md-3 col-xs-12">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Módulo</span>
                        </div>
                        <select id="select1" name="id_modulo" class="form-control">
                          <option value="">Selecione ...</option>
                          @foreach ($rota['modulos'] as $key)
                            <option value="{{ $key->id_modulo }}" {{ $key->id_modulo == $idM ? 'selected':''}}>{{ $key->modulo }}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="col-md-3 col-xs-12">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Nome</span>
                        </div>
                        <input type="text" name="hrm_tipo" class="form-control" value="{{ $rota['edit']->hrm_tipo or '' }}" oninput="this.value = this.value.toUpperCase()">
                      </div>
                    </div>

                    <div class="col-md-2 col-xs-12">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" title="Quantidade de dias">Vence</span>
                        </div>
                        <input type="number" name="vencimento" class="form-control" value="{{ $rota['edit']->vencimento or old('vencimento') }}">
                      </div>
                    </div>

                    <div class="col-md-2 col-xs-12">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text" title="Quantidade de dias antes de vencer">Notifica</span>
                        </div>
                        <input type="number" name="notifica" class="form-control" value="{{ $rota['edit']->notifica or old('notifica') }}">
                      </div>
                    </div>

                    <div class="col-md-2 col-xs-12">
                      <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                        {{ isset($rota['edit'])?"Alterar":"Cadastrar"}}
                      </button>
                    </div>
                  </div>

                </div>
              </form>
            </div>

            <div class="car-body">
              {{-- DATATABLE --}}
              <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper  dt-bootstrap4 no-footer mt-3">
                <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                <thead>
                  <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                      Tipos
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                      Ação
                    </th>
                  </tr>
                </thead>
                {{-- LIST --}}
                <tbody>
                  @foreach ($rota[$rota['rota']] as $key)
                    <tr>
                      <td>
                        {{ $key[$rota['rota']] }}
                      </div>
                    </td>
                    <td>
                      {{-- ACL ACOES TABLE --}}
                      <div class="row">
                        @if ($acl->u == 1)
                          <div class="">
                            <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                              <i class="far fa-edit"></i>
                            </a>
                          </div>
                        @else
                          <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                        @endif

                        @if ($acl->d == 1)
                          <div class="">
                            <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                <i class="far fa-trash-alt"></i>
                              </button>
                            </form>
                          </div>
                        @else
                          <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                        @endif
                      </div>
                      {{-- ACL ACOES TABLE --}}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
