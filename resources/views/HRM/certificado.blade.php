@extends('layouts.sgs')

@section('content')
  @php
  $idC = $idC2 = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])){
    $idC = $rota['edit']->id_curso;
    $idC2 = $rota['edit']->id_colaborador;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form" enctype="multipart/form-data">
                  @method('PUT')
                @else
                  <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form" enctype="multipart/form-data">
                    <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                  @endif
                  @csrf
                  <div class="row justify-content-between">
                    <div class="col-8">
                      <div class="form-group">
                        <label for="exampleInputName2">CERTIFICADOS</label>
                      </div>
                    </div>
                    {{-- ACL FORM CAD --}}
                    <div class="col-1">
                      @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                    </div>
                    {{-- ACL FORM CAD --}}
                  </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                    <div class="row">

                      <div class="col-md-3 col-xs-12 mt-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Cursos</span>
                          </div>
                          <select id="select1" name="id_curso" class="form-control">
                            <option>Selecione ...</option>
                            @foreach ($rota['cursos'] as $key)
                              <option value="{{ $key->id_curso }}" {{ $key->id_curso==$idC?"selected":"" }}>
                                {{ $key->curso }}
                              </option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-md-4 col-xs-12 mt-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Colaborador</span>
                          </div>
                          <select id="select1" name="id_colaborador" class="form-control" required>
                            <option value="">Selecione ...</option>
                            @foreach ($rota['colaboradores'] as $key)
                              <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador==$idC2?"selected":"" }}>
                                {{ $key->pessoas->pessoa }}
                              </option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-md-5 col-xs-12 mt-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Título</span>
                          </div>
                          <input type="text" name="certificado" class="form-control" oninput="this.value = this.value.toUpperCase()" value="{{ $rota['edit']->certificado or old('certificado') }}" required>
                        </div>
                      </div>

                    </div>
                    <div class="row">

                      <div class="col-md-4 col-xs-12 mt-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Institução</span>
                          </div>
                          <input type="text" name="instituicao" class="form-control" oninput="this.value = this.value.toUpperCase()" value="{{ $rota['edit']->instituicao or old('instituicao') }}" required>
                        </div>
                        {{-- ACL UPLOAD --}}
                        @if ($acl->up == 1)
                          <div class="mt-3">
                            <div class="input-group">
                              <input id="arquivo" type="file" name="arquivo" class="form-control" value="{{ $rota['edit']->arquivo or old('arquivo') }}">
                            </div>
                          </div>
                        @endif
                        {{-- ACL UPLOAD --}}
                      </div>

                      <div class="col-md-3 col-xs-12">
                        <div class="input-group mt-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Início</span>
                          </div>
                          <input type="date" name="dt_inicio" class="form-control" value="{{ $rota['edit']->dt_inicio or old('dt_inicio') }}">
                        </div>

                        <div class="input-group mt-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Fim</span>
                          </div>
                          <input type="date" name="dt_fim" class="form-control" value="{{ $rota['edit']->dt_fim or old('dt_fim') }}">
                        </div>
                      </div>

                      <div class="col-md-5 col-xs-12 mt-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text text-p0"><div class="text-vert">DESCRIÇÃO</div></span>
                          </div>
                          <textarea name="desk" rows="4" class="form-control">{{ $rota['edit']->desk or old('desk') }}</textarea>
                        </div>
                      </div>


                      <div class="col-md-2 col-xs-12 mt-3">
                        <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                          {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                        </button>
                      </div>
                    </div>
                  </div>

                </form>
              </div>

              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper  dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Certificados
                      </th>
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Instituição
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 90px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                  {{-- LIST --}}
                  <tbody>
                  </div>
                    @foreach ($rota[$rota['rota']] as $key)
                      <tr>
                        <td>
                            {{ $key[$rota['rota']] }}
                        </td>
                        <td>
                            {{ $key['instituicao'] }}
                        </td>
                      <td>
                        {{-- ACL ACOES TABLE --}}
                        <div class="row">
                          @if (isset($key->arquivo))
                            @if ($acl->dw == 1)
                              <div class="">
                                <a href="{{ asset('storage/'.$rota['pasta'].$key->arquivo)}}" target="new" title="{{$key->arquivo}}" class="btn btn-block btn-ghost-success">
                                  <i class="far fa-file-pdf"></i>
                                </a>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-file-pdf"></i></div>
                            @endif
                          @else
                            <div class="btn text-dark"><i class="fas fa-ban"></i></div>
                          @endif

                          @if ($acl->u == 1)
                            <div class="">
                              <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                <i class="far fa-edit"></i>
                              </a>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                          @endif

                          @if ($acl->d == 1)
                            <div class="">
                              <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                  <i class="far fa-trash-alt"></i>
                                </button>
                              </form>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                          @endif
                        </div>
                        {{-- ACL ACOES TABLE --}}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
