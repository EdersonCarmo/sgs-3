@extends('layouts.sgs')

@section('content')
  @php
  $acl = $rota['acl'];
    if(isset($rota['edit'])){
    }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form">
                  <input type="hidden" name="id_dado" value="{{$rota['edit']->id_dado}}">
                  @method('PUT')
                @else
                  <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form">
                    <input type="hidden" name="id_empresa" value="1">
                    <input type="hidden" name="id_dado" value="0">
                  @endif
                  @csrf
                  <div class="row justify-content-between">
                    <div class="col-8">
                      <div class="form-group">
                        <label for="exampleInputName2">COLABORADORES</label>
                      </div>
                    </div>
                    {{-- ACL FORM CAD --}}
                    <div class="col-1">
                      @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                    </div>
                    {{-- ACL FORM CAD --}}
                  </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}">
                    <div>
                      <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                          <a class="nav-link active show" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                            Colaborador
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                            Documentos
                          </a>
                        </li>
                        {{-- <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#contrato" role="tab" aria-controls="contrato" aria-selected="false">
                            Contrato
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="tab" href="#messages" role="tab" aria-controls="messages" aria-selected="false">
                            Pessoal
                          </a>
                        </li> --}}
                      </ul>
                      <div class="tab-content">

                        @component('HRM/colab_tabpanel',['rota' => $rota])
                        @endcomponent

                      </div>
                    </div>

                    <div class="col-4 mt-3">
                      <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                        {{ isset($rota['edit'])?"Alterar":"Cadastrar"}}</button>
                      </div>

                      <input type="hidden" name="id_tipo" value="1">
                    </div>
                  </form>
                </div>
                {{-- END FORM --}}

                {{-- START LIST --}}
                <div class="car-body">
                  {{-- DATATABLE --}}
                  <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer mt-3">
                    <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                    <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                    aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                    <thead>
                      <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 215px;">
                          {{ ucfirst($rota['rota']) }}
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                          Vigência
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                          Ação
                        </th>
                      </tr>
                    </thead>
                    {{-- LIST --}}
                    <tbody>
                      @foreach ($rota[$rota['rota']] as $key)
                        <tr>
                          <td>{{ $key->pessoas->pessoa }}</td>
                          <td>{{ $key->vigencia }}</td>
                          <td>
                            {{-- ACL ACOES TABLE --}}
                            <div class="row">
                              @if ($acl->u == 1)
                                <div class="">
                                  <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                    <i class="far fa-edit"></i>
                                  </a>
                                </div>
                              @else
                                <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                              @endif

                              @if ($acl->d == 1)
                                <div class="">
                                  <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                      <i class="far fa-trash-alt"></i>
                                    </button>
                                  </form>
                                </div>
                              @else
                                <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                              @endif
                            </div>
                            {{-- ACL ACOES TABLE --}}
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>

              </div>
              </div>
            </div>

          </div>
        </div>
      </main>
    @endsection
