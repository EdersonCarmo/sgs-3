@php
  $idC = $idP = NULL;
  if(isset($rota['edit'])){
    $idC = $rota['edit']->id_cargo;
    $idP = $rota['edit']->id_pessoa;
  }
@endphp
{{--  TABPANEL COLABORADOR TABPANEL COLABORADOR TABPANEL COLABORADOR TABPANEL COLABORADOR TABPANEL COLABORADOR TABPANEL COLABORADOR TABPANEL COLABORADOR --}}
  <div class="tab-pane active show" id="home" role="tabpanel">

    <div class="row">
      @if (!isset($rota['edit']))
        <div class="col-4">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">LEADS</span>
            </div>
            <div class="form-control">
              <select class="js-example-basic-single col-12" name="id_pessoa" style="width: 300px">
                <option>Selecione ...</option>
                @foreach ($rota['pessoas'] as $key)
                  <option value="{{ $key->id_pessoa }}"{{ $key->id_pessoa==$idP?"selected":"" }}>
                    {{ substr($key->pessoa , 0, 40)}}
                  </option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
      @else
        <div class="col-6">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Pessoa</span>
            </div>
            <input type="text" name="nome" class="form-control" value="{{ $rota['edit']->pessoas->pessoa }}">
            {{-- <select id="select1" name="id_pessoa" class="form-control">
              <option>Selecione ...</option>
              @foreach ($rota['pessoas'] as $key)
                <option value="{{ $key->id_pessoa }}"{{ $key->id_pessoa==$idP?"selected":"" }}>{{ $key->pessoa }}</option>
              @endforeach
            </select> --}}
          </div>
        </div>
      @endif


        <div class="col-4">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Nome</span>
            </div>
            <input type="text" name="pessoa" class="form-control" value="{{ $rota['pessoa']->valor or old('pessoa') }}">
          </div>
        </div>

        <div class="col-4">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Tipo</span>
            </div>
            <select id="select1" name="id_tipo" class="form-control">
              <option>Selecione ...</option>
              {{-- @foreach ($rota['tipos'] as $key)
                <option value="{{ $key->id_tipo }}">{{ $key->tipo }}</option>
              @endforeach --}}
            </select>
          </div>
        </div>

      {{-- <div class="col-4">
      </div> --}}

      {{-- <div class="col-2">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Pontos</span>
          </div>
          <input type="text" name="pontos" class="form-control" value="{{ $rota['edit']->pontos or old('pontos') }}">
        </div>
      </div> --}}
    </div>

  </div>
  {{-- TABPANEL DOCUMENTOS  TABPANEL DOCUMENTOS  TABPANEL DOCUMENTOS  TABPANEL DOCUMENTOS  TABPANEL DOCUMENTOS  TABPANEL DOCUMENTOS  TABPANEL DOCUMENTOS --}}
  <div class="tab-pane" id="profile" role="tabpanel">
    <div class="row">

      <div class="col-4">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">CNH</span>
          </div>
          <input type="text" name="cnh" class="form-control" value="{{ $rota['edit']->dados->cnh or old('cnh') }}">
        </div>
      </div>

      <div class="col-4">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Catégoria</span>
          </div>
          <input type="text" name="cnh_categoria" class="form-control" value="{{ $rota['edit']->dados->cnh_categoria or old('cnh_categoria') }}" oninput="this.value = this.value.toUpperCase()">
        </div>
      </div>

      <div class="col-5">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">CNH Expedição</span>
          </div>
          <input type="date" name="cnh_dt_exp" class="form-control" value="{{ $rota['edit']->dados->cnh_dt_exp or old('cnh_dt_exp') }}">
        </div>
      </div>

      <div class="col-5">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">CNH Vencimento</span>
          </div>
          <input type="date" name="cnh_dt_vcto" class="form-control" value="{{ $rota['edit']->dados->cnh_dt_vcto or old('cnh_dt_vcto') }}">
        </div>
      </div>

    </div>
  </div>
  {{--  TABPANEL CONTRATO TABPANEL CONTRATO TABPANEL CONTRATO TABPANEL CONTRATO TABPANEL CONTRATO TABPANEL CONTRATO TABPANEL CONTRATO TABPANEL CONTRATO TABPANEL CONTRATO --}}
  {{-- <div class="tab-pane" id="contrato" role="tabpanel">
    <div class="row">

      <div class="col-4">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Contrato</span>
          </div>
          <input type="number" name="valor" class="form-control" value="{{ $rota['edit']->valor or old('valor') }}">
        </div>
      </div>

      <div class="col-4">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Data da multa</span>
          </div>
          <input type="date" name="dt_multa" class="form-control" value="{{ $rota['edit']->dt_multa or old('dt_multa') }}">
        </div>
      </div>

      <div class="col-4">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Veículo</span>
          </div>
          <select id="select1" name="id_veiculo" class="form-control">
            <option>Selecione ...</option>
          </select>
        </div>
      </div>

    </div>
  </div> --}}
  {{-- FORM PESSOAL --}}
  {{-- <div class="tab-pane" id="messages" role="tabpanel">

    <div class="row">

      <div class="col-4">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Motorista</span>
          </div>
          <select id="select1" name="id_motorista" class="form-control">
            <option>Selecione ...</option>
          </select>
        </div>
      </div>

      <div class="col-4">
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text">Vencimento</span>
          </div>
          <input type="date" name="dt_vencimento" class="form-control" value="{{ $rota['edit']->dt_vencimento or old('dt_vencimento') }}">
        </div>
      </div>

    </div>
  </div> --}}
