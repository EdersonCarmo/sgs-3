@php
$idC = $idT = $idEC = $idGI = $idCP = $idGC = $idUF = $contr = $demis = 0; //dd($rota['edit']->pessoa->end->cidade->uf);
$emp = $cargo = $idP = $ctps = $cid = $gen = 0;
$acl = $rota['acl'];
if(isset($rota['edit'])){
  $edit = $rota['edit'];
  $dtA = date('d/m/Y', strtotime($edit->created_at));
  $idC = $edit->id_colaborador;
  $idEC = $edit->pessoa->dados->id_estado_civil;
  $idGI = $edit->pessoa->dados->grau_instrucao;
  $idGC = $edit->pessoa->dados->grau_instrucao_completo;
  $idCP = $edit->pessoa->dados->cor_pele;
  $idT = $edit->id_hrm_tipo;
  $idUF = $edit->pessoa->dados->rg_id_uf;
  $ctps = $edit->pessoa->dados->ctps_id_uf;
  $emp = $edit->id_empresa;
  $contr = $edit->id_contratador;
  $demis = $edit->id_demissor;
  $cargo = $edit->id_cargo;
  $idP = $edit->id_pessoa;
  $cid = $edit->pessoa->end->id_cidade;
  $gen = $edit->pessoa->dados->genero;

}
@endphp

<div class="tab-content">
  {{-- TAB-PANE COLABORADOR TAB-PANE COLABORADOR TAB-PANE COLABORADOR TAB-PANE COLABORADOR TAB-PANE COLABORADOR TAB-PANE COLABORADOR --}}
  <div class="tab-pane active" id="colaborador" role="tabpanel">
    <div class="row">

      <div class="col-md-3 col-xs-12">
        <div class="card-body bg-dark">
          @isset($edit)
            <div class="col-xs-12">
                <img src="{{ asset('storage/COLABORADORES/'.$edit->id_pessoa.'/ADM/'.$edit->pessoa->arq['url']) }}" width="180"  alt="">
            </div>
            <div class="progress progress-xs m-2">
              <div class="progress-bar bg-success" role="progressbar" style="width: 50%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" title="50%"></div>
            </div>
          @endisset
        </div>

        {{-- ACL UPLOAD --}}
        @if ($acl->up == 1)
          <div class="">
            <div class="input-group">
              <input type="file" name="arq_pessoa" class="form-control" value="">
            </div>
          </div>
        @endif
        {{-- ACL UPLOAD --}}

          @if (!isset($rota['edit']))
            <div class="mt-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">LEADS</span>
                </div>
                <div class="form-control">
                  <select class="js-example-basic-single col-12" id="lead" name="id_pessoa2" style="width: 300px" onchange='getValor2(this)'>
                    <option value="">...</option>
                    @foreach ($rota['pessoas'] as $key)
                      <option value="{{ $key->id_pessoa }}" data-valor2="{!! $key->pessoa !!}" {{ $key->id_pessoa==$idP?"selected":"" }}>
                        {{ substr($key->pessoa , 0, 40)}}
                      </option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          @endif

      </div>

      <div class="col-md-9 col-xs-12">
        <div class="row">

        <div class="col-12">
        <div class="row">

          <div class="col-md-7 col-xs-12 mt-sm-5 mt-md-0">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Nome</span>
              </div>
              <input id="pessoa" type="text" name="pessoa" class="form-control" value="{{ $edit->pessoa->pessoa or old('pessoa') }}" oninput="this.value = this.value.toUpperCase()" >
            </div>
          </div>

          <div class="col-md-5 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Genero</span>
              </div>
              <select class="form-control" name="genero">
                <option value="1"{{ $idEC == 1?"selected":"" }}>Masculino</option>
                <option value="2"{{ $idEC == 2?"selected":"" }}>Feminino</option>
                <option value="3"{{ $idEC == 3?"selected":"" }}>Outros</option>
              </select>
            </div>
          </div>

        </div>
        </div>
        <div class="mt-3 col-12">
        <div class="row">

          <div class="col-md-7 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" title="Naturalidade">Naturalidade</span>
              </div>
              <input type="text" name="naturalidade" class="form-control" value="{{ $edit->pessoa->dados->naturalidade or old('naturalidade') }}" oninput="this.value = this.value.toUpperCase()" >
            </div>
          </div>

          <div class="col-md-5 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Estado Civil</span>
              </div>
              <select id="select1" name="id_hrm_tipo" class="form-control" >
                <option value="">Selecione ...</option>
                @foreach ($rota['civil'] as $key)
                  <option value="{{ $key->id_estado_civil }}" {{ $key->id_estado_civil==$idEC?"selected":"" }}>
                    {{ $key->estado_civil }}
                  </option>
                @endforeach
              </select>
            </div>
          </div>

        </div>
        </div>
        <div class="col-12 mt-3">
        <div class="row">

          <div class="col-md-7 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Pai</span>
              </div>
              <input type="text" name="pai" class="form-control" value="{{ $edit->pessoa->dados->pai or old('pai') }}" oninput="this.value = this.value.toUpperCase()" >
            </div>
          </div>

          <div class="col-md-5 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Nascimento</span>
              </div>
              <input type="date" name="dt_nascimento" class="form-control" value="{{ $edit->pessoa->dados->dt_nascimento or old('dt_nascimento') }}">
            </div>
          </div>

        </div>
        </div>
        <div class="col-12 mt-3">
        <div class="row">

          <div class="col-md-7 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Mâe</span>
              </div>
              <input type="text" name="mae" class="form-control" value="{{ $edit->pessoa->dados->mae or old('mae') }}" oninput="this.value = this.value.toUpperCase()" >
            </div>
          </div>

          <div class="col-md-5 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Casamento</span>
              </div>
              <input type="date" name="dt_casamento" class="form-control" value="{{ $edit->pessoa->dados->dt_casamento or old('dt_casamento') }}">
            </div>
          </div>

        </div>
        </div>

        <div class="col-12 mt-3">
        <div class="row">
          <div class="col-md-8 col-xs-12">
          <div class="row">
            <div class="input-group-prepend">
              <span class="">Escolaridade</span>
            </div>
            <div class="custom-control custom-checkbox ml-2">
              <input name="grau_instrucao" id="radio1" type="radio" value="1" class="custom-control-input" {{ $idGI == 1?"checked":"" }}>
              <label class="custom-control-label" for="radio1">Primário</label>
            </div>
            <div class="custom-control custom-checkbox ml-2">
              <input name="grau_instrucao" id="radio2" type="radio" value="2" class="custom-control-input" {{ $idGI == 2?"checked":"" }}>
              <label class="custom-control-label" for="radio2">Fundamental</label>
            </div>
            <div class="custom-control custom-checkbox ml-2">
              <input name="grau_instrucao" id="radio3" type="radio" value="3" class="custom-control-input" {{ $idGI == 3?"checked":"" }}>
              <label class="custom-control-label" for="radio3">Médio</label>
            </div>
            <div class="custom-control custom-checkbox ml-2 mr-2">
              <input name="grau_instrucao" id="radio4" type="radio" value="4" class="custom-control-input" {{ $idGI == 4?"checked":"" }}>
              <label class="custom-control-label" for="radio4">Superior</label>
            </div>
          </div>
          </div>

          <div class="col-md-3 col-xs-12">
            <div class="row">
              <div class="col-7">
                <span class="input-text" title="Completo">Completo</span>
              </div>
              <div class="col-5">
                <label class="switch switch-label switch-pill switch-outline-success ml-xs-2">
                  <input name="grau_instrucao_completo" type="checkbox" class="switch-input" {{ $idGC == 0?"":"checked" }}>
                  <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
                </label>
              </div>
            </div>
          </div>

        </div>
        </div>

        <div class="row col-md-12 col-xs-12">
          <div class="input-group-prepend">
            <span class="">Cor da pele</span>
          </div>
          <div class="custom-control custom-checkbox ml-3">
            <input name="cor_pele" id="radio9" type="radio" value="1" class="custom-control-input" {{ $idCP == 1?"checked":"" }}>
            <label class="custom-control-label" for="radio9">Amarela</label>
          </div>
          <div class="custom-control custom-checkbox ml-3">
            <input name="cor_pele" id="radio7" type="radio" value="2" class="custom-control-input" {{ $idCP == 2?"checked":"" }}>
            <label class="custom-control-label" for="radio7">Branca</label>
          </div>
          <div class="custom-control custom-checkbox ml-3">
            <input name="cor_pele" id="radio10" type="radio" value="3" class="custom-control-input" {{ $idCP == 3?"checked":"" }}>
            <label class="custom-control-label" for="radio10">Indígena</label>
          </div>
          <div class="custom-control custom-checkbox ml-3">
            <input name="cor_pele" id="radio11" type="radio" value="4" class="custom-control-input" {{ $idCP == 4?"checked":"" }}>
            <label class="custom-control-label" for="radio11">Parda</label>
          </div>
          <div class="custom-control custom-checkbox ml-3">
            <input name="cor_pele" id="radio8" type="radio" value="5" class="custom-control-input" {{ $idCP == 5?"checked":"" }}>
            <label class="custom-control-label" for="radio8">Preta</label>
          </div>
          <div class="custom-control custom-checkbox ml-3">
            <input name="cor_pele" id="radio12" type="radio" value="6" class="custom-control-input" {{ $idCP == 6?"checked":"" }}>
            <label class="custom-control-label" for="radio12">Outras</label>
          </div>
        </div>

      </div>
      </div>
    </div>

  </div>
  {{-- TAB-PANE COLABORADOR TAB-PANE COLABORADOR TAB-PANE COLABORADOR TAB-PANE COLABORADOR TAB-PANE COLABORADOR TAB-PANE COLABORADOR --}}
  {{-- TAB-PANE ENDEREÇO TAB-PANE ENDEREÇO TAB-PANE ENDEREÇO TAB-PANE ENDEREÇO TAB-PANE ENDEREÇO TAB-PANE ENDEREÇO --}}
  <div class="tab-pane" id="endereco" role="tabpanel">
    <div class="row">

      <div class="col-md-12 col-xs-12">
      <div class="row">
        <div class="col-md-12 col-xs-12">
        <div class="row">


          <div class="col-md-5 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Cidade</span>
              </div>
              <div class="form-control">
              <select class="js-example-basic-single col-12" style="width: 250px" name="id_cidade" class="form-control" required>
                <option value="">Selecione ...</option>
                @foreach ($rota['cidades'] as $key)
                  <option value="{{ $key->id_cidade }}" {{ $cid==$key->id_cidade?"selected":"" }}>
                    {{ $key->cidade }}
                  </option>
                @endforeach
              </select>
            </div>
            </div>
          </div>

          <div class="col-md-3 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">CEP</span>
              </div>
              <input type="text" id="cep" name="cep" class="form-control" value="{{ $edit->pessoa->dados->cep or old('cep') }}" onkeydown="javascript: fMasc( this, mCEP );">
            </div>
          </div>

          @if (isset($edit->pessoa->end->arq['url']))
            <div class="">
                <a href="{{ asset('storage/COLABORADORES/'.$edit->id_pessoa.'/END/'.$edit->pessoa->end->arq['url']) }}" target="new" title="Endereço" class="btn btn-block btn-ghost-success">
                <i class="fas fa-map-marker-alt"></i>
              </a>
            </div>
          @endif

        </div>
        </div>
        <div class="mt-3 col-md-12 col-xs-12">
        <div class="row">

          <div class="col-md-5 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Bairro</span>
              </div>
              <input type="text" id="bairro" name="bairro" class="form-control" value="{{ $edit->pessoa->end->bairro or old('bairro') }}" oninput="this.value = this.value.toUpperCase()" >
            </div>
          </div>

          <div class="col-md-3 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" title="Naturalidade">Número</span>
              </div>
              <input type="text" name="num" class="form-control" value="{{ $edit->pessoa->dados->num or old('num') }}" oninput="this.value = this.value.toUpperCase()" >
            </div>
          </div>
          {{-- ACL UPLOAD --}}
          @if ($acl->up == 1)
            <div class="col-md-4 col-xs-12">
              <div class="input-group">
                <input type="file" name="arq_endereco" class="form-control" value="">
              </div>
            </div>
          @endif
          {{-- ACL UPLOAD --}}

      </div>
      </div>
      <div class="mt-3 col-md-12 col-xs-12">
        <div class="row">

          <div class="col-md-6 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Logradouro</span>
              </div>
              <input type="text" id="rua" name="logradouro" class="form-control" value="{{ $edit->pessoa->end->logradouro or old('logradouro') }}" oninput="this.value = this.value.toUpperCase()" >
            </div>
          </div>

          <div class="col-md-5 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Complemento</span>
              </div>
              <input type="text" name="complemento" class="form-control" value="{{ $edit->pessoa->dados->complemento or old('complemento') }}" oninput="this.value = this.value.toUpperCase()" >
            </div>
          </div>
        </div>
        </div>


      </div>
      <hr>
      <div class="row">

        <div class="col-md-3 col-xs-12">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Fixo</span>
            </div>
            <input type="text" id="fixo1" name="fixo1" maxlength="13" class="form-control" value="{{ $edit->pessoa->contato->fixo1 or old('fixo1') }}" onkeydown="javascript: fMasc( this, mFIXO );">
          </div>
        </div>

        <div class="col-md-3 col-xs-12">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Fixo</span>
            </div>
            <input type="text" id="fixo2" name="fixo2" maxlength="13" class="form-control" value="{{ $edit->pessoa->contato->fixo2 or old('fixo2') }}" onkeydown="javascript: fMasc( this, mFIXO );">
          </div>
        </div>

        <div class="col-md-3 col-xs-12">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Cel</span>
            </div>
            <input type="text" id="cel1" name="cel1" maxlength="15" class="form-control" value="{{ $edit->pessoa->contato->cel1 or old('cel1') }}" onkeydown="javascript: fMasc( this, mCEL );">
          </div>
        </div>

        <div class="col-md-3 col-xs-12">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Cel</span>
            </div>
            <input type="text" id="cel2" name="cel2" maxlength="15" class="form-control" value="{{ $edit->pessoa->contato->cel2 or old('cel2') }}" onkeydown="javascript: fMasc( this, mCEL );">
          </div>
        </div>

        <div class="col-md-6 col-xs-12 mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">E-Mail</span>
            </div>
            <input type="text" id="email1" name="email1" class="form-control" value="{{ $edit->pessoa->contato->email1 or old('email1') }}" >
          </div>
        </div>

        <div class="col-md-6 col-xs-12 mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">E-Mail</span>
            </div>
            <input type="text" id="email2" name="email2" class="form-control" value="{{ $edit->pessoa->contato->email2 or old('email2') }}" >
          </div>
        </div>

      </div>
      </div>
    </div>

  </div>
  {{-- TAB-PANE ENDEREÇO TAB-PANE ENDEREÇO TAB-PANE ENDEREÇO TAB-PANE ENDEREÇO TAB-PANE ENDEREÇO TAB-PANE ENDEREÇO --}}
  {{-- TAB-PANE DOCUMENTO TAB-PANE DOCUMENTO TAB-PANE DOCUMENTO TAB-PANE DOCUMENTO TAB-PANE DOCUMENTO TAB-PANE DOCUMENTO --}}
  <div class="tab-pane" id="documento" role="tabpanel">
    <div class="row">
      <div class="col-md-4 col-xs-12">

        <div class="">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">RG</span>
            </div>
            <input type="text" name="rg" class="form-control" value="{{ $edit->pessoa->dados->rg or old('rg') }}" onkeydown="javascript: fMasc( this, mCEP );" >
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text" title="Data de expedição do RG">Expedição</span>
            </div>
            <input type="date" name="rg_dt_exp" class="form-control" value="{{ $edit->pessoa->dados->rg_dt_exp or old('rg_dt_exp') }}">
          </div>
        </div>

        <div class="row mt-3">
          <div class="col-md-6 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" title="Orgão expedidor do RG">Orgão</span>
              </div>
              <input type="text" name="rg_orgao" class="form-control" value="{{ $edit->pessoa->dados->rg_orgao or old('rg_orgao') }}" oninput="this.value = this.value.toUpperCase()" >
            </div>
          </div>

          <div class="col-md-6 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">UF</span>
              </div>
              <select id="select1" name="rg_id_uf" class="form-control" >
                <option value="">Selecione ...</option>
                @foreach ($rota['ufs'] as $key)
                  <option value="{{ $key->id_uf }}" {{ $idUF==$key->id_uf?"selected":"" }}>
                    {{ $key->uf }}
                  </option>
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Título Eleitor</span>
            </div>
            <input type="text" name="eleitor" class="form-control" value="{{ $edit->pessoa->dados->eleitor or old('eleitor') }}" oninput="this.value = this.value.toUpperCase()" >
          </div>
        </div>

        <div class="row mt-3">
          <div class="col-md-6 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Zona</span>
              </div>
              <input type="text" name="eleitor_zona" class="form-control" value="{{ $edit->pessoa->dados->eleitor_zona or old('eleitor_zona') }}" oninput="this.value = this.value.toUpperCase()" >
            </div>
          </div>
          <div class="col-md-6 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Seção</span>
              </div>
              <input type="text" name="eleitor_secao" class="form-control" value="{{ $edit->pessoa->dados->eleitor_secao or old('eleitor_secao') }}" oninput="this.value = this.value.toUpperCase()" >
            </div>
          </div>
        </div>

      </div>
      <div class="col-md-4 col-xs-12">

        <div class="">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">CPF</span>
            </div>
            <input type="text" name="cpf" class="form-control" value="{{ $edit->pessoa->dados->cpf or old('cpf') }}" onkeydown="javascript: fMasc( this, mCPF );" >
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">CNH</span>
            </div>
            <input type="text" name="cnh" class="form-control" value="{{ $edit->pessoa->dados->cnh or old('cnh') }}" oninput="this.value = this.value.toUpperCase()" >
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Categoria</span>
            </div>
            <select id="select1" name="categoria" class="form-control" >
              <option value="0">Selecione ...</option>
              <option value="1">A</option>
              <option value="2">AB</option>
              <option value="3">B</option>
              <option value="3">AC</option>
              <option value="4">C</option>
              <option value="5">AD</option>
              <option value="6">D</option>
              <option value="7">AE</option>
              <option value="8">E</option>
            </select>
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Expedição</span>
            </div>
            <input type="date" name="cnh_dt_exp" class="form-control" value="{{ $edit->pessoa->dados->cnh_dt_exp or old('cnh_dt_exp') }}">
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Vencimento</span>
            </div>
            <input type="date" name="cnh_dt_vcto" class="form-control" value="{{ $edit->pessoa->dados->cnh_dt_vcto or old('cnh_dt_vcto') }}">
          </div>
        </div>

      </div>
      <div class="col-md-4 col-xs-12">

        <div class="">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">CTPS</span>
            </div>
            <input type="text" name="ctps" class="form-control" value="{{ $edit->pessoa->dados->ctps or old('ctps') }}" oninput="this.value = this.value.toUpperCase()" >
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Expedição</span>
            </div>
            <input type="date" name="ctps_dt_exp" class="form-control" value="{{ $edit->pessoa->dados->ctps_dt_exp or old('ctps_dt_exp') }}">
          </div>
        </div>

        <div class="row mt-3">
          <div class="col-md-6 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Série</span>
              </div>
              <input type="text" name="ctps_serie" class="form-control" value="{{ $edit->pessoa->dados->ctps_serie or old('ctps_serie') }}" oninput="this.value = this.value.toUpperCase()" >
            </div>
          </div>

          <div class="col-md-6 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">UF</span>
              </div>
              <select id="select1" name="rg_id_uf" class="form-control" >
                <option value="">Selecione ...</option>
                @foreach ($rota['ufs'] as $key)
                  <option value="{{ $key->id_uf }}" {{ $ctps==$key->id_uf?"selected":"" }}>
                    {{ $key->uf }}
                  </option>
                @endforeach
              </select>
            </div>
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">NIT</span>
            </div>
            <input type="text" name="nit" class="form-control" value="{{ $edit->pessoa->dados->nit or old('nit') }}" oninput="this.value = this.value.toUpperCase()" >
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">CNIS-PIS</span>
            </div>
            <input type="text" name="cnis" class="form-control" value="{{ $edit->pessoa->dados->cnis or old('cnis') }}" oninput="this.value = this.value.toUpperCase()" >
          </div>
        </div>

      </div>
      <div class="col-12 mt-3 form-control">
        <div class="row">
          {{-- ACL UPLOAD --}}
          @if ($acl->up == 1)
            <div class="col-md-4 colxs-12">
              <div class="input-group">
                <input type="file" name="arq_dado" class="form-control" value="">
              </div>
            </div>
          @endif
          {{-- ACL UPLOAD --}}
          <div class="col-md-4 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Nome arquivo</span>
              </div>
              <select id="select1" name="nm_arq_dado" class="form-control">
                <option value="">Selecione ...</option>
                @foreach ($rota['tipos'] as $key)
                  @if ($key->id_modulo == 65)
                    <option value="{{ $key->hrm_tipo }}" {{ $key->id_hrm_tipo==$idT?"selected":"" }}>
                      {{ $key->hrm_tipo }}
                    </option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>

          @if (isset($rota['edit']) && $edit->pessoa->dados->arq->count() > 0)
            <div class="col-md-4 col-xs-12">
              <div class="row">
                @foreach ($edit->pessoa->dados->arq as $key1)
                  <div class="btn-group">
                    <button class="btn btn-ghost-primary" type="button" onclick="window.open('{{ asset('storage/COLABORADORES/'.$edit->id_pessoa.'/DOCUMENTO/'.$key1->url) }}', '_blank');" title="Documento">
                      <i class="far fa-id-card"></i>
                    </button>
                    <button class="btn btn-sm btn-ghost-primary dropdown-toggle dropdown-toggle-split" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(49px, 35px, 0px);">
                      @if ($acl->d == 1)
                        <div class="">
                          <a href="{{route('hrm_arq.edit',$key1->id_arquivo) }}" class="btn btn-block btn-ghost-danger" title="Editar" onclick="return confirm('Confirma a exclusão?')">
                            <i class="far fa-trash-alt"></i>
                          </a>
                        </div>
                      @else
                        <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                      @endif
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          @endif

        </div>
      </div>

    </div>
  </div>
  {{-- TAB-PANE DOCUMENTO TAB-PANE DOCUMENTO TAB-PANE DOCUMENTO TAB-PANE DOCUMENTO TAB-PANE DOCUMENTO TAB-PANE DOCUMENTO --}}
  {{-- TAB-PANE DEPENDENTES TAB-PANE DEPENDENTES TAB-PANE DEPENDENTES TAB-PANE DEPENDENTES TAB-PANE DEPENDENTES TAB-PANE DEPENDENTES --}}
  @if (isset($rota['edit']))
  <div class="tab-pane " id="dependente" role="tabpanel">
    <div class="row">
      <div class="col-1">
        <div class="">
          <a href="{{route('dependente.show',$rota['edit']->id_pessoa) }}" class="btn btn-block btn-ghost-warning" target="new" title="Editar">
            <i class="far fa-edit"></i>
          </a>
        </div>
    </div>
    <div class="row col-11">
        @foreach ($rota['edit']->filhos as $filho)
          <div class=" col-md-6 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Nome</span>
              </div>
              <input type="text" name="ctps" class="form-control" value="{{ $filho->dependente }}" readonly>
            </div>
          </div>

          <div class="col-3">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Tipo</span>
              </div>
              @foreach ($rota['tipos'] as $key)
              @if ($key->id_hrm_tipo==$filho->id_hrm_tipo)
                <input type="text" name="ctps" class="form-control" value="{{ $key->hrm_tipo }}" readonly>
              @endif
            @endforeach


            </div>
          </div>

          <div class=" col-md-3 col-xs-12 mb-2">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">CPF</span>
              </div>
              <input type="text" name="ctps" class="form-control" value="{{ $filho->cpf }}" readonly>
            </div>
          </div>
        @endforeach
      </div>

    </div>
  </div>
@endif
  {{-- TAB-PANE DEPENDENTES TAB-PANE DEPENDENTES TAB-PANE DEPENDENTES TAB-PANE DEPENDENTES TAB-PANE DEPENDENTES TAB-PANE DEPENDENTES --}}
  {{-- TAB-PANE CONTRATO TAB-PANE CONTRATO TAB-PANE CONTRATO TAB-PANE CONTRATO TAB-PANE CONTRATO TAB-PANE CONTRATO --}}
  <div class="tab-pane " id="contrato" role="tabpanel">
    <div class="row">
      <div class="col-md-5 col-xs-12">

        <div class="">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Empresa</span>
            </div>
            <select id="select1" name="id_empresa" class="form-control" required>
              <option value="">Selecione ...</option>
              @foreach ($rota['empresas'] as $key)
                <option value="{{ $key->id_empresa }}" {{ $key->id_empresa==$emp?"selected":"" }}>
                  {{ $key->empresa }}
                </option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Contratante</span>
            </div>
            <select id="select1" name="id_contratador" class="form-control" required>
              <option value="">Selecione ...</option>
              @foreach ($rota['colaboradores'] as $key)
                <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador==$contr?"selected":"" }}>
                  {{ $key->pessoas['pessoa'] }}
                </option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Demissor</span>
            </div>
            <select id="select1" name="id_demissor" class="form-control"  >
              <option value="">Selecione ...</option>
              @foreach ($rota['colaboradores'] as $key)
                <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador==$demis?"selected":"" }}>
                  {{ $key->pessoas['pessoa'] }}
                </option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="row mt-3">
          <div class="col-md-6 col-xs-12">
            <div class="input-group" title="Horário de entrada período da manhã">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-clock"></i></span>
              </div>
              <input type="time" name="hora_1" class="form-control" oninput="this.value = this.value.toUpperCase()" value="{{ $edit->hora_1 or old('hora_1') }}" >
            </div>
          </div>
          <div class="col-md-6 col-xs-12">
            <div class="input-group" title="Horário de saída período da manhã">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-clock"></i></span>
              </div>
              <input type="time" name="hora_2" class="form-control" oninput="this.value = this.value.toUpperCase()" value="{{ $edit->hora_2 or old('hora_2') }}" >
            </div>
          </div>
        </div>

        <div class="row mt-3">
          <div class="col-md-6 col-xs-12">
            <div class="input-group" title="Horário de entrada período da tarde">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-clock"></i></span>
              </div>
              <input type="time" name="hora_3" class="form-control" oninput="this.value = this.value.toUpperCase()" value="{{ $edit->hora_3 or old('hora_3') }}" >
            </div>
          </div>
          <div class="col-md-6 col-xs-12">
            <div class="input-group" title="Horário de saída período da tarde">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-clock"></i></span>
              </div>
              <input type="time" name="hora_4" class="form-control" oninput="this.value = this.value.toUpperCase()" value="{{ $edit->hora_4 or old('hora_4') }}" >
            </div>
          </div>
        </div>

      </div>
      <div class="col-md-3 col-xs-12">

        <div class="">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Salário</span>
            </div>
            <input type="number" name="salario" class="form-control" value="{{ $edit->salario or old('salario') }}" >
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Cargos</span>
            </div>
            <select id="select1" name="id_cargo" class="form-control" required>
              <option value="">Selecione ...</option>
              @foreach ($rota['cargos'] as $key)
                <option value="{{ $key->id_cargo }}" {{ $key->id_cargo==$cargo?"selected":"" }}>
                  {{ $key->cargo }}
                </option>
              @endforeach
            </select>
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text" title="Data de expedição do RG">Registro</span>
            </div>
            <input type="number" name="registro" class="form-control" value="{{ $edit->registro or old('registro') }}">
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text" title="Orgão expedidor do RG">Comissão</span>
            </div>
            <input type="number" step="0.1" name="comissao" class="form-control" placeholder="0,0" value="{{ $edit->comissao or old('comissao') }}" >
          </div>
        </div>

        <div class="mt-3">
          <div class="row">
            <div class="col-8">
              <span class="input-text" title="Periculosidade">Periculosidade</span>
            </div>
            <div class="col-4">
              <label class="switch switch-label switch-pill switch-outline-success ml-2">
                <input name="periculosidade" type="checkbox" class="switch-input" {{ isset($edit)?$edit->periculosidade == 0?"":"checked":"" }}>
                <span class="switch-slider" data-checked="Sim" data-unchecked="Não"></span>
              </label>
            </div>
          </div>
        </div>

      </div>
      <div class="col-md-4 col-xs-12">
        @if (isset($rota['edit']))
          <div class="mb-3">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Cadastro</span>
              </div>
              <input type="text" name="cadastroo" class="form-control" value="{{ $dtA }}" disabled>
            </div>
          </div>
        @endif

        <div class="">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Admissão</span>
            </div>
            <input type="date" name="dt_admissao" class="form-control" value="{{ $edit->dt_admissao or old('dt_admissao') }}"  >
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Renovação</span>
            </div>
            <input type="date" name="dt_renovacao" class="form-control" value="{{ $edit->dt_renovacao or old('dt_renovacao') }}"  >
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Efetivação</span>
            </div>
            <input type="date" name="dt_efetivacao" class="form-control" value="{{ $edit->dt_efetivacao or old('dt_efetivacao') }}"  >
          </div>
        </div>

        <div class="mt-3">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Demissão</span>
            </div>
            <input type="date" name="dt_demissao" class="form-control" value="{{ $edit->dt_demissao or old('dt_demissao') }}" >
          </div>
        </div>

      </div>

      <div class="col-12 mt-3 form-control">
        <div class="row">
          {{-- ACL UPLOAD --}}
          @if ($acl->up == 1)
            <div class="col-md-4 colxs-12">
              <div class="input-group">
                <input type="file" name="arq_contrato" class="form-control" value="">
              </div>
            </div>
          @endif
          {{-- ACL UPLOAD --}}
          <div class="col-md-4 col-xs-12">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Nome arquivo</span>
              </div>
              <select id="select1" name="nm_arq_contrato" class="form-control">
                <option value="">Selecione ...</option>
                @foreach ($rota['tipos'] as $key)
                  @if ($key->id_modulo == 66)
                    <option value="{{ $key->hrm_tipo }}" {{ $key->id_hrm_tipo==$idT?"selected":"" }}>
                      {{ $key->hrm_tipo }}
                    </option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>

          @if (isset($rota['edit']) && $edit->arq->count() > 0)
            <div class="col-md-4 col-xs-12">
              <div class="row">
                @foreach ($edit->arq as $key1)
                  <div class="btn-group">
                    <button class="btn btn-ghost-primary" type="button" onclick="window.open('{{ asset('storage/COLABORADORES/'.$edit->id_pessoa.'/CONTRATO/'.$key1->url) }}');" title="Contrato">
                      <i class="fas fa-file-contract"></i>
                    </button>
                    <button class="btn btn-sm btn-ghost-primary dropdown-toggle dropdown-toggle-split" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(49px, 35px, 0px);">
                      @if ($acl->d == 1)
                        <div class="">
                          <a href="{{route('hrm_arq.edit',$key1->id_arquivo) }}" class="btn btn-block btn-ghost-danger" title="Editar" onclick="return confirm('Confirma a exclusão?')">
                            <i class="far fa-trash-alt"></i>
                          </a>
                        </div>
                      @else
                        <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                      @endif
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          @endif

        </div>
      </div>

    </div>
  </div>
  {{-- TAB-PANE CONTRATO TAB-PANE CONTRATO TAB-PANE CONTRATO TAB-PANE CONTRATO TAB-PANE CONTRATO TAB-PANE CONTRATO --}}
  {{-- TAB-PANE PRINT TAB-PANE PRINT TAB-PANE PRINT TAB-PANE PRINT TAB-PANE PRINT TAB-PANE PRINT  TAB-PANE PRINT --}}
  <div class="tab-pane " id="print" role="tabpanel">
    <div class="">
      {{-- DATATABLE --}}
      <div id="DataTables_Table_0_wrapper2" class="dataTables_wrapper  dt-bootstrap4 no-footer mt-3">

        <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
        aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
        <thead>
          <tr role="row">
            <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
              Documento
            </th>
            <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
              Descrição
            </th>
            <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 90px;">
              Ação
            </th>
          </tr>
        </thead>
        {{-- LIST --}}
        <tbody>
        </div>
          @foreach ($rota[$rota['rota']] as $key)
            <tr>
              <td>
                  {{ $key[$rota['rota']] }}
              </td>
              <td>
                  {{ $key['instituicao'] }}
              </td>
            <td>
              {{-- ACL ACOES TABLE --}}
              <div class="row">
                @if (isset($key->arquivo))
                  @if ($acl->dw == 1)
                    <div class="">
                      <a href="{{ asset('storage/'.$rota['pasta'].$key->arquivo)}}" target="new" title="{{$key->arquivo}}" class="btn btn-sm btn-block btn-ghost-success">
                        <i class="far fa-print"></i>
                      </a>
                    </div>
                  @else
                    <div class="btn btn-sm text-secondary"><i class="far fa-print"></i></div>
                  @endif
                @else
                  <div class="btn btn-sm text-dark"><i class="fas fa-ban"></i></div>
                @endif

                @if ($acl->u == 1)
                  <div class="">
                    <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-sm btn-block btn-ghost-warning" title="Editar">
                      <i class="far fa-edit"></i>
                    </a>
                  </div>
                @else
                  <div class="btn btn-sm text-secondary"><i class="far fa-edit"></i></div>
                @endif

                @if ($acl->d == 1)
                  <div class="">
                    <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-sm btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                        <i class="far fa-trash-alt"></i>
                      </button>
                    </form>
                  </div>
                @else
                  <div class="btn btn-sm text-secondary"><i class="far fa-trash-alt"></i></div>
                @endif
              </div>
              {{-- ACL ACOES TABLE --}}
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>

    </div>
  </div>
  {{-- TAB-PANE PRINT TAB-PANE PRINT TAB-PANE PRINT TAB-PANE PRINT TAB-PANE PRINT TAB-PANE PRINT TAB-PANE PRINT  --}}

</div>
