@extends('layouts.sgs')

@section('content')
<main class="main">
  <div class="container-fluid">
  <div class="row justify-content-center">
    <div class="col-md-12">

      <center>

        <h1>Cadastro de empresas</h1><br><br>
        <form id="myForm" style="width: 40%" method="get">		<!-- form id must -->
          <input type="text" class="form-control" name="fname" id="fname" placeholder="First Name"><br>
          <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name"><br>
          <input type="text" class="form-control" name="email" id="email" placeholder="Email"><br>
          <input type="text" class="form-control" name="password" id="password" placeholder="Password"><br>
          <button type="submit" class="btn btn-warning" name="submit" id="submit">Submit</button>  <!-- button id must -->
        </form>
      </center>

    </div>
  </div>
  </main>
@endsection
