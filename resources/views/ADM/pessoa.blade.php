
@extends('layouts.sgs')

@section('content')
  @php
    $acl = $rota['acl'];
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form">
                  @method('PUT')
                @else
                  <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form">
                    <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                  @endif
                  @csrf
                  <div class="row justify-content-between">
                    <div class="col-8">
                      <div class="form-group">
                        <label for="exampleInputName2">CADASTRO DE {{ strtoupper($rota['rota']) }} </label>
                      </div>
                    </div>
                    {{-- ACL FORM CAD --}}
                    <div class="col-1">
                      @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                    </div>
                    {{-- ACL FORM CAD --}}
                  </div>

                    <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}">
                      <div class="row">

                        <div class="col-4">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Nome</span>
                            </div>
                            <input type="text" name="pessoa" class="form-control" value="{{ $rota['pessoa']->valor or old('pessoa') }}">
                          </div>
                        </div>

                        <div class="col-4">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Tipo</span>
                            </div>
                            <select id="select1" name="id_tipo" class="form-control">
                              <option>Selecione ...</option>
                              @foreach ($rota['tipos'] as $key)
                                <option value="{{ $key->id_tipo }}">{{ $key->tipo }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>

                        <div class="col-4">
                          <button type="submit" class="btn btn-outline-success" id="submit">
                            {{ isset($rota['edit'])?"Alterar":"Cadastrar"}}</button>
                          </div>
                        </div>

                      </div>
                    </form>
                  </div>

                  <div class="car-body">

                    <div class="form-control">
                      <div class="row">
                        <div class="col-8">

                          @foreach (range("0","9") as $element)
                            <a href="{{route($rota['rota'].'.show',$element) }}"> {{ $element }} </a>
                          @endforeach
                          @foreach (range("A","Z") as $element)
                            <a href="{{route($rota['rota'].'.show',$element) }}"> {{ $element }} </a>
                          @endforeach
                          <form class="" action="{{ route($rota['rota'].'.index') }}" method="GET">
                            @csrf
                          </div>
                          <div class="col-sm-4">

                            <div class="input-group">
                              <span class="input-group-prepend">
                                <button type="submit" class="btn btn-dark btn-sm">
                                  <i class="fa fa-search"></i> ID:
                                </button>
                              </span>
                              <input type="text" id="input1-group2" name="id_pessoa" class="form-control" placeholder="12345">
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    {{-- DATATABLE --}}
                    <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                      <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                      aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                      <thead>
                        <tr role="row">
                          <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 215px;">
                            Nome
                          </th>
                          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                            Status
                          </th>
                          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                            Ação
                          </th>
                        </tr>
                      </thead>
                      {{-- LIST --}}
                      <tbody>
                        @foreach ($rota['pess'] as $key)
                          <tr>
                            <td>{{ $key[$rota['rota']]}}</td>
                            <td>
                              <span class="badge badge-{{ $key->tipo=1?'success':'secondary' }}">
                                {{ $key->tipo=1?'ATIVO':'INATIVO' }}
                              </span>
                            </td>
                            <td>
                              {{-- ACL ACOES TABLE --}}
                              <div class="row">
                                @if ($acl->u == 1)
                                  <div class="">
                                    <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                      <i class="far fa-edit"></i>
                                    </a>
                                  </div>
                                @else
                                  <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                                @endif

                                @if ($acl->d == 1)
                                  <div class="">
                                    <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                      @csrf
                                      @method('DELETE')
                                      <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                        <i class="far fa-trash-alt"></i>
                                      </button>
                                    </form>
                                  </div>
                                @else
                                  <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                                @endif
                              </div>
                              {{-- ACL ACOES TABLE --}}
                            </td>
                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>

                </div>
                </div>
              </div>

            </div>
          </div>
        </main>
      @endsection
