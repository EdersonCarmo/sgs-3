@extends('layouts.sgs')

@section('content')
  @php
  $idV = $idM = $idF = $idC = $idSet = 0;
  $idS = 1;
  $acl = $rota['acl'];
  if(isset($rota['edit'])){
    $idS = $rota['edit']->sts_solicitacao;
    $idC = $rota['edit']->id_colaborador;
    $idSet = $rota['edit']->id_setor;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
                <div class="row justify-content-between">

                  <div class="col-1" style="display: {{ isset($rota['edit'])?'':'none'}}">
                    @if (isset($rota['edit']) and $rota['edit']->sts_compra == 1)
                      {{-- CONFIRMA COMPRA --}}
                        <form method="POST" action="{{route($rota['rota'].'.update',$rota['edit']->id_compra) }}">
                          @csrf
                          @method('PUT')
                          <input type="hidden" name="confirma" value="true">
                          <button type="submit" class="btn btn-block btn-outline-warning" title="Confirma e pedido de compra" onclick="return confirm('Após confirmar NÃO será possivel realizar alterações no pedido. Confirma o pedido?')">
                            <i class="fas fa-clipboard-check fa-2x"></i>
                          </button>
                        </form>
                    @elseif (isset($rota['edit']) and $rota['edit']->sts_compra == 2 and $acl->dw == 1)
                      {{-- AUTORIZA COMPRA --}}
                      <form method="POST" action="{{route($rota['rota'].'.update',$rota['edit']->id_compra) }}">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="autoriza" value="true">
                        <button type="submit" class="btn btn-block btn-outline-warning" title="Autoriza o pedido de compra" onclick="return confirm('Após confirmar NÃO será possivel realizar alterações no pedido. Confirma o pedido?')">
                          <i class="far fa-check-circle fa-2x"></i>
                        </button>
                      </form>
                    @elseif (isset($rota['edit']) and $rota['edit']->sts_compra == 3 and $acl->f == 1)
                      {{-- REALIZA COMPRA --}}
                      <form method="POST" action="{{route($rota['rota'].'.update',$rota['edit']->id_compra) }}">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="finaliza" value="true">
                        <button type="submit" class="btn btn-block btn-outline-warning" title="Confirma efetivação da compra" onclick="return confirm('Após confirmar NÃO será possivel realizar alterações no pedido. Confirma o pedido?')">
                          <i class="fas fa-shopping-basket fa-2x"></i>
                        </button>
                      </form>
                    {{-- @elseif (isset($rota['edit']) and $rota['edit']->sts_pedido == 4 and $acl->dw == 1)
                      <button type="button" class="btn btn-outline-success ml-4" onClick="document.formBaixa.submit()">
                        Baixar Estoque
                      </button> --}}
                    @endif
                      <br>
                  </div>

                    <div class="col-10">
                      <div class="form-group">
                        <label for="exampleInputName2">COMPRA{{ isset($rota['edit'])? ': '.$rota['edit']->id_compra:'' }} </label>
                      </div>
                    </div>
                    {{-- ACL FORM CAD --}}
                    <div class="col-1">
                      @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                    </div>
                    {{-- ACL FORM CAD --}}
                  </div>


                    @if (isset($rota['edit']))
                      <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form" enctype="multipart/form-data">
                        @method('PUT')
                    @else
                      <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form" enctype="multipart/form-data">
                        <input type="hidden" name="id_user" value="{{ Auth::id() }}">
                    @endif
                      @csrf


                    <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}} col-12" style="display: none">
                      <div class="row">

                      <div class="col-6">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Descrição</span>
                          </div>
                          <textarea name="compra" rows="6" class="form-control">{{ $rota['edit']->compra or '' }}</textarea>
                        </div>

                        <div class="row mt-3">


                          <div class="col-6">

                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text">Setor</span>
                              </div>
                              <select id="select1" name="id_setor" class="form-control" required>
                                @foreach ($rota['setores'] as $key)
                                  <option value="{{ $key->id_setor}}" {{ $key->id_setor==$idSet?"selected":"" }}>
                                    {{ $key->setor }}
                                  </option>
                                @endforeach
                              </select>
                            </div>

                            <div class="input-group mt-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text">Colaborador</span>
                              </div>
                              <select id="select1" name="id_colaborador" class="form-control" required>
                                <option>Selecione ...</option>
                                @foreach ($rota['colaboradores'] as $key)
                                  <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador==$idC?"selected":"" }}>
                                    {{ $key->pessoas['pessoa'] }}
                                  </option>
                                @endforeach
                              </select>
                            </div>

                            @if (isset($rota['edit']) and $rota['edit']->sts_compra == 3)
                              <div class="mt-3">
                                <div class="input-group">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" title="Data da entrega">Entrega</span>
                                  </div>
                                  <input class="form-control" type="date" name="dt_entrega" value="{{ $rota['edit']->dt_entrega or old('dt_entrega') }}">
                                </div>
                              </div>
                            @endif

                          </div>
                          <div class="col-6">

                            <div class="">
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text" title="Forma de pagamento">Pagto</span>
                                </div>
                                <input class="form-control" type="text" name="pagto" value="{{ $rota['edit']->pagto or old('pagto') }}">
                              </div>
                            </div>

                            <div class="mt-3">
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text" title="Forma de pagamento">Valor</span>
                                </div>
                                <input class="form-control" type="number" step="0.01" name="valor" value="{{ $rota['edit']->valor or old('valor') }}">
                              </div>
                            </div>

                            <div class="mt-3">
                              <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                                {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                              </button>
                            </div>
                          </div>
                        </div>
                      </form>

                      </div>

                      @if (isset($rota['edit']))
                        <div class="col-6">

                          <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                          aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                          <thead>
                            <tr role="row">
                              <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                                  <a href="{{route('orcamento.show',$rota['edit']->id_compra) }}" class="text-primary">Orçamentos</a>
                                  /<a href="{{route('adm_item.show',$rota['edit']->id_compra) }}" class="text-success">Itens</a>
                              </th>
                              <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending">
                                <pp class="text-primary">Ação</pp>/<pp class="text-success">Qnt</pp>
                              </th>
                            </tr>
                          </thead>
                          {{-- LIST --}}
                          <tbody>
                            @foreach ($rota['orcamentos'] as $key2)
                              @if ($key2->id_compra == $rota['edit']->id_compra)
                                <tr>
                                  <td class="text-primary">
                                    # {{ $key2->fornecedor }} Valor: R${{ number_format($key2->frete + $key2->valor, 2, ',', ' ') }}
                                    {{-- Valor: R${{ number_format($key2->valor, 2, ',', ' ') }}
                                    Frete: R${{ number_format($key2->frete, 2, ',', ' ') }}  --}}
                                  </td>
                                  <td>
                                    <div class="row">
                                      <div class="">
                                        @if (isset($key2->arquivo))
                                          @if ($acl->dw == 1)
                                            <a href="{{ asset('storage/'.$rota['pasta'].$key2->arquivo)}}" target="new" title="{{$key2->arquivo}}" class="btn btn-ghost-success">
                                              <i class="far fa-file-pdf"></i>
                                            </a>
                                          @else
                                            <div class="btn text-secondary"><i class="far fa-file-pdf"></i></div>
                                          @endif
                                        @else
                                          <div class="btn text-dark"><i class="fas fa-ban"></i></div>
                                        @endif
                                      </div>
                                      <div class="">
                                        @if ($key2->sts_orcamento == 2)
                                          <a href="{{ route("orcamento.update",$key2->id_orcamento) }}" class="btn btn-ghost-success">
                                            <i class="far fa-thumbs-up"></i>
                                          </a>
                                        @elseif ($acl->a == 1)
                                          <form method="POST" action="{{route('aprovaOrc',$key2->id_orcamento) }}">
                                            @csrf
                                            <button type="submit" class="btn btn-ghost-secondary" title="Aprovar" onclick="return confirm('Confirma a aprovação?')">
                                              <i class="far fa-thumbs-up"></i>
                                            </button>
                                          </form>
                                        @else
                                          <div class="btn text-dark"><i class="far fa-thumbs-up"></i></div>
                                        @endif
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              @endif
                            @endforeach
                            @foreach ($rota['itens'] as $key2)
                              @if ($key2->id_compra == $rota['edit']->id_compra)
                                <tr>
                                  <td class="text-success">* {{ $key2->produtos->produto }}</td>
                                  <td>{{ str_replace('.', ',', $key2->qnt) }}</td>
                                </tr>
                              @endif
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    @endif

                    </div>

                  </div>

              </div>

              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper  dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Compras
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 240px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota[$rota['rota']] as $key)
                      @if ($acl->r == 1 OR $key->id_user == Auth::id())
                      <tr>
                        <td>
                          <div class="">
                            <a class="badge badge-warning" href="#">#_{{ $key->id_compra }}</a>

                            {{ $key[$rota['rota']] }}
                            <a href="#" class="card-header-action btn-maximize" data-toggle="collapse" data-target="#collapse-{{$key['id_'.$rota['rota']]}}" aria-expanded="true">
                              <i class="fas fa-search-plus"></i>
                            </a>
                          </div>

                        </div>


                        <div class="collapse close col-12" id="collapse-{{$key['id_'.$rota['rota']]}}" >
                          <div class="row text-white">
                            <div class="col-5">
                              <h6>
                                Valor: R${{ number_format($key->valor, 2, ',', ' ')}} <br>
                                Pagamento: {{ $key->pagto }} <br>
                                Setor: {{ $key->setores->setor }}
                              </h6>
                            </div>
                            <div class="col-7">
                              <h6>
                                @foreach ($rota['orcamentos'] as $key1)
                                  Fornecedor: {{ $key1->fornecedor }} Valor: R${{ number_format($key1->frete + $key1->valor, 2, ',', ' ') }} <br>
                                @endforeach
                              </h6>

                            </div>
                          </div>
                        </div>


                      </td>
                      <td>
                        {{-- ACL ACOES TABLE --}}
                        <div class="row">
                        {{-- REALIZADA COMPRA --}}
                        @if ($key->sts_compra == 3 and $acl->f == 1)
                          <div class="">
                            <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Realizar compra">
                              <i class="fas fa-shopping-basket"></i>
                            </a>
                          </div>
                        @elseif ($key->sts_compra > 3)
                          <div class="btn text-success" title="Compra já realizada"><i class="fas fa-shopping-basket"></i></div>
                          @else
                            <div class="btn text-secondary" title="Compra não confirmada"><i class="fas fa-shopping-basket"></i></div>
                        @endif
                        {{-- REALIZADA COMPRA --}}
                        {{-- AUTORIZA COMPRA --}}
                        @if ($key->sts_compra == 2 and $acl->a == 1)
                          <div class="">
                            <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Autorizar o pedido">
                              <i class="far fa-check-circle"></i>
                            </a>
                          </div>
                        @elseif ($key->sts_compra > 2)
                          <div class="btn text-success" title="Compra autorizada"><i class="far fa-check-circle"></i></div>
                          @else
                            <div class="btn text-secondary" title="Compra não confirmada"><i class="far fa-check-circle"></i></div>
                        @endif
                        {{-- AUTORIZA COMPRA --}}
                        {{-- CONFIRMA PEDIDO --}}
                        @if ($key->sts_compra == 1)
                          {{-- CONFIRMA COMPRA --}}
                          <div class="">
                            <form method="POST" action="{{route($rota['rota'].'.update',$key['id_'.$rota['rota']]) }}">
                              @csrf
                              @method('PUT')
                              <input type="hidden" name="confirma" value="true">
                              <button type="submit" class="btn btn-block btn-ghost-warning" title="Confirma e pedido de compra" onclick="return confirm('Após confirmar NÃO será possivel realizar alterações no pedido. Confirma o pedido?')">
                                <i class="fas fa-clipboard-check"></i>
                              </button>
                            </form>
                          </div>
                          {{-- ABRE ORCAMENTOS --}}
                          @php
                          $ico = 'warning'; $tx = 'Sem orçamentos para o pedido de compra';
                          foreach ($rota['orcamentos'] as $key1)
                            if ($key1->id_compra == $key->id_compra){
                              $ico = 'success';
                              $tx = 'Orçamentos';
                            }
                          @endphp
                          <div class="">
                            <a href="{{route('orcamento.show',$key->id_compra) }}" class="btn btn-block btn-ghost-{{$ico}}" title="{{ $tx }}">
                              <i class="fas fa-clipboard-list"></i>
                            </a>
                          </div>
                          {{-- ABRE ITENS --}}
                          @php
                          $ico = 'warning'; $tx = 'Sem itens no pedido de compra';
                          foreach ($rota['itens'] as $key1)
                            if ($key1->id_compra == $key->id_compra){
                              $ico = 'success';
                              $tx = 'Itens do pedido de compra';
                            }
                          @endphp
                          <div class="">
                            <a href="{{route('adm_item.show',$key->id_compra) }}" class="btn btn-block btn-ghost-{{$ico}}" title="Itens do pedido de compra">
                              <i class="fas fa-toolbox"></i>
                            </a>
                          </div>
                          {{-- EBRE EDIÇÃO --}}
                          @if ($acl->u == 1)
                            <div class="">
                              <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                <i class="far fa-edit"></i>
                              </a>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                          @endif
                          {{-- DELETE LANÇAMENTO --}}
                          @if ($acl->d == 1)
                            <div class="">
                              <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                  <i class="far fa-trash-alt"></i>
                                </button>
                              </form>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                          @endif

                        @else
                          {{-- PEDIDO CONFIRMADO --}}
                          <div class="btn text-secondary" title="Pedido de compra fechado."><i class="fas fa-clipboard-check"></i></div>
                          {{-- ABRE ORCAMENTOS --}}
                          <div class="btn text-secondary" title="Pedido de compra fechado."><i class="fas fa-clipboard-list"></i></div>
                          {{-- ABRE ITENS --}}
                          <div class="btn text-secondary" title="Pedido de compra fechado."><i class="fas fa-toolbox"></i></div>
                          {{-- EBRE EDIÇÃO --}}
                          <div class="btn text-secondary" title="Pedido de compra fechado."><i class="far fa-edit"></i></div>
                          {{-- DELETE LANÇAMENTO --}}
                          <div class="btn text-secondary" title="Pedido de compra fechado."><i class="far fa-trash-alt"></i></div>
                        @endif
                        {{-- CONFIRMA PEDIDO --}}
                        </div>
                        {{-- ACL ACOES TABLE --}}
                      </td>
                    </tr>
                    @endif
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
