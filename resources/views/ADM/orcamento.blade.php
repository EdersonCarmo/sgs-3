@extends('layouts.sgs')

@section('content')
  @php
  $idSTS = $idM = $idF = 0;
  $idS = 1;
  $acl = $rota['acl'];
  if(isset($rota['edit'])){
    $idSTS = $rota['edit']->sts_orcamento;
    $idS = $rota['edit']->id_compra;
  }else{
    $idS = $rota['compra']->id_compra;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form" enctype="multipart/form-data">
                  @method('PUT')
                @else
                  <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form" enctype="multipart/form-data">
                    <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                    <input type="hidden" name="id_compra" value="{{$rota['input']['id']}}">
                  @endif
                  @csrf
                  <div class="row justify-content-between">
                    <div class="col-8">
                      <div class="form-group">
                        <label for="exampleInputName2"><a href="{{ route("compra.edit",$idS) }}">ORÇAMENTOS COMPRA: {{ $idS }}</a> </label>
                      </div>
                    </div>
                    {{-- ACL FORM CAD --}}
                    <div class="col-1">
                      @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                    </div>
                    {{-- ACL FORM CAD --}}
                  </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                    <div class="row">
                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Colaborador</span>
                          </div>
                          <select id="select1" name="id_colaborador" class="form-control">
                            <option>Selecione ...</option>
                            @foreach ($rota['colaboradores'] as $key)
                              <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador==$idM?"selected":"" }}>
                                {{ $key->pessoas['pessoa'] }}
                              </option>
                            @endforeach
                          </select>
                        </div>

                      <div class="input-group mt-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Fornecedor</span>
                        </div>
                        <input type="text" name="fornecedor" class="form-control" value="{{ $rota['edit']->fornecedor or old('fornecedor') }}">
                      </div>

                    </div>

                    <div class="col-3">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Valor</span>
                        </div>
                        <input type="text" name="valor" class="form-control" value="{{ $rota['edit']->valor or old('valor') }}">
                      </div>

                      <div class="input-group mt-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">E-Mail</span>
                        </div>
                        <input type="text" name="email" class="form-control" value="{{ $rota['edit']->email or old('email') }}">
                      </div>
                    </div>

                    <div class="col-3">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Data</span>
                        </div>
                        <input type="date" name="dt_orcamento" class="form-control" value="{{ $rota['edit']->dt_orcamento or old('dt_orcamento') }}">
                      </div>

                      <div class="input-group mt-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Validade</span>
                        </div>
                        <input type="date" name="validade" class="form-control" value="{{ $rota['edit']->validade or old('validade') }}">
                      </div>
                    </div>

                    <div class="col-3">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Frete</span>
                        </div>
                        <input type="text" name="frete" class="form-control" value="{{ $rota['edit']->frete or old('frete') }}">
                      </div>
                      <div class="input-group mt-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Telefone</span>
                        </div>
                        <input type="text" name="telefone" class="form-control" value="{{ $rota['edit']->telefone or old('telefone') }}">
                      </div>
                    </div>


                      <div class="col-5 mt-3">
                        <div class="input-group">
                          <input id="arquivo" type="file" name="arquivo" class="form-control" value="{{ $rota['edit']->arquivo or old('arquivo') }}">
                        </div>
                      </div>

                      <div class="col-5 mt-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Descrição</span>
                          </div>
                          <textarea name="orcamento" rows="3" class="form-control">{{ $rota['edit']->orcamento or '' }}</textarea>
                        </div>
                      </div>


                      <div class="col-2 mt-3">
                        <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                          {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                        </button>
                      </div>

                      </div>

                  </div>

                </form>
              </div>

              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper  dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 215px;">
                        Fornecedor
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                        Valor
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                        Frete
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                        Total
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota[$rota['rota']] as $key)
                      <tr>
                        <td>
                          {{ $key->fornecedor }}
                          <a href="#" class="card-header-action btn-maximize" data-toggle="collapse" data-target="#collapse-{{$key['id_'.$rota['rota']]}}" aria-expanded="true">
                            <i class="fas fa-search-plus"></i>
                          </a>
                        </div>

                        <div class="collapse close" id="collapse-{{$key['id_'.$rota['rota']]}}" >
                          <div class="text-white">
                            <h6>
                              Manutenção: {{$key->manutencao}} <br>
                              Veículo:  <br>
                              Valor: {{$key->valor}} <br>
                              Motorista:  <br>
                              Fornecedor:  <br>
                              Data Início: {{ date('d/m/Y', strtotime($key->dt_inicio)) }} <br>
                              Data Fim: {{ date('d/m/Y', strtotime($key->dt_fim)) }} <br>
                            </h6>
                          </div>
                        </div>
                      </td>
                      <td>R${{ number_format($key->valor, 2, ',', ' ') }}</td>
                      <td>R${{ number_format($key->frete, 2, ',', ' ') }}</td>
                      <td>R${{ number_format($key->valor + $key->frete , 2, ',', ' ') }}</td>
                      <td>
                        <div class="row">

                          @if (isset($key->arquivo))
                            @if ($acl->dw == 1)
                              <div class="">
                                <a href="{{ asset('storage/'.$rota['pasta'].$key->arquivo)}}" target="new" title="{{$key->arquivo}}" class="btn btn-block btn-ghost-success">
                                  <i class="far fa-file-pdf"></i>
                                </a>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-file-pdf"></i></div>
                            @endif
                          @else
                            <div class="btn text-dark"><i class="fas fa-ban"></i></div>
                          @endif

                          <div class="">
                            <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                              <i class="far fa-edit"></i>
                            </a>
                          </div>
                          <div class="">
                            <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                <i class="far fa-trash-alt"></i>
                              </button>
                            </form>
                          </div>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
