@extends('layouts.sgs')

@section('content')
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- START FORM --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']->id_adm_tipo) }}" method="post" class="form-inline">
                  <input type="hidden" name="rota" value="{{$rota[$rota['rota']][0]->rota}}">
                  @method('PUT')
              @else
                <form method="post" class="form-inline">
                  <input type="hidden" name="rota" value="empresa">
              @endif
                @csrf
                <div class="form-group">
                  <label for="exampleInputName2">TIPOS DE {{strtoupper($rota[$rota['rota']][0]->rota)}} </label>
                    <input name="tipo" type="text" class="form-control mr-2 ml-2" placeholder="" value="{{ $rota['edit']->tipo or old('tipo') }}" oninput="this.value = this.value.toUpperCase()">
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-outline-success" name="submit" id="submit">
                    {{ isset($rota['edit'])?"Alterar":"Cadastrar"}}</button>
                </div>
              </form>
            </div>
            {{-- END FORM --}}
            {{-- START LIST --}}
            <div class="car-body">

              <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer mt-3">

                <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                {{-- CAD TIPOS DE EMPRESAS --}}
                <thead>
                  <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 215px;">
                      Tipos de empresas
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                      Status
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                      Ação
                    </th>
                  </tr>
                </thead>
                {{-- LIST TIPOS DE EMPRESAS --}}
                <tbody>
                  @foreach ($rota[$rota['rota']] as $key)
                    <tr>
                      <td>{{ $key->adm_tipo }}</td>
                      <td>
                        <span class="badge badge-{{ $key->tipo=1?'success':'secondary' }}">
                          {{ $key->tipo=1?'ATIVO':'INATIVO' }}
                        </span>
                      </td>
                      <td>
                        <div class="row">
                          <div class="col-6">
                            <a href="{{route($rota['rota'].'.edit',$key->id_adm_tipo) }}" class="btn btn-block btn-outline-warning" title="Editar">
                              <i class="far fa-edit"></i>
                            </a>
                          </div>
                          <div class="col-6">
                            <form method="POST" action="{{route($rota['rota'].'.destroy',$key->id_adm_tipo) }}">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class="btn btn-block btn-outline-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                <i class="far fa-trash-alt"></i>
                              </button>
                            </form>
                          </div>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>

          </div>
          {{-- END LIST --}}
        </div>

      </div>
    </div>
  </main>
@endsection
