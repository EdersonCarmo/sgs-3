@extends('layouts.sgs')

@section('content')
  @php
  $acl = $rota['acl'];
  $idM = $idMod = 0;
    if(isset($rota['edit'])){
      $idM = $rota['edit']->id_tipo;
    }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- START FORM --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']->id_ramo) }}" method="post" class="form-inline">
                  <input type="hidden" name="rota" value="{{$rota[$rota['rota']][0]->rota}}">
                  @method('PUT')
              @else
                <form method="post" class="form-inline">
                  <input type="hidden" name="rota" value="empresa">
              @endif
                @csrf
                <div class="form-group">
                  <label for="exampleInputName2">CADASTRO DE RAMOS DE ATIVIDADES {{$rota[$rota['rota']][0]->tabela}} </label>
                    <input name="ramo" type="text" class="form-control mr-2 ml-2" placeholder="" value="{{ $rota['edit']->ramo or old('ramo') }}" oninput="this.value = this.value.toUpperCase()">
                </div>

                <div class="col-4">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Módulo</span>
                    </div>
                    <select id="select1" size="5" name="id_modulo[]" class="form-control" multiple>
                    @php
                      if(isset($rota['edit'])) $idMod = $rota['edit']->modulosramos;
                      // TI_option_modulos($rota['modulos'],$idMod);
                    @endphp
                      @foreach ($rota['modulos'] as $key)
                          <option value="{{ $key->id_modulo }}"
                            @if (isset($rota['edit']))
                              @foreach ($rota['edit']->modulosramos as $key1)
                                {{ $key->id_modulo==$key1->id_modulo?"selected":"" }}
                              @endforeach
                            @endif
                            >
                            {{ $key->modulo }}
                          </option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-outline-success" name="submit" id="submit">
                    {{ isset($rota['edit'])?"Alterar":"Cadastrar"}}
                  </button>
                </div>

              </form>
            </div>
            {{-- END FORM --}}
            {{-- START LIST --}}
            <div class="car-body">

              <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer mt-3">

                <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                {{-- CAD TIPOS DE EMPRESAS --}}
                <thead>
                  <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending" style="width: 215px;">
                      Tipos de atividades
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending" style="width: 184px;">
                      Status
                    </th>
                    <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                      Ação
                    </th>
                  </tr>
                </thead>
                {{-- LIST TIPOS DE EMPRESAS --}}
                <tbody>
                  @foreach ($rota[$rota['rota']] as $key)
                    <tr>
                      <td>{{ $key->ramo }}</td>
                      <td>
                        <span class="badge badge-{{ $key->tipo=1?'success':'secondary' }}">
                          {{ $key->tipo=1?'ATIVO':'INATIVO' }}
                        </span>
                      </td>
                      <td>
                        {{-- ACL ACOES TABLE --}}
                        <div class="row">
                          @if ($acl->u == 1)
                            <div class="">
                              <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                <i class="far fa-edit"></i>
                              </a>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                          @endif

                          @if ($acl->d == 1)
                            <div class="">
                              <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                  <i class="far fa-trash-alt"></i>
                                </button>
                              </form>
                            </div>
                          @else
                            <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                          @endif
                        </div>
                        {{-- ACL ACOES TABLE --}}
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>

          </div>
          {{-- END LIST --}}
        </div>

      </div>
    </div>
  </main>
@endsection
