@extends('layouts.sgs')

@section('content')
  @php
  $idP = $idE = 0; $d = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])) {
    $idP = $rota['edit']->id_produto;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form-inline">
                @method('PUT')
              @else
                <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form-inline">
                @endif
                <input type="hidden" name="id_{{$rota['input']['rota']}}" value="{{$rota['input']['id']}}">
                <input type="hidden" name="rota" value="{{$rota['input']['rota']}}">
                @csrf


                <div class="row">
                  <div class="col-12 ml-2">
                    <div class="form-group">
                      <label for="Name2"><a href="JavaScript: window.history.back();">ITENS COMPRA:</a></label>&nbsp;
                      {{ $rota['input']['id'] }}
                    </div>
                  </div>
                </div>

                  <div>
                    <div class="row">

                      <div class="col-7 ml-5">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Item</span>
                          </div>
                          <div class="col-12 form-control">
                            <select class="js-example-basic-single" name="id_produto" style="width: 300px" {{ isset($rota['edit']) ? "disabled":""}} required>
                              <option value="" selected>Selecione ...</option>
                              @foreach ($rota['produtos'] as $key)
                                <option value="{{ $key->id_produto }}" {{ $key->id_produto === $idP ? 'selected':''}}>{{ $key->produto }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>

                      {{-- <div class="col-5">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Item</span>
                          </div>
                          <select id="select1" name="id_produto" class="form-control">
                            <option {{ $idP == 0 ? 'selected':'' }}>Selecione ...</option>
                            @foreach ($rota['produtos'] as $key)
                              <option value="{{ $key->id_produto }}" {{ $key->id_produto === $idP ? 'selected':''}}>{{ $key->produto }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div> --}}

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">QNT</span>
                          </div>
                          <input type="text" name="qnt" class="form-control" value="{{ $rota['edit']->qnt or old('qnt') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      @if (isset($rota['input']['rota']) AND $rota['input']['rota'] == 'estoque')

                      @endif
                      {{-- <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Local</span>
                          </div>
                          <input type="text" name="local" class="form-control" value="{{ $rota['edit']->local or old('local') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div> --}}

                      @if (isset($rota['edit']))
                        <button type="submit" class="btn btn-outline-info" id="submit">
                          Salvar
                        </button>
                      @endif
                      @if (isset($rota['adm_item']))
                        <button type="submit" name="add" value="true" class="btn btn-outline-success" id="submit">
                          Adicionar
                        </button>
                      @endif


                    </div>

                  </div>
                </form>
              </div>
              {{-- END FORM --}}
              {{-- START LIST --}}
              <div class="car-body">
                {{-- DATATABLE --}}
                <form action="{{ route('baixa_item') }}" method="POST" name="formBaixa">
                  @csrf
                  <input type="hidden" name="id_compra" value="{{ $rota['input']['id'] }}">
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Item
                      </th>
                      @if ($acl->dw == 1)
                        <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                          Entrada
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                          Local
                        </th>
                      @endif
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                        QNT
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 79px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                </div>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota['item'] as $key)
                      @php
                      switch ($key['sts_mrp_item']) {
                        case '0': $cor = 'dark';      $tx = 'Cancelado';  break;
                        case '1': $cor = 'warning';   $tx = 'Pendente';   break;
                        case '2': $cor = 'secondary'; $tx = 'Confirmado'; break;
                        case '3': $cor = 'info';      $tx = 'Autorizado'; break;
                        case '4': $cor = 'light';     $tx = 'Separado';   break;
                        case '5': $cor = 'success';   $tx = 'Entrege';    break;
                        default:  $cor = 'dark';      $tx = 'Erro';       break;
                      }
                      @endphp
                      <tr>
                        <td>
                          {{-- <input name="select_item" class="form-check-input chk" type="checkbox" value="{{ $key->id_mrp_item }}" id="check{{ $key->id_mrp_item }}"> --}}
                          <i class="fas fa-tag text-{{$cor}}" title="{{$tx}}"></i>
                          <input type="hidden" name="id_mrp_item[]" value="{{$key['id_mrp_item']}}">
                          <input type="hidden" name="id_produto[]" value="{{$key['id_produto']}}">
                          {{ $key->produtos->produto }}
                        </td>

                        @if ($acl->dw == 1)
                          <td>
                            @if ($key->atendido != $key->qnt)
                              <div class="row">
                                <div class="col-8">
                                  <div class="input-group">
                                    <select id="select1" name="id_estoque" class="form-control">
                                      <option value="0" {{ $idE == 0 ? 'selected':'' }}>Estoque ...</option>
                                      @foreach ($rota['estoques'] as $key1)
                                          <option value="{{ $key1->id_estoque }}">{{ $key1->estoque }}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                </div>
                              </div>
                              @else
                                <input type="hidden" name="id_estoque[]" value="0">
                            @endif
                          </td>
                          <input type="hidden" name="qnt[]" value="{{$key->qnt}}">
                          <input type="hidden" name="atendido[]" value="{{$key->atendido}}">
                          <td>
                            <div class="col-8">
                              <input class="form-control" type="text" name="local" value="">
                            </div>
                          </td>
                        @endif

                        <td>
                          <pp title="Quantidade atendida">{{ str_replace('.', ',', $key->atendido) }}</pp>
                           / <pp title="Quantidade solicitada">{{ str_replace('.', ',', $key->qnt) }}</pp>
                        </td>
                      </form>
                        <td>
                          {{-- ACL ACOES TABLE --}}
                          <div class="row">
                            {{-- @if ($acl->u == 1)
                              <div class="">
                                <a href="{{route($rota['rota'].'.edit',$key['id_mrp_item']) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                  <i class="far fa-edit"></i>
                                </a>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                            @endif --}}

                            @if ($acl->d == 1)
                              <div class="">
                                <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_mrp_item']) }}" name="formDestroy">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                    <i class="far fa-trash-alt"></i>
                                  </button>
                                </form>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                            @endif
                          </div>
                          {{-- ACL ACOES TABLE --}}
                        </td>
                    </tr>
                  @endforeach
                  {{-- <input id="ckAll" name="select_item" class="form-check-input ml-3" type="checkbox" value="{{ $key->id_mrp_item }}" id="check{{ $key->id_mrp_item }}"> --}}
                </tbody>
              </table>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
