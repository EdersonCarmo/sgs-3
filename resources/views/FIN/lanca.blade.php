
<div class="row">
  <div class="col-lg-12">
    <table class="table table-responsive-sm table-striped table-sm">
      <thead>
        <tr>
          <th>Tarefa</th>
          <th>Data</th>
          <th>Solicitante</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><a href="#">Autorizar contratação</a></td>
          <td>01/01/2019</td>
          <td>André Melo</td>
          <td>
            <span class="badge badge-success">Aprovado</span>
          </td>
        </tr>
        <tr>
          <td><a href="#">Aprovar cotação</a></td>
          <td>01/01/2019</td>
          <td>Nátaly Araujo</td>
          <td>
            <span class="badge badge-danger">Vetado</span>
          </td>
        </tr>
        <tr>
          <td><a href="#">Cobrar boleto</a></td>
          <td>01/01/2019</td>
          <td>Nayla Fiorete </td>
          <td>
            <span class="badge badge-secondary">Cancelado</span>
          </td>
        </tr>
        <tr>
          <td><a href="#">Aprovar pagamento</a></td>
          <td>01/01/2019</td>
          <td>André Melo</td>
          <td>
            <span class="badge badge-warning">Pendente</span>
          </td>
        </tr>
        <tr>
          <td><a href="#">Solicitação de compra</a></td>
          <td>01/01/2019</td>
          <td>Nátaly Araujo</td>
          <td>
            <span class="badge badge-success">Aprovado</span>
          </td>
        </tr>
        <tr>
          <td><a href="#">Cobrar boleto</a></td>
          <td>01/01/2019</td>
          <td>Nayla Fiorete </td>
          <td>
            <span class="badge badge-secondary">Cancelado</span>
          </td>
        </tr>
        <tr>
          <td><a href="#">Aprovar pagamento</a></td>
          <td>01/01/2019</td>
          <td>André Melo</td>
          <td>
            <span class="badge badge-warning">Pendente</span>
          </td>
        </tr>
        <tr>
          <td><a href="#">Solicitação de compra</a></td>
          <td>01/01/2019</td>
          <td>Nátaly Araujo</td>
          <td>
            <span class="badge badge-success">Aprovado</span>
          </td>
        </tr>
        <tr>
          <td><a href="#">Cobrar boleto</a></td>
          <td>01/01/2019</td>
          <td>Nayla Fiorete </td>
          <td>
            <span class="badge badge-secondary">Cancelado</span>
          </td>
        </tr>
        <tr>
          <td><a href="#">Aprovar pagamento</a></td>
          <td>01/01/2019</td>
          <td>Member</td>
          <td>
            <span class="badge badge-warning">Pendente</span>
          </td>
        </tr>
        <tr>
          <td><a href="#">Solicitação de compra</a></td>
          <td>01/01/2019</td>
          <td>Nátaly Araujo</td>
          <td>
            <span class="badge badge-success">Aprovado</span>
          </td>
        </tr>
        <tr>
          <td><a href="#">Cobrar boleto</a></td>
          <td>01/01/2019</td>
          <td>Nayla Fiorete </td>
          <td>
            <span class="badge badge-secondary">Cancelado</span>
          </td>
        </tr>
        <tr>
          <td><a href="#">Aprovar pagamento</a></td>
          <td>01/01/2019</td>
          <td>André Melo</td>
          <td>
            <span class="badge badge-warning">Pendente</span>
          </td>
        </tr>
        <tr>
          <td><a href="#">Solicitação de compra</a></td>
          <td>01/01/2019</td>
          <td>Nátaly Araujo</td>
          <td>
            <span class="badge badge-success">Aprovado</span>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
