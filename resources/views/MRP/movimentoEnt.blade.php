@extends('layouts.sgs')

@section('content')
  @php
  $idP = $idE = $idT = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])) {
    $idP = $rota['edit']->id_produto;
    $idE = $rota['edit']->id_estoque;
    $idT = $rota['edit']->id_mrp_tipo;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
                <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form">
                  <input type="hidden" name="id_user" value="{{ Auth::user()->id }}">
                  <input type="hidden" name="id_mrp_tipo" value="{{ $rota['insert']['id_mrp_tipo'] }}">
                  <input type="hidden" name="id_estoque" value="{{ $rota['insert']['id_estoque'] }}">
                  <input type="hidden" name="mais" value="movimentoEnt">
                @csrf
                <div class="row justify-content-between">
                  <div class="col-8">
                    <div class="form-group">
                      <label for="exampleInputName2">MOVIMENTAÇÃO</label>
                    </div>
                  </div>
                </div>

                  <div>
                    <div class="row">

                      <div class="col-5">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Produtos</span>
                          </div>
                          <div class="col-12 form-control">
                            <select class="js-example-basic-single" name="id_produto" style="width: 250px" {{ isset($rota['edit']) ? "disabled":""}} required>
                              <option value="" selected>Selecione ...</option>
                              @foreach ($rota['produtos'] as $key)
                                <option value="{{ $key->id_produto }}" {{ $key->id_produto === $idP ? 'selected':''}}>{{ $key->cod.'_'.$key->produto }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>

                        <div class="col-2">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Qnt.</span>
                            </div>
                            <input type="text" name="qnt" step="0.01" class="form-control" value="{{ $rota['edit']->qnt or old('qnt') }}" oninput="this.value = this.value.toUpperCase()" required>
                          </div>
                        </div>

                        <div class="col-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Local</span>
                            </div>
                            <input id="local" type="text" name="local" class="form-control" onkeypress="mascara(this, '##-##-#')" maxlength="7" oninput="this.value = this.value.toUpperCase()" required>
                          </div>
                        </div>

                        <button type="submit" value="movimentoEnt" name="mais" class="btn btn-outline-success" id="submit">+</button>
                      </div>

                  </div>
                </form>
              </div>
              {{-- END FORM --}}

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
