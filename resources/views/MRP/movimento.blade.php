@extends('layouts.sgs')

@section('content')
  @php
  $idP = $idE = $idT = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])) {
    $idP = $rota['edit']->id_produto;
    $idE = $rota['edit']->id_estoque;
    $idT = $rota['edit']->id_mrp_tipo;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form">
                @method('PUT')
              @else
                <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form">
                  <input type="hidden" name="id_user" value="{{ Auth::user()->id }}">
                @endif
                @csrf
                <div class="row justify-content-between">
                  <div class="col-8">
                    <div class="form-group">
                      <label for="exampleInputName2">MOVIMENTAÇÃO </label>
                    </div>
                  </div>
                  {{-- ACL FORM CAD --}}
                  <div class="col-1">
                    @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                  </div>
                  {{-- ACL FORM CAD --}}
                </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                    <div class="row">

                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Estoque</span>
                          </div>
                          <select id="select1" name="id_estoque" class="form-control">
                            <option>Selecione ...</option>
                            @foreach ($rota['estoques'] as $key)
                              <option value="{{ $key->id_estoque }}" {{ $key->id_estoque==$idE?"selected":"" }}>
                                {{ $key->estoque }}
                              </option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Tipos</span>
                          </div>
                          <select id="select1" name="id_mrp_tipo" class="form-control">
                            <option {{ $idT == 0 ? 'selected':''}}>Selecione ...</option>
                            @foreach ($rota['tipos'] as $key)
                              <option value="{{ $key->id_mrp_tipo }}" {{ $key->id_mrp_tipo === $idT ? 'selected':''}}>{{ $key->mrp_tipo }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-5">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Produtos</span>
                          </div>
                          <div class="col-12 form-control">
                            <select class="js-example-basic-single" name="id_produto" style="width: 250px" {{ isset($rota['edit']) ? "disabled":""}} required>
                              <option value="" selected>Selecione ...</option>
                              @foreach ($rota['produtos'] as $key)
                                <option value="{{ $key->id_produto }}" {{ $key->id_produto === $idP ? 'selected':''}}>{{ $key->cod.'_'.$key->produto }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
{{--
                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Produtos</span>
                          </div>
                          <select id="select1" name="id_produto" class="form-control">
                            <option {{ $idP == 0 ? 'selected':'' }}>Selecione ...</option>
                            @foreach ($rota['produtos'] as $key)
                              <option value="{{ $key->id_produto }}" {{ $key->id_produto === $idP ? 'selected':''}}>{{ $key->produto }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div> --}}

                    </div>
                      <div class="row mt-2">

                        <div class="col-4">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Data</span>
                            </div>
                            <input type="date" name="dt_movimentacao" class="form-control" value="{{ $rota['edit']->dt_movimentacao or date("Y-mm-dd") }} }}">
                          </div>
                        </div>

                        <div class="col-2">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Qnt.</span>
                            </div>
                            <input type="text" name="qnt" step="0.01" class="form-control" value="{{ $rota['edit']->qnt or old('qnt') }}" oninput="this.value = this.value.toUpperCase()" required>
                          </div>
                        </div>

                        <div class="col-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Local</span>
                            </div>
                            <input id="local" type="text" name="local" class="form-control" onkeypress="mascara(this, '##-##-#')" maxlength="7" oninput="this.value = this.value.toUpperCase()" required>
                          </div>
                        </div>

                        <button type="submit" class="btn btn-outline-success" id="submit">
                          {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                        </button>
                        <button type="submit" value="movimentoEnt" name="mais" class="btn btn-outline-success" id="submit">+</button>
                      </div>

                  </div>
                </form>
              </div>
              {{-- END FORM --}}
              {{-- START LIST --}}
              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Data
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                        Produto
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                        Qnt
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                        Operação
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending">
                        Ação
                      </th>
                    </tr>
                  </thead>
                </div>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota[$rota['rota']] as $key)
                      <tr>
                        <td>
                          @php
                            switch ($key['id_mrp_tipo']) {
                              case '4': $cor = 'primary'; $tex = 'Entrada';  $ico = 'down-alt'; break;
                              case '5': $cor = 'success'; $tex = 'Saída';    $ico = 'up-alt'; break;
                              default:  $cor = 'dark';    $tex = 'Cancelado';   break;
                            }
                            $show = ['id'=>$key['id_'.$rota['rota']],'rota'=>$rota['rota']]
                          @endphp
                            <i class="fas fa-level-{{$ico}} text-{{$cor}}" title="{{$tex}}"></i>
                          {{ date('d/m/Y', strtotime($key->created_at)) }}
                        </td>
                        <td>{{ $key->produtos->produto }}</td>
                        <td>{{ $key->qnt }}</td>
                        <td>{{ $key->tipos->mrp_tipo }}</td>
                        <td>
                          {{-- ACL ACOES TABLE --}}
                          <div class="row">
                            @if ($acl->u == 1)
                              <div class="">
                                <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-sm btn-ghost-warning" title="Editar">
                                  <i class="far fa-edit"></i>
                                </a>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                            @endif

                            @if ($acl->d == 1)
                              <div class="">
                                <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-ghost-danger btn-sm" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                    <i class="far fa-trash-alt"></i>
                                  </button>
                                </form>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                            @endif
                          </div>
                          {{-- ACL ACOES TABLE --}}
                        </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>

              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">DOCUMENTO</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>One fine body…</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                  </div>

                </div>

              </div>
            </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
