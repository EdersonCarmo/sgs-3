@extends('layouts.sgs')

@section('content')
  @php
  $idU = 4; $idT = 0;
  $idT = session('tipo');
  $acl = $rota['acl'];
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
                <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form">
                @csrf
                <div class="row justify-content-between">
                  <div class="col-8">
                    <div class="form-group">
                      <label for="exampleInputName2">FILTRAR CONSULTA</label>
                    </div>
                  </div>
                </div>

                  <div id="div-cad-{{$rota['rota']}}">
                    <div class="row">

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Nome</span>
                          </div>
                          <input type="text" name="produto" class="form-control" value="" oninput="this.value = this.value.toUpperCase()" autofocus required>
                        </div>
                      </div>

                      <div class="col-2">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Qnt.</span>
                          </div>
                          <input type="text" name="qnt" class="form-control" value="{{ $rota['edit']->qnt or old('qnt') }}" required>
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Local</span>
                          </div>
                          <input id="local" value="{{ session('local') }}" type="text" name="local" class="form-control" onkeypress="mascara(this, '##-##-#')" maxlength="7" oninput="this.value = this.value.toUpperCase()" required>
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Tipos</span>
                          </div>
                          <select id="select1" name="id_mrp_tipo" class="form-control" required>
                            <option value="">Selecione ...</option>
                            {{-- @foreach ($rota['tipos'] as $key)
                                <option value="{{ $key->id_mrp_tipo }}" {{ $key->id_mrp_tipo == 19 ? 'selected':''}}>{{ $key->mrp_tipo }}</option>
                            @endforeach --}}
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row mt-3">
                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Código</span>
                          </div>
                          <input type="text" name="cod" class="form-control" value="{{ $rota['edit']->cod or old('cod') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Peso</span>
                          </div>
                          <input type="text" name="peso" class="form-control" value="{{ $rota['edit']->peso or old('peso') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                        <div class="col-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">UND</span>
                            </div>
                            <select id="select1" name="id_und" class="form-control" required>
                              <option>Selecione ...</option>
                              {{-- @foreach ($rota['unds'] as $key)
                                <option value="{{ $key->id_und }}" {{ $key->id_und == $idU ? 'selected':''}}>{{ $key->und }}</option>
                              @endforeach --}}
                            </select>
                          </div>
                        </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">CA</span>
                          </div>
                          <input type="text" name="ca" class="form-control" value="{{ $rota['edit']->ca or old('ca') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>
                    </div>

                    <div class="row mt-3">
                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Minímo</span>
                          </div>
                          <input type="text" name="qnt_min" class="form-control" value="{{ $rota['edit']->qnt_min or old('qnt_min') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Validade</span>
                          </div>
                          <input type="date" name="validade" class="form-control" value="{{ $rota['edit']->validade or old('validade') }}">
                        </div>
                      </div>

                      <div class="col-5">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Observação</span>
                          </div>
                          <input type="text" name="obs" class="form-control" value="{{ $rota['edit']->obs or old('obs') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      </div>
                    <hr>
                    <div class="row mt-3">
                        <div class="col-4">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Estoque</span>
                            </div>
                            <select id="select1" name="id_estoque" class="form-control" required>
                              <option value="">Selecione ...</option>
                              {{-- @foreach ($rota['estoques'] as $key)
                                <option value="{{ $key->id_estoque }}" {{ $key->id_estoque == 1 ? 'selected':''}}>
                                  {{ $key->estoque }}
                                </option>
                              @endforeach --}}
                            </select>
                          </div>
                        </div>

                        <div class="col-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Marca</span>
                            </div>
                            <input type="text" name="marca" class="form-control" value="{{ $rota['edit']->marca or old('marca') }}" oninput="this.value = this.value.toUpperCase()">
                          </div>
                        </div>

                        <div class="col-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Modelo</span>
                            </div>
                            <input type="text" name="modelo" class="form-control" value="{{ $rota['edit']->modelo or old('modelo') }}" oninput="this.value = this.value.toUpperCase()">
                          </div>
                        </div>

                        <div class="col-2">
                          <button type="submit" class="btn btn-outline-success" id="submit">
                            {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                          </button>
                        </div>
                    </div>

                  </div>
                </form>
              </div>
              {{-- END FORM --}}
          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
