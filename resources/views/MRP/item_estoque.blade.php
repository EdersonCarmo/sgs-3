@extends('layouts.sgs')

@section('content')
  @php
  $idE = $idT = 0;
  if(count($rota['item']) == 0) $idE = 0;
  else  $idE = $rota['item']['0']->id_estoque;

  $print = ['id'=>$idE,'rota'=>$rota['rota'],'act'=>'print'];
  $cor = 'success';
  $tx = 'Em estoque';
  $acl = $rota['acl'];
  if(isset($rota['edit'])) {
    // $idP = $rota['edit']->id_produto;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card animated fadeIn">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
                <form action="{{route('mrp_filtro') }}" method="post" class="form">
                  <input type="hidden" name="act" value="filtro">
                  <input type="hidden" name="id_estoque" value="{{$idE}}">
                @csrf
                <div class="row justify-content-between">
                  <div class="col-8">
                    <div class="form-group">
                      <label for="exampleInputName2">ITENS ESTOQUE</label>
                    </div>
                  </div>

                  <div class="row col-2 justify-content-between">
                    <div class="">
                      {{-- PRINT --}}
                      @if ($acl->p == 1)
                        <a href="{{route('estoque.show',$print) }}" target="new" class="btn btn-ghost-warning ml-2" title="Imprimir">
                          <i class="fas fa-print fa-2x"></i>
                        </a>
                      @endif
                      {{-- PRINT --}}
                    </div>
                    {{-- ACL FORM CAD --}}
                    @if ($acl->c == 1)
                      <button id="btn-plus" type="button" class="btn btn-lg btn-pill btn-outline-info ml-3" onclick="MudarestadoSeach('{{$rota['rota']}}')" title="Filtros">
                        <i class="fa fa-search"></i>
                      </button>
                    @endif
                    {{-- ACL FORM CAD --}}
                  </div>
                </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">

                    <div class="row">

                      <div class="col-3">
                        <button class="btn btn-block btn-secondary" type="button" aria-pressed="true">Filtros</button>

                      <div class="mt-1">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Nome</span>
                          </div>
                          <input type="text" class="form-control" name="produto" value="">
                        </div>
                      </div>

                      <div class="mt-2">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Qnt.</span>
                          </div>
                          <input type="number" min="0" step="0.01" name="qnt_min" class="form-control" placeholder="Min" title="Quantidade mínima" value="">
                          <input type="number" min="0" step="0.01" name="qnt_max" class="form-control" placeholder="Max" title="Quantidade máxima" value="">
                        </div>
                      </div>

                    </div>

                      <div class="col-3">
                      <div class="">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Código</span>
                          </div>
                          <input type="text" class="form-control" name="cod" value="">
                        </div>
                      </div>

                      <div class="mt-1">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Marca</span>
                          </div>
                          <input type="text" class="form-control" name="marca" value="">
                        </div>
                      </div>

                      <div class="mt-1">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Modelo</span>
                          </div>
                          <input type="text" class="form-control" name="modelo" value="">
                        </div>
                      </div>

                    </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Local</span>
                          </div>
                          <select id="select1" size="5" name="local[]" class="form-control" multiple>
                            @foreach ($rota['local'] as $key)
                              <option value="{{ $key->local }}">{{ $key->local }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Grupo</span>
                          </div>
                          <select id="select1" size="5" name="id_mrp_tipo[]" class="form-control" multiple>
                            @foreach ($rota['tipo_item'] as $key)
                              <option value="{{ $key->id_mrp_tipo }}" {{ $key->id_mrp_tipo === $idT ? 'selected':''}}>{{ $key->mrp_tipo }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      </div>
                      <div class="row mt-2">

                      <div class="col-5">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Entrada</span>
                          </div>
                          <input type="date" name="ent_ini" class="form-control" title="Data inicial" value="">
                          <input type="date" name="ent_fim" class="form-control" title="Data final" value="">
                        </div>
                      </div>

                      <div class="col-5">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Movimentação</span>
                          </div>
                          <input type="date" name="mov_ini" class="form-control" title="Data inicial" value="">
                          <input type="date" name="mov_fim" class="form-control" title="Data final" value="">
                        </div>
                      </div>


                      <button type="submit" class="btn btn-outline-success" id="submit">
                        <i class="fa fa-search"></i> Buscar
                      </button>

                    </div>

                  </div>
                </form>
              </div>
              {{-- END FORM --}}
              {{-- START LIST --}}
              <div class="car-body">
                {{-- DATATABLE --}}
                <form action="{{ route('baixa_item') }}" method="POST" name="formBaixa">
                  @csrf
                  <input type="hidden" name="id_pedido" value="">
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Cod / Item
                      </th>
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Grupo
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                        Localização
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                        QNT
                      </th>
                    </tr>
                  </thead>
                </div>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota['item'] as $key)
                      @php
                      if($rota['rota'] != 'estoque'){
                        switch ($key->sts_mrp_item) {
                          case '0': $cor = 'dark';      $tx = 'Cancelado';  break;
                          case '1': $cor = 'warning';   $tx = 'Pendente';   break;
                          case '2': $cor = 'secondary'; $tx = 'Confirmado'; break;
                          case '3': $cor = 'info';      $tx = 'Autorizado'; break;
                          case '4': $cor = 'light';     $tx = 'Separado';   break;
                          case '5': $cor = 'success';   $tx = 'Entrege';    break;
                          default:  $cor = 'dark';      $tx = 'Erro';       break;
                        }
                      }
                      @endphp
                      <tr>
                        <td>
                          {{ $key->produtos->cod }}
                          {{-- <input name="select_item" class="form-check-input chk" type="checkbox" value="{{ $key->id_mrp_item }}" id="check{{ $key->id_mrp_item }}"> --}}
                          <i class="fas fa-tag text-{{$cor}}" title="{{$tx}}"></i>
                          <input type="hidden" name="id_mrp_item[]" value="{{$key->id_mrp_item}}">
                          <input type="hidden" name="id_produto[]" value="{{$key->id_produto}}">
                          {{ $key->produtos->produto }}
                        </td>
                        <td>{{ $key->produtos->tipos->mrp_tipo }}</td>

                        <td>{{ $key->local }}</td>
                        <td>
                          @if ($key->produtos->id_und == 4)
                            {{ round($key->qnt) }}
                          @else
                            {{ str_replace('.', ',', $key->qnt) }}
                          @endif
                        </td>
                      </form>
                    </tr>
                  @endforeach
                  {{-- <input id="ckAll" name="select_item" class="form-check-input ml-3" type="checkbox" value="{{ $key->id_mrp_item }}" id="check{{ $key->id_mrp_item }}"> --}}
                </tbody>
              </table>
            </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
