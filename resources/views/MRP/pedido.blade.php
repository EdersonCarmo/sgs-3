@extends('layouts.sgs')

@section('content')
  @php
  $idP = $idS = $idT = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])) {
    $idP = $rota['edit']->id_produto;
    $idS = $rota['edit']->id_solicitante;
    $idT = $rota['edit']->id_mrp_tipo;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form">
                @method('PUT')
              @else
                <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form">
                  <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                @endif
                @csrf
                <div class="row justify-content-between">
                  <div class="col-8">
                    <div class="form-group">
                      <label for="exampleInputName2">PEDIDOS </label>
                    </div>
                  </div>
                  {{-- ACL FORM CAD --}}
                  <div class="col-1">
                    @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                  </div>
                  {{-- ACL FORM CAD --}}
                </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                    <div class="row">

                    <div class="col-4">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Solicitante</span>
                        </div>
                        <select id="select1" name="id_solicitante" class="form-control">
                          <option>Selecione ...</option>
                          @foreach ($rota['colaborador'] as $key)
                            <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador==$idS?"selected":"" }}>
                              {{ $key->pessoas['pessoa'] }}
                            </option>
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="col-3">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">Entrega</span>
                        </div>
                        <input type="date" name="dt_entrega" class="form-control" value="{{ $rota['edit']->dt_entrega or old('dt_entrega') }}">
                      </div>
                    </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Tipos</span>
                          </div>
                          <select id="select1" name="id_mrp_tipo" class="form-control">
                            <option {{ $idT == 0 ? 'selected':''}}>Selecione ...</option>
                            @foreach ($rota['tipos'] as $key)
                                <option value="{{ $key->id_mrp_tipo }}" {{ $key->id_mrp_tipo === $idT ? 'selected':''}}>{{ $key->mrp_tipo }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-2">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" title="Código da obra">Cód.</span>
                          </div>
                          <input type="text" name="cod_obra" class="form-control" value="{{ $rota['edit']->cod_obra or old('cod_obra') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      </div>
                      <div class="row mt-3">

                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" title="Nome da obra">Obra</span>
                          </div>
                          <input type="text" name="obra" class="form-control" value="{{ $rota['edit']->obra or old('obra') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-5">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Obs.</span>
                          </div>
                          <input type="text" name="obs" class="form-control" value="{{ $rota['edit']->obs or old('obs') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-3">
                        <button type="submit" class="btn btn-outline-success" id="submit">
                          {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                        </button>
                      </div>
                    </div>

                  </div>
                </form>
              </div>
              {{-- END FORM --}}
              {{-- START LIST --}}
              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Pedido
                      </th>
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        Tipo
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                        Qnt Itens
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                        Solicitante
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 240px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                </div>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota[$rota['rota']] as $key)
                      <tr>
                        <td>
                          @php
                            switch ($key['sts_pedido']) {
                              case '1': $cor = 'secondary'; $tex = 'Pendente';    break;
                              case '2': $cor = 'light';     $tex = 'Confirmado';  break;
                              case '3': $cor = 'primary';   $tex = 'Autorizado';  break;
                              case '4': $cor = 'warning';   $tex = 'Separando';   break;
                              case '5': $cor = 'success';   $tex = 'Entregue';    break;
                              default:  $cor = 'dark';      $tex = 'Cancelado';   break;
                            }
                            $show = ['id'=>$key['id_'.$rota['rota']],'rota'=>$rota['rota']];
                            $print = ['id'=>$key['id_'.$rota['rota']],'rota'=>$rota['rota'],'act'=>'print'];
                            $dow = ['id'=>$key['id_'.$rota['rota']],'rota'=>$rota['rota'],'act'=>'baixar'];
                          @endphp
                            <i class="fas fa-certificate text-{{$cor}}" title="{{$tex}}"></i>
                          #_{{ $key->id_pedido }}
                        </td>
                        <td>{{ $key->tipos->mrp_tipo }}</td>
                        <td>{{ $key->atendido }} / {{ $key->total }}</td>
                        <td>{{ $key->colaboradores->pessoas->pessoa }}</td>
                        <td>
                          {{-- ACL ACOES TABLE --}}
                          <div class="row">
                            {{-- BAIXA ESTOQUE --}}
                            @if ($key->sts_pedido == 4 and $acl->dw == 1)
                              <div class="">
                                <a href="{{route('mrp_item.show',$dow) }}" class="btn btn-block btn-ghost-warning" title="Baixar pedido">
                                  <i class="fas fa-download"></i>
                                </a>
                              </div>
                            @elseif ($key->sts_pedido == 5)
                              <div class="">
                                <a class="btn btn-block btn-ghost-success text-success" title="Pedido Entrege">
                                  <i class="fas fa-download"></i>
                                </a>
                              </div>
                              @else
                                <div class="">
                                  <a class="btn btn-block btn-ghost-dark text-secondary" title="Pedido não autorizado">
                                    <i class="fas fa-download"></i>
                                  </a>
                                </div>
                            @endif
                            {{-- BAIXA ESTOQUE --}}
                            {{-- SEPARA PEDIDO --}}
                            @if ($key->sts_pedido == 3 and $acl->dw == 1)
                              <div class="">
                                <a href="{{route('mrp_item.show',$print) }}" target="new" class="btn btn-block btn-ghost-warning" title="Separar pedido">
                                  <i class="fas fa-shopping-bag"></i>
                                </a>
                              </div>
                            @elseif ($key->sts_pedido >= 4 and $acl->dw == 1)
                              <div class="">
                                <a href="{{route('mrp_item.show',$print) }}" target="new" class="btn btn-block btn-ghost-success text-success" title="Pedido em separação">
                                  <i class="fas fa-shopping-bag"></i>
                                </a>
                              </div>
                            @elseif ($key->sts_pedido >= 4)
                              <div class="">
                                <a class="btn btn-block btn-ghost-dark text-success" title="Pedido já autorizado">
                                  <i class="fas fa-shopping-bag"></i>
                                </a>
                              </div>
                            @else
                              <div class="">
                                <a class="btn btn-block btn-ghost-dark text-secondary" title="Pedido não autorizado">
                                  <i class="fas fa-shopping-bag"></i>
                                </a>
                              </div>
                            @endif
                            {{-- SEPARA PEDIDO --}}
                            {{-- AUTORIZA PEDIDO --}}
                            @if ($key->sts_pedido == 2 and $acl->a == 1)
                              <div class="">
                                <a href="{{route('sts_pedido',$key->id_pedido) }}" class="btn btn-block btn-ghost-warning" title="Autorizar o pedido" onclick="return confirm('Após confirmar, o pedido passará para separação e entrega.')">
                                  <i class="far fa-check-circle"></i>
                                </a>
                              </div>
                            @elseif ($key->sts_pedido > 2)
                              <div class="">
                                <a class="btn btn-block btn-ghost-success text-success" title="Pedido autorizado">
                                  <i class="far fa-check-circle"></i>
                                </a>
                              </div>
                            @else
                              <div class="">
                                <a class="btn btn-block btn-ghost-dark text-secondary" title="Pedido não confirmado">
                                  <i class="far fa-check-circle"></i>
                                </a>
                              </div>
                            @endif
                            {{-- AUTORIZA PEDIDO --}}
                            {{-- ABRE ITENS / EDITA PEDIDO --}}
                            @if ($key->sts_pedido == 1)
                              <div class="">
                                <a href="{{route('sts_pedido',$key->id_pedido) }}" class="btn btn-block btn-ghost-warning" title="Confirma e fecha o pedido" onclick="return confirm('Após confirmar NÃO será possivel realizar alterações no pedido. Confirma o pedido?')">
                                  <i class="fas fa-clipboard-check"></i>
                                </a>
                              </div>
                              <div class="">
                                <a href="{{route('mrp_item.show',$show) }}" class="btn btn-block btn-ghost-warning" title="Abrir Pedido">
                                  <i class="fas fa-toolbox"></i>
                                </a>
                              </div>
                              @if ($acl->u == 1)
                                <div class="">
                                  <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                    <i class="far fa-edit"></i>
                                  </a>
                                </div>
                              @else
                                <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                              @endif
                            @else
                              <div class="text-success">
                                <a class="btn btn-block btn-ghost-dark" title="Pedido fechado">
                                  <i class="fas fa-clipboard-check"></i>
                                </a>
                              </div>
                              <div class="text-success">
                                <a class="btn btn-block btn-ghost-dark" title="Pedido fechado">
                                  <i class="fas fa-toolbox"></i>
                                </a>
                              </div>
                              @if ($acl->u == 1)
                                <div class="text-success">
                                  <a class="btn btn-block btn-ghost-dark" title="Pedido fechado">
                                    <i class="far fa-edit"></i>
                                  </a>
                                </div>
                              @else
                                <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                              @endif
                            @endif
                            {{-- ABRE ITENS / EDITA PEDIDO --}}

                            @if ($acl->d == 1 AND $key->sts_pedido <= 4)
                              <div class="">
                                <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                    <i class="far fa-trash-alt"></i>
                                  </button>
                                </form>
                              </div>
                            @else
                              <div class="btn text-dark"><i class="far fa-trash-alt"></i></div>
                            @endif
                          </div>
                          {{-- ACL ACOES TABLE --}}
                        </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>

            </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
