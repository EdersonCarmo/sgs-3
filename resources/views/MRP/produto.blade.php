@extends('layouts.sgs')

@section('content')
  @php
  $idU = 4; $idT = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])) {
    $idU = $rota['edit']->id_und;
    $idT = $rota['edit']->id_mrp_tipo;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form">
                @method('PUT')
              @else
                <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form">
                  <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                  <input type="hidden" name="rota" value="documento">
                @endif
                @csrf
                <div class="row justify-content-between">
                  <div class="col-8">
                    <div class="form-group">
                      <label for="exampleInputName2">PRODUTOS </label>
                    </div>
                  </div>
                  {{-- ACL FORM CAD --}}
                  <div class="col-1">
                    @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                  </div>
                  {{-- ACL FORM CAD --}}
                </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                    <div class="row">

                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Nome</span>
                          </div>
                          <input type="text" name="produto" class="form-control" value="{{ $rota['edit']->produto or old('produto') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Tipos</span>
                          </div>
                          <select id="selectTipoProd" name="id_mrp_tipo" class="form-control" onchange="Chip('div-cad-chip',this)">
                            <option {{ $idT == 0 ? 'selected':''}}>Selecione ...</option>
                            @foreach ($rota['tipos'] as $key)
                              {{-- @if ($key['rota'] == 'estoque') --}}
                              <option value="{{ $key->id_mrp_tipo }}" {{ $key->id_mrp_tipo === $idT ? 'selected':''}}
                                {{-- onselect="{{ $key->id_mrp_tipo==23?"Mudarestado('div-cad-produto')":""}}" --}}
                                {{-- onchange="{{ $key->id_mrp_tipo==23?"<script> Alert();</script>":""}}" --}}
                                {{-- @php
                                  if($key->id_mrp_tipo == 23) echo "<script> Mudarestado('div-cad-produto');</script>";
                                  //echo 'onclick="Mudarestado('div-cad-produto')"';
                                @endphp --}}
                                >{{ $key->mrp_tipo }}</option>
                              {{-- @endif --}}
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Marca</span>
                          </div>
                          <input type="text" name="marca" class="form-control" value="{{ $rota['edit']->marca or old('marca') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-2">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" title="Código do item">Cód</span>
                          </div>
                          <input type="text" name="cod" class="form-control" value="{{ $rota['edit']->cod or old('cod') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>
                    </div>

                    <div id="div-cad-modelo" class="row mt-3">
                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Modelo</span>
                          </div>
                          <input type="text" name="modelo" class="form-control" value="{{ $rota['edit']->modelo or old('modelo') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                        <div class="col-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">UND</span>
                            </div>
                            <select id="select1" name="id_und" class="form-control">
                              <option {{ $idU == 0 ? 'selected':''}}>Selecione ...</option>
                              @foreach ($rota['unds'] as $key)
                                <option value="{{ $key->id_und }}" {{ $key->id_und == $idU ? 'selected':''}}>{{ $key->und }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" title="Certificado de Aprovação">CA</span>
                          </div>
                          <input type="text" name="ca" class="form-control" value="{{ $rota['edit']->ca or old('ca') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                    <div class="col-2">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"title="Peso em quilos">Peso</span>
                        </div>
                        <input type="text" name="peso" class="form-control" value="{{ $rota['edit']->peso or old('peso') }}" oninput="this.value = this.value.toUpperCase()">
                      </div>
                    </div>
                  </div>

                    <div id="div-cad-chip" class="row mt-3" style="display: none">
                      <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Chip</span>
                          </div>
                          <input type="text" name="chip" class="form-control" value="{{ $rota['edit']->chip or old('chip') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                        <div class="col-3">
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <span class="input-group-text">Operadora</span>
                            </div>
                            <select id="select1" name="operadora" class="form-control">
                              <option value="">Selecione ...</option>
                              <option value="1" {{ 1 == $idU ? 'selected':''}}>CLARO</option>
                              <option value="2" {{ 2 == $idU ? 'selected':''}}>NEXTEL</option>
                              <option value="3" {{ 3 == $idU ? 'selected':''}}>OI</option>
                              <option value="4" {{ 4 == $idU ? 'selected':''}}>TIM</option>
                              <option value="5" {{ 5 == $idU ? 'selected':''}}>VIVO</option>
                            </select>
                          </div>
                        </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" title="Certificado de Aprovação">Imei</span>
                          </div>
                          <input type="text" name="imei" class="form-control" value="{{ $rota['edit']->imei or old('imei') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>
                  </div>

                    <div class="row mt-3">
                      <div class="col-2">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" title="Quantidadte mínima no estoque">Minímo</span>
                          </div>
                          <input type="text" name="qnt_min" class="form-control" value="{{ $rota['edit']->qnt_min or old('qnt_min') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-2">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" title="Tempo de espera após compra">Prazo</span>
                          </div>
                          <input type="text" name="prazo_entrega" class="form-control" value="{{ $rota['edit']->prazo_entrega or old('prazo_entrega') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      {{-- <div class="col-4">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Validade</span>
                          </div>
                          <input type="date" name="validade" class="form-control" value="{{ $rota['edit']->validade or old('validade') }}">
                        </div>
                      </div> --}}

                      <div class="col-6">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Obs.:</span>
                          </div>
                          <input type="text" name="obs" class="form-control" value="{{ $rota['edit']->obs or old('obs') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-2">
                        <button type="submit" class="btn btn-outline-success btn-block" id="submit">
                          {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                        </button>
                      </div>
                    </div>
                    <div class="row mt-3">

                    </div>

                  </div>
                </form>
              </div>
              {{-- END FORM --}}
              {{-- START LIST --}}
              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                        Código
                      </th>
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        {{ ucfirst($rota['rota']) }}
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                        Modelo
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending">
                        Ação
                      </th>
                    </tr>
                  </thead>
                </div>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota[$rota['rota']] as $key)
                      <tr>
                        <td>{{ $key->cod }}</td>
                        <td>{{ $key[$rota['rota']] }}</td>
                        <td>{{ $key->modelo }}</td>
                        <td>
                          {{-- ACL ACOES TABLE --}}
                          <div class="row">
                            @if ($acl->u == 1)
                              <div class="">
                                <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                  <i class="far fa-edit"></i>
                                </a>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                            @endif

                            @if ($acl->d == 1)
                              <div class="">
                                <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                    <i class="far fa-trash-alt"></i>
                                  </button>
                                </form>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                            @endif
                          </div>
                          {{-- ACL ACOES TABLE --}}
                        </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>

              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">DOCUMENTO</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>One fine body…</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                  </div>

                </div>

              </div>
            </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
