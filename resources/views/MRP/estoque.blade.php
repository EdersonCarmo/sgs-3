@extends('layouts.sgs')

@section('content')
  @php
  $idC = $idE = $idT = 0;
  $acl = $rota['acl'];
  if(isset($rota['edit'])) {
    $idE = $rota['edit']->id_empresa;
    $idC = $rota['edit']->id_almoxarife;
    $idT = $rota['edit']->id_mrp_tipo;
  }
  @endphp
  <main class="main">
    <br>
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-md-12">

          @component('includes/alerts')
          @endcomponent

          <div class="card">
            {{-- FORM CADASTRO --}}
            <div class="card-header">
              @if (isset($rota['edit']))
                <form action="{{ route($rota['rota'].'.update',$rota['edit']['id_'.$rota['rota']]) }}" method="post" class="form">
                @method('PUT')
              @else
                <form action="{{ route($rota['rota'].'.store') }}" method="post" class="form">
                  <input type="hidden" name="id_colaborador" value="{{ Auth::user()->id }}">
                  <input type="hidden" name="rota" value="documento">
                @endif
                @csrf
                <div class="row justify-content-between">
                  <div class="col-8">
                    <div class="form-group">
                      <label for="exampleInputName2">ESTOQUE </label>
                    </div>
                  </div>
                  {{-- ACL FORM CAD --}}
                  <div class="col-1">
                    @if ($acl->c == 1)
                        <button id="btn-plus" type="button" class="btn btn-pill btn-outline-success ml-3" onclick="Mostra_form_table('{{$rota['rota']}}')">
                          <i class='fa fa-plus'></i>
                        </button>
                      @endif
                  </div>
                  {{-- ACL FORM CAD --}}
                </div>

                  <div id="div-cad-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divOpen':'divClose'}}" style="display: none">
                    <div class="row">

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Empresa</span>
                          </div>
                          <select id="select1" name="id_empresa" class="form-control">
                            <option {{ $idE == 0 ? 'selected':'' }}>Selecione ...</option>
                            @foreach ($rota['empresas'] as $key)
                              <option value="{{ $key->id_empresa }}" {{ $key->id_empresa === $idE ? 'selected':''}}>{{ $key->empresa }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Estoque</span>
                          </div>
                          <input type="text" name="estoque" class="form-control" value="{{ $rota['edit']->estoque or old('estoque') }}" oninput="this.value = this.value.toUpperCase()">
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Tipos</span>
                          </div>
                          <select id="select1" name="id_mrp_tipo" class="form-control">
                            <option {{ $idT == 0 ? 'selected':'' }}>Selecione ...</option>
                            @foreach ($rota['tipos'] as $key)
                              <option value="{{ $key->id_mrp_tipo }}" {{ $key->id_mrp_tipo === $idT ? 'selected':''}}>{{ $key->mrp_tipo }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-3">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Almoxarife</span>
                          </div>
                          <select id="select1" name="id_almoxarife" class="form-control">
                            <option {{ $idC == 0 ? 'selected':'' }}>Selecione ...</option>
                            @foreach ($rota['colaboradores'] as $key)
                              <option value="{{ $key->id_colaborador }}" {{ $key->id_colaborador === $idC ? 'selected':''}}>{{ $key->pessoas->pessoa }}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <button type="submit" class="btn btn-outline-success" id="submit">
                        {{ isset($rota['edit'])?"Salvar":"Cadastrar"}}
                      </button>
                    </div>

                  </div>
                </form>
              </div>
              {{-- END FORM --}}
              {{-- START LIST --}}
              <div class="car-body">
                {{-- DATATABLE --}}
                <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer mt-3">
                  <div id="div-list-{{$rota['rota']}}" class="{{isset($rota['edit'])?'divClose':'divOpen'}}">

                  <table class="table table-striped table-hover datatable dataTable no-footer table-sm" id="DataTables_Table_1" role="grid"
                  aria-describedby="DataTables_Table_0_info" style="border-collapse: collapse !important">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Username: activate to sort column descending">
                        {{ ucfirst($rota['rota']) }}
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                        Tipo
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date registered: activate to sort column ascending">
                        Empresa
                      </th>
                      <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Role: activate to sort column ascending" style="width: 140px;">
                        Ação
                      </th>
                    </tr>
                  </thead>
                </div>
                  {{-- LIST --}}
                  <tbody>
                    @foreach ($rota[$rota['rota']] as $key)
                      <tr>
                        <td>{{ $key[$rota['rota']] }}</td>
                        <td>{{ $key->tipos->mrp_tipo }}</td>
                        <td>{{ $key->empresas->empresa }}</td>
                        <td>
                          {{-- ACL ACOES TABLE --}}
                          @php
                            $show = ['id'=>$key['id_'.$rota['rota']],'rota'=>$rota['rota']];
                            $print = ['id'=>$key['id_'.$rota['rota']],'rota'=>$rota['rota'],'act'=>'print'];
                            $filtro = ['id'=>$key['id_'.$rota['rota']],'act'=>'filtro'];
                          @endphp
                          <div class="row">
                              <div class="">
                                <a href="{{route('estoque.show',$show) }}" class="btn btn-block btn-ghost-success" title="Abrir Estoque">
                                  <i class="fas fa-toolbox"></i>
                                </a>
                              </div>
                              {{-- PRINT --}}
                                @if ($acl->r == 1)
                                  <div class="">
                                    <a href="{{route('estoque.show',$print) }}" class="btn btn-block btn-ghost-warning" title="Imprimir" target="new">
                                      <i class="fas fa-print"></i>
                                    </a>
                                  </div>
                                @else
                                  <div class="btn text-secondary"><i class="fas fa-print"></i></div>
                                @endif

                            @if ($acl->u == 1)
                              <div class="">
                                <a href="{{route($rota['rota'].'.edit',$key['id_'.$rota['rota']]) }}" class="btn btn-block btn-ghost-warning" title="Editar">
                                  <i class="far fa-edit"></i>
                                </a>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-edit"></i></div>
                            @endif

                            @if ($acl->d == 1)
                              <div class="">
                                <form method="POST" action="{{route($rota['rota'].'.destroy',$key['id_'.$rota['rota']]) }}">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-block btn-ghost-danger" title="Excluir" onclick="return confirm('Confirma a exclusão?')">
                                    <i class="far fa-trash-alt"></i>
                                  </button>
                                </form>
                              </div>
                            @else
                              <div class="btn text-secondary"><i class="far fa-trash-alt"></i></div>
                            @endif
                          </div>
                          {{-- ACL ACOES TABLE --}}
                        </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>

              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">DOCUMENTO</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>One fine body…</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                  </div>

                </div>

              </div>
            </div>
            </div>

          </div>
        </div>

      </div>
    </div>
  </main>
@endsection
