<?php
Route::get('/', function () { return view('auth/entrar'); });
Route::get('/login', function () { return view('auth/entrar'); });
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
// EXCEL
Route::get('import-export-view', 'ExcelController@importExportView')->name('import.export.view');
Route::post('import-file', 'ExcelController@importFile')->name('import.file');
Route::get('export-file/{type}', 'ExcelController@exportFile')->name('export.file');
// ROUTE ADM
Route::prefix('ADM')->group(function () {
  Route::resource('/adm_tipo', 'ADM\TipoC');
  Route::resource('/adm_item', 'ADM_ItemC');
  Route::post('/aprovaOrc/{id}', 'ADM\OrcamentoC@aprovaOrc')->name('aprovaOrc');
  Route::resource('/compra', 'ADM\CompraC');
  Route::resource('/g_link', 'ADM\G_LinkC');
  Route::resource('/link', 'ADM\LinkC');
  Route::resource('/orcamento', 'ADM\OrcamentoC');
  Route::resource('/pessoa', 'ADM\PessoaC');
  Route::resource('/ramo', 'EMP\RamoC');
});
//  ROUTE EMP
Route::prefix('EMP')->group(function () {
  Route::resource('/empresa', 'EMP\EmpresaC');
  Route::resource('/emp_tipo', 'EMP\TipoC');
  Route::resource('/inventario', 'EMP_InventarioC');
  Route::resource('/setor', 'EMP\SetorC');
  Route::resource('/emp_item', 'EMP_ItemC');
  Route::resource('/emp_emprestimo', 'EMP\EmprestimoC');
  Route::resource('/documento', 'TI\DocumentoC');
});
// ROUTE HRM
Route::prefix('HRM')->group(function () {
  Route::resource('/hrm_arq', 'TI\ArquivoC');
  Route::resource('/cargo', 'HRM\CargoC');
  Route::resource('/certificado', 'HRM\CertificadoC');
  Route::resource('/colaborador', 'HRM\ContratoC');
  Route::resource('/contrato', 'HRM\ContratoC');
  Route::resource('/curriculo', 'HRM\CurriculoC');
  Route::resource('/curso', 'HRM\CursoC');
  Route::resource('/exame', 'HRM\ExameC');
  Route::resource('/ferias', 'HRM\FeriasC');
  Route::resource('/ferias_pagto', 'HRM\Ferias_PagtoC');
  Route::resource('/hrm_tipo', 'HRM\TipoC');
  Route::resource('/dependente', 'HRM\DependenteC');
});
// ROUTE LOG - FROTA
Route::prefix('LOG')->group(function () {
  Route::resource('/abastecimento', 'LOG\AbastecimentoC');
  Route::resource('/documento', 'LOG\DocumentoC');
  Route::resource('/log_tipo', 'LOG\TipoC');
  Route::resource('/manutencao', 'LOG\ManutencaoC');
  Route::resource('/multa', 'LOG\MultaC');
  Route::resource('/ocorrencia', 'LOG\OcorrenciaC');
  Route::resource('/vei_seg', 'LOG\Vei_SegC');
  Route::resource('/veiculo', 'LOG\VeiculoC');
  Route::resource('/vistoria', 'LOG\VistoriaC');
});
// ROUTE LOG - MRP
Route::prefix('MRP')->group(function () {
  Route::post('/baixa_item', 'MRP\ItemC@baixa')->name('baixa_item');
  Route::resource('/emprestimo', 'MRP_EmprestimoC');
  Route::resource('/estoque', 'MRP\EstoqueC');
  Route::resource('/kit', 'MRP\KitC');
  Route::resource('/kit_item', 'MRP\Kit_ItemC');
  Route::resource('/movimento', 'MRP\MovimentoC');
  Route::post('/mrp_filtro', 'MRP\EstoqueC@mrp_filtro')->name('mrp_filtro');
  Route::resource('/mrp_item', 'MRP\ItemC');
  Route::resource('/mrp_tipo', 'MRP\TipoC');
  Route::resource('/pedido', 'MRP\PedidoC');
  Route::resource('/produto', 'MRP\ProdutoC');
  Route::get('/sts_pedido/{id}', 'MRP\PedidoC@status')->name('sts_pedido');
  Route::resource('/und', 'MRP\UndC');
});
// ROUTE TI
Route::prefix('TI')->group(function () {
  Route::resource('/help', 'TI\HelpC');
  Route::resource('/modulo', 'TI\ModuloC');
  Route::resource('/permissao', 'TI\AclC');
  Route::resource('/produtoENT', 'MRP_ProdutoEntC');
  Route::resource('/ti_tipo', 'TI\TipoC');
  Route::resource('/update', 'TI\UpdateC');
  Route::resource('/usuario', 'TI\UsuarioC');
});

/*
GET       /photos	               index	   photos.index
GET	      /photos/create	       create	   photos.create
POST	    /photos	               store	   photos.store
GET	      /photos/{photo}	       show	     photos.show
GET	      /photos/{photo}/edit   edit	     photos.edit
PUT/PATCH	/photos/{photo}        update	   photos.update
DELETE	  /photos/{photo}	       destroy	 photos.destroy
*/
