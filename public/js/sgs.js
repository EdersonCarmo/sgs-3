$(document).ready(function(){  // success error info warning
  var tx = $("#testetoastr").attr('texto');
  var vl = $("#testetoastr").val();

  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": 300,
    "hideDuration": 1000,
    "timeOut": 5000,
    "extendedTimeOut": 1000,
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

  if(vl=="error" || vl=="success" || vl=="info" || vl=="warning"){
    toastr[vl](tx)
  }
});

$(document).ready(function(){

  $("#ckAll").click(function()  {  // minha chk que marcará as outras

    if ($("#ckAll").prop("checked"))   // se ela estiver marcada...
    $(".chk").prop("checked", true);  // as que estiverem nessa classe ".chk" tambem serão marcadas
    else $(".chk").prop("checked", false);   // se não, elas tambem serão desmarcadas

  });

});

function mascara(t, mask){
  var i = t.value.length;
  var saida = mask.substring(1,0);
  var texto = mask.substring(i)
  if (texto.substring(0,1) != saida){
    t.value += texto.substring(0,1);
  }
}

function EditListView(el) {
  var display = document.getElementById("div-cad-"+el).style.display;
  // $("#"+el).slideToggle(500);
  if(display == "none"){
    document.getElementById("btn-plus").innerHTML = "<i class='fa fa-minus'></i>";
    $("#div-cad-"+el).slideDown();
    $("#div-list-"+el).slideUp();
  }else{
    document.getElementById("btn-plus").innerHTML = "<i class='fa fa-plus'></i>";
    $("#div-cad-"+el).slideUp();
    $("#div-list-"+el).slideDown();
  }
}


function Chip(el,val) {
  var display = document.getElementById(el).style.display;
  // window.alert(val.value);
  if(val.value == 23) {
    $("#"+el).slideDown(500);
    $("#div-cad-modelo").slideUp();
  }else {
    $("#"+el).slideUp();
    $("#div-cad-modelo").slideDown(500);
  }
}

function MudarestadoSeach(el) {
  var display = document.getElementById(el).style.display;
  $("#"+el).slideToggle(500);
  if(display == "none"){
    document.getElementById("btn-plus").innerHTML = "<i class='fa fa-search-minus'></i>";
    $("#div-cad-"+el).slideUp();
  }else{
    document.getElementById("btn-plus").innerHTML = "<i class='fa fa-search'></i>";
    $("#div-cad-"+el).slideDown();
  }
}

function MudaEstadoMenu(el) {
  $("#div-cad").slideDown();
  var display = document.getElementById(el).style.display;
  $("#"+el).slideToggle(500);
  if(display == "none"){
    $("#div-cad-"+el).slideUp();
  }else{
    $("#div-cad-"+el).slideDown();
  }
}

$(document).ready(function(){
  $(".divClose").hide();
  $(".divOpen").show();
});

function matchStart(params, data) {
  // If there are no search terms, return all of the data
  if ($.trim(params.term) === '') {
    return data;
  }

  // Skip if there is no 'children' property
  if (typeof data.children === 'undefined') {
    return null;
  }

  // `data.children` contains the actual options that we are matching against
  var filteredChildren = [];
  $.each(data.children, function (idx, child) {
    if (child.text.toUpperCase().indexOf(params.term.toUpperCase()) == 0) {
      filteredChildren.push(child);
    }
  });

  // If we matched any of the timezone group's children, then set the matched children on the group
  // and return the group object
  if (filteredChildren.length) {
    var modifiedData = $.extend({}, data, true);
    modifiedData.children = filteredChildren;

    // You can return modified objects from here
    // This includes matching the `children` how you want in nested data sets
    return modifiedData;
  }

  // Return `null` if the term should not be displayed
  return null;
}

$(".js-example-matcher-start").select2({
  matcher: matchStart
});

// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
