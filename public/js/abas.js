$(document).ready(function(){  // success error info warning
  var tx = $("#testetoastr").attr('texto');
  var vl = $("#testetoastr").val();

  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": true,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": 300,
    "hideDuration": 1000,
    "timeOut": 5000,
    "extendedTimeOut": 1000,
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

  if(vl=="error" || vl=="success" || vl=="info" || vl=="warning"){
    toastr[vl](tx)
  }
});

$(document).ready(function(){

  $("#ckAll").click(function()  {  // minha chk que marcará as outras

    if ($("#ckAll").prop("checked"))   // se ela estiver marcada...
    $(".chk").prop("checked", true);  // as que estiverem nessa classe ".chk" tambem serão marcadas
    else $(".chk").prop("checked", false);   // se não, elas tambem serão desmarcadas

  });

});

// CEP
$(document).ready(function() {
  function limpa_formulário_cep() {
    // Limpa valores do formulário de cep.
    $("#rua").val("");
    $("#bairro").val("");
    $("#cidade").val("");
    $("#uf").val("");
    $("#ibge").val("");
  }
  //Quando o campo cep perde o foco.
  $("#cep").blur(function() {
    //Nova variável "cep" somente com dígitos.
    var cep = $(this).val().replace(/\D/g, '');
    //Verifica se campo cep possui valor informado.
    if (cep != "") {
      //Expressão regular para validar o CEP.
      var validacep = /^[0-9]{8}$/;
      //Valida o formato do CEP.
      if(validacep.test(cep)) {
        //Preenche os campos com "..." enquanto consulta webservice.
        $("#rua").val("...");
        $("#bairro").val("...");
        $("#cidade").val("...");
        $("#uf").val("...");
        $("#ibge").val("...");
        //Consulta o webservice viacep.com.br/
        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
          if (!("erro" in dados)) {
            //Atualiza os campos com os valores da consulta.
            $("#rua").val(dados.logradouro);
            $("#bairro").val(dados.bairro);
            $("#cidade").val(dados.localidade);
            $("#uf").val(dados.uf);
            $("#ibge").val(dados.ibge);
          } //end if.
          else {
            //CEP pesquisado não foi encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
          }
        });
      } //end if.
      else {
        //cep é inválido.
        limpa_formulário_cep();
        alert("Formato de CEP inválido.");
      }
    } //end if.
    else {
      //cep sem valor, limpa formulário.
      limpa_formulário_cep();
    }
  });
});


//var tabs = $('#tabs').bootstrapDynamicTabs();

// tabs.addTab({
//   title: 'FLUXOS',
//   id: 'fluxos',
//   ajaxUrl: 'http://localhost:8000/fluxos',
// });
//
// $('#btnHtml').click(function(){
//   var rota = jQuery('#btnHtml').attr("data-rota");
// 	tabs.addTab({
// 		title: rota.toUpperCase(),
// 		html: 'http://localhost:8000/home#'
// 	})
// });
//
// $('#btnAjax').click(function(){
//   var rota = jQuery('#btnAjax').attr("data-rota");
// 	tabs.addTab({
// 		title: rota.toUpperCase(),
// 		id: rota,
// 		ajaxUrl: 'http://localhost:8000/'+rota,
// 	});
// });
//
// $('#btnAjax2').click(function(){
//   var rota = jQuery('#btnAjax2').attr("data-rota");
// 	tabs.addTab({
// 		title: rota.toUpperCase(),
// 		id: rota,
// 		ajaxUrl: 'http://localhost:8000/'+rota,
// 	});
// });
//
// $('#btnAjax3').click(function(){
//   var rota = jQuery('#btnAjax3').attr("data-rota");
// 	tabs.addTab({
// 		title: rota.toUpperCase(),
// 		id: rota,
// 		ajaxUrl: 'http://localhost:8000/'+rota,
// 		loadScripts: 'js2/load.js',
// 	});
// });
//
// $('#btnAjax4').click(function(){
//   var rota = jQuery('#btnAjax4').attr("data-rota");
// 	tabs.addTab({
// 		title: rota.toUpperCase(),
// 		id: rota,
// 		ajaxUrl: 'http://localhost:8000/'+rota+'/create',
// 	});
// });
//
// $('#btnAjax5').click(function(){
//   var rota = jQuery('#btnAjax5').attr("data-rota");
// 	tabs.addTab({
// 		title: rota.toUpperCase(),
// 		id: rota,
// 		ajaxUrl: 'http://localhost:8000/'+rota,
// 	});
// });
//
// $('#btnIon').click(function(){
// 	tabs.addTab({
// 		title: 'Aba com ícone',
// 		html: '<h2>Aba com ícone</h2>Um icone Font Awesome, exemplo: fa fa-user. O icone será mostrado na aba. Você também pode usar os glyphicon do Bootstrap </code>',
// 		icon: 'fa fa-user'
// 	})
// });

function getValor2(e){
    var valor = $(e).val(),
        valor2 = $(e).children(':selected').data("valor2");
// alert("data-valor2: "+valor2);

    document.getElementById("pessoa").value = valor2;
    document.getElementById("id_pessoa").value = valor;
};

function mascara(t, mask){
  var i = t.value.length;
  var saida = mask.substring(1,0);
  var texto = mask.substring(i)
  if (texto.substring(0,1) != saida){
    t.value += texto.substring(0,1);
  }
}

function Mudarestado(el) {
  var display = document.getElementById(el).style.display;
  $("#"+el).slideToggle(500);
  if(display == "none"){
    document.getElementById("btn-plus").innerHTML = "<i class='fa fa-list-ol' title='Abre lista'></i>";
    $("#div-cad-"+el).slideUp();
    $("#div-list-"+el).slideDown();
  }else{
    document.getElementById("btn-plus").innerHTML = "<i class='fa fa-pencil' title='Abre formulário'></i>";
    $("#div-cad-"+el).slideDown();
    $("#div-list-"+el).slideUp();
  }
}

function Mostra_form_table(el) {
  var display = document.getElementById("div-cad-"+el).style.display;
  // $("#"+el).slideToggle(500);
  if(display == "none"){
    document.getElementById("btn-plus").innerHTML = "<i class='fa fa-list-ol' title='Abre lista'></i>";
    $("#div-cad-"+el).slideDown();
    $("#div-list-"+el).slideUp();
  }else{
    document.getElementById("btn-plus").innerHTML = "<i class='fa fa-pencil' title='Abre formulário'></i>";
    $("#div-cad-"+el).slideUp();
    $("#div-list-"+el).slideDown();
  }
}

function Chip(el,val) {
  var display = document.getElementById(el).style.display;
  // window.alert(val.value);
  if(val.value == 23) {
    $("#"+el).slideDown(500);
    $("#div-cad-modelo").slideUp();
  }else {
    $("#"+el).slideUp();
    $("#div-cad-modelo").slideDown(500);
  }
}

function MudarestadoSeach(el) {
  var display = document.getElementById("div-cad-"+el).style.display;
  // $("#"+el).slideToggle(500);
  if(display == "none"){
    document.getElementById("btn-plus").innerHTML = "<i class='fa fa-search-minus'></i>";
    $("#div-cad-"+el).slideDown();
    $("#div-list-"+el).slideUp();
  }else{
    document.getElementById("btn-plus").innerHTML = "<i class='fa fa-search'></i>";
    $("#div-cad-"+el).slideUp();
    $("#div-list-"+el).slideDown();
  }
}

function MudaEstadoMenu(el) {
  $("#div-cad").slideDown();
  var display = document.getElementById(el).style.display;
  $("#"+el).slideToggle(500);
  if(display == "none"){
    $("#div-cad-"+el).slideUp();
  }else{
    $("#div-cad-"+el).slideDown();
  }
}

$(document).ready(function(){
  $(".divClose").hide();
  $(".divOpen").show();
});

// function matchStart(params, data) {
//   // If there are no search terms, return all of the data
//   if ($.trim(params.term) === '') {
//     return data;
//   }
//
//   // Skip if there is no 'children' property
//   if (typeof data.children === 'undefined') {
//     return null;
//   }
//
//   // `data.children` contains the actual options that we are matching against
//   var filteredChildren = [];
//   $.each(data.children, function (idx, child) {
//     if (child.text.toUpperCase().indexOf(params.term.toUpperCase()) == 0) {
//       filteredChildren.push(child);
//     }
//   });
//
//   // If we matched any of the timezone group's children, then set the matched children on the group
//   // and return the group object
//   if (filteredChildren.length) {
//     var modifiedData = $.extend({}, data, true);
//     modifiedData.children = filteredChildren;
//
//     // You can return modified objects from here
//     // This includes matching the `children` how you want in nested data sets
//     return modifiedData;
//   }
//
//   // Return `null` if the term should not be displayed
//   return null;
// }

$(".js-example-matcher-start").select2({
  matcher: matchStart
});

// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});

// function Select(index, el) {
//     var spans = el.parentElement.querySelectorAll('span');
//     for (var i = 0, l = spans.length; i < l; i++) {
//         spans[i].style.display = i == index ? 'block' : 'none';
//     }
// }

// var searchTerm;
// $('#searchAtribuido').keyup(function(){
//     searchTerm = $(this).val();
//
//     if(searchTerm != ''){
//         $.ajax({
//             url: '/EMP/empresa',
//             data: {
//                 search_type: 'json',
//                 search_term: searchTerm
//             },
//             dataType: 'json',
//             success: function(json){
//                 for(i in json.result_data){
//                     $('#search-results').append('<li>' . json.result_data[i].name);
//                 }
//             }
//         });
//     }
// });
//
// $.ajaxSetup({
//       headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
//   });
//
// $("#btn-cad-user").click(function(e){ alert('OK');
//   var loginForm = $("#form_cad_user");
//       e.preventDefault();
//       var formData = loginForm.serialize();
//
//       $.ajax({
//          type:'POST',
//          url:'http://localhost:8000/formtest2',
//          data:formData,
//          success:function(data){ alert(data.success); }
//       });
// });
//------------------------

function fMasc(objeto,mascara) {
				obj=objeto
				masc=mascara
				setTimeout("fMascEx()",1)
			}
			function fMascEx() {
				obj.value=masc(obj.value)
			}
			function mTel(tel) {
				tel=tel.replace(/\D/g,"")
				tel=tel.replace(/^(\d)/,"($1")
				tel=tel.replace(/(.{3})(\d)/,"$1)$2")
				if(tel.length == 9) {
					tel=tel.replace(/(.{1})$/,"-$1")
				} else if (tel.length == 10) {
					tel=tel.replace(/(.{2})$/,"-$1")
				} else if (tel.length == 11) {
					tel=tel.replace(/(.{3})$/,"-$1")
				} else if (tel.length == 12) {
					tel=tel.replace(/(.{4})$/,"-$1")
				} else if (tel.length > 12) {
					tel=tel.replace(/(.{4})$/,"-$1")
				}
				return tel;
			}
			function mCNPJ(cnpj){
				cnpj=cnpj.replace(/\D/g,"")
				cnpj=cnpj.replace(/^(\d{2})(\d)/,"$1.$2")
				cnpj=cnpj.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3")
				cnpj=cnpj.replace(/\.(\d{3})(\d)/,".$1/$2")
				cnpj=cnpj.replace(/(\d{4})(\d)/,"$1-$2")
				return cnpj
			}
			function mCPF(cpf){
				cpf=cpf.replace(/\D/g,"")
				cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
				cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
				cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
				return cpf
			}
			function mCEP(cep){
				cep=cep.replace(/\D/g,"")
				cep=cep.replace(/^(\d{2})(\d)/,"$1.$2")
				cep=cep.replace(/\.(\d{3})(\d)/,".$1-$2")
				return cep
			}
      function mCEL(cel){
				cel=cel.replace(/\D/g,"")
        cel=cel.replace(/(\d{3})(\d)/,"$1 $2")
				cel=cel.replace(/^(\d{2})(\d)/,"($1)$2")
				cel=cel.replace(/(\d{4})(\d)/,"$1-$2")
				return cel
			}
      function mFIXO(fixo){
				fixo=fixo.replace(/\D/g,"")
				fixo=fixo.replace(/^(\d{2})(\d)/,"($1)$2")
				fixo=fixo.replace(/(\d{4})(\d)/,"$1-$2")
				return fixo
			}
			function mNum(num){
				num=num.replace(/\D/g,"")
				return num
			}
